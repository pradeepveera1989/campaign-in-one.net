<?php


namespace App\Utils;

class CioLog
{
    public static function write($data, string $method='', string $line='')
    {
        $mth = (empty($method)) ? '': " [$method] ";
        $ln = (empty($line)) ? '': " [$line] ";
        $prefix = "[". date('Y-m-d H:i:s')."]{$mth}{$ln}";
        file_put_contents(
            __DIR__.'/../../exchange/log/app.log',
            "\n{$prefix}".json_encode($data,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT),
        FILE_APPEND,
        );
    }
}
