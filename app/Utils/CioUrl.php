<?php


namespace App\Utils;


/**
 * Class CioUrl
 * @package App\Utils
 * @author jsr
 * @since 2019-10-24
  */
class CioUrl
{
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function baseUrl()
    {
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://';
        return $protocol . $_SERVER['HTTP_HOST'];
    }

    /**
     * @return string  => http[s]://
     */
    public function httpProtocol()
    {
        return stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://';
    }

    /**
     * @return string the ip of this host
     */
    public function serverIp()
    {
        return $_SERVER['SERVER_ADDR'];
    }

    public function getAllSettings(): array
    {
        return [
            'baseUrl' => $this->baseUrl(),
            'httpProtocol' => $this->httpProtocol(),
            'serverIp' => $this->serverIp(),
        ];
    }

}

