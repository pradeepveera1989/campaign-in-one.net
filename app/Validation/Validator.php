<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validator
 *
 * @author Pradeep
 */

namespace App\Validation;

use Respect\Validation\Validator as Respect;

use Respect\Validation\Exceptions\NestedValidationException;


class Validator {
    
    protected $errors;

    public function validate($request, array $rules) {
        
        foreach($rules as $field => $rule){
            
            try{
                
                 $rule->setName(ucfirst($field))->assert($request->getParam($field));
                 
            } catch (NestedValidationException $e) {
                    
                $this->errors[$field] = $e->getMessages();
                
            }          
        }
        
        $_SESSION['validation_errors'] = $this->errors;
        
        return $this;
    }
    
    
    public function failed(){

        return !empty($this->errors);      
    }
    
    public function validatePassword($pwd) {

        $field = "password";
        if (strlen($pwd) < 8) {
            $this->errors[$field] = "Password is not matching password policy";
        }

        if (!preg_match("#[0-9]+#", $pwd)) {
            $this->errors[$field] = "Password is not matching password policy";
        }

        if (!preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $pwd)) {
            $this->errors[$field] = "Password is not matching password policy";
        }
        
        if (!preg_match("#[a-zA-Z]+#", $pwd)) {
            $this->errors[$field] = "Password is not matching password policy";
        }
        
        if (!preg_match("#[A-Z]+#", $pwd)) {
            $this->errors[$field] = "Password is not matching password policy";
        }

        $_SESSION['validation_errors'] = $this->errors;

        return $this;
    }    
}
