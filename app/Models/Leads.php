<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Leads
 *
 * @author Pradeep
 */
class Leads extends Model
{
    //put your code here

    /**
     * getLeads
     *
     * @param $company_id : Id for the Company
     *        $campaign_id : Id for the Campaign (optional)
     *
     * @return Array | Boolean;
     *         Boolean(False) : When Company Id not provided
     *         Array : List of leads for the Company
     */
    public function getLeads(int $acc_id):array
    {
        $result = [];
        if (!is_numeric($acc_id) || $acc_id <= 0) {
            return $result;
        }
 
        $sql = ""
            . "SELECT "
            . "`cio.leads`.`id`, "
            . "`cio.leads`.* , "
            . "`cio.company`.`account_no`  "
            . "FROM `cio.leads` "
            . "INNER JOIN `cio.company` ON `cio.company`.`id`=`cio.leads`.`company_id` "
            . "WHERE `cio.company`.`account_no` =" . $acc_id;

        $result['tracking'] = $this->excuteQueryGET($sql);
        $result['campaigns'] = $this->getCampaignLinksForLeads($acc_id);
        $result['lead_status'] = $this->getCampaignLeadStatus($acc_id);

        return $result;
    }

    
    

    /**
     * getCampaignLinksForLeads
     *
     * @param $company_id : Id for the Company
     *
     * @return Array ;
     *         Empty : When Company Id not provided
     *         Array : List of leads for the Company
     */
    private function getCampaignLinksForLeads(int $company_id) : array
    {
        $links = [];

        if ($company_id <= 0) {
            return $links;
        }
        $qry = 'SELECT DISTINCT 
         `cio.campaign`.`name`, 
         `cio.campaign`.`id`  
         FROM `cio.campaign`  
         INNER JOIN `cio.company` ON `cio.company`.`id`=`cio.campaign`.`company_id` 
         WHERE `cio.company`.`account_no` = ' . $company_id . '
         GROUP BY (`cio.campaign`.`name`)
         ORDER BY LOWER(`cio.campaign`.`name`) ASC ';

        $camp_details = $this->excuteQueryGET($qry);
        foreach ($camp_details as $camp) {
            $links[] = [
                'name' => $camp['name'],
                'id' => $camp['id'],
                'url' => 'https://campaign-in-one.net/account/' . $company_id . '/campaigns/' . $camp['id'] . '/leads'
            ];
        }

        return $links;
    }

    /**
     * getCampaignLeadStatus
     *
     * @param $company_id : Id for the Company
     *
     * @return Array ;
     *         Empty : When Company Id not provided
     *         Array : List of leads for the Company
     */
    private function getCampaignLeadStatus(int $company_id) : array
    {
        $status= [];
 
        if ($company_id <= 0) {
            return $links;
        }
        
        #$sql = "SELECT DISTINCT `status` FROM `cio.leads` WHERE 1 ";
        $sql = "SELECT COLUMN_TYPE \n"
                . "FROM INFORMATION_SCHEMA.COLUMNS \n"
                . "WHERE TABLE_SCHEMA = 'db13026946-devcio' \n"
                . "AND TABLE_NAME = 'cio.leads' \n"
                . "AND COLUMN_NAME = 'status' ";

        $lead_status = $this->excuteQueryGET($sql)[0];
        $enum = substr($lead_status['COLUMN_TYPE'], 6, -2);
        #Collect the substring from the string enum('active','inactive','storno')
        $status = explode("','", $enum);    # Change the string to array
        return $status;
    }
    

    /**
     * reactivateLead
     *
     * @param $params : Contains Array of Data
     *                   1. Campaign Id
     *                   2. Lead Id
     *                   3. Account Id
     *
     * @return Boolean ;
     *         False : When Lead Id not provided
     *         True : Lead has been reactivated
     */
    public function reactivateLead(int $lead_id)
    {
        $result= [];
        
        if (!is_numeric($lead_id) || $lead_id <= 0) {
            return $result;
        }
        
        $sql = "UPDATE `cio.leads` set `status` = 'active' where `id` = ".$lead_id;
        
        $result = $this->excuteQuerySET($sql);
        
        return $result;
    }

    /**
     * cancelLead
     *
     * @param $params : Contains Array of Data
     *                   1. Campaign Id
     *                   2. Lead Id
     *                   3. Account Id
     *
     * @return Boolean ;
     *         False : When Lead Id not provided
     *         True : Lead has been canceled
     */
    public function cancelLead(int $lead_id):bool
    {
        $result = false;
        
        if (!is_numeric($lead_id) || $lead_id <= 0) {
            return $result;
        }
        
        $sql = "UPDATE `cio.leads` set `status` = 'inactive' where `id` = ".$lead_id;

        $result = $this->excuteQuerySET($sql);

        return $result;
    }

    /**
     * getLeadInfoById function
     *
     * @param integer $lead_id
     * @return array
     */
    public function getLeadInfoById(int $lead_id):array
    {
        $return = [];

        if (!is_numeric($lead_id) || $lead_id <= 0) {
            return $return;
        }

        $sql = "SELECT "
            ."`cio.leads`.* , "
            ."`cio.campaign`.* ,"
            ."`cio.campaign`.`name` AS camp_name, "
            ."`cio.campaign`.`url` AS camp_url, "
            ."`cio.campaign`.`created` AS camp_create, "
            ."`cio.campaign`.`status` AS camp_status "
            ."FROM `cio.leads` "
            ."INNER JOIN `cio.campaign` "
            ."ON `cio.leads`.`campaign_id` = `cio.campaign`.`id` "
            ."WHERE `cio.leads`.`id` = ".$lead_id;

        $return = $this->excuteQueryGET($sql);

        return $return;
    }

    /**
     * getLeadsInfoForPeriod function
     *
     * @param integer $acc_id
     * @param integer $days
     * @return array
     */
    public function getLeadsInfoForPeriod(int $acc_id, int $days) : array
    {
        $return = [];
        if ($acc_id <= 0 || $days <= 0) {
            return $return;
        }

        $sql = "SELECT "
        . "`cio.leads`.`id`, "
        . "`cio.leads`.* , "
        . "`cio.company`.`id`  "
        . "FROM `cio.leads` "
        . "INNER JOIN `cio.company` ON `cio.company`.`id`=`cio.leads`.`company_id` "
        . "WHERE "
        . "`cio.leads`.`created` BETWEEN CURDATE() - INTERVAL ".$days." DAY AND SYSDATE()"
        . "AND `cio.company`.`id` =" . $acc_id ." "
        . "ORDER BY `cio.leads`.`id` DESC";

        $return = $this->excuteQueryGET($sql);

        return $return;
    }

    /**
     * getLeadCountForPeriod  function
     *
     * @param integer $acc_id
     * @return array Count of Leads Group By Date
     */
    public function getLeadCountForPeriod(int $acc_id) : array
    {
        $return = [];

        if ($acc_id <= 0) {
            return $return;
        }
       
        $sql = "SELECT "
            . "count(*) as cnt, "
            . "MONTHNAME(`created`) as create_month "
            . "FROM `cio.leads` "
            . "WHERE `created` >= date_sub(now(), interval 6 month) AND `cio.leads`.`company_id` = ".$acc_id ." "
            . "GROUP BY create_month ";

        $return = $this->excuteQueryGET($sql);

        return $return;
    }

    /**
     * getActiveCampaignCountByAccountId function
     *
     * @param integer $acc_id
     * @return array Count of Leads for Each Cammpaign
     */
    public function getLeadCountForCampaign(int $acc_id) :array
    {
        $return = [];

        if ($acc_id <= 0) {
            return $return;
        }

        $sql = "SELECT "
            . "count(*) as leads, `cio.campaign`.`name` as campaign "
            . "FROM `cio.leads` "
            . "INNER JOIN `cio.campaign` ON `cio.leads`.`campaign_id`=`cio.campaign`.`id` "
            . "WHERE `cio.leads`.company_id = ".$acc_id ." "
            . "GROUP BY `cio.leads`.`campaign_id`";
        

        $return =  $this->excuteQueryGET($sql);

        return $return;
    }

    public function getLeadsCountForCampaign(string $camp_url, string $camp_status="") : int
    {
        $count = 0;

        $sql = "SELECT COUNT(*) FROM `cio.leads` WHERE `url` LIKE '%$camp_url%'";

        $result =  $this->excuteQueryGET($sql)[0];
        if(!empty($result['COUNT(*)'])){
            $count = $result['COUNT(*)'];
        }

        return $count;
    }

    public function getLeadsForCampaign(int $acc_id, int $camp_id) :array
    {
        $return = [];

        $camp_obj = new \App\Models\Campaign($this->db);
        $camp_name =  $camp_obj->getCampaignDetailsById($camp_id)['name'];

        $sql = ' SELECT 
                `cio.campaign`.`name` AS `Campaign Name`,
                `cio.leads`.`lead_reference` AS `Lead Reference`,
                `cio.leads`.`created` AS Created,
                `cio.leads`.`salutation` AS `Salutation`,
                `cio.leads`.`first_name` AS `Firstname`,
                `cio.leads`.`last_name` AS `Firstname`,
                `cio.leads`.`email` AS `Email`,
                `cio.leads`.`postal_code` AS `Postalcode`,
                `cio.leads`.`status` AS `Status`,
                `cio.leads`.`trafficsource` AS `Trafficsource`,
                `cio.leads`.`utm_parameters` AS `UTM Parameters`,
                `cio.leads`.`targetgroup` AS `Targetgroup`,
                `cio.leads`.`affiliateID` AS `AffiliateID`,
                `cio.leads`.`affiliateSubID` AS `AffiliateSubID`,
                `cio.leads`.`campaign_id` AS `CampaignId`,
                `cio.leads`.`company_id` AS `CompanyId`,
                `cio.leads`.`id` AS lead_id
                FROM `cio.leads` 
                LEFT OUTER JOIN `cio.campaign` 
                ON `cio.campaign`.`id` = `cio.leads`.`campaign_id` 
                LEFT OUTER JOIN `cio.company` 
                ON `cio.company`.`id` = `cio.leads`.`company_id`
                WHERE `cio.campaign`.`name` LIKE \'%'.$camp_name.'%\' and `cio.company`.`account_no`='.$acc_id.'
                ORDER BY `cio.leads`.`id` DESC ';
        $return = $this->excuteQueryGET($sql);
        $cus_return = [];
        $iterate = 0;
        while($iterate < \count($return)) {
            $l_id = $return[$iterate];
            $sql = 'SELECT `lead_id`,`name`,`value` FROM `cio.leads_customdata` WHERE `lead_id` = '.$l_id['lead_id'];
            $tmp = $this->excuteQueryGET($sql);
            if(!empty($tmp)) {
                $cus_return[] = $tmp;
            }
            $iterate++;
        }

        $arr_lead=[];
        $iterate = 0;
        while ($iterate < \count($return)) {
            foreach($cus_return as $cus_return_1) {
                foreach($cus_return_1 as $cus){
                    if((int)$cus['lead_id'] === (int) $return[$iterate]['lead_id']) {
                        $return[$iterate][$cus['name']] = $cus['value'];
                    }elseif (!isset($return[$iterate][$cus['name']])) {
                        $return[$iterate][$cus['name']] = '';
                    }
                }
            }
            foreach($return[$iterate] as $key=>$val) {
                // Create sapareate entry for actions.
                if(!empty($return[$iterate]['CampaignId']) && !empty($return[$iterate]['CompanyId'])
                    && !empty($return[$iterate]['lead_id'])) {
                    $return[$iterate]['Actions'] = [
                        $return[$iterate]['CompanyId'],
                        $return[$iterate]['CampaignId'],
                        $return[$iterate]['lead_id']
                    ];
                }
            }
            if(\is_array($return[$iterate]) && !empty($return[$iterate])) {
                $arr_lead[$iterate] = $return[$iterate];
            }
            $iterate++;
        }

        $return['camp_name']  = $camp_name;
        $return['acc_id']  = $acc_id;
        $return['leads'] = $arr_lead;
        $return['campaign'] = $this->getCampaignLinksForLeads($acc_id);

        return $return;
    }
}
