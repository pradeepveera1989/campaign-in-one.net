<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Campaign
 *
 * @author Pradeep
 */
class Campaign extends Model
{

    /**
     * createCampaign function
     *
     * @param array $params
     * @return boolean
     */
    public function createCampaign(array $params):bool
    {
        $_SESSION['campaign'] = $params;
                
        if (empty($params) || empty($params['camp_url'] || $params['camp_name'])) {
            return false;
        }
        
        // Params for New campaign
        $params['status'] = "draft";
        $params['version'] = "1";
        $params['name'] = $params['camp_name'];
        $params['comment'] = $params['comp_comment'];
        $params['url'] = $this->createCampaignURL($params).'/versions/'.$params['version'];

        if (!empty($this->getCampaignDetialsByURL($params['url'], "")) ||
            !empty($this->getCampaignByName($params['name']))) {
            return false;
        }

        $result = $this->insertCampaignToDB($params);
        
        return $result;
    }


    /**
     * getCampaignByName function
     *
     * @param string $camp_name
     * @return array
     */
    private function getCampaignByName(string $camp_name):array
    {
        $camp_details = [];
        $acc_id = $_SESSION['acc_id'];

        if (empty($camp_name) || !(is_numeric($acc_id)) || ((int) $acc_id <= 0)) {
            return $camp_details;
        }

        $sql = "SELECT *  "
            . " FROM `cio.campaign` "
            . " INNER JOIN `cio.company`  "
            . " ON `cio.campaign`.`company_id` = `cio.company`.`id`"
            . " WHERE `cio.company`.`account_no` = ".$acc_id." AND `cio.campaign`.`name` LIKE '".$camp_name."'";
        
        $result = $this->excuteQueryGET($sql);

        if (!empty($result)) {
            $camp_details = $result[0];
        }

        return $result;
    }

    /**
     * deleteCampaign
     *
     * @param $url : Id of the Campaign
     *
     * @return Boolean
     *        False : If Campaign entry is not deleted in Campaign Table
     *        True : if the Campaign entry deleted successfull.
     */
    
    public function deleteCampaignById(array $params) :bool
    {
        $status = false;

        if (empty($params) || !is_numeric($params['camp_id']) || (int)($params['camp_id'] <= 0)) {
            return $status;
        }

        $sql = "DELETE FROM `cio.campaign` WHERE `id` = ".$params['camp_id'];
        $result = $this->excuteQuerySET($sql);
        if (is_bool($result) && $result == true) {
            $status = true;
        }

        return $status;
    }


    /**
     * updateCampaignDetails function
     *
     * @param array $camp_detials
     * @return boolean
     */
    public function updateCampaignDetails(array $camp_detials): bool
    {
        $status = false;

        if (empty($camp_detials) || (int)$camp_detials['id'] <= 0) {
            die("updateCampaignDetails");
            return $status;
        }
        // Get the campaign update details
        $update_params = $this->getUpdateDetials();
        $sury_id = (is_numeric($camp_detials['cio.survey_id']) && (int) $camp_detials['cio.survey_id'] > 0) ?
                    "`cio.survey_id` = '".$camp_detials['cio.survey_id']."',": "";
        $sql = "UPDATE `cio.campaign` "
                ."SET "
                ."`url`= '".$camp_detials['url']."', `activationdate`='".$camp_detials['activationdate']."', "
                ."$sury_id"
                ."`activationby`='".$camp_detials['activationby']."', "
                ."`archivedate`='".$camp_detials['archivedate']."', "
                ."`archiveby`='".$camp_detials['archiveby']."', `company_id`='".$camp_detials['company_id']."', "
                ."`version`='".$camp_detials['version']."',`status`='".$camp_detials['status']."', "
                ."`created`='".$camp_detials['created']."',`created_by`='".$camp_detials['created_by']."', "
                ."`updated`='".$camp_detials['updated']."',`updated_by`='".$camp_detials['updated_by']."'  "
                ."WHERE `id` = '".$camp_detials['id']."'";
 
        $result = $this->excuteQuerySET($sql);
        if (is_bool($result) && $result == true) {
            $status = $result;
        }
        return $status;
    }

    /**
     * getCampaignDetialsByURL function
     *
     * @param string $url
     * @param string $camp_status
     * @return array
     */
    public function getCampaignDetialsByURL(string $url, string $camp_status = null) : array
    {
        $return = [];
        $sql_status = "";
        if (empty($url) || !filter_var($url, FILTER_VALIDATE_URL)) {
            return $return;
        }

        if (!empty($camp_status)) {
            $sql_status = " AND `status` = '".$camp_status."'";
        }
        
        $sql = "SELECT * FROM `cio.campaign` WHERE `url` LIKE '%".$url."%'".$sql_status;
        $result = $this->excuteQueryGET($sql)[0];
        
        if (is_array($result) && !empty($result)) {
            $return = $result;
        }
        return $return;
    }
    

    /**
     * getActiveArchivedCampaignsByAccountId function
     *
     * @param integer $acc_id
     * @param integer $count
     * @return array
     */
    public function getActiveArchivedCampaignsByAccountId(int $acc_id, int $count = 0):array
    {
        $return = [];

        if ($acc_id <= 0 || $count <= 0) {
            return $return;
        }

        if ($count > 0) {
            $sql_limit = "LIMIT ".$count;
        }

        $sql = "SELECT `cio.campaign`.*, `cio.users`.`email`, `cio.company`.`account_no`,"
            ." `cio.company`.`account_no`  \n"
            . "FROM `cio.campaign` \n"
            . "INNER JOIN `cio.company` ON `cio.company`.`name`= `cio.campaign`.`account` \n"
            . "INNER JOIN `cio.users`ON `cio.users`.`id`=`cio.campaign`.`activationby` \n"
            . "AND `cio.campaign`.`status`!= 'draft' AND `cio.company`.`account_no` = ".$acc_id ." "
            . "ORDER BY `cio.campaign`.`updated` ".$sql_limit." ";

        $result = $this->excuteQueryGET($sql);

        // Get the Email id for each Updated_by User id
        foreach ($result as &$r) {
            if ((int)$r['updated_by'] > 0) {
                $r['updated_by_email'] = $this->getUserDetials($r['updated_by'])[0]['email'];
            }
            if ((int)$r['activationby'] > 0) {
                $r['activationby_email'] = $this->getUserDetials($r['activationby'])[0]['email'];
            }
        }

        return $result;
    }

    /**
     * getCampaignsByStatusForAccount function
     *
     * @param integer $acc_id
     * @param string $camp_status
     * @return array
     */
    public function getCampaignsByStatusForAccount(int $acc_id, string $camp_status = "") :array
    {
        $qry_status = "";

        if ($acc_id <= 0) {
            return [];
        }
        
        if (!empty($camp_status)) {
            $qry_status = " AND `cio.campaign`.`status` = '".$camp_status."'";
        }
        $lead_Obj = new \App\Models\Leads($this->db);
        switch ($camp_status) {
            case active:
                $order = "ORDER BY `cio.campaign`.`activationdate` DESC";
                break;
            case draft:
                $order = "ORDER BY `cio.campaign`.`created` DESC";
                break;
            case archive:
                $order = "ORDER BY `cio.campaign`.`archivedate` DESC";
                break;
        };

        $sql = "SELECT `cio.campaign`.*, `cio.campaign`.`name`, `cio.company`.`name`"
            . "FROM `cio.campaign`\n"
            . "INNER JOIN `cio.company` ON `cio.company`.`name`=`cio.campaign`.`account` \n"
            . "AND `cio.company`.`account_no` = ".$acc_id . $qry_status.$order;


        $result = $this->excuteQueryGET($sql);

        // Get the Email id for each Updated_by User id
        foreach ($result as &$r) {
            if ((int)$r['created_by'] > 0) {
                $r['created_by_email'] = $this->getUserDetials($r['created_by'])[0]['email'];
            }
            if ((int)$r['updated_by'] > 0) {
                $r['updated_by_email'] = $this->getUserDetials($r['updated_by'])[0]['email'];
            }
            if ((int)$r['archiveby'] > 0) {
                $r['archiveby_email'] = $this->getUserDetials($r['archiveby'])[0]['email'];
            }
            if ((int)$r['activationby'] > 0) {
                $r['activationby_email'] = $this->getUserDetials($r['activationby'])[0]['email'];
            }
            $r['lead_conversions'] = $lead_Obj->getLeadsCountForCampaign($r['url']);
        }

        return $result;
    }
    
        
    /*
     * activateCampaignById
     *
     * @param
     *        $params : parameters of the Campaign
     *
     * @return : Boolean
     *        False : If Campaign entry is not created in Campaign Table
     *        True : if the Campaign entry is successfull.
     *
     */
    public function activateCampaignById(array $params, string $path = null) : bool
    {
        if (empty($params) || empty($params['camp_id'])) {
            return false;
        }

        $camp_detials = $this->getCampaignDetailsById($params['camp_id']);
        $camp_detials['acc_id'] = $params['acc_id'];
        $camp_detials['camp_url'] = $params['camp_url'];
        $camp_detials['url'] =  $this->createCampaignURL($camp_detials);

        # Archive the campaign if active campaigns is present
        $active_camp = $this->getCampaignDetialsByURL($camp_detials['url'], "active");
        if (!empty($active_camp) && $active_camp['status'] == 'active') {
            $active_camp['camp_id'] = $active_camp['id'];
            $status = $this->archiveCampaignById($active_camp, $path);
            if (is_bool($status) && $status == false) {
                return false;
            }
        }

        # Update the campaign params
        $camp_detials['old_version'] = $camp_detials['version'];
        $camp_detials['old_status'] = $camp_detials['status'];
        $camp_detials['activationdate'] = date('Y-m-d H:i:s');
        $camp_detials['activationby'] = $_SESSION['user_id'] ? $_SESSION['user_id'] : " ";
        $camp_detials['status'] = 'active';

        $result = $this->updateCampaignDetails($camp_detials);

        if (is_bool($result) && $result == true  && isset($path)) {
            $result = $this->createCampaignVersionStucture($camp_detials, $path);
        }

        return true;
        ;
    }
    

    /**
     * createVersionOfCampaign function
     *
     * @param integer $camp_id
     * @param string $status
     * @return int
     */
    public function createVersionOfCampaign(int $camp_id, string $path, string $camp_url):int
    {
        $new_camp_id = 0;
        if ($camp_id <= 0 || empty($path) || empty($camp_url)) {
            return $new_camp_id;
        }

        $camp_details = $this->getCampaignDetailsById($camp_id);
        $camp_details['acc_id'] = $this->getAccountNumberByCompanyId($camp_details['company_id']);
        $camp_details['camp_url'] =  $camp_url;
        $camp_url_no_version = $this->createCampaignURL($camp_details);

        $camp_details['old_status']     = $camp_details['status'];
        $camp_details['status']         = "draft";
        $camp_details['old_version']    = $camp_details['version'];
        $camp_details['version']        = $this->getNextVersionNumber($camp_url_no_version);
        $camp_details['url']            =  $camp_url_no_version.'/versions/'.$camp_details['version'];

        // Check for draft campaigns in DB
        $draft_camp = $this->getCampaignDetialsByURL($camp_url_no_version, $camp_details['status']);
        if (is_array($draft_camp) && !empty($draft_camp)) {
            $draft_camp['camp_id'] = $draft_camp['id'];
            $status = $this->archiveCampaignById($draft_camp, $path);
            if (is_bool($status) && $status == false) {
                die("Campaign could not be revised");
                return $new_camp_id;
            }
        }

        $id = (int) $this->insertCampaignToDB($camp_details);
    
        if (is_numeric($id) && (int)$id >= 0) {
            if (!$this->createCampaignVersionStucture($camp_details, $path)) {
                return $new_camp_id;
            } else {
                $new_camp_id = $id;
            }
        }

        return $new_camp_id;
    }


    /**
     * createCampaignURL function
     *
     * @param array $camp_details
     * @return string
     */
    public function createCampaignURL(array $camp_details):string
    {
        $url = "";

        if (empty($camp_details) || empty($camp_details['acc_id'])) {
            return $url;
        }

        $url_option = $this->getCompanyDetailsByAccountId($camp_details['acc_id'])['url_option'];
        $result = $this->getURLBasedOnURLOption($camp_details, $url_option);

        if (!empty($url_option) && isset($url_option)) {
            $url = $result;
        }

        return $url;
    }


    /**
     * getCampaignURLBasedOnURLOption function
     *
     * @param array $camp_details
     * @param string $url_option
     * @return string
     */
    private function getURLBasedOnURLOption(array $camp_details, string $url_option) : string
    {
        $url = "";
        if (empty($url_option) || !isset($url_option)) {
            return $url;
        }

        if (empty($camp_details) || empty($camp_details['account']) ||
           empty($camp_details['name']) || empty($camp_details['camp_url'])) {
            return $url;
        }

        $acc_name = str_replace(' ', '', strtolower($camp_details['account']));
        $camp_name = str_replace(' ', '', $camp_details['name']);
        $cio_domain = $camp_details['camp_url'];

        if (empty($acc_name) || empty($camp_name) || empty($cio_domain)) {
            return $url;
        }

        switch ($url_option) {
            case "folder":
                $url = "https://".$cio_domain."/".$acc_name."/".$camp_name;
                break;
            case "sub-domain":
                $url = "https://".$acc_name.".".$cio_domain."/".$camp_name;
                break;
            case "delegate-domain":
                $domain = $this->getDelegatedDomain($camp_details['company_id']);
                if (!empty($domain) && isset($domain)) {
                    $url = "https://".$domain."/".$camp_name;
                }
                break;
        }

        return $url;
    }

    private function getDelegatedDomain(int $company_id)
    {
        $domain = "";

        if ($company_id <= 0) {
            return $domain;
        }
        $sql = "SELECT `delegated_domain` "
            ."FROM `cio.company` "
            ." WHERE `id` = $company_id";
        $result = $this->excuteQueryGET($sql)[0];
        if (is_array($result) && isset($result['delegated_domain'])) {
            $domain = $result['delegated_domain'];
        }

        return $domain;
    }

    /**
     * insertCampaignToDB function
     *
     * @param array $camp_details
     * @return boolean
     */
    public function insertCampaignToDB(array $camp_details): int
    {
        $new_camp_id = 0;

        if (empty($camp_details) || !is_numeric($camp_details['company_id']) ||
             empty($camp_details['url']) || empty($camp_details['account'])) {
            return $new_camp_id;
        }

        $create_params = $this->getCreateDetails();
        $udpate_params = $this->getUpdateDetials();

        $data = [
            'name'          => $camp_details['name']              ? $camp_details['name']: " ",
            'survey_id'     => $camp_details['cio.survey_id'],
            'account'       => $camp_details['account']           ? $camp_details['account']: " ",
            'url'           => $camp_details['url']               ? $camp_details['url']: " ",
            'comment'       => $camp_details['comment']           ? $camp_details['comment']: " ",
            'activationdate'=> " ",
            'activationby'  => " ",
            'archivedate'   => " ",
            'archiveby'     => " " ,
            'company_id'    => $camp_details['company_id']        ? $camp_details['company_id']: " " ,
            'version'       => $camp_details['version']           ? $camp_details['version']: " " ,
            'status'        => $camp_details['status']            ? $camp_details['status']: " " ,
            'created'       => $create_params['created']          ? $create_params['created']: " ",
            'created_by'    => $create_params['created_by']       ? $create_params['created_by']: " ",
            'updated'       => $udpate_params['updated']          ? $udpate_params['updated']: " ",
            'updated_by'    => $udpate_params['updated_by']       ? $udpate_params['updated_by'] : " "
        ];

        $sql = "INSERT INTO `cio.campaign`(`name`, `cio.survey_id`, `account`, "
                ."`url`, `comment`, `activationdate`, `activationby`, `archivedate`, `archiveby`, "
                ."`company_id`, `version`, `status`, `created`, `created_by`, `updated`, `updated_by`) "
                ." VALUES (:name, :survey_id, :account, :url, :comment, :activationdate,"
                .":activationby, :archivedate, :archiveby, "
                .":company_id, :version, :status, :created, :created_by, :updated, :updated_by)";
                
        $result = $this->excuteQuerySET($sql, $data);
        
        if (is_bool($result) && $result == true) {
            $new_camp_id = $this->getLastestRecordFromTable('cio.campaign', 'id')['id'];
        }

        return $new_camp_id;
    }

    /*
     * archiveCampaignById
     *
     * @param
     *        $params : parameters of the Campaign
     *
     * @return : Boolean
     *        False : If Campaign entry is not created in Campaign Table
     *        True : if the Campaign entry is successfull.
     *
     */
    public function archiveCampaignById(array $params, string $path): bool
    {
        if (empty($params) || (int)($params['camp_id']) <= 0 || empty($path)) {
            return false;
        }
        #Get the campaign details by Campaign Id
        $camp_detials = $this->getCampaignDetailsById($params['camp_id']);
        $camp_detials['acc_id'] = $params['acc_id'];
        $camp_detials['camp_url'] = $params['camp_url'];
        $camp_detials['url'] =  $this->createCampaignURL($camp_detials).'/versions/'.$camp_detials['version'];
        var_dump($camp_detials['url']);
        # Arhived campaigns copied to version folder.
        # $params['url'] = 'https://'.str_replace(' ','',strtolower($camp_detials['account'])).'
        //                  dev-campaign-in-one.de/'.str_replace(' ','',$camp_detials['name']).
        //                  '/'.$camp_detials['version'];
        $camp_detials['old_status'] = $camp_detials['status'];
        $camp_detials['archivedate'] = date('Y-m-d H:i:s');
        $camp_detials['archiveby'] = $_SESSION['user_id'] ? $_SESSION['user_id'] : " ";
        $camp_detials['status'] = 'archive';

        $result = $this->updateCampaignDetails($camp_detials);

        return $result;
    }
    
    
    /**
     * appendLinksForCampaigns function
     *
     * @param array $campaigns
     * @return array
     */
    public function appendLinksForCampaigns(array $campaigns):array
    {
        $result = array();
        
        if (empty($campaigns)) {
            return $result;
        }

        foreach ($campaigns as $campaign) {
            if ((int)$campaign['id'] <= 0) {
                continue;
            }
            $campaign['preview_url']    = $this->getCampaignDetailsById($campaign['id'])['url'];
            $campaign['delete_url']     = $this->generateCampaignLinks($campaign['id'], "delete");
            $campaign['update_url']     = $this->generateCampaignLinks($campaign['id'], "update");
            $campaign['copy_url']       = $this->generateCampaignLinks($campaign['id'], "copy");
            $campaign['activate_url']   = $this->generateCampaignLinks($campaign['id'], "activate");
            $campaign['archive_url']    = $this->generateCampaignLinks($campaign['id'], "archive");
            $campaign['reactivate_url'] = $this->generateCampaignLinks($campaign['id'], "reactivate");
            $campaign['edit_settings']  = $this->generateCampaignLinks($campaign['id'], "survey");
            $campaign['splittest']  = $this->generateCampaignLinks($campaign['id'], "splittest");
            
            if (!empty($campaign)) {
                array_push($result, $campaign);
            }
        }
        
        return $result;
    }
    
    /**
     * generateCampaignLinks function
     *
     * @param integer $campaign_id
     * @param string $operation
     * @return string
     */
    private function generateCampaignLinks(int $campaign_id, string $operation):string
    {
        $url = [];

        if ($campaign_id <= 0 || empty($_SESSION['acc_id'] || empty($operation))) {
            return $url;
        }
        // Generate URL for given Campaign Operation.
        $str_url = "https://campaign-in-one.net/account/".$_SESSION['acc_id']."/campaigns/".$campaign_id."/".$operation;
        // Validate the generated URL.
        if (filter_var($str_url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
            $url = $str_url;
        }
        
        return $url;
    }
    
    
    /*
     * getCampaignDetailsById
     *
     * @param
     *        $params : Campaign Id
     *
     * @return : Array
     *        Empty  : Campaign Id is missing
     *        Array  : Campaign Details
     *
     */
    public function getCampaignDetailsById(string $id) :array
    {
        $camp_details = array();
        
        if (!is_numeric($id) || (int)$id <= 0) {
            return $camp_details;
        }
        
        $sql = "SELECT * FROM `cio.campaign` WHERE `id` = ".$id;
        $camp_details = $this->excuteQueryGET($sql)[0];
        
        return $camp_details;
    }

    /*
     * getCampaignDetailsByURL
     *
     * @param
     *        $params : Campaign URL
     *
     * @return : Array
     *        Empty  : Campaign URL is missing
     *        Array  : Campaign Details
     *
     */
    public function getCampaignDetailsByURL(string $url):array
    {
        $camp_details = [];
        var_dump($url);
        if (empty($url) || !filter_var($url, FILTER_VALIDATE_URL)) {
            #var_dump(debug_backtrace());
            die("URL");
            return $camp_details;
        }

        $sql = "SELECT * FROM `cio.campaign` WHERE `URL` = '".$url."'";
        $result = $this->excuteQueryGET($sql)[0];
        
        if (is_array($result) && !empty($result)) {
            $camp_details = $result;
        }

        return $camp_details;
    }
    
    /*
     * getNextVersionNumber
     *
     * @param
     *        $params : Campaign Id
     *
     * @return : Array
     *        Empty  : Campaign Id is missing
     *        Array  : Campaign Details
     *
     */
    private function getNextVersionNumber($url)
    {
        $new_version = "";

        if (empty($url)) {
            $new_version = "1";
            return $new_version;
        } else {
            $sql = "SELECT `version`  FROM `cio.campaign` WHERE `url` LIKE '%".$url."%'";
            $versions = $this->excuteQueryGET($sql);
            $temp = array();
            # Collect all the versions in temp array
            foreach ($versions as $v) {
                array_push($temp, (int)$v['version']);
            }
            if (empty($temp)) {
                return $new_version;
            }
            # Get the largest number in the versions.
            $number = max($temp);
            $number = $number + 1;
            $new_version = $number;
        }

        return $new_version;
    }
    
    /*
     * createCampaignFolderStructure
     *
     * @param
     *        $params : Campaign Id
     *
     * @return : Array
     *        Empty  : Campaign Id is missing
     *        Array  : Campaign Details
     *
     */
    public function createCampaignFolderStructure(array $params, string $path):bool
    {
        if (empty($params) || empty($params['camp_name']) || empty($params['account'])
            || empty($path) || empty($params['camp_url'])) {
            return false;
        }

        // Get the version of the Campaign
        $url = "https://".$params['account'].$params['camp_url']."/".str_replace(' ', '', $params['camp_name']);
        $version = $this->getCampaignDetailsByURL($url)['version'] ;

        if (empty($version)) {
            $version = "1";
        }

        $master_campaign = "/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.net/libraries/campaign/template/";
        $new_campaign = $this->getCampaignFilePath($params['account'], $params['camp_name'], $version, $path);
        
        $dir = new \App\Directory\Directory();
        $result = $dir->rcopy($master_campaign, $new_campaign);

        return $result;
    }

    /*
     * createCampaignVersionStucture
     *
     * @param
     *        $params : Campaign Parameters
     *        $path   : Campaign Path
     *
     * @return : Boolean
     *        False  : If campaign parameters are missing.
     *        True   : New Version Folder structure is created successfully.
     *
     */
    private function createCampaignVersionStucture(array $params, string $path): bool
    {
        $dest = "";
        if (empty($params) || empty($params['name']) || empty($params['account']) ||
             empty($path) || empty($params['old_version']) || empty($params['version'])) {
            return false;
        }
        
        # Path to old version of Campaign
        $src =  $this->getCampaignFilePath($params['account'], $params['name'], $params['old_version'], $path);
        # Path to New version of Campaign
        if ($params['status'] !== "active") {
            $dest = $this->getCampaignFilePath($params['account'], $params['name'], $params['version'], $path);
        } else {
            $dest = $this->getCampaignFilePath($params['account'], $params['name'], "", $path);
        }

        $dir = new \App\Directory\Directory();
        $result = $dir->rcopy($src, $dest);
        
        return $result;
    }
    


    /**
     * getCampaignFilePath function
     *
     * @param array $camp_details
     * @param string $path
     * @return string
     */
    private function getCampaignFilePath(string $account, string $camp_name, string $version = "", string $path) :string
    {
        $file_path = "";
        if (empty($account) || empty($camp_name) || empty($path)) {
            return $file_path;
        }

        if (empty($version)) {
            $file_path = $path.'/'.str_replace(' ', '', strtolower($account)).'/'.str_replace(' ', '', $camp_name).'/';
        } else {
            $file_path = $path.'/'.str_replace(' ', '', strtolower($account)).'/'
                            .str_replace(' ', '', $camp_name).'/versions/'.$version.'/';
        }
        
        return $file_path;
    }
}
