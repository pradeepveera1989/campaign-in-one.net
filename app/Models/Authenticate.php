<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Respect\Validation\Validator as Respect;

/**
 * Description of Authenticate
 *
 * @author Pradeep
 */
class Authenticate extends Model
{
    public $db;
        
    /**
     * check function
     *
     * @return void
     */
    public function check()
    {
        if ($_SESSION['user_id'] || $_SESSION['acc_id']) {
            return true;
        }
        return false;
    }
    
    /**
     * getAuthIds function
     *
     * @return void
     */
    public function getAuthIds()
    {
        if (!($_SESSION['user_id']) && !($_SESSION['acc_id'])) {
            return false;
        }
        
        return [
            'user_id'     => $_SESSION['user_id'],
            'acc_id'      => $_SESSION['acc_id'],
            'acc_name'    => $_SESSION['acc_name'],
            'super_admin' => $_SESSION['super_admin']
        ];
    }
    
    /**
     * user function
     *
     * @return void
     */
    public function user()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        
        return false;
    }

    public function accName()
    {
        if (isset($_SESSION['acc_name'])) {
            return $_SESSION['acc_name'];
        }
        
        return false;
    }
    
    /**
     * checkSuperAdmin function
     *
     * @return void
     */
    public function checkSuperAdmin()
    {
        if (isset($_SESSION['super_admin'])) {
            return $_SESSION['super_admin'];
        }
        
        return false;
    }
    
    /**
     * validateUserSignIn function
     * 1. Validates User Email with Password.
     * 2. Checks if the User is Blocked
     * 3. Checks if the User is Activated or Not activated.
     *
     * @param array $params
     * @return boolean
     */
    public function validateUserSignIn(array $params) : bool
    {
        $status = false;
        if (empty($params) || !filter_var($params['email'], FILTER_VALIDATE_EMAIL) || empty($params['password'])) {
            return $status;
        }
  
        $user_details = $this->getUserDetailsByEmail($params['email']);
        if (!is_array($user_details) || empty($user_details['id'])) {
            $_SESSION['error'] = "Email address is not registered";
            return false;
        }

        // Check for Blocked User
        if ($user_details["status"] === "blocked") {
            $_SESSION['error'] = "Your is currently blocked";
            return false;
        }
        // Check for Non Activated User
        if ($user_details["status"] === "new") {
            $_SESSION['error'] = "Your account is not activated, Please check your inbox for Activation link";
            return false;
        }
        
        //code added for salt and pepper
        //checking for date created
        if ($user_details['created'] > "2019-08-15") {
            $UserPassword = $params["password"];
            $pepper = '2015';
            $salt = 'treactionA.G';
            $peperPassword = ($pepper . $UserPassword . $salt);
        } else {
            $peperPassword =  $params["password"];
        }
        
        // Validate Password
        if (!password_verify($peperPassword, $user_details["password"])) {
            $this->userLoginFailed($user_details);
            $_SESSION['error'] = "Email address and/or password do not match";
            return false;
        }
     
        $_SESSION['user'] = $user_details['first_name'].' '. $user_details['last_name'];
        $_SESSION['user_id'] = $user_details['id'];
        $_SESSION['super_admin'] = $user_details['super_admin'];
        $_SESSION['comp_id'] = (int) $user_details['company_id'];
        $_SESSION['acc_id'] = $this->getAccountNumberByCompanyId((int) $user_details['company_id']);
        $_SESSION['acc_name'] = $this->getCompanyDetailsByAccountId($_SESSION['acc_id'])['name'];

        $status = true;
 
        return status;
    }
    
    /**
     * userLoginFailed function
     *
     * @param array $user_details
     * @return boolean
     */
    public function userLoginFailed(array $user_details): bool
    {
        if (empty($user_details)) {
            return false;
        }

        $failed_logins = $user_details['failed_logins'];
          
        if ((int)$failed_logins >= 5) {
            // User is blocked
            $this->blockUser($user_details);
            return false;
        }
        
        $id =  $user_details['id'];
        (int)$failed_logins = (int)$failed_logins + 1;
        $qry = "UPDATE `cio.users` set `failed_logins` = $failed_logins where `id` = $id";
        $result = $this->excuteQuerySET($qry);
        
        return $result;
    }

    /**
     * blockUser function
     *
     * @param array $params
     * @return boolean
     */
    public function blockUser(array $params): bool
    {
        $qry = "UPDATE `cio.users` set `status` = 'blocked' where `id` = ";
        
        try {
            if (empty($params) || empty($params['id'])) {
                throw new Exception('Empty array sent to query');
            }
            
            $qry .= $params['id'];
            $result = $this->excuteQuerySET($qry);
            $_SESSION['blocked'] = "User has been blocked";
        } catch (Exception $ex) {
            echo "Caught Exception:". $e->getMessage();
            die();
        }
        return $result;
    }
    
    /**
     * unblockUser function
     *
     * @param array $params
     * @return boolean
     */
    public function unblockUser(array $params): bool
    {
        $qry = "UPDATE `cio.users` set `status` = 'active' where `id` = ";
        
        try {
            if (empty($params) || empty($params['id'])) {
                throw new Exception('Empty array sent to query');
            }
            
            $qry .= $params['id'];
            $result = $this->excuteQuerySET($qry);
        } catch (Exception $ex) {
            echo "Caught Exception:". $e->getMessage();
            die();
        }
        return $result;
    }
    
    /**
     * blockAccount function
     *
     * @param array $params
     * @return boolean
     */
    public function blockAccount(array $params):bool
    {
        $update = $this->getUpdateDetials();
        $qry = "UPDATE `cio.users` set `status` = 'blocked', `update` = '".$update['date']."' where `company_id` = ";
        try {
            if (empty($params) || empty($params['account_no'])) {
                throw new Exception('Empty array sent to query');
            }
            
            $qry .= $params['account_no'];
            $result = $this->excuteQuerySET($qry);
            $_SESSION['Company'] = "All the Users under this company has been blocked";
        } catch (Exception $ex) {
            echo "Caught Exception:". $e->getMessage();
            die();
        }
        return $result;
    }
    
    /**
     * unBlockAccount function
     *
     * @param array $params
     * @return boolean
     */
    public function unBlockAccount(array $params): bool
    {
        $update = $this->getUpdateDetials;
        $qry = "UPDATE `cio.users` set `status` = 'active', `update` = '".$update['date']."'  where `company_id` = ";
        
        try {
            if (empty($params) || empty($params['account_no'])) {
                throw new Exception('Empty array sent to query');
            }
            
            $qry .= $params['account_no'];
            $result = $this->excuteQuerySET($qry);
        } catch (Exception $ex) {
            echo "Caught Exception:". $e->getMessage();
            die();
        }
        return $result;
    }
    

    /**
     * signOut function
     *
     * @return void
     */
    public function signOut()
    {
        unset($_SESSION['user']);
        unset($_SESSION['user_id']);
        unset($_SESSION['super_admin']);
        unset($_SESSION['acc_id']);
        unset($_SESSION['acc_name']);
        
        return true;
    }
    
    /**
     * validateUserEmail function
     *  - Vaidates the Email format
     *  - Check if the Email is present in the DB
     *
     * @param string $email
     * @return boolean
     */
    public function validateUserEmail(string $email): bool
    {
        $return = false;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $return;
        }
        $user_id = $this->getUserDetailsByEmail($email)['id'];
        if (!is_numeric($user_id) || (int)$user_id <= 0) {
            return $return;
        }
        $return = true;

        return $return;
    }
}
