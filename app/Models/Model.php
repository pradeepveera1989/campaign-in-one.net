<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Model
 *
 * @author Pradeep
 */
class Model
{
    //put your code here
    
    public $db;
    public $c_params;
    public $u_params;
      
    public function __construct($db)
    {
        $this->db = $db;
    }
    
    public function getIpaddressOfUser()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
    
    public function getCreateDetails()
    {
        $params = array();
        $params["created"] = date('Y-m-d H:i:s');
        $params["created_by"] = $_SESSION['user_id'];
        return $params;
    }
    
    public function getUpdateDetials()
    {
        //Call to Authenticate to get the User detials
        // Get the current time stamp.
        $params = array();
        $params["date"] = date('Y-m-d');
        $params["updated"] = date('Y-m-d H:i:s');
        $params["updated_by"] = $_SESSION['user_id'];
        return $params;
    }


    /**
     * getCampaignsByCompanyId
     *
     * @param $company_id : Id for the Company
     *
     * @return Array | Boolean;
     *         Boolean(False) : When Company Id not provided
     *         Array : List of Campaigns for the Company
     */
    public function getCampaignsByCompanyId($company_id)
    {
        if (empty($company_id)) {
            return false;
        }
        $qry = "SELECT `cio.campaign`.`name`
                FROM `cio.campaign`  
                INNER JOIN `cio.company` ON `cio.company`.`id`=`cio.campaign`.`company_id` 
                WHERE `cio.company`.`account_no` = " .$company_id;
        $result = $this->excuteQueryGET($qry);
        
        return $result;
    }
    
    /**
    * updateSessionValue function
    *
    * @return boolean
    */
    public function updateSessionValue() : bool
    {
        $status = false;
        if (!isset($_SESSION['acc_id'])) {
            return $status;
        }
        $acc_id = (int) $_SESSION['acc_id'];
        $camp_details = $this->getCompanyDetailsByAccountId($acc_id);
        if (is_array($camp_details) && isset($camp_details['id']) && (int)$camp_details['id'] > 0) {
            $_SESSION['comp_id'] = (int)$camp_details['id'];
            $status = true;
        }

        return $status;
    }
    
    /**
     * getCompanyDetailsByAccountId function
     *
     * @param integer $acc_id
     * @return array
     */
    public function getCompanyDetailsByAccountId(int $acc_id) : array
    {
        $acc_details = [];
        if ($acc_id <= 0) {
            return $acc_details;
        }

        $sql = "SELECT * FROM `cio.company` WHERE `account_no` = ".$acc_id;
        $result = $this->excuteQueryGET($sql)[0];
        if (is_array($result) && (int)$result['id'] > 0) {
            $acc_details = $result;
        }

        return $acc_details;
    }

    /*
     * getLastestRecordFromTable
     *
     * @param
     *        $table  : Name of the Table
     *        $column : Name of the Column Name
     *
     * @return : Id
     *        Zero : if Table or column is missing
     *        Unique Number : Get the lastest record from the table
     */
    public function getLastestRecordFromTable($table, $column)
    {
        $record = 0;
        if (empty($table) || empty($column)) {
            return $record;
        }
        
        $sql = "\n"
            . "SELECT `".$column."` FROM `$table` \n"
            . "ORDER BY `".$column."` DESC\n"
            . "LIMIT 1";

        $record = $this->excuteQueryGET($sql)[0];

        return $record;
    }
    
    public function getUserByEmailFromMailinone($email)
    {
        $id = 0;
        # No email address.
        if (empty($email)) {
            return $id;
        }
        # Intiate the MailInONe object
        $mailinone = new \App\Mailinone\Mailinone();
        #Get Contact from MailInone
        $contact = $mailinone->getContactIdByEmail($email);
        #If no Contact with active status exits.
        if (is_null($contact)) {
            return $id;
        }
        #Contact with active status is found.
        $id = (int) $contact[0];
        return $id;
    }


    /**
     * getUserDetailsByEmail function
     *
     * @param string $email
     * @return array
     */
    public function getUserDetailsByEmail($email):array
    {
        $details = [];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $details;
        }

        $qry = "SELECT "
                ."`cio.users`.`id`, `cio.users`.`company_id`, `cio.users`.`email`, `cio.users`.`password`, "
                ."`cio.users`.`first_name`, `cio.users`.`last_name`, `cio.users`.`salutation`, "
                ."`cio.users`.`registration`, "
                ."`cio.users`.`login_date`, `cio.users`.`phone`, `cio.users`.`mobile`, `cio.users`.`failed_logins`, "
                ."`cio.users`.`super_admin`, `cio.users`.`created`, `cio.users`.`created_by`, `cio.users`.`update`, "
                ."`cio.users`.`updated_by`, `cio.users`.`status`, `cio.company`.`account_no`"
                ." FROM `cio.users`"
                ." INNER JOIN `cio.company` ON `cio.users`.`company_id`=`cio.company`.`id`"
                ." WHERE `email` LIKE  '$email' ";
                
        $result = $this->excuteQueryGET($qry)[0];
        if (!is_array($result) || empty($result['id'])) {
            return $details;
        }
        $details = $result;

        return $details;
    }
    
    public function getUserDetials($user_Id = null)
    {
        try {
            $id = empty($this->id) ? $user_Id : $this->id;
            
            if (empty($id)) {
                throw new \InvalidArgumentException('Invalid User Id passed');
            }
                
            # $qry = "Select * From `cio.users` WHERE `id` =  '$id' ";
            $qry = "SELECT `cio.users`.`id`, `cio.users`.`company_id`, `cio.users`.`email`, "
                    ."`cio.users`.`password`,`cio.users`.`first_name`, `cio.users`.`last_name`,"
                    ." `cio.users`.`salutation`, `cio.users`.`registration`, `cio.users`.`login_date`, "
                    ." `cio.users`.`phone`, `cio.users`.`mobile`, `cio.users`.`failed_logins`,"
                    ." `cio.users`.`super_admin`, `cio.users`.`created`, `cio.users`.`created_by`,"
                    ." `cio.users`.`update`, `cio.users`.`updated_by`, `cio.users`.`status`,"
                    ." `cio.company`.`id` as company_id , `cio.company`.`name`, "
                    ." `cio.company`.`account_no` as account_id  "
                    ." FROM `cio.users` "
                    ." INNER JOIN `cio.company`  ON  `cio.company`.`id` = `cio.users`.`company_id`"
                    ." WHERE `cio.users`.`id` =  '$id' ";
 
            $result = $this->excuteQueryGET($qry);
            
            if (empty($result)) {
                throw new Exception('Could not find User details');
            }
        } catch (Exception $ex) {
            echo "Caught Exception:". $e->getMessage();
        }
       
        return $result;
    }

    /**
     * getAccountNumberByCompanyId function
     * Get Company Account Number based on Campany Id.
     *
     * @param integer $company_id
     * @return integer
     */
    public function getAccountNumberByCompanyId(int $company_id):int
    {
        $acc_number = 0;
        
        if ($company_id <= 0) {
            return $acc_number;
        }
        
        $qry = "SELECT `account_no` FROM `cio.company` WHERE `id` =";
        $qry .= $company_id;
        $id = $this->excuteQueryGET($qry)[0];

        if (isset($id["account_no"]) && (int) $id["account_no"] > 0) {
            $acc_number = (int)$id["account_no"];
        }
        
        return $acc_number;
    }
    
    
    public function getCompanyIdByAccountNumber($acc_no)
    {
        $cmpny_id = 0;
        
        if (empty($acc_no)) {
            return $cmpny_id;
        }
        
        $qry = "SELECT `id` FROM `cio.company` WHERE `account_no` =";
        
        $qry .= $acc_no;
        
        $id = $this->excuteQueryGET($qry)[0];

        if (!is_null($id["id"])) {
            $cmpny_id = $id["id"];
        }
        
        return $cmpny_id;
    }
    
    
    /**
     * getCompanyParams
     *
     * @param Place   $where  Where something interesting takes place
     * @param integer $repeat How many times something interesting should happen
     *
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return cio.company Params
     */
    public function getCompanyParams($params)
    {
    }
    
    /**
     * getUserParams
     *
     * @param Place   $where  Where something interesting takes place
     * @param integer $repeat How many times something interesting should happen
     *
     * @throws Some_Exception_Class If something interesting cannot happen
     * @return cio.user table Params
     */
    public function getUserParams($params)
    {
    }
    
    
    
    public function excuteQueryGET($stmt = null)
    {
        try {
            if (!$stmt) {
                throw new \InvalidArgumentException("Invalid query statement passed");
            }

            $query = $this->db->prepare($stmt);
            
            $query->execute();
            
            $result = $query->fetchAll();
        } catch (PDOException  $e) {
            $e->getMessage();
            
            die();
        }
        
        return $result;
    }
    
    
    public function excuteQuerySET($stmt = null, $data = array())
    {
        try {
            if (!$stmt) {
                throw new \InvalidArgumentException("Invalid query statement passed");
            }

            $query = $this->db->prepare($stmt);

            $result = $query->execute($data);
        } catch (PDOException  $e) {
            $e->getMessage();
            die();
        }
        
        return $result;
    }
}


    // /**
    //  * getUserByEmail
    //  *
    //  * @param email id of the User
    //  *
    //  * @return User id;
    //  */
    // public function getUserByEmail($email) {

    //     $id = 0;

    //     if (empty($email)) {
    //         return $id;
    //     }

    //     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

    //         return $id;
    //     }

    //     $qry = "SELECT `id` FROM `cio.users` WHERE `email` LIKE  '$email' ";
    //     $result = $this->excuteQueryGET($qry)[0];

    //     if (!isset($result['id'])) {

    //         return $id;
    //     }

    //     $this->id = (int)$result['id'];

    //     return $this->id;
    // }
