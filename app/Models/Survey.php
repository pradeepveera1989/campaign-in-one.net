<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//put your code here
// cio.survey
// cio.question_types
// cio.question_context
// cio.questions
// cio.answers

namespace App\Models;

/**
 * Description of Survey
 *
 * @author Pradeep
 */
class Survey extends Model
{
    public $acc_id = 0;
    public $survey_id   = 0;
    public $campaign_id = 0;
    public $survey      = [
        "questions" => "",
        "desc" => ""
    ];
    
    public function __construct($db, $acc_id, $camp_id, $sury_id, $ques_id)
    {
        //Do something here
        parent::__construct($db);
        $this->acc_id       = intval($acc_id);
        $this->survey_id    = intval($sury_id);
        $this->campaign_id  = intval($camp_id);
        $this->ques_id      = intval($ques_id);
    }

    /*
     * getSurvey
     *
     * @param
     *
     * @return : Array
     *      Empty Array : If Survey is not present.
     *      Array       : If Survey is present.
     */

    public function getSurvey(array $params)
    {
        if (empty($params) || !preg_match('/^[0-9]*[1-9][0-9]*$/', $this->campaign_id)) {
            return $this->survey;
        }
        #$this->campaign_id         = $params['camp_id'];
        $this->survey_id           = $this->getSurveyIdFromCampaign($params['camp_id']);
        $this->survey['questions'] = $this->getQuestionsBySurveyId();
        $this->survey['desc']      = $this->getSurveyDataFromSurvey();

        return $this->survey;
    }

    /*
     * getSurveyIdFromCampaign
     *
     * @param
     *        $campaign_id : Id for Campaign
     *
     * @return : Int

     *      Survey Id   : If Survey is present.
     *      Zero        : If no Survey is present.
     */

    private function getSurveyIdFromCampaign():int
    {
        $survey_id = 0;

        if (empty($this->campaign_id) || !preg_match('/^[0-9]*[1-9][0-9]*$/', $this->campaign_id)) {
            return $survey_id;
        }

        $sql = "SELECT `cio.survey_id`  FROM `cio.campaign` WHERE `id` = ".$this->campaign_id;
        $result = $this->excuteQueryGET($sql)[0];

        if (!empty($result) && !empty($result['cio.survey_id']) && preg_match('/^[0-9]*[1-9][0-9]*$/', intval($result['cio.survey_id']))) {
            $survey_id = $result['cio.survey_id'];
        }

        return $survey_id;
    }

    /*
     * getSurveyDataFromSurvey
     *
     * @param
     *
     * @return : Array
     *      Survey data   : If Survey is present.
     *      Null          : If no Survey is present.
     */
    private function getSurveyDataFromSurvey():array
    {
        $survey = [];
        if (!isset($this->survey_id) || !is_numeric($this->survey_id)) {
            return $survey;
        }

        $sql = "SELECT * FROM `cio.survey` WHERE `cio.survey`.`id` = ".$this->survey_id;
        $result = $this->excuteQueryGET($sql)[0];
 
        if (is_array($result) && !empty($result)) {
            $survey = $result;
        }

        return $survey;
    }
    
    /*
     * getQuestionsBySurveyId
     *
     * @param
     *
     * @return : Array
     *      Empty  : If No Question found for the Survey.
     *      Array  : If Question found for the Survey.
     */

    private function getQuestionsBySurveyId():array
    {
        $questions = [];

        if (!preg_match('/^[0-9]*[1-9][0-9]*$/', $this->survey_id)) {
            // die("getQuestionsBySurveyId");
            return $questions;
        }

        $sql = "SELECT `cio.questions`.`id` As question_id, `cio.questions`.`cio.survey_id`, `cio.questions`.`cio.context_id`,`cio.questions`.`cio.question_types_id` As question_type_id, "
            ."`cio.questions`.`question_question_txt`, `cio.questions`.`question_hint_txt` ,`cio.questions`.`question_json_representation` ,"
            ."`cio.question_types`.`id`,  `cio.question_types`.`question_type_html_tag`,  `cio.question_types`.`question_type_html_template`, "
            ."`cio.question_types`.`question_type_frontend_hint`,  `cio.question_types`.`question_type_description`\n"
            ." FROM `cio.questions`"
            ."INNER JOIN `cio.question_types` ON `cio.questions`.`cio.question_types_id` = `cio.question_types`.`id`\n"
            ." WHERE `cio.questions`.`cio.survey_id`= ".$this->survey_id;

        $result = $this->excuteQueryGET($sql);
        // No rows found in SQL query.
        if (empty($result) || !is_array($result)) {
            return $questions;
        }
        // rows found in SQL Query
        $questions = $result;
        $i  = 0;
        // Twig missing Json decode functionality
        // Hence Json string is converted to Array
        while ($i < sizeof($result)) {
            if (empty($result[$i]['question_json_representation'])) {
                continue;
            }
            $questions[$i]['question_json_representation'] = (array) json_decode($result[$i]['question_json_representation'], true);
            $i++;
        }
        // Sort the question by Id(Not Id from Database but Id defiend by User)
        usort($questions, array($this, "usortQuestionsById"));

        return $questions;
    }


    /*
     * putSurvey
     *
     * @param  Array : Survey Parameters
     *
     * @return : Boolean
     *      True     : If Survey is inserted .
     *      False    : If Survey is not inserted.
     */
    public function putSurvey(array $params):bool
    {
        $status = false;
        if (empty($params) || !isset($this->campaign_id) || !isset($this->acc_id) ||
            (int) $this->campaign_id <= 0 || (int) $this->acc_id <= 0) {
            die("putSurvey_0");
            return $status;
        }
        // Check for survey_id If present in the params
        $this->survey_id   = $params['survey']['id'] > 0 ? $params['survey']['id'] : 0;
        $questions         = $params['survey'];
        $uploads           = $params['uploaded_files'];
        $upload_path       = [
            'upload_url'    => $params['upload_url'],
            'upload_dir'    => $params['image_dir']
        ];

        // Insert or Update Survey details to Survey and Campaign Table.
        if (!is_array($questions) || !$this->insertOrUpdateSurvey($questions) || !$this->updateSurveyIdToCampaign()) {
            // die("putSurvey_1");
            return $status;
        }

        // Adjusting the Question id to Upload image id
        foreach ($questions['question'] as $k => $v) {
            $q = $questions['question'][$k];
            $u = $uploads['survey']['question'][$k];
            if (!is_array($q) || !$this->insertQuestions($q, $u, $upload_path)) {
                break;
            }
        }

        $status = true;
        return $status;
    }



    /**
     * createVersionOfSurvey function
     *
     * @param integer $old_camp_id
     * @param integer $new_camp_id
     * @return boolean
     */
    public function createVersionOfSurvey() : int
    {
        $new_survy_id = 0;
        if (!isset($this->survey_id) || (int) $this->survey_id <= 0) {
            return $new_survy_id;
        }

        // Versioning of Survey
        $sury_sql_stmt = "INSERT INTO `cio.survey` (`survey_is_online`,`survey_description`,`layout_status_id`,`created_by`,`updated_by`) "
                        ."SELECT `survey_is_online`,`survey_description`,`layout_status_id`,`created_by`,`updated_by` "
                        ."FROM `cio.survey` WHERE `id` = ".$this->survey_id;
        
        $result = $this->excuteQuerySET($sury_sql_stmt);
        if (!is_bool($result) || $result == false) {
            // Failed to create new Survey id.
            return $status;
        }

        $id = $this->getLastestRecordFromTable('cio.survey', 'id')['id'];
        var_dump("ID", $id);
        if (!empty($id) && (int) $id > 0) {
            $new_survy_id = $id;
        }

        return  $new_survy_id;
    }

    /**
     * createVersionOfQuestions function
     *
     * @return boolean
     */
    public function createVersionOfQuestions(int $new_survey_id): bool
    {
        $status = false;

        if (!isset($this->survey_id) || (int)$this->survey_id <= 0 ||(int) $new_survey_id <= 0) {
            return $status;
        }

        // Versioning of Questions
        $ques_sql_stmt = "INSERT INTO `cio.questions` (`cio.survey_id`,`cio.context_id`,`cio.question_types_id`, "
                        ."`question_question_txt`,`question_hint_txt`,`question_json_representation`, `created_by`,`updated_by`) "
                        ."SELECT '".$new_survey_id."',`cio.context_id`,`cio.question_types_id`,`question_question_txt`, "
                        ."`question_hint_txt`,`question_json_representation`, `created_by`,`updated_by` "
                        ."FROM `cio.questions` "
                        ."WHERE `cio.survey_id` = ".$this->survey_id;
        
        $result = $this->excuteQuerySET($ques_sql_stmt);
        if (is_bool($result) && $result == true) {
            $status = $result;
        }

        return $status;
    }

    /*
     * updateSurveyIdToCampaign
     *
     * @param
     *
     * @return : Boolean
     *      True     : If Survey Id is updated to Campaign Table .
     *      False    : If Survey Id is not updated.
     */
    private function updateSurveyIdToCampaign():bool
    {
        $result = false;
        if (empty($this->campaign_id) || !preg_match('/^[0-9]*[1-9][0-9]*$/', $this->campaign_id) ||
            empty($this->survey_id) || !preg_match('/^[0-9]*[1-9][0-9]*$/', $this->survey_id)) {
            // die("updateSurveyIdToCampaign");
            return $result;
        }

        $sql = " UPDATE `cio.campaign` SET `cio.survey_id`= ".$this->survey_id." WHERE `id` = ".$this->campaign_id;
        $result = $this->excuteQuerySET($sql);

        return $result;
    }

    /*
     * insertOrUpdateSurvey
     *
     * @param  Array : Survey Table Parameters
     *
     * @return : Boolean
     *      True     : If Survey is inserted .
     *      False    : If Survey is not inserted.
     */

    private function insertOrUpdateSurvey(array $survey):bool
    {
        $result = false;
        if (empty($survey) || empty($survey['description'])) {
            return $result;
        }

        $data = [
            "id" => $this->survey_id ,
            "survey_is_online" => $survey['is_online'],
            "survey_description" => $survey['description'],
            "layout_status_id" => $survey['layout_status_id'],
            "created_at" => '',
            "created_by" => 0,
            "updated_at" => '',
            "updated_by" => 0,
        ];

        $sql = "INSERT INTO `cio.survey`(`id`, `survey_is_online`, `survey_description`, `layout_status_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) "
            ." VALUES (:id, :survey_is_online, :survey_description, :layout_status_id, :created_at, :created_by, :updated_at, :updated_by)"
            ." ON DUPLICATE KEY UPDATE "
            ."`cio.survey`.`survey_is_online` = :survey_is_online, "
            ."`cio.survey`.`survey_description` = :survey_description, "
            ."`cio.survey`.`layout_status_id` = :layout_status_id";

        $result = $this->excuteQuerySET($sql, $data);
        if (is_bool($result) && $result == true && empty($this->survey_id)) {
            $this->survey_id = $this->getLastestRecordFromTable("cio.survey", "id")['id'];
        }

        return $result;
    }

    /*
     * insertOrUpdateQuestions
     *
     * @param  Array : question Parameters
     *
     * @return : Boolean
     *      True     : If Survey is inserted .
     *      False    : If Survey is not inserted.
     */

    public function insertQuestions(array $question, array $s_uploads = null, array $upload_path):bool
    {
        $return  = false;
        $options = $question[$question['type']]['options'];

        if (!is_array($question) || intval($question['id']) <= 0 || empty(trim($question['type']))
            || empty(trim($question['text'])) || intval($this->survey_id) <= 0) {
            return $return;
        }

        $question['context_type'] = 1;  // Hard codded
        $ques_context_id          = $this->getQuestionContextId($question['context_type']);
        $ques_type_id             = $this->getQuestionTypeId($question['type']);

        if (intval($ques_type_id) <= 0 || intval($ques_context_id) <= 0) {
            return $return;
        }
        
        // Check and Upload the Image
        $i = 1;
        while ($i <= count($options)) {
            $image_attr = $s_uploads[$question['type']]['options'][$i];
            if ($image_attr['link_ref'] && $image_attr['link_ref']->getError() === UPLOAD_ERR_OK && !empty($upload_path)) {
                $file_name = $this->updateOrInsertImage($image_attr, $upload_path);
                $question[$question['type']]['options'][$i]["link_ref"] = strlen($file_name) > 0 ? $file_name : "";
            }
            $i++;
        }

        $ques_json_representation = json_encode($question,JSON_UNESCAPED_UNICODE);
        $insert_data = [
            "id"                            => $question['db_id'],
            "survey_id"                     => $this->survey_id,
            "context_id"                    => $ques_context_id,
            "question_types_id"             => $ques_type_id,
            "question_question_txt"         => $question['text'],
            "question_hint_txt"             => $question['hint'],
            "question_json_representation"  => $ques_json_representation,
            "created_at" => '',
            "created_by" => 0,
            "updated_at" => '',
            "updated_by" => 0,
        ];
        
        $sql = "INSERT INTO `cio.questions`(`id`, `cio.survey_id`, `cio.context_id`, `cio.question_types_id`, `question_question_txt`, `question_hint_txt`, `question_json_representation`, `created_at`, `created_by`, `updated_at`, `updated_by`) "
            ." VALUES(:id, :survey_id, :context_id, :question_types_id, :question_question_txt, :question_hint_txt, :question_json_representation, :created_at, :created_by, :updated_at, :updated_by)";

        $result = $this->excuteQuerySET($sql, $insert_data);
        if (is_bool($result) && $result == true) {
            $return = $result;
        }
        return $return;
    }

    /*
     * deleteQuestionById
     *
     * @param  Array : question Parameters
     *
     * @return : Boolean
     *      True     : If Rows are deleted .
     *      False    : If Rows are not deleted.
     */
    public function deleteQuestionById(array $question) :bool
    {
        $result = false;
        // Check for Questiion array
        if (!is_array($question) || empty($question) || intval($question['ques_id']) <= 0) {
            // die("Invalid Array passed to deleteQuestionById");
            return $result;
        }

        // Mysql Query to delete the Question by Question Id
        $sql    = "DELETE FROM `cio.questions` WHERE id = ".$question['ques_id'];
        $return = $this->excuteQuerySET($sql);
        // Check if the Query is successfull or failure.
        // Update $result only if Query is succesfull.
        if (is_bool($return) && $return == true) {
            $result = $return;
        }

        return $result;
    }

    /*
     * updateQuestionById
     *
     * @param  Array : question Parameters
     *
     * @return : Boolean
     *      True     : If Rows are deleted .
     *      False    : If Rows are not deleted.
     */
    public function updateQuestionById(array $question) :bool
    {
        $return    = false;
        $s_ques    = $question['survey']['question'][1];
        $s_uploads = $question['uploaded_files'];
        $i         = 1;
        $options   = $s_ques[$s_ques['type']]['options'];
        $upload_path       = [
            'upload_url'    => $question['upload_url'],
            'upload_dir'    => $question['image_dir']
        ];
        // Check for Questiion array
        if (!is_array($question) || empty($question) || $this->survey_id <= 0 || intval($question['ques_id']) <= 0) {
            // die("Invalid Array passed to updateQuestionById");
            return $return;
        }

        // Check for Images for Question Type Select or Checkbox
        if ($s_ques['type'] == "select" || $s_ques['type'] == "checkbox") {
            while ($i <= count($options)) {
                //Get the Image details for each Option
                $image_attr = $s_uploads['survey']['question'][1][$s_ques['type']]['options'][$i];
                if ($image_attr['link_ref'] && $image_attr['link_ref']->getError() === UPLOAD_ERR_OK && !empty($question['upload_url'])) {
                    // Upload the Image to Server
                    $file_name = $this->updateOrInsertImage($image_attr, $upload_path);
                    // Update the filename to Question array
                    $s_ques[$s_ques['type']]['options'][$i]["link_ref"] = strlen($file_name) > 0 ? $file_name : "";
                }
                $i++;
            }
        }
        // Encode question to Json representation
        $ques_json_representation = json_encode($s_ques,JSON_UNESCAPED_UNICODE);
        $sql = "UPDATE `cio.questions` "
            ."SET "
            ."`question_question_txt`= '".$s_ques['text']."',"
            ."`question_hint_txt`='".$s_ques['hint']."',"
            ."`question_json_representation`= '".$ques_json_representation."'"
            ." WHERE "
            ."`cio.survey_id` = ".$this->survey_id." AND "
            ."`id` = ".$question['ques_id'];
        
        $result = $this->excuteQuerySET($sql);
        if (is_bool($result) && $result == true) {
            $return = $result;
        }

        return $return;
    }

    /*
     * getSurveyQuestionById
     *
     * @param  Array : question Id,
     *
     * @return : Array
     *      Question  : Question detials for question Id
     *      Null      : No question found.
     */
    public function getSurveyQuestionById(array $ids) : array
    {
        $return = [];

        if (!is_array($ids) || empty($ids) || $this->survey_id <= 0 || intval($ids['ques_id']) <= 0) {
            // die("Invalid Array passed to updateQuestionById");
            return $return;
        }

        $sql = "SELECT `cio.questions`.`id` As question_id, `cio.questions`.`cio.survey_id` As survey_id, `cio.questions`.`cio.context_id`,`cio.questions`.`cio.question_types_id` As question_type_id, "
            ."`cio.questions`.`question_question_txt`, `cio.questions`.`question_hint_txt` ,`cio.questions`.`question_json_representation` ,"
            ."`cio.question_types`.`id`,  `cio.question_types`.`question_type_html_tag`,  `cio.question_types`.`question_type_html_template`, "
            ."`cio.question_types`.`question_type_frontend_hint`,  `cio.question_types`.`question_type_description`\n"
            ." FROM `cio.questions`"
            ." INNER JOIN `cio.question_types` ON `cio.questions`.`cio.question_types_id` = `cio.question_types`.`id`\n"
            ." WHERE `cio.questions`.`cio.survey_id`= ".$this->survey_id." AND `cio.questions`.`id` = ".$ids['ques_id'];

        $result = $this->excuteQueryGET($sql)[0];
        if (is_array($result) && !empty($result)) {
            $return                                 = $result;
            $return['question_json_representation'] = (array) json_decode($result['question_json_representation'], true);
        }

        return $return;
    }

    /*
     * getQuestionTypeId
     *
     * @param  String : $question_type
     *
     * @return : Interger
     *      0           : If Question type is not found
     *      interger    : Question type is found.
     */

    private function getQuestionTypeId(string $question_type):int
    {
        $question_type_id = 0;
        if (empty(trim($question_type))) {
            return $question_type_id;
        }

        $sql = "SELECT `id` FROM `cio.question_types` WHERE `question_type_html_tag` LIKE '".$question_type."'";
        
        $result = $this->excuteQueryGET($sql)[0];
        if (!empty($result)) {
            $question_type_id = $result['id'];
        }

        return $question_type_id;
    }

    /**
     * getQuestionContextId function
     *
     * @param string $context_type
     * @return integer
     */
    private function getQuestionContextId(string $context_type):int
    {
        $context_id = 1;
        if (empty(trim($context_type))) {
            return $context_id;
        }

        $sql    = "SELECT * FROM `cio.question_context` WHERE `context_category` LIKE '".$context_type."'";
        
        $result = $this->excuteQueryGET($sql)[0];
        if (!empty($result)) {
            $context_id = $result['id'];
        }

        return $context_id;
    }
    
    /**
     * updateOrInsertImage
     *
     * @param array $image
     * @param array $upload
     * @return string
     */
    private function updateOrInsertImage(array $image, array $upload) :string
    {
        if (empty($image) || empty($upload['upload_url']) ||empty($upload['upload_dir']) || $this->acc_id <= 0 || $this->campaign_id <= 0) {
            // die("updateOrInsertImage");
            return false;
        }

        $file_name = array();
        $folder_str = '/account/'.$this->acc_id.'/campaign/'.$this->campaign_id.'/survey/';
        $path       = $upload['upload_dir'].$folder_str;
 
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $dir    = new \App\Directory\Directory();
        $result = $dir->moveUploadedFile($path, $image['link_ref']);
        if (!empty($result)) {
            $file_name = $upload['upload_url'].$folder_str.$result;
        }

        return $file_name;
    }

    /*
     * usortQuestionsById
     *
     * @param  $a : Question Array First
     *         $b : Question Array Second
     *
     * @return : Interger
     *      1     : if 1st array element is less than 2nd array element
     *      -1    : if 1st array element is not less than 2nd array element
     */

    private function usortQuestionsById($a, $b)
    {
        $x = intval($a['question_json_representation']['id']);
        $y = intval($b['question_json_representation']['id']);

        // if x is less than y then it returns -1
        // else it returns 1
        if ($x < $y) {
            return -1;
        } else {
            return 1;
        }
    }
}
