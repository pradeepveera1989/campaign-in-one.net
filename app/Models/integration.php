<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * This is for account level integrations same as the tabel name
 */
namespace App\Models;

class integration extends Model
{
    public $type; // Integration type

    public function __construct($db, $type)
    {
        //Do something here
        parent::__construct($db);
        $this->type = $type;
    }
    
    /**
     * getIntegrationDetails
     *
     * @param integer $camp_id
     * @return array
     */
    public function getIntegrationDetails(int $camp_id):array
    {
        $return = [];
  
        if ($camp_id <= 0 || empty($this->type)) {
            return $return;
        }
        //update sql query
        $sql = "SELECT *"
            ."FROM `cio.integrations` "
            ."WHERE `type` = '" .$this->type."' AND `account_no` = " . $camp_id;
        
        $result = $this->excuteQueryGET($sql);

        if (!is_array($result) || !empty($result)) {
            $return = $result;
        }
 
        return $return;
    }

    /**
     * insertOrUpdateIntegration
     *
     * @param array $params
     * @return boolean
     */
    public function insertOrUpdateIntegration(array $params):bool
    {
        $result = false;
        if (empty($params)) {
            return $result;
        }

        $id = is_numeric($params['id']) ? $params['id'] : " ";
        $sql = $this->getInsertOrUpdateSqlQuery($params);
        $result = $this->excuteQuerySET($sql);

        return $result;
    }

    /**
     * createVersionOfIntegration function
     *
     * @param integer $camp_id
     * @return boolean
     */
    public function createVersionOfIntegration(int $old_camp_id, int $new_camp_id): bool
    {
        $status = false;

        if ($old_camp_id <= 0 || $new_camp_id <= 0) {
            return $status;
        }

        $int_details = $this->getIntegrationDetails($old_camp_id)[0];
        // No integration details found for this campaign
        if (!is_array($int_details) || empty($int_details)) {
            $status = true;
            return $status;
        }
        
        unset($int_details['id']);
        $int_details['camp_id'] = $new_camp_id;

        // Create a new Version for new campaign.
        $sql = $this->getInsertOrUpdateSqlQuery($int_details);
        $status = $this->excuteQuerySET($sql);

        return $status;
    }

    /**
     * getInsertOrUpdateSqlQuery
     *
     * @param array $params
     * @return string
     */
    private function getInsertOrUpdateSqlQuery(array $params):string {

        $params['company_id'] = is_numeric($params['company_id']) ?
         $params['company_id'] : $this->getCompanyIdByAccountNumber($params['acc_id']);
        $stmt = 'INSERT INTO `cio.integrations`(`id`, `type`, `api_key`, `user`, `password`, `url`, `account_no`, `first_name`, `last_name`, `owner_id`, `sync`, `additional_settings`, `created`, `created_by`, `updated`, `updated_by`)'
                . ' VALUES ('
                . " '" . $params[ 'id' ] . "',"
                . " '" . $this->type . "',"
                . " '" . $params[ 'apikey' ] . "',"
                . " '" . $params[ 'user' ] . "',"
                . " '" . $params[ 'password' ] . "',"
                . " '" . $params[ 'url' ] . "',"
                . " '" . $params[ 'acc_no' ] . "' ,"
                . " '" . $params[ 'first_name' ] . "',"
                . " '" . $params[ 'last_name' ] . "',"
                . " '" . $params[ 'owner_id' ] . "',"
                . " '" . $params[ 'sync' ] . "',"
                . " '" . $params[ 'value' ] . "',"
                . "' ',"
                . "' ',"
                . " '" . $params[ 'updated' ] . "',"
                . "' '"
                . ') ON DUPLICATE KEY UPDATE '
                . "`cio.integrations`.`type`        = '" . $this->type . "', "
                . "`cio.integrations`.`api_key`      = '" . $params[ 'apikey' ] . "', "
                . "`cio.integrations`.`user`        = '" . $params[ 'user' ] . "', "
                . "`cio.integrations`.`password`    = '" . $params[ 'password' ] . "', "
                . "`cio.integrations`.`url`         = '" . $params[ 'url' ] . "', "
                . "`cio.integrations`.`first_name`  = '" . $params[ 'first_name' ] . "', "
                . "`cio.integrations`.`last_name`   = '" . $params[ 'last_name' ] . "', "
                . "`cio.integrations`.`owner_id`    = '" . $params[ 'owner_id' ] . "',"
                . "`cio.integrations`.`sync`        = '" . $params[ 'sync' ] . "',"
                . "`cio.integrations`.`additional_settings`  = '" . $params[ 'value' ] . "'";

        return $stmt;
    }


    /**
     * getCIOStandardField
     * 
     * @return array
     */
    public function getCIOStandardField() :array
    {
        $std_fields = [];

        $sql = 'SELECT 
                `id`,`standard_fields`  
                FROM `cio.standard_fields`';
        $result = $this->excuteQuerySET($sql);
        if(is_array($result) && !empty($result)) {
            $std_fields = $result;
        }

        return $std_fields;
    }
}
