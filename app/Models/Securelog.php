<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Securelog
 *
 * @author Pradeep
 */
class Securelog extends Model
{
    //put your code here
    
    public $logindate;      // Login attempt date and Time
    public $user_id;        // User id
    public $company_id;     // Company Id
    public $ip_address;     // Ipaddress
    public $result;         // status of the attempt'success' or 'failure'
    
    
    /**
     * getTheDateTime
     *
     * @param None
     *
     * @return Current DateTime;
     */
    public function getTheDateTime()
    {
        return date('Y-m-d H:i:s', time());
    }
    
    /**
     * createLogEntry
     *
     * @param
     *      $params        : User details
     *      $login_attempt : Login attempt 'success or failure'
     *
     * @return boolean
     *      True    : entry to secureLog Table is successful
     *      False   : could not make entry to secureLog Table
     */
    
    public function createLogEntry(array $params, string $login_attempt) :bool
    {
        $status = false;
        #Email is missing or Status of the login attempt
        if (empty($params['email']) && !isset($login_attempt)) {
            return $status;
        }

        $usr_details = $this->getUserDetailsByEmail($params['email']);
        if (!is_array($usr_details) || !isset($usr_details['id']) || !isset($usr_details['company_id'])) {
            return $status;
        }
        
        #Get the LoginDate
        $login_date = $this->getTheDateTime();
        $ip_addr = $this->getIpaddressOfUser();
        
        #Prepare the data for Insert
        $data = [
            'logindate' => $login_date,
            'user_id' =>  $usr_details['id'],
            'company_id' => $usr_details['company_id'],
            'ip' => $ip_addr,
            'result' => login_attempt
        ];
    
        $qry = "INSERT INTO `cio.securelog`(`logindate`, `user_id`, `company_id`, `ip`, `result`) "
                ."VALUES (:logindate, :user_id, :company_id, :ip, :result)";
        
        $result = $this->excuteQuerySET($qry, $data);
        if (is_bool($result) && $result) {
            $status = $result;
        }
        return $status;
    }
}
