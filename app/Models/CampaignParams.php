<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Models;

/**
 * Description of CampaignParams
 *
 * @author Aravind
 */

class CampaignParams extends Model
{
    public $name; // Integration name

    public function __construct($db, $name)
    {
        //Do something here
        parent::__construct($db);
        $this->name = $name;
    }
    
    /**
     * getSettingDetails
     *
     * @param integer $camp_id
     * @return array
     */
    public function getSettingDetails(int $camp_id):array
    {
        $return = [];
  
        if ($camp_id <= 0 || empty($this->name)) {
            return $return;
        }
        
        $sql = 'SELECT *'
               . 'FROM `cio.campaign_params` '
               . "WHERE `name` = '" . $this->name . "' AND `campaign_Id` = " . $camp_id;
        $result = $this->excuteQueryGET($sql);

        if (!is_array($result) || !empty($result)) {
            $return = $result;
        }
 
        return $return;
    }

    /**
     * insertOrUpdateIntegration
     *
     * @param array $params
     * @return boolean
     */
    public function insertOrUpdateSettings(array $params):bool
    {
        $result = false;
        if (empty($params)) {
            return $result;
        }
        $id = is_numeric($params['id']) ? $params['id'] : " ";
        $sql = $this->getInsertOrUpdateSqlQuery($params);
        
        $result = $this->excuteQuerySET($sql);

        return $result;
    }

    /**
     * createVersionOfIntegration function
     *
     * @param integer $camp_id
     * @return boolean
     */
    public function createVersionOfSettings(int $old_camp_id, int $new_camp_id): bool
    {
        $status = false;

        if ($old_camp_id <= 0 || $new_camp_id <= 0) {
            return $status;
        }

        $int_details = $this->getSettingDetails($old_camp_id)[0];
        // No integration details found for this campaign
        if (!is_array($int_details) || empty($int_details)) {
            $status = true;
            return $status;
        }
        
        unset($int_details['id']);
        $int_details['camp_id'] = $new_camp_id;

        // Create a new Version for new campaign.
        $sql = $this->getInsertOrUpdateSqlQuery($int_details);
        $status = $this->excuteQuerySET($sql);

        return $status;
    }

    /**
     * getInsertOrUpdateSqlQuery
     *
     * @param array $params
     * @return string
     */
    private function getInsertOrUpdateSqlQuery(array $params):string
    {
        $params['company_id'] = is_numeric($params['company_id']) ?
            $params['company_id'] : $this->getCompanyIdByAccountNumber($params['acc_id']);
        $stmt = "INSERT INTO "
        ."`cio.campaign_params`("
        ."`Id`, `campaign_Id`, `group_name`, `name`, "
        ." `type`, `value`, `created`, `created_by`, `updated`, `updated_by`)"
        . " VALUES ("
        . " '" .$params['id'] ."',"
        . " '" .$params['camp_id'] ."',"
        . " '" .$params['group_name'] ."',"
        . " '" .$params['name'] ."',"
        . " '" .$params['type'] ."',"
        . " '" .$params['value'] ."',"
        .    "' ',"
        .    "' ',"
        . " '" .$params['updated'] ."',"
        .    "' '"
        . ") ON DUPLICATE KEY UPDATE "
        . "`cio.campaign_params`.`campaign_Id`      = '" .$params['camp_id'] ."', "
        . "`cio.campaign_params`.`group_name`        = '" .$params['group_name'] ."', "
        . "`cio.campaign_params`.`name`    = '" .$params['name'] ."', "
        . "`cio.campaign_params`.`type`         = '" .$params['type'] ."', "
        . "`cio.campaign_params`.`value`        = '" .$params['value'] ."'";

        return $stmt;
    }

    /**
     * getCIOStandardMapping
     *
     * @return array
     */
    public function getCIOStandardMapping() :array
    {
        $std_fields = [];

        $sql = 'SELECT 
                *
                FROM `cio.mapping`';
        $result = $this->excuteQueryGET($sql);
        if(\is_array($result) && !empty($result)) {
            $std_fields = $result;
        }

        return $std_fields;
    }
}
