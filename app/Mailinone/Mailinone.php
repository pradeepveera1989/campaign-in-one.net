<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mailinone
 *
 * @author Pradeep
 */
namespace App\Mailinone;

require __DIR__ . '/../../vendor/autoload.php';

class Mailinone {
    //put your code here
    
    protected $key;
    protected $doi;


    public function __construct() {     
        $this->key = "08b90d60-d439-404f-bb93-4bade6e9582a";  
        $this->doi = "b13IZKDL";
        $this->getConfigParams();
	} 

 /**
  * @Function  getConfigParams.
  *
  * @Description create config parameter required for Maileon
  *
  * @return void
  * 
  */    
    
    private function getConfigParams(){
        if(isset($this->key)){
            $this->config = array(
                'BASE_URI' => 'https://api.maileon.com/1.0',
                'API_KEY' => $this->key,
                'THROW_EXCEPTION' => true,
                'TIMEOUT' => 60,
                'DEBUG' => 'TRUE' // NEVER enable on production
            );             
        }
    }
    
    
 /**
  * @Function  createContactService.
  *
  * @Description Initiate Contact Service to insert or update a contact in Maileon
  *
  * @return void
  * 
  */     
    public function createContactService(){
        
        try {
            
            if(count($this->config) == 0){
                throw new Exception("Maileon: Invalid Config Parameters ".PHP_EOL);     
            }
            
            $contactsService = new \com_maileon_api_contacts_ContactsService($this->config);
            \com_maileon_api_contacts_Permission::init();
            \com_maileon_api_contacts_SynchronizationMode::init();
            
            $contactsService->setDebug(false);
            $newContact = new \com_maileon_api_contacts_Contact();
            $newContact->email = $this->email;
            $newContact->anonymous = false;
            $newContact->permission = \com_maileon_api_contacts_Permission::$NONE;

            foreach ($this->stnd_params as $key => $value){
                $newContact->standard_fields[$key] = $value;
            }

            $this->response = $contactsService->createContact($newContact, \com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, false, $this->doi);   
              
            if(!($this->response->isSuccess())){
                
               $this->response = false;
               throw new Exception("Maileon: Contact could not be created or updated ".PHP_EOL);     
            }
            
        } catch (Exception $e) {
            
             echo $e->getMessage(); 
        }         
    }
    
    
    
  /**
  * @Function  getContactStatus.
  *
  * @Description Verifies the Maileon where the contact is present or
  *              not based on Email address of the contact.
  *
  * @return contact Id on success
  */      
        
    public function getContactIdByEmail($email){
        
        $id = 0;
        
        $contact_service = new \com_maileon_api_contacts_ContactsService($this->config);

        $contact_service->setDebug(false);
        $getContact = $contact_service->getContactByEmail($email);

        // Contact is present in the Mail-in-one
        if(($getContact->getResult()->id)){
            
            // Get the id of the contact.
            $id = $getContact->getResult()->id;
        }       
        
        return $id;
    }
    
    
  /**
  * @Function  getStandardParams.
  *
  * @Description Maps the Standard parameters required to create a contact in Maileon
  *            
  * @return array of parameter
  * 
  */        
    public function getStandardParams($param){
        
        $return = false;
        
        if(empty($param["email"])){
            return $return;
        }
       
        $this->email = $param["email"];
        return array(
            
            "SALUTATION" => $param['salutation'],
            "FIRSTNAME" => $param['first_name'],
            "LASTNAME" => $param['last_name'],
            "ADDRESS" => $param['address'],
            "ZIP" => $param['plz'],
            "CITY" => $param['city']
        );

    }  

  /**
  * @Function  insertContact.
  *
  * @Description inserts the contact data to the by creating contact service.
  *              Maps the Standard and Custom parameters of the contact. 
  *              Intiates the contact service only if there are standard parameters.
  *            
  * @return response data of contact service.
  * 
  */     
    public function insertContact($params){
                      
        try{
            
            if(empty($params)){
                
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);     
            }
            
            $this->stnd_params = $this->getStandardParams($params);

            if(!$this->stnd_params){  
                
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);     
            }
           
            $this->createContactService();
            $return = $this->response;
                        
        } catch (Exception $e) {
            echo $e->getMessage();    
        }
        return $return;        
    }    
    
}
