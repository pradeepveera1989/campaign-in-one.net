<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mail
 *
 * @author Pradeep
 */

namespace App\Mailer;
require __DIR__ . '/../../vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class mail {
    //put your code here
    public $params;
    public function __construct($params) {
        
        $this->params = $params;
        $this->user_id = $params['id'];
        $this->account_id = $params['account_no'];
        $this->email = $params['email'];
        $this->user_details = $params;
        $this->helpdesk = "https://treaction.net/kontakt/";
        $this->support_email = "campaign@treaction.net";
    }
    
        
    private function getUserName(){
        
        if(empty($this->user_details)){
            
            return false;
        }
        
        return $this->user_details['first_name'] .' '.$this->user_details['last_name'];
    }
    
    
    public function getLoginConformationParms(){
        
    }

    
    public function getResetLink(){
        
        if(empty($this->user_id) && empty($this->account_id)){
            
            return false;
        }
        $current_date = base64_encode(strtotime(now));
        $link = "https://campaign-in-one.net/account/$this->account_id/user/$this->user_id/resetpassword/".$current_date;

        return $link;
    }

    
    private function getActivationLink(){
        
        if(empty($this->user_id) && empty($this->account_id)){
            return false;
        }
        $current_date = base64_encode(strtotime(now));
        $link = "https://campaign-in-one.net/account/$this->account_id/user/$this->user_id/activateuser/".$current_date ;
        return $link;
    }
    
    
    
    public function getResetPasswordParms(){
        
        $this->reset_link = $this->getResetLink();
        $user = $this->getUserName();
        return [
            "subject" => "Landing Page tool Password reset Link",
            "message" => "Hello $user"
            . "<br>"
            . "<br>"            
            . "Please follow the link bellow to reset your password at Landing Page tool"
            . "<br>"
            . "<br>"  
            . "<a href='$this->reset_link'>Link to reset password</a>"
            . "<br>"
            . "<br>"
            . "<br>"            
        ];
    }
    
    
    public function sendMailLoginConformation(){
        
        
    }
    
    
    private function getUserUpdateParams(){
        
        $user = $this->getUserName();
        return [
            "subject" => "User Data Updated",
            "message" => "Hello $user"
            . "<br>"
            . "<br>"            
            . "Your information at Campaign-In-One is updated,"
            . "<br>"
            . "Salutation : ". $this->params['salutation']."<br>"
            . "First Name : ". $this->params['first_name']."<br>"
            . "Last Name  : ". $this->params['last_name']."<br>"
            . "Phone      : ". $this->params['phone']."<br>"
            . "Mobile     : ". $this->params['mobile']."<br>"
            . "Email      : ". $this->params['email']."<br>"
            . "<br>"
            . "If the update is not done by you, please contact us immediately on our <a href='$this->helpdesk'>Helpdesk</a> or by an <a href='mailto:$this->support_email'>email</a> . "
            . "<br>"
            . "<br>"
            . "<br>"
            . "Regards,"
            . "<br>"            
            . "Support Team"
        ];
    }

    
    private function getUserRegistrationParams() {
        
        $user = $this->getUserName();
        return [
            "subject" => "Campaing-In-One User Registration",
            "message" => "Hello $user"
            . "<br>"
            . "<br>"            
            . "This is a message from Landing page tool that this email address has been succesfull registered,"
            . "<br>"
            . "<br>"
            . "<br>"
            . "Regards,"
            . "<br>"            
            . "Support Team"
        ];
    }
    
    
    private function getAccountCreateParms(){
        
        $this->activation_link = $this->getActivationLink();
        $user = $this->getUserName();
        return [
            "subject" => "Campaign In One Account Activation Link",
            "message" => "Hello $user"
            . "<br>"
            . "<br>"            
            . "Please click the link below to activate your account"
            . "<br>"
            . "<br>"  
            . "<a href='$this->activation_link'>Activate Campaign-In-One</a>"
            . "<br>"
            . "<br>"
            . "<br>"            
        ];    
    }
    
    
    private function getAccountUpdateParams(){
        
        $user = $this->getUserName();
        return [
            "subject" => "Company Data Updated",
            "message" => "Hello $user"
            . "<br>"
            . "<br>"            
            . "The information about your Company is update,"
            . "<br>"
            . "If you have any issues about the update, please contact us immediately on our <a href='$this->helpdesk'>Helpdesk</a> or by an <a href='mailto:$this->support_email'>email</a> . "
            . "<br>"
            . "<br>"
            . "<br>"
            . "Regards,"
            . "<br>"            
            . "Support Team"
        ];        
    }
    
    
    private function getEmail(){
        
        return $this->email;
    }
    
    
    private function getHeaders(){
        
        return [
           'MIME-Version: 1.0',
           'Content-type: text/html; charset=iso-8859-1',
           'From:   campaign@treaction.net'
        ];
    }
    
    
    public function sendMailAccountCreate(){
        
        if(empty($this->email)){
            return false;
        }      
        $this->to = $this->getEmail();
        $this->subject = $this->getAccountCreateParms()['subject'];
        $this->msg = $this->getAccountCreateParms()['message'];
        $this->headers = implode("\r\n", $this->getHeaders());
        $result = mail(
                $this->to, 
                $this->subject, 
                $this->msg, 
                $this->headers);  
        
        return $result;
    }
    
    
    public function sendMailAccountUpdate(){
        
        if(empty($this->email)){
            return false;
        }      

        $this->to = $this->getEmail();
        $this->subject = $this->getAccountUpdateParams()['subject'];
        $this->msg = $this->getAccountUpdateParams()['message'];
        $this->headers = implode("\r\n", $this->getHeaders());
        $result = mail(
                $this->to, 
                $this->subject, 
                $this->msg, 
                $this->headers);  
        
        return true;
    }
    
    
    public function sendMailResetPassword(){

        if(empty($this->email)){
            return false;
        }
        
        $this->to = $this->getEmail();
        $this->subject = $this->getResetPasswordParms()['subject'];
        $this->msg = $this->getResetPasswordParms()['message'];
        $this->headers = implode("\r\n", $this->getHeaders());
        $result = mail(
                $this->to, 
                $this->subject, 
                $this->msg, 
                $this->headers);  
        
        return true;
    }
    
    
    public function sendMailUserRegistration(){
        
        if(empty($this->email)){
            
            return false;
        }
        $this->to = $this->getEmail();
        $this->subject = $this->getUserRegistrationParams()['subject'];
        $this->msg = $this->getUserRegistrationParams()['message'];
        $this->headers = implode("\r\n", $this->getHeaders());   
        $result = mail(
                $this->to, 
                $this->subject, 
                $this->msg, 
                $this->headers);  
        
        return true;        
    }
    
    
    public function sendMailUserUpdate(){

        if(empty($this->email)){
            return false;
        }        
        
        $this->to = $this->getEmail();
        $this->headers = implode("\r\n", $this->getHeaders());
        $this->subject = $this->getUserUpdateParams()['subject'];
        $this->msg = $this->getUserUpdateParams()['message'];        
        $result = mail(
                $this->to, 
                $this->subject, 
                $this->msg, 
                $this->headers);  
        
        return $result;        
    }        
}
