<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Middleware
 *
 * @author Pradeep
 */

namespace App\Middleware;

class Middleware {
    //put your code here
    protected $container;
    protected $server;
    
    public function __construct($container){
        
        $this->container = $container;
    }
    
    public function __invoke($request, $response, $next) {

        if($_SESSION['error']){
            
            $this->container->view->getEnvironment()->addGlobal('error', $_SESSION['error']);
            unset($_SESSION['error']);          
        }   
        
        if($_SESSION['success']){
            
            $this->container->view->getEnvironment()->addGlobal('success', $_SESSION['success']);
            unset($_SESSION['success']);          
        }          
        
        $response = $next($request, $response);
        return $response;
    }
}
