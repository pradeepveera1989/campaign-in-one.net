<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Middleware;

/**
 * Description of Authorization
 *
 * @author Pradeep
 */
class Authorization extends Middleware{
    
    public function __invoke($request, $response, $next) {

        $response = $next($request, $response);
        
        return $response;        
    }
}
