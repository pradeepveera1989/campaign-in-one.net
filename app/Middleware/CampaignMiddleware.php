<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Middleware;

/**
 * Description of CampaignMiddleware
 *
 * @author Pradeep
 */
class CampaignMiddleware extends Middleware {
    
    public function __invoke($request, $response, $next) {

        if($_SESSION['campaign']){

            $this->container->view->getEnvironment()->addGlobal('campaign', $_SESSION['campaign']);
            unset($_SESSION['campaign']);          
        }          
        
        $response = $next($request, $response);
        return $response;
    }    
}