<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Middleware;

/**
 * Description of AccountMiddleware
 *
 * @author Pradeep
 */
class AccountMiddleware extends Middleware {

    public function __invoke($request, $response, $next) {
 
        // Convert Session to Global variable.
        if($_SESSION['error_chg_pass']){

            $this->container->view->getEnvironment()->addGlobal('error_chg_pass', $_SESSION['error_chg_pass']);
            unset($_SESSION['error_chg_pass']);
        }     

        $response = $next($request, $response);
        return $response;
    }
}