<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Middleware;

/**
 * Description of AuthMiddleware
 *
 * @author Pradeep
 */
class AuthMiddleware extends Middleware
{
    
//    function __construct($session, $access)
//    {
//        $this->session = $session;
//        $this->access = $access;
//    }
        
    public function __invoke($request, $response, $next)
    {
        if ($_SESSION['signin']) {
            $this->container->view->getEnvironment()->addGlobal('signin', $_SESSION['signin']);
            unset($_SESSION['signin']);
        }

        // Trigger only for Super Admin
        // Check if the Super admin changes the Account and accordingly update the Session Id
        if ($_SESSION['super_admin'] &&
            !empty($request->getAttribute('routeInfo')[2]['acc_id']) &&
            $request->getAttribute('routeInfo')[2]['acc_id'] != $_SESSION['acc_id']) {
            $_SESSION['acc_id'] = $request->getAttribute('routeInfo')[2]['acc_id'];
        }
                      
        $response = $next($request, $response);
        return $response;
    }
}
