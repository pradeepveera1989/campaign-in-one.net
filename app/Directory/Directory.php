<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Directory
 *
 * @author Pradeep
 */

namespace App\Directory;
use Slim\Http\UploadedFile;

class Directory {
    public $src;   # Source File Path
    public $des;   # Destination File Path 
    

    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $directory directory to which the file is moved
     * @param UploadedFile $uploaded file uploaded file to move
     * @return string filename of moved file
     */
    public function moveUploadedFile($directory, UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }      
   
    /**
     * Recursively copy files from one directory to another
     * 
     * @param String $src - Source of files being moved
     * @param String $dest - Destination of files being moved
     * 
     */
    public function rcopy($src, $des){

        if(empty($src) || empty($des)){
            
            return false;
        }
               
        // If source is not a directory stop processing
        if(!is_dir($src)) return false;

        // If the destination directory does not exist create it
        if(!is_dir($des)) { 
            if(!mkdir($des, 0777, true)) {              
                // If the destination directory could not be created stop processing
                return false;
            }    
        }
      
        $dir = new \DirectoryIterator($src);
                
        foreach($dir as $f) {
            // Check for the file
            if($f->isFile()) {

                copy($f->getRealPath(), "$des/" . $f->getFilename());
              // Check for Directory  
            } else if(!$f->isDot() && $f->isDir()) {
               
                $this->rcopy($f->getRealPath(), "$des/$f");
            }
        }
        
        return true;
    }


    /**
     * fileEdit function
     *
     * @param string $file_path
     * @param string $file_name
     * @param string $search_text
     * @param string $replace_text
     * @return boolean
     */
    public function fileEdit(string $file_path, string $file_name, string $search_text, string $replace_text) : bool {

        $status = false;
        if(empty($file_path) || empty($file_name) || empty($search_text) || empty($replace_text)) {
            die("fileEdit");
            return $status;
        }

        $lines = file($file_path.'/'.$file_name);
        foreach ($lines as &$line){
            // Modify file.
            if((trim($line)== trim($search_text))){
                $line = $replace_text;
            }            
        }
        var_dump($lines);
        # Opens file in write mode        
        $fp = fopen($file_path.'/'.$file_name, 'w');
     
        # Writes each and every line to the file using handler
        foreach ($lines as $line){
            $file_write = fwrite($fp, $line);
            var_dump($file_write);
        }    
        # Closes the file handler
        fclose("handler", $fp);

        if(\is_numeric($file_write)){
            $status = true;
        }
        
        return $status;

    }
    
}


