<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of oauth2
 *
 * @author Pradeep
 */
namespace App\Oauth2;
require __DIR__ . '/../../vendor/autoload.php';
use \Curl\Curl;
use \Datetime;
use \DateTimeZone;
use \DateInterval;
use OAuth2\GrantType;

class oauth2 {
    
    public $curl;
    public $model;
    public $db;
    
    public function __construct($db) {
        
        $this->curl = new Curl();
        $this->db = $db;
        if(!empty($this->db)){
            $this->model = new \App\Models\Model($this->db);
        }
    }
    
    public function getAccessToken($params){

        if(empty($this->curl) || empty($params)) {
            
            return false;
        }
        
        if(empty($params['id'])){
            $params['id'] = $this->model->getUserDetailsByEmail($params['email'])['id'];
        }    
        $user_access_token = $this->getAccessTokenDetialsById($params['id']);

        if(!empty($user_access_token) && $this->checkExpireTime($user_access_token)){
            // return the token detials
            return $user_access_token;
        }

        $response = $this->getNewAccessToken($params);
        if(empty($response)){
            
            return false;
        }
        
        return $response;
    }
    
    private function getNewAccessToken($params){
        
        if(empty($params)){
            
            return false;
        }
        
        $url = "https://campaign-in-one.net/token";
        $clientdata = $this->getClientDetails($params);
        $this->curl->post($url, $clientdata);
        if($this->curl->error){
            
            return $this->curl->errorMessage ;  
        }
        $oauth_params = [
            'access_token' => $this->curl->response->access_token,
            'expires_in' => $this->getExpireTime($this->curl->response->expires_in),
            'user_id' => $params['id']
        ];
        
        $user_access_token = $this->getAccessTokenDetialsById($oauth_params['user_id']);

        if(empty($user_access_token)){
            $this->saveToken($oauth_params);
        }else{
            $this->updateToken($oauth_params);
        }      
           
        return $this->curl->response;
    }
    
    public function refreshToken($params){
        
        if(empty($params)){
            
            return false;
        }
        
        return true;
    }
    
    private function updateToken($params){
        
        if(empty($params) || !array_key_exists("access_token",$params) || !array_key_exists("user_id",$params)){
            return false;
        }
        
        $qry = "UPDATE `cio.oauth2_tokens` SET `access_tokens`= '".$params['access_token']."',`expire_time`= '".$params['expires_in']."' WHERE `user_id`='".$params['user_id']."'";
        
        $result = $this->model->excuteQuerySET($qry);
        if(!is_bool($result) || !result){
            
            return false;
        }
        
        return $result;      
    }
    
    /*
     *
     * @returns: 
     *          True  : User token is not Expired
     *          False : User token is expired
     */
    
    private function checkExpireTime($params){
        
        if(empty($params)){
            
            return false;
        }

        $date = new DateTime("now");
        $current_time = $date->format('Y-m-d H:i:s');
        $expire_time = $params['expire_time'];

        if((strtotime($current_time) - strtotime($expire_time)) >= 3600){
            // If the time stamp is more than 1hr it is expired.
            return false;
        }
        
        return true;
    }
    
    public function getAccessTokenDetialsById($user_id){
        
        if(empty($user_id)){
            
            return false;
        }

        $qry = "SELECT * FROM `cio.oauth2_tokens` WHERE `user_id` = $user_id";
        $result = $this->model->excuteQueryGET($qry)[0];

        if(is_null($result) && !isset($result['access_tokens'])){
            // User id does not exists 
            return [];
        }
        
        return $result;
    }
    
    
    private function getExpireTime($time){
       
        if(empty($time)){
            
            return false;
        }
        $date = new DateTime("now");
        $date->add(new DateInterval('PT'.$time.'S'));
        return $date->format('Y-m-d H:i:s');
      
    }
    
    public function validateToken($params){
        
    }
    
    private function saveToken($params){
        
        if(empty($params) || empty($params['access_token']) || empty($params['user_id'])){
            
            return false;
        }
        
        $data = [
            'user_id' => $params['user_id'],
            'access_tokens' => $params['access_token'],
            'expire_time' => $params['expires_in']
        ];

        $insert = "INSERT INTO `cio.oauth2_tokens`(`user_id`, `access_tokens`, `expire_time` ) VALUES (:user_id, :access_tokens, :expire_time)";
        $result = $this->model->excuteQuerySET($insert, $data);
        if(!is_bool($result) || !result){
            
            return false;
        }
        return $result;
    }
    
    public function checkUserOauthToken($user_id){
        
        if(empty($this->model)){
            return false;
        }
        $qry = "SELECT * FROM `cio.oauth2_tokens` WHERE `user_id` = ";
        $qry .= $user_id;
        
        $result = $this->model->excuteQueryGET($qry);
        if(empty($result[0]["id"]) || !$this->checkExpireTime($result[0])){
            return false;
        }
        
        return true;
    }
    
    public function isTokenExpired($params){
        
        if(empty($params)){
            
            return false;
        }
        
        
        
        return true;
    } 
    
    private function getClientDetails($params) {

        return[
            'client_id' => 'administrator',
            'client_secret' => 'password',
            'grant_type' => 'client_credentials'
        ];
    }

}
