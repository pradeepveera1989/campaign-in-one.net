<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of CampaignController
 *
 * @author Pradeep
 */
class CampaignController extends Controller {
    

    /**
     * getCampaigns
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Generate all the campaigns in array 
     *                    send it to the campaign.twig template
     *        
     */         
    public function getCampaigns($request, $response){

        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);     
        $params = $auth->getAuthIds();
        if((empty($params))){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }      
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);   

        if(empty($oauth_params)){
            
            $_SESSION['message'] = "Could not verify your account"; 
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }        
        
        // Get the account id
        $params['acc_id'] = $request->getAttribute('acc_id');
        if(empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
       
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->getCampaignsByAccountId($params['acc_id']);
               
        return $this->container->view->render($response, '/campaign/campaigns.twig', array('campaigns' => $camp_details));
    }
    
    
    /**
     * createCampaign
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Get the account Id from session and redirect to 
     *        'camp.createById' template
     *        
     */     
    public function createCampaign($request, $response){
        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);     
        $params = $auth->getAuthIds();
        if((empty($params)) && empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }      
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);   

        if(empty($oauth_params)){
            
            $_SESSION['message'] = "Could not verify your account"; 
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }        
        
        return $response->withRedirect($this->container->router->pathFor('camp.createById', $params));   
    }
    
    /**
     * createCampaignById
     *
     * @param $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return $response 
     *         auth.signin : if could not find the accound id
     *         createcampaign.twig : if redirect is 
     */     
    public function getcreateCampaignById($request, $response){
        
        $params['acc_id'] = $request->getAttribute('acc_id'); 
        
        if(empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }                   
        
        return $this->container->view->render($response, 'createcampaign.twig');   
    }
    
    
   /**
     * createCampaignById
     *
     * @param $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return $response 
     *         auth.signin : if could not find the accound id
     *         createcampaign.twig : if redirect is 
     */     
    public function postcreateCampaignById($request, $response){
        
        $params = $request->getParams();
        
        if(empty($params) && empty($params['url'])){
            
            return $response->withRedirect($this->container->router->pathFor('camp.create'));
        }
        
        $auth = new \App\Models\Authenticate($this->container->dbh);

        $acc_number = $auth->getAuthIds()['acc_id'];
        
        if(empty($acc_number)){
            
            return $response->withRedirect($this->container->router->pathFor('camp.create'));
        }
        // Update company params
        $model = new \App\Models\Model($this->container->dbh);
        $params['company_id'] = $model->getCompanyIdByAccountNumber($acc_number);
        
        $company = new \App\Models\Company($this->container->dbh);
        $params['account'] = $company->getCompanyDetailsById($params['company_id'])['name'];       

        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->createCampaign($params);   
        var_dump($camp_details);
        //Error in creating a new campaign
        if(is_bool($camp_details) && $camp_details == False){

            #return $response->withRedirect($this->container->router->pathFor('camp.create'));
            #return $this->container->view->render($response, 'createcampaign.twig');
            return $response->withRedirect($this->container->router->pathFor('camp.create')); 
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.draft')); 
    }
    
    
    /**
     * getCampaignsByDraft
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Get the account Id from session and redirect to 
     *        'camp.activeById' template
     */      
    public function getCampaignsByStatusActive($request, $response){

        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);     
        $params = $auth->getAuthIds();
        if((empty($params)) && empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }      
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);   

        if(empty($oauth_params)){
            
            $_SESSION['message'] = "Could not verify your account"; 
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }         
        
        return $response->withRedirect($this->container->router->pathFor('camp.activeById', $params));        
    }
    
    
    /**
     * getCampaignById
     *
     * @param $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return $response 
     *        $response : 
     *        True : if the Campaign entry deleted successfull.
     */      
    public function getCampaignById($request, $response){
        
    }
    
    public function getActiveCampaignsById($request, $response){
        
        $params['acc_id'] = $request->getAttribute('acc_id'); 
        
        if(empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }        

        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->getActiveCampaignsByAccountId($params['acc_id']);
        
        $camp_details = $camp->appendLinksForCampaigns($camp_details);
               
        return $this->container->view->render($response, '/campaign/campaigns.twig', array('campaigns' => $camp_details));        
        
    }


    /**
     * getCampaignsByDraft
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Get the account Id from session and redirect to 
     *        'camp.draftById' template
     */     
       
    public function getCampaignsByStatusDraft($request, $response){

        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);     
        $params = $auth->getAuthIds();
        if((empty($params)) && empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }      
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);   

        if(empty($oauth_params)){
            
            $_SESSION['message'] = "Could not verify your account"; 
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }         
        
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));
        
    }
 
    
   /**
     * getCampaignsByDraft
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Generate all the draft campaigns in array 
     *                    send it to the campaign.twig template
     *        
     */  
    
    public function getDraftCampaignsById($request, $response){
        
        $params['acc_id'] = $request->getAttribute('acc_id'); 
        
        if(empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
            
        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->getDraftCampaignsByAccountId($params['acc_id']); 
   
        $camp_details = $camp->appendLinksForCampaigns($camp_details);       

        return $this->container->view->render($response, '/campaign/draftcampaigns.twig', array('campaigns' => $camp_details));          
        
    }
    
    
    /**
     * getCampaignsByArchive
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Get the account Id from session and redirect to 
     *        'camp.draftById' template
     */         
    
    public function getCampaignsByStatusArchive($request, $response){
        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);     
        $params = $auth->getAuthIds();
        if((empty($params)) && empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }      
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);   

        if(empty($oauth_params)){
            
            $_SESSION['message'] = "Could not verify your account"; 
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }             
        
        return $response->withRedirect($this->container->router->pathFor('camp.archiveById', $params));
        
    }
    
    
    /**
     * getCampaignsArchiveById
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Collect all the archived campaigns and send it to 
     *        'camp.archiveById' template.
     */       
    public function getArchiveCampaignsById($request, $response){
        
        $params['acc_id'] = $request->getAttribute('acc_id'); 
        
        if(empty($params['acc_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
            
        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->getArchiveCampaignsByAccountId($params['acc_id']);
        
        $camp_details = $camp->appendLinksForCampaigns($camp_details);     
                       
        return $this->container->view->render($response, '/campaign/archivecampaigns.twig', array('campaigns' => $camp_details));          
    }
    
    
    /**
     * getCampaignActivateById
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Activate the campaign by campaign id and redirect to
     *          'camp.activeById' template
     *  
     */      
    public function getCampaignActivateById($request, $response){
               
        $params['acc_id'] = $request->getAttribute('acc_id');
        
        $params['camp_id'] = $request->getAttribute('camp_id');
        
        if(empty($params['acc_id']) || empty($params['camp_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->activateCampaignById($params);
        
        return $response->withRedirect($this->container->router->pathFor('camp.activeById', $params));           
    }
    
    
    /**
     * getCampaignArchivedById
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Archive the campaign by campaign id and redirect to
     *          'camp.archiveById' template
     */      
    
    public function getCampaignArchivedById($request, $response){
        
        $params['acc_id'] = $request->getAttribute('acc_id');
        
        $params['camp_id'] = $request->getAttribute('camp_id');
        
        if(empty($params['acc_id']) || empty($params['camp_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->archiveCampaignById($params);
        
        return $response->withRedirect($this->container->router->pathFor('camp.archiveById', $params));           
    }    
    
    
    /**
     * getCampaignDeleteById
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Archive the campaign by campaign id and redirect to
     *          'camp.archiveById' template
     */      
    
    public function getCampaignDeleteById($request, $response){
        
        $params['acc_id'] = $request->getAttribute('acc_id');
        
        $params['camp_id'] = $request->getAttribute('camp_id');
        
        if(empty($params['acc_id']) || empty($params['camp_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->deleteCampaignById($params);
        
        return $response->withRedirect($this->container->router->pathFor('camp.archiveById', $params));           
    }      


    /**
     * getCampaignReactivateById
     *
     * @param 
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     * 
     * @return  
     *        $response : Reactivate the campaign by campaign id and redirect to
     *          'camp.draft' template
     */      
        
    public function getCampaignReactivateById($request, $response){

        $params['acc_id'] = $request->getAttribute('acc_id');
        
        $params['camp_id'] = $request->getAttribute('camp_id');      
        
        if(empty($params['acc_id']) || empty($params['camp_id'])){
            
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->reactivateCampaignById($params);
        
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));                   
    }
}
