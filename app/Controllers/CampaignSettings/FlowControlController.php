<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\CampaignSettings;

use Slim\Http\UploadedFile;

/**
 * Description of SettingsController
 *
 * @author Pradeep Veera
 */

class FlowControlController extends CampaignSettingsController
{

    /**
     * getGeneralSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getBasicFlowControlById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests;
        $httpMethod = "GET";
        $name = "Basic Control";
        $group_name = "Flow Control";
        $values;
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $flow_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/flow_control/flow_control.twig',
            array('c_set' => $flow_settings)
        );
    }


    /**
     * postGeneralSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postBasicFlowControlById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Flow Control";
        $group_name = "Flow Control";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $flow_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/flow_control/flow_control.twig',
            array('c_set' => $flow_settings)
        );
    }

    public function getSplitTest($request, $response)
    {
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        return $this->container->view->render(
            $response,
            '/campaign_settings/flow_control/split_test.twig',
            array('c_set' => $requests)
        );
    }
}
