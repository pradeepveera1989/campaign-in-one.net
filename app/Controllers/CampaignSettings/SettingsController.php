<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\CampaignSettings;

use Slim\Http\UploadedFile;

/**
 * Description of SettingsController
 *
 * @author Aravind
 */

class SettingsController extends CampaignSettingsController
{

    /**
     * getGeneralSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getGeneralSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests;
        $httpMethod = "GET";
        $name = "General";
        $group_name = "Settings";
        $values;
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/general.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postGeneralSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postGeneralSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "General";
        $group_name = "Settings";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/general.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * getDataflowSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getDataflowSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests;
        $httpMethod = "GET";
        $name = "Data_Flow";
        $group_name = "Settings";
        $values;
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/dataflow.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postDataflowSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postDataflowSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Data_Flow";
        $group_name = "Settings";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/dataflow.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * getFraudSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getFraudSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests;
        $httpMethod = "GET";
        $name = "Fraud_Settings";
        $group_name = "Settings";
        $values;
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/fraud.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postFraudSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postFraudSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests;
        $httpMethod = "POST";
        $name = "Fraud_Settings";
        $group_name = "Settings";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
    
        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/fraud.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * getmessageSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getmessageSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        //requests is to send data to get info function
        $requests;
        $httpMethod = "GET";
        $name = "Messages";
        $group_name = "Settings";
        $values;
        $requests[camp_id] = $request->getAttribute('camp_id');
        $requests[acc_id ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/messages.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postmessageSettingsByid function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postmessageSettingsByid($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests;
        $httpMethod = "POST";
        $name = "Messages";
        $group_name = "Settings";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/settings/messages.twig',
            array('c_set' => $info_settings)
        );
    }


    // /**
    // * getInfo
    // *
    // * @param [type] $httpMethod = GET /POST
    // * @param [type] $values = goes in json
    // * @param [type] $groupName = type of group
    // * @param [type] $responses = campaign Info
    // * @return $info_settings = variable need to send to twig files
    // */
    // public function getInfo($httpMethod, $requests, $name, $values)
    // {
    //     if ($httpMethod == "POST") {
    //         //object of the Model file
    //         $in_Obj = new \App\Models\CampaignParams($this->container->dbh, $name);
    //         //making params to send to model
    //         $params = $this->createParams($requests, $name, $values);
    //         //sending $params to model
    //         $result = $in_Obj->insertOrUpdateSettings($params);
    //         //receving
    //         $info_settings = $this->getValues($requests, $name);

    //         if (!is_array($info_settings) || empty($info_settings) || !is_bool($result) || $result == false) {
    //             $result['error_integration'] = "Something went wrong, Failed to fetch Information";
    //         }
    //         $result['notify_integration'] = "General Settings details have been saved successfully";

    //         return $info_settings;
    //     } elseif ($httpMethod == "GET") {
    //         //getting information from models
    //         $info_settings = $this->getValues($requests, $name);
    //         return $info_settings;
    //     }
    // }

    // /**
    // * getValues
    // *
    // * @param [type] $requests = getting values from URL
    // * @param [type] $name = name of the setting
    // * @return $info_settings = variable need to send to twig files
    // */

    // public function getValues(array $requests, string $name) :array
    // {
    //     $in_Obj = new \App\Models\CampaignParams($this->container->dbh, $name);
    //     $camp_id = $requests['camp_id'];
    //     $acc_id = $requests['acc_id'];
    //     $getValue = $in_Obj->getSettingDetails($camp_id);

    //     //taking only useful information
    //     $getValues = $getValue[0];
    //     $id = $getValues[id];
    //     //string to object
    //     $info_setting = json_decode($getValues[value]);
    //     //typecasting to array
    //     $info_settings = (array)$info_setting;
    //     $info_settings['camp_id'] = $camp_id;
    //     $info_settings['acc_id'] = $acc_id;
    //     $info_settings['id'] = $id;

    //     return $info_settings;
    // }

    // public function createParams($requests, $name, $values)
    // {
    //     $value = json_encode($values, JSON_UNESCAPED_UNICODE);
    //     //gettings campaign and account details
    //     $camp_id = $requests['camp_id'];
    //     //Giving update value
    //     $date = date('Y-m-d h:i:s', time());
    //     $params['updated'] = $date;
    //     //Making params to send to databse
    //     //only this change according to requirments
    //     $params['id'] = $values[id];
    //     $params['camp_id'] = $requests['camp_id'];
    //     $params['acc_id'] = $requests['acc_id'];
    //     $params['group_name'] = "Settings" ;
    //     $params['name'] = $name ;
    //     $params['type'] = "JSON";
    //     $params['value'] = $value;

    //     return $params;
    // }
}
