<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers\CampaignSettings;

use Slim\Http\UploadedFile;

/**
 * Description of TrackingController
 *
 * @author Aravind
 */

class TrackingController extends CampaignSettingsController
{

    /**
     * getFacebookById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getFacebookById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "GET";
        $name = "Facebook";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/facebook.twig',
            array('c_set' => $info_settings)
        );
    }

    /**
     * postFacebookById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postFacebookById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Facebook";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/facebook.twig',
            array('c_set' => $info_settings)
        );
    }

    /**
     * getGoogleById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getGoogleById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "GET";
        $name = "Google";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/google.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postGoogleById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postGoogleById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Google";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/google.twig',
            array('c_set' => $info_settings)
        );
    }

    /**
     * getAdebloById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getAdebloById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "GET";
        $name = "Adeblo";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/adeblo.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postAdebloById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postAdebloById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Adeblo";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/adeblo.twig',
            array('c_set' => $info_settings)
        );
    }

    /**
     * getWebgainsById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getWebgainsById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "GET";
        $name = "Webgains";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/webgains.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postWebgainsById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postWebgainsById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Webgains";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
    
        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/webgains.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * getOutbrainById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getOutbrainById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "GET";
        $name = "Outbrain";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);

        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/outbrain.twig',
            array('c_set' => $info_settings)
        );
    }


    /**
     * postOutbrainById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postOutbrainById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests;
        $httpMethod = "POST";
        $name = "Outbrain";
        $group_name = "Tracking";
        $requests['camp_id'] = $request->getAttribute('camp_id');
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_settings = $this->getInfo($httpMethod, $requests, $name, $group_name, $values);
        
        return $this->container->view->render(
            $response,
            '/campaign_settings/tracking/outbrain.twig',
            array('c_set' => $info_settings)
        );
    }


    // /**
    // * getInfo
    // *
    // * @param [type] $httpMethod = GET /POST
    // * @param [type] $values = goes in json
    // * @param [type] $groupName = type of group
    // * @param [type] $responses = campaign Info
    // * @return $info_settings = variable need to send to twig files
    // */
    // public function getInfo(string $httpMethod, array $requests, string $name, array $values = null)
    // {
    //     $track_settings = [];
    //     if (empty($httpMethod)||empty($name)||empty($requests)) {
    //         return $track_settings;
    //     }

    //     if ($httpMethod == "POST") {
    //         //object of the Model file
    //         $in_Obj = new \App\Models\CampaignParams($this->container->dbh, $name);
    //         //making params to send to model
    //         $params = $this->createParams($requests, $name, $values);
    //         //sending $params to model
    //         $result = $in_Obj->insertOrUpdateSettings($params);
    //         //receving
    //         $track_settings = $this->getValuesFromDB($requests, $name);

    //         if (!is_array($track_settings) || empty($track_settings) || !is_bool($result) || $result == false) {
    //             $track_settings['error'] = "Something went wrong, Failed to fetch Information";
    //         }
    //         $track_settings['success'] = "General Settings details have been saved successfully";
    //     } elseif ($httpMethod == "GET") {
    //         //getting information from models
    //         $track_settings = $this->getValuesFromDB($requests, $name);
    //     }
    //     return $track_settings;
    // }


    // /**
    //  * getValuesFromDB function
    //  *
    //  * @param array $requests
    //  * @param string $name
    //  * @return array
    //  */
    // public function getValuesFromDB(array $requests, string $name) : array
    // {
    //     $track_values = [];

    //     if (empty($name) || (int)$requests['camp_id'] <= 0 || (int)$requests['acc_id'] <= 0) {
    //         return $track_values;
    //     }

    //     $camp_id = $requests['camp_id'];
    //     $acc_id = $requests['acc_id'];
    //     $camp_Obj = new \App\Models\CampaignParams($this->container->dbh, $name);
    //     $s_details = $camp_Obj->getSettingDetails($camp_id)[0];

    //     $id = $s_details['id'];
    //     //string to object
    //     $track_values = json_decode($s_details['value']);
    //     //typecasting to array
    //     $track_values = (array)$track_values;
    //     $track_values['camp_id'] = $camp_id;
    //     $track_values['acc_id'] = $acc_id;
    //     $track_values['id'] = $id;

    //     return $track_values;
    // }

    // /**
    //  * createParams function
    //  *
    //  * @param array $requests
    //  * @param string $name
    //  * @param array $values
    //  * @return array
    //  */
    // public function createParams(array $requests, string $name, array $values) : array
    // {
    //     $params = [];
    //     $date = date('Y-m-d h:i:s', time());
    //     if (empty($values) || empty($name) || (int)$requests['camp_id'] <= 0 || (int)$requests['acc_id'] <= 0) {
    //         return $params;
    //     }

    //     $value = json_encode($values, JSON_UNESCAPED_UNICODE);
    //     //gettings campaign and account details
    //     $camp_id = $requests['camp_id'];
    //     $params['updated'] = $date;
    //     //Making params to send to databse
    //     //only this change according to requirments
    //     $params['id'] = $values['id'];
    //     $params['camp_id'] = $requests['camp_id'];
    //     $params['acc_id'] = $requests['acc_id'];
    //     $params['group_name'] = "Tracking" ;
    //     $params['name'] = $name ;
    //     $params['type'] = "JSON";
    //     $params['value'] = $value;

    //     return $params;
    // }
}
