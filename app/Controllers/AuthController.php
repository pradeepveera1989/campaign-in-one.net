<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use Slim\Views\Twig as View;
use Chadicus\Slim\OAuth2\Middleware;
use Respect\Validation\Validator as v;

/**
 * Description of AuthController
 *
 * @author Pradeep
 */
class AuthController extends Controller
{
    //put your code here
    public $auth;
    
    /**
     * getSignIn function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getSignIn($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        // Check User session
        if ($auth->check()) {
            return $response->withRedirect($this->container->router->pathFor('auth.home'));
        }
        return $this->container->view->render($response, 'login/signin.twig');
    }
    
    /**
     * postSignIn function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postSignIn($request, $response)
    {
        $params = $request->getParams();
        if (!is_array($params) || empty($params['email']) || empty($params['password'])) {
            $_SESSION['error']  = "Email address and/or password do not match";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        #Initiate object for secure log
        $this->securelog = new \App\Models\Securelog($this->container->dbh);
     
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);

        if (empty($oauth_params)) {
            $_SESSION['error'] = "Session Expired, Please login to continue";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        // User Details Validation
        $this->auth = new \App\Models\Authenticate($this->container->dbh);
        $result = $this->auth->validateUserSignIn($params);
        if (!$result) {
            #Secure log for failed attempt
            $this->securelog->createLogEntry($params, 'failure');
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        } else {
            $this->user_details = $this->auth->getUserDetailsByEmail($params['email']);
            $comp_details = $this->auth->getCompanyDetailsByAccountId($this->user_details['account_no']);
            $detials = [
               'user_id'        => $this->user_details['id'],
               'acc_id'         => $this->user_details['account_no'],
               'acc_name'       => $comp_details['name'],
               'super_admin'    => $this->user_details['super_admin']
            ];

            # Secure log for success login attempt
            $this->securelog->createLogEntry($params, 'success');
            #return $response->withRedirect($this->container->router->pathFor('camp.draft', $detials));
            return $response->withRedirect($this->container->router->pathFor('auth.home', $detials));
        }
    }
    
    /**
     * getUserResetPassword function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUserResetPassword($request, $response)
    {
        $this->auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $this->auth->getAuthIds();
        if ((empty($params))) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $params['expire_time'] = base64_encode(date("Y-m-d h:i:s"));
        return $response->withRedirect($this->container->router->pathFor('auth.resetpasswordbyId', $params));
    }
    
    /**
     * getSignOut function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getSignOut($request, $response)
    {
        $this->auth = new \App\Models\Authenticate($this->container->dbh);
        $this->auth->signOut();
        $message = [ "success" => "User has been successfully logged out of the system"];
        
        return $response->withRedirect($this->container->router->pathFor('auth.signin'));
    }
    
    /**
     * getForgotPassword function
     * Loads forgotpassword.twig template
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getForgotPassword($request, $response)
    {
        return $this->container->view->render($response, 'login/forgotpassword.twig');
    }
    
    /**
     * postForgotPassword function
     * - Post method for forgotpassword.twig template.
     * - validates the Email and send the reset link to Email address.
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postForgotPassword($request, $response)
    {
        $fgemail = $request->getParams();
        $auth_Obj = new \App\Models\Authenticate($this->container->dbh);
        $status = $auth_Obj->validateUserEmail($fgemail['email']);

        if (!$status) {
            $msg['failure'] = "Email address not found";
            #return $response->withRedirect($this->container->router->pathFor('auth.forgotpassword'));
            return $this->container->view->render($response, 'login/forgotpassword.twig', array('fgpwd' => $msg));
        }
        $this->user_details = $auth_Obj->getUserDetailsByEmail($fgemail['email']);
        $mail = new \App\Mailer\mail($this->user_details);
        $status = $mail->sendMailResetPassword();

        if (!$status) {
            $msg['failure'] = "Failed to send reset link";
            return $this->container->view->render($response, 'login/forgotpassword.twig', array('fgpwd' => $msg));
        }
        $msg['success'] = "Please check your inbox for reset link";
        return $this->container->view->render($response, 'login/forgotpassword.twig', array('fgpwd' => $msg));
    }
    
    /**
     * getDashboard function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getDashboard($request, $response)
    {
        
        // Check the Authencity of the User
        $dashboard = [];
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $auth_Obj = new \App\Models\Authenticate($this->container->dbh);
        $status = $auth_Obj->updateSessionValue();
        $session_ids = $auth_Obj->getAuthIds();
        $acc_id = (int)$session_ids['acc_id'];

        // Lead Object
        $lead_Obj = new \App\Models\Leads($this->container->dbh);
        $dashboard['leads'] = $lead_Obj->getLeadsInfoForPeriod($_SESSION['comp_id'], 30);
        $dashboard['leads_count'] = $lead_Obj->getLeadCountForPeriod($_SESSION['comp_id']);
        $dashboard['leads_for_camp'] = $lead_Obj->getLeadCountForCampaign($_SESSION['comp_id']);
        // Campaign Object
        $camp_Obj =  new \App\Models\Campaign($this->container->dbh);
        $dashboard['campaigns'] = $camp_Obj->getActiveArchivedCampaignsByAccountId($acc_id, 10);
         
      
        // Leads from last 30days.
        // Campaigns from last 30 days.
        return $this->container->view->render($response, 'dashboard.twig', array("dashboard" => $dashboard));
    }
}
