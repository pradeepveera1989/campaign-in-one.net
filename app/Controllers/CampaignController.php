<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of CampaignController
 *
 * @author Pradeep
 */
class CampaignController extends Controller
{
    

    /**
     * getCampaigns
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Generate all the campaigns in array
     *                    send it to the campaign.twig template
     *
     */
    public function getCampaigns($request, $response)
    {
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        $params['acc_id'] = $request->getAttribute('acc_id');
        if (!$this->getUserAuthenticity() || !is_numeric($params['acc_id']) || (int)$params['acc_id'] <= 0) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
       
        // Get the Campaign details
        $camp = new \App\Models\Campaign\Campaign($this->container->dbh);
        $camp_details = $camp->getCampaignsByAccountId($params['acc_id']);
               
        return $this->container->view->render(
            $response,
            '/campaign/campaigns.twig',
            array('campaigns' => $camp_details)
        );
    }
    
    
    /**
     * createCampaign
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Get the account Id from session and redirect to
     *        'camp.createById' template
     *
     */
    public function createCampaign($request, $response)
    {
        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();

        if (!$this->getUserAuthenticity() ||
            (!is_array($params)) ||
            !is_numeric($params['acc_id']) ||
            ((int)$params['acc_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        return $response->withRedirect($this->container->router->pathFor('camp.createById', $params));
    }
    
    /**
     * createCampaignById
     *
     * @param $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return $response
     *         auth.signin : if could not find the accound id
     *         createcampaign.twig : if redirect is
     */
    public function getcreateCampaignById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
                     
        if (!$this->getUserAuthenticity() || (empty($params['acc_id'])) || ((int)$params['acc_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        // Get Company Name
        $model = new \App\Models\Model($this->container->dbh);
        $params['account'] = $model->getCompanyDetailsByAccountId($params['acc_id'])['name'];

        #return $response->withRedirect($this->container->router->pathFor('camp.createById', $params));
        return $this->container->view->render($response, 'campaign/createcampaign.twig', array("campaign" => $params));
    }
    
    
    /**
      * createCampaignById
      *
      * @param $request  : Requested parameters by Campaign Object
      *        $response : Response parameters given by Campaign Object
      *
      * @return $response
      *         auth.signin : if could not find the accound id
      *         createcampaign.twig : if redirect is
      */
    public function postcreateCampaignById($request, $response)
    {
        $params = $request->getParams();
        $params['acc_id'] = $request->getAttribute('acc_id');
        if (!$this->getUserAuthenticity()) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        // Get Company Name
        $model = new \App\Models\Model($this->container->dbh);
        $company_details = $model->getCompanyDetailsByAccountId($params['acc_id']);
        $params['account'] = $company_details['name'];
        $params['company_id'] = $company_details['id'];
        // Create Campaign
        $camp = new \App\Models\Campaign($this->container->dbh);
        $params['camp_url'] = $this->container->campaign_url;
        $camp_details = $camp->createCampaign($params);

        //Error in creating a new campaign
        if (is_bool($camp_details) && $camp_details == false) {
            $params['error'] = "Campaign name already exists";
            return $this->container->view->render(
                $response,
                'campaign/createcampaign.twig',
                array("campaign" => $params)
            );
        }

        // create campaign file strutre at "campaign-in-one.de"
        $result = $camp->createCampaignFolderStructure($params, $this->container->campaign_path);

        if (is_bool($result) && $result == false) {
            $params['error'] = "Something went wrong , Unable to create Folder structure";
            return $response->withRedirect($this->container->router->pathFor('camp.createById', $params));
        }

        $_SESSION['success'] = "Campaign is created successfully" ;
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));
    }
    
    
    /**
     * getCampaignsByDraft
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Get the account Id from session and redirect to
     *        'camp.activeById' template
     */
    public function getCampaignsByStatusActive($request, $response)
    {

        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        
        if ((empty($params)) && empty($params['acc_id'])) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);

        if (empty($oauth_params)) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('camp.activeById', $params));
    }
    
    
    public function getActiveCampaignsById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        
        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        $camp_details = $camp->getCampaignsByStatusForAccount((int)$params['acc_id'], "active");
        $camp_details = $camp->appendLinksForCampaigns($camp_details);

        return $this->container->view->render(
            $response,
            '/campaign/campaigns.twig',
            array('campaigns' => $camp_details)
        );
    }


    /**
     * getCampaignsByDraft
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Get the account Id from session and redirect to
     *        'camp.draftById' template
     */
       
    public function getCampaignsByStatusDraft($request, $response)
    {

        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if ((empty($params)) && empty($params['acc_id'])) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);

        if (empty($oauth_params)) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));
    }
 
    
    /**
      * getCampaignsByDraft
      *
      * @param
      *        $request  : Requested parameters by Campaign Object
      *        $response : Response parameters given by Campaign Object
      *
      * @return
      *        $response : Generate all the draft campaigns in array
      *                    send it to the campaign.twig template
      *
      */
    
    public function getDraftCampaignsById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');

        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        $camp_details = $camp->getCampaignsByStatusForAccount((int)$params['acc_id'], "draft");
        $camp_details = $camp->appendLinksForCampaigns($camp_details);

        return $this->container->view->render(
            $response,
            '/campaign/draftcampaigns.twig',
            array('campaigns' => $camp_details)
        );
    }
    
    
    /**
     * getCampaignsByArchive
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Get the account Id from session and redirect to
     *        'camp.draftById' template
     */
    
    public function getCampaignsByStatusArchive($request, $response)
    {
        
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if ((empty($params)) && empty($params['acc_id'])) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);

        if (empty($oauth_params)) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.archiveById', $params));
    }
    
    
    /**
     * getCampaignsArchiveById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Collect all the archived campaigns and send it to
     *        'camp.archiveById' template.
     */
    public function getArchiveCampaignsById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        
        if (empty($params['acc_id'])) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
            
        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->getCampaignsByStatusForAccount((int)$params['acc_id'], "archive");
        
        $camp_details = $camp->appendLinksForCampaigns($camp_details);
                       
        return $this->container->view->render(
            $response,
            '/campaign/archivecampaigns.twig',
            array('campaigns' => $camp_details)
        );
    }
    
    
    /**
     * getCampaignActivateById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Activate the campaign by campaign id and redirect to
     *          'camp.activeById' template
     *
     */
    public function getCampaignActivateById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['camp_id'] = $request->getAttribute('camp_id');
        $params['camp_url'] = $this->container->campaign_url;

        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0) || ((int)$params['camp_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        $campaign_status = $camp->activateCampaignById($params, $this->container->campaign_path);
  
        if (is_bool($campaign_status) && ($campaign_status == false)) {
            $_SESSION['error'] = "Something went wrong, Unable to activate Campaign";
        } else {
            $_SESSION['success'] = "Campaign is activated succesfully";
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.activeById', $params));
    }
    
    
    /**
     * getCampaignArchivedById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Archive the campaign by campaign id and redirect to
     *          'camp.archiveById' template
     */
    
    public function getCampaignArchivedById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['camp_id'] = $request->getAttribute('camp_id');
        $params['camp_url'] = $this->container->campaign_url;

        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0) || ((int)$params['camp_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        $campaign_status = $camp->archiveCampaignById($params, $this->container->campaign_path);

        if (is_bool($campaign_status) && ($campaign_status == false)) {
            $_SESSION['error'] = "Something went wrong, Unable to archive Campaign";
        } else {
            $_SESSION['success'] = "Campaign is archived succesfully";
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.archiveById', $params));
    }
    
    
    /**
     * getCampaignDeleteById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Archive the campaign by campaign id and redirect to
     *          'camp.archiveById' template
     */
    
    public function getCampaignDeleteById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['camp_id'] = $request->getAttribute('camp_id');

        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0) || ((int)$params['camp_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        $campaign_status = $camp->deleteCampaignById($params);

        if (is_bool($campaign_status) && ($campaign_status == false)) {
            $_SESSION['error'] = "Something went wrong, Unable to delete Campaign";
        } else {
            $_SESSION['success'] = "Campaign is deleted succesfully";
        }
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));
    }


    /**
     * getCampaignReactivateById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Reactivate the campaign by campaign id and redirect to
     *          'camp.draft' template
     */
        
    public function getCampaignReactivateById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['camp_id'] = $request->getAttribute('camp_id');
        $params['camp_url'] = $this->container->campaign_url;

        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0) || ((int)$params['camp_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        $new_camp_id = $camp->createVersionOfCampaign(
            (int)$params['camp_id'],
            $this->container->campaign_path,
            $this->container->campaign_url
        );

        $status = $this->createVersionOfCampaignSettings((int)$params['camp_id'], (int)$new_camp_id);

        if (is_bool(status) && (status == false)) {
            $_SESSION['error'] = "Something went wrong, Unable to create new version";
        } else {
            $_SESSION['success'] = "New version of campaign is created succesfully";
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));
    }


    /**
     * getCampaignUpdateById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Update the campaign by campaign id and redirect to
     *          'camp.update' template
     */
    public function getCampaignUpdateById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
      
        $params['camp_id'] = $request->getAttribute('camp_id');
        
        if (empty($params['acc_id']) || empty($params['camp_id'])) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $camp = new \App\Models\Campaign($this->container->dbh);
        
        $camp_details = $camp->getCampaignDetailsById($params['camp_id']);
        
        return $this->container->view->render(
            $response,
            'campaign/campaignupdate.twig',
            array("camp_details" => $camp_details)
        );
    }
    
    
    /**
     * postCampaignUpdateById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Update the campaign by campaign id and redirect to
     *          'camp.update' template
     */
    
    public function postCampaignUpdateById($request, $response)
    {
        $params = $request->getParams();
        $params['acc_id'] = $request->getAttribute('acc_id');
        
        if (empty($params['camp_id']) || empty($params['camp_name']) || empty($params['acc_id'])) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        $camp_details = $camp->updateCampaignById($params);
        if (is_bool($camp_details) && ($camp_details == false)) {
            return $response->withRedirect($this->container->router->pathFor('camp.updateById', $params));
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.draft'));
    }

    
    /**
     * getCampaignCopyById
     *
     * @param
     *        $request  : Requested parameters by Campaign Object
     *        $response : Response parameters given by Campaign Object
     *
     * @return
     *        $response : Copy the campaign by campaign id and redirect to
     *          'camp.copy' template
     */
    public function getCampaignCopyById($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['camp_id'] = $request->getAttribute('camp_id');
        $params['camp_url'] = $this->container->campaign_url;

        if (!$this->getUserAuthenticity() || ((int)$params['acc_id'] <= 0) || ((int)$params['camp_id'] <= 0)) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $camp = new \App\Models\Campaign($this->container->dbh);
        $new_camp_id = $camp->createVersionOfCampaign(
            (int)$params['camp_id'],
            $this->container->campaign_path,
            $this->container->campaign_url
        );

        $status = $this->createVersionOfCampaignSettings((int)$params['camp_id'], (int)$new_camp_id);

        if (is_bool($status) && ($status == false)) {
            $_SESSION['error'] = "Something went wrong, Unable to revise Campaign";
        } else {
            $_SESSION['success'] = "Campaign is revised succesfully";
        }
        
        return $response->withRedirect($this->container->router->pathFor('camp.draftById', $params));
    }
   
    /**
     * createVersionOfCampaignSettings function
     *
     * @param integer $old_camp_id
     * @param integer $new_camp_id
     * @return boolean
     */
    private function createVersionOfCampaignSettings(int $old_camp_id, int $new_camp_id):bool
    {
        $status = false;
        if ($old_camp_id <= 0 || $new_camp_id <= 0) {
            die("createVersionOfCampaignSettings");
            return $status;
        }
        
        // versioning of settings 

        $settings = ['General','Data_Flow','Fraud_Settings','Messages','Facebook','Google','Adeblo','Webgains','Outbrain','Email','Webinaris','Digistore','MailInOne','Hubspot','HTTPGet','Flow Control','Mapping'];
        $settings_status = [];
        foreach ($settings as $name){
            $setting_Obj = new \App\Models\CampaignParams($this->container->dbh, "$name") ;
            $setting_status = $setting_Obj->createVersionOfSettings($old_camp_id, $new_camp_id);
            $settings_status[] = $setting_status;
        }

        $settings_result = (bool) array_product($settings_status);
        
        // get old campaign detials.
        $camp_Obj =  new \App\Models\Campaign($this->container->dbh);
        $camp_details = $camp_Obj->getCampaignDetailsById($old_camp_id);
        $acc_id = $camp_Obj->getAccountNumberByCompanyId((int) $camp_details['company_id']);
        $camp_new_details = $camp_Obj->getCampaignDetailsById($new_camp_id);

        if (is_array($camp_details) || isset($camp_details['id']) || isset($camp_details['cio.survey_id'])) {
            // Version Campaign Settings for Survey.
            $sury_Obj = new \App\Models\Survey(
                $this->container->dbh,
                $acc_id,
                $camp_details['id'],
                $camp_details['cio.survey_id'],
                ""
            );
            $camp_new_details['cio.survey_id'] = $sury_Obj->createVersionOfSurvey();
            if ((int)$camp_new_details['cio.survey_id'] >= 0) {
                $camp_status = $camp_Obj->updateCampaignDetails($camp_new_details);
                $ques_status = $sury_Obj->createVersionOfQuestions((int) $camp_new_details['cio.survey_id']);
            }
        }

        // Check the status
        if( is_bool($settings_result) && $settings_result == TRUE ){
            $status = true;
        }

        return $status;
    }
}
