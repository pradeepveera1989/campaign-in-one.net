<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountController
 *
 * @author Pradeep
 */

namespace App\Controllers;

use App\Models\User;
use Slim\Views\Twig as View;

class AccountController extends Controller
{
    //put your code here
    protected $view;
    protected $query;
    public $result;
       

    /**
     * getUserResetPasswordById function
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUserResetPasswordById($request, $response)
    {
        return $this->container->view->render($response, 'account/change_password/change_password.twig');
    }
    

    /**
     * AUserResetPasswordById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postUserResetPasswordById($request, $response)
    {
        $return = [];
        $req_attrs = ['user_id', 'acc_id', 'expire_time'];
        # No Oauth2 Validation here.
        # This postUserResetPasswordById is also used to forgotPassword.
        $params = $request->getParams();
        foreach ($req_attrs as $attr) {
            $params[$attr] = ($attr == "expire_time") ?
            $request->getAttribute($attr) : (int)$request->getAttribute($attr);
        }

        // Validates the Expire time for the Email Link (Used only for Forgot Password before Sign).
        if (!$this->checkExpireTime($params['expire_time'])) {
            $return = $params;
            $return['error'] = "Password Link expired";
            return $this->container->view->render(
                $response,
                'account/change_password/change_password.twig',
                array("password" => $return)
            );
        }

        $u_obj = new \App\Models\User($this->container->dbh);
        $status = $u_obj->updatePassword($params);

        if (is_bool($status) && $status == false) {
            $return = $params;
            $return['error'] = true;
            $return['errorMsg'] = "Current Password mismatch";
        } else {
            $return['success'] = "Password is updated successfully,  Please login with new password";
        }

        return $this->container->view->render(
            $response,
            'account/change_password/change_password.twig',
            array("password" => $return)
        );
    }


    /**
     * getUpdateAccount function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateAccount($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        return $response->withRedirect($this->container->router->pathFor('acc.updateById', $params));
    }

    
    /**
     * getUpdateAccountById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateAccountById($request, $response)
    {
        // Check the Authencity of the User
        $params['acc_id'] = $request->getAttribute('acc_id');

        if (!$this->getUserAuthenticity() || !is_numeric($params['acc_id']) || (int) $params['acc_id'] <= 0) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $model = new \App\Models\Model($this->container->dbh);
        $acc_details = $model->getCompanyDetailsByAccountId($params['acc_id']);

        return $this->container->view->render(
            $response,
            'account/account_information/accupdate.twig',
            array('details' => $acc_details)
        );
    }

    
    /**
     * postUpdateAccountById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postUpdateAccountById($request, $response)
    {
        $params = $request->getParams();
        $params['acc_id'] = $request->getAttribute('acc_id');

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity() || !is_numeric($params['acc_id']) || (int) $params['acc_id'] <= 0) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        // Update Company Detials
        $company = new \App\Models\Company($this->container->dbh);
        $result = $company->updateCompanyById($params);
        $acc_details = $company->getCompanyDetailsByAccountId($params['acc_id']);

        if (is_bool($result) && $result == false) {
            // Failed to Update
            $acc_details['error'] = "Failed to Update Account Information";
            return $this->container->view->render(
                $response,
                'account/account_information/accupdate.twig',
                array('details' => $acc_details)
            );
        }

        // Send email to All Users of Company
        $users = $company->getAllUsersOfCompany((int)$acc_details['id']);
        foreach ($users as $u) {
            $mail = new \App\Mailer\mail($u);
            $result = $mail->sendMailAccountUpdate();
        }

        $acc_details['success'] = "Account Information is Updated Successfully";

        return $this->container->view->render(
            $response,
            'account/account_information/accupdate.twig',
            array('details' => $acc_details)
        );
    }

    /**
     * getSwitchCustomer function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getSwitchCustomer($request, $response)
    {
        $params['acc_id'] = $request->getAttribute('acc_id');
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity() || !is_numeric($params['acc_id']) || (int) $params['acc_id'] <= 0) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        // Middleware automatically updates $_SESSION['acc_id']
        #return $this->container->view->render($response, 'dashboard.twig');
        return $response->withRedirect($this->container->router->pathFor('auth.home'));
    }





    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //   Functions for future development.
    //   Currently bellow functions are supported and Used in the Tool.
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function read($request, $response)
    {
        $company = new \App\Models\Company($this->container->dbh);
        
        $acc_details = $company->getCompanyDetials();
        $i = 0;
        foreach ($acc_details as $acc_detail) {
            $detail[$i] = $acc_detail;
            $i++;
        }
        return $this->container->view->render($response, 'userdetials.twig', array('d' => $detail));
    }
    
    
    public function unblock($request, $response)
    {
        return $this->container->view->render($response, 'userunblock.twig');
    }
    
    
    public function getBlockAccountById($request, $response)
    {
        $model = new \App\Models\Model($this->container->dbh);
        $params['account_no'] = $model->getCompanyIdByAccountNumber($request->getAttribute('acc_id'));
        
        try {
            if (empty($params)) {
                throw new Exception("Invalid empty User and account id");
            }

            $auth = new \App\Models\Authenticate($this->container->dbh);
            $auth->blockAccount($params);
            $message = [
                
                "success" => "All the Users under this account has been blocked"
            ];
        } catch (Exception $ex) {
            echo "Caught Exception : ". $e->getMessage();
        }

        return $this->container->view->render($response, 'message.twig', array("message" => $message));
    }
    
    
    
    public function getUnBlockAccountById($request, $response)
    {
        $model = new \App\Models\Model($this->container->dbh);
        $params['account_no'] = $model->getCompanyIdByAccountNumber($request->getAttribute('acc_id'));
        
        try {
            if (empty($params)) {
                throw new Exception("Invalid account id");
            }

            $auth = new \App\Models\Authenticate($this->container->dbh);
            $auth->unBlockAccount($params);
            $message = [
                
                "success" => "All the Users under this account has been unblocked"
            ];
            
            return $this->container->view->render($response, 'message.twig', array("message" => $message));
        } catch (Exception $ex) {
            echo "Caught Exception : ". $e->getMessage();
        }

        return $this->container->view->render($response, 'userblock.twig');
    }

    // public function getAccountSignUp($request, $response){
     
    //     return $this->container->view->render($response, 'accsignup.twig');
    // }
    
    
    // public function postAccountSignUp($request, $response){
        
    //     $params = $request->getParams();
        
    //     $user = new \App\Models\User($this->container->dbh);
        
    //     $company = new \App\Models\Company($this->container->dbh);
        
    //     $company_id = $company->createCompany($params);
        
    //     if(!is_numeric($company_id)){
            
    //         $msg = ["success" => "Something went wrong :( "];
    //         return $this->container->view->render($response, 'message.twig', array("message" => $msg));
    //     }
        
    //     $params["company_id"] = $company_id;
    //     $usr_id = $user->createUser($params);
        
    //     if(!is_numeric($usr_id)){
    //         $msg = ["success" => "Something went wrong :( "];
    //         return $this->container->view->render($response, 'message.twig', array("message" => $msg));
    //     }
        
    //     $msg = ["success" => "New Company has been regestured succesfully"];

    //     return $this->container->view->render($response, 'message.twig', array("message" => $msg));
    // }
    
//    public function create($request, $response){
//
//        $db = $this->container->dbh;
//
//        $queryUsers = $db->prepare(" SELECT * FROM `cio.company`");
//        $queryUsers->execute();
//        $users = $queryUsers->fetchAll();
//
//
//        var_dump($users);
////        $s = $this->container->db->table('campaign_params')->where('campaign_params_id', 13);
////        var_dump("result",$s);
//       // die();
//        return $this->container->view->render($response, 'home.twig');
//    }

    // public function getAccountDetialsById($request,  $response){
        
    //      # Get Company Id From Account id
    //     $model = new \App\Models\Model($this->container->dbh);
    //     $params['id'] = $model->getCompanyIdByAccountNumber($request->getAttribute('acc_id'));
    //     try{
            
    //         if(empty($params)){
                
    //             throw new Exception("Invalid empty User and account id");
    //             // Invalid User credentials
    //             #return $this->container->view->render($response, 'signin.twig');
    //         }
            
    //         $company = new \App\Models\Company($this->container->dbh);
    //         $this->company_id = $params['id'];
    //         $acc_detials = $company->getCompanyDetailsById($this->company_id);

    //         $details = [
                
    //             // Account Details
    //             "acc_id" =>$acc_detials["id"],
    //             "acc_name" => $acc_detials["name"],
    //             "acc_address" => $acc_detials["street"],
    //             "acc_postal_code" => $acc_detials["postal_code"],
    //             "acc_city" => $acc_detials["city"],
    //             "acc_country" => $acc_detials["country"],
    //             "acc_website" => $acc_detials["website"],
    //             "acc_phone" => $acc_detials["phone"],
    //             "acc_vat" => $acc_detials["vat"],
    //         ];
            
    //         return $this->container->view->render($response, 'accdetails.twig', array('details' => $details));
            
    //     } catch (Exception $e) {
            
    //         $e->getMessage();
    //         die();
    //     }
              
    //     return true;
    // }
}
