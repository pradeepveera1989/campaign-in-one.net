<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of Controller
 *
 * @author Pradeep
 */
class Controller
{
    //put your code here
    
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getUserAuthenticity()
    {
        $usr_status = false;
        // Check the Session Detials
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if ((empty($params))) {
            // Session expired
            return $usr_status;
        }
        // Oauth2 validation
        $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
        $oauth_params = $oauth2->getAccessToken($params);
        if (empty($oauth_params)) {
            // 0auth params are missing
            return $usr_status;
        }
        $_SESSION['acc_name'] = $auth->getCompanyDetailsByAccountId($_SESSION['acc_id'])['name'];
        $usr_status = true;
        return $usr_status;
    }


    /**
     * checkExpireTime function
     *
     * @param string $time
     * @return boolean
     */
    public function checkExpireTime(string $time) : bool
    {
        $status = false;
        $expire_hours = 2;
        # Check for empty string
        if (empty($time)) {
            return $status;
        }
        $current_time = strtotime(now);
        $given_time = (int) base64_decode($time);
        # Check if base64_decode is not valid number
        if (!is_int($given_time) || $given_time < 0) {
            return $status;
        }
        # Calculate the difference in hours
        $diff_in_hours = round(($current_time - $given_time) / 3600);
        if ($diff_in_hours <=$expire_hours) {
            $status = true;
        }
        return $status;
    }
}
