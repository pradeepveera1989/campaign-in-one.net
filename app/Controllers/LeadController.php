<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Description of LeadController
 *
 * @author Pradeep
 */
class LeadController extends Controller
{
    //put your code here
    
    /**
     * getAllLeads function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getAllLeads($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $auth_ids = $auth->getAuthIds();

        if (!is_array($auth_ids) || !is_numeric($auth_ids['acc_id']) || $auth_ids['acc_id'] <= 0) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect(
            $this->container->router->pathFor('leads.LeadsByAccountId', $auth_ids)
        );
    }
    
    
    /**
     * getLeadsByAccountId function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getLeadsByAccountId($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $acc_id = $request->getAttribute('acc_id');
        // Get the Lead Details
        $lead_obj = new \App\Models\Leads($this->container->dbh);
        $leads = $lead_obj->getLeads($acc_id);

        return $this->container->view->render(
            $response,
            '/tracking/lead_tracking.twig',
            array('leads' => $leads)
        );
    }

    public function getLeadsByCampaignId(Request $request, Response $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $acc_id = $request->getAttribute('acc_id');
        $camp_id = $request->getAttribute('camp_id');

        // Get the Lead Details
        $lead_obj = new \App\Models\Leads($this->container->dbh);
        $leads = $lead_obj->getLeadsForCampaign($acc_id, $camp_id);

        return $this->container->view->render(
            $response,
            '/tracking/lead_tracking_by_campaign.twig',
            array('leads' => $leads)
        );

    }

    
    /**
     * reactivateLeadById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function reactivateLeadById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $auth_ids = $auth->getAuthIds();
        // Get required Parameters
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['lead_id'] = $request->getAttribute('lead_id');

        $lead_obj = new \App\Models\Leads($this->container->dbh);
        $lead_status = $lead_obj->reactivateLead(intval($params['lead_id']));
        $leads = $lead_obj->getLeads((int)$params['acc_id'], "");

        if (is_bool($lead_status) && $lead_status == true) {
            $auth_ids["notify_leads"] = "Lead is activated successfully";
        } else {
            $auth_ids["error_leads"] = "Something went wront while activating the Lead ";
        }

        return $response->withRedirect(
            $this->container->router->pathFor('leads.LeadsByAccountId', $auth_ids)
        );
    }


    /**
     * deactivateLeadById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function deactivateLeadById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $auth_ids = $auth->getAuthIds();
        // Get required Parameters
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['lead_id'] = $request->getAttribute('lead_id');
        // Deactviate the Lead status.
        $lead_obj = new \App\Models\Leads($this->container->dbh);
        $lead_status = $lead_obj->cancelLead($params['lead_id']);
        // Get all the leads from DB
        $leads = $lead_obj->getLeads((int)$params['acc_id'], "");
        // Notification as per Lead status
        if (is_bool($lead_status) && $lead_status == true) {
            $auth_ids["notify_leads"] = "Lead is deactivated successfully";
        } else {
            $auth_ids["error_leads"] = "Something went wrong while deactivating the Lead ";
        }

        return $response->withRedirect(
            $this->container->router->pathFor('leads.LeadsByAccountId', $auth_ids)
        );
    }


    /**
     * getDetailLeadViewById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getDetailLeadViewById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        // Get required Parameters
        $lead_id = intval($request->getAttribute('lead_id'));
        // Lead Obj
        $lead_obj = new \App\Models\Leads($this->container->dbh);
        $lead_info = $lead_obj->getLeadInfoById($lead_id);

        if (!is_array($lead_info) || empty($lead_info)) {
            // Stay at Leads view with error msg
            $leads["error_leads"] = "Something went wrong while getting lead information";
            return $this->container->view->render(
                $response,
                '/tracking/lead_tracking.twig',
                array('leads' => $lead_info)
            );
        }

        return $this->container->view->render(
            $response,
            '/tracking/lead_tracking_detail.twig',
            array('leads' => $lead_info)
        );
    }
}
