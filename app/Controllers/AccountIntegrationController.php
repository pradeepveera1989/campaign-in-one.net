<?php

namespace App\Controllers;

use Slim\Http\UploadedFile;

//This file contains controller for mapping , email , digistore ,webinaris;

class AccountIntegrationController extends Controller
{

    /**getUpdateSettingsDigistore
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsDigistore($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.digistore',$params));
    }

    /**
     * getDigistoreSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getDigistoreSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "Digistore";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Digistore.twig', array('digistore' => $info_hb));
    }

    /**
     * postDigistoreSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postDigistoreSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "Digistore";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Digistore.twig', array('digistore' => $info_hb));
    }

    /**getUpdateSettingsWebinaris
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsWebinaris($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.Webinaris',$params));
    }

    /**
     * getWebinarisSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getWebinarisSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "Webinaris";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Webinaris.twig', array('webinaris' => $info_hb));
    }

    /**
     * postWebinarisSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postWebinarisSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "Webinaris";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Webinaris.twig', array('webinaris' => $info_hb));
    }

    /**getUpdateSettingsEmail
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsEmail($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.Email',$params));
    }

    /**
     * getEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getEmailSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "Email";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Email.twig', array('email' => $info_hb));
    }

    /**
     * postEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postEmailSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "Email";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Email.twig', array('email' => $info_hb));
    }

    /**getUpdateSettingsEmail
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsFraud($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.Fraud',$params));
    }

    /**
     * getEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getFraudSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests= [];
        $httpMethod = "GET";
        $type = "Fraud";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Fraud.twig', array('fraud' => $info_hb));
    }

    /**
     * postEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postFraudSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "Fraud";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Fraud.twig', array('fraud' => $info_hb));
    }

    /**getUpdateSettingsEmail
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsMIO($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.MIO',$params));
    }

    /**
     * getEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getMIOSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "Mail-In-One";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/MIO.twig', array('mio' => $info_hb));
    }

    /**
     * postEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postMIOSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "Mail-In-One";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/MIO.twig', array('mio' => $info_hb));
    }

    /**getUpdateSettingsEmail
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsHubspot($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = 'Session expired, Please Login';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.Hubspot',$params));
    }

    /**
     * getEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getHubspotSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "Hubspot";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Hubspot.twig', array('hubspot' => $info_hb));
    }

    /**
     * postEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postHubspotSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "Hubspot";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/Hubspot.twig', array('hubspot' => $info_hb));
    }

    /**getUpdateSettingsEmail
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsGeneral($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.General',$params));
    }

    /**
     * getEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getGeneralSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "General";
        $values =[];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/General.twig', array('general' => $info_hb));
    }

    /**
     * postEmailSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postGeneralSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests =[];
        $httpMethod = "POST";
        $type = "General";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/General.twig', array('general' => $info_hb));
    }

    /**getUpdateSettings
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsURLApi($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        return $response->withRedirect($this->container->router->pathFor('acc.UrlApi',$params));
    }

    /**
     * getSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getURLApiSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = "GET";
        $type = "URLAPI";
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/UrlApi.twig', array('urlapi' => $info_hb));
    }

    /**
     * postSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postURLApiSettingsById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $requests = [];
        $httpMethod = "POST";
        $type = "URLAPI";
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();

        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);

        return $this->container->view->render($response, '/account/account_integrations/UrlApi.twig', array('urlapi' => $info_hb));
    }

    /**getUpdateSettings
     *  function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateSettingsHttpInbound($request, $response)
    {
        $auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $auth->getAuthIds();
        if (!is_array($params) || empty($params['acc_id'])) {
            $_SESSION['error'] = "Session expired, Please Login";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        return $response->withRedirect($this->container->router->pathFor('acc.HttpInbound',$params));
    }
    /**
     * getSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getHttpInboundSettingsById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        //requests is to send data to get info function
        $requests = [];
        $httpMethod = 'GET';
        $type = 'HTTPInbound';
        $values = [];
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        //getting info from getInfo function
        //$info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        return $this->container->view->render($response, '/account/account_integrations/HttpInbound.twig', array('outbound' => $info_hb));
    }
    /**
     * postSettingsById
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postHttpInboundSettingsById($request, $response)
    {
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = 'Could not verify your account';
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $requests = [];
        $httpMethod = 'POST';
        $type = 'HTTPInbound';
        $requests['acc_id' ]= $request->getAttribute('acc_id');
        $values = $request->getParams();
        $info_hb = $this->getInfo($httpMethod, $requests, $type, $values);
        return $this->container->view->render($response, '/account/account_integrations/HttpInbound.twig', array('outbound' => $info_hb));
    }


    /**
     * getInfo
     *
     * @param string $httpMethod
     * @param array $requests
     * @param [type] $httpMethod = GET /POST
     * @param array|null $values
     * @return array $info_settings = variable need to send to twig files
     */
    public function getInfo(string $httpMethod, array $requests, $type, array $values = null)
    {
        if ($httpMethod == "POST") {
            //object of the Model file
            $in_Obj = new \App\Models\Integration($this->container->dbh, $type);
            //making params to send to model
            $params = $this->createParams($requests, $type, $values);

            //sending $params to model
            $result = $in_Obj->insertOrUpdateIntegration($params);
            //receving
            $info_settings = $this->getValues($requests, $type);

            if (!is_array($info_settings) || empty($info_settings) || !is_bool($result) || $result === false) {
                $result['error_integration'] = ['Something went wrong, Failed to fetch Information'];
            }
            $result['notify_integration'] = ['General Settings details have been saved successfully'];

            return $info_settings;
        } elseif ($httpMethod == "GET") {
            //getting information from models
            $info_settings = $this->getValues($requests, $type);
            return $info_settings;
        }
    }

    /**
     * getValues
     *
     * @param [type] $requests = getting values from URL
     * @param [type] $type = type of the setting
     * @return $info_settings = variable need to send to twig files
     */

    public function getValues($requests, $type)
    {
        $info_settings = [];
        $in_Obj = new \App\Models\Integration($this->container->dbh, $type);
        $acc_id = $requests['acc_id'];
        $getValue = $in_Obj->getIntegrationDetails($acc_id);
        //taking only useful information
        if(isset($getValue[0])) {
            $getValues = $getValue[0];
            $id = $getValues['id'];
            //string to object
            $info_setting = json_decode($getValues['additional_settings']);
            //typecasting to array
            $info_settings = (array)$info_setting;
            $info_settings['acc_id'] = $acc_id;
            $info_settings['id'] = $id;
        }

        return $info_settings;
    }

    public function createParams($requests, $type, $values)
    {
        //encripting the password
        if(isset($values['password'])){
            $values['password'] = base64_encode($values['password']);
        }
        $value = json_encode($values, JSON_UNESCAPED_UNICODE);

        //Giving update value
        $date = date('Y-m-d h:i:s');
        $params['updated'] = $date;
        $params['created'] = $date;
        $params['created_by'] = 1;
        $params['updated_by'] = 1;
        //Making params to send to databse
        //only this change according to requirments
        $params['id'] = $values['id'];
        $params['acc_no'] = $requests['acc_id'];
        $params['type'] = $type ;
        $params['value'] = $value;
        $params['apikey'] = array_key_exists('apikey', $values) ? values['apikey']: '';
        $params['user'] = array_key_exists('user', $values) ? values['user']: '';
        $params['password'] = array_key_exists('password', $values) ? values['password']: '';
        $params['url'] = array_key_exists('url', $values) ? values['url']: '';
        $params['first_name'] = array_key_exists('first_name', $values) ? values['first_name']: '';
        $params['last_name'] = array_key_exists('last_name', $values) ? values['last_name']: '';
        $params['owner_id'] = array_key_exists('ownerId', $values) ? values['ownerId']: '';
        $params['sync'] = array_key_exists('sync', $values) ? values['sync']: '';


        return $params;
    }
}
