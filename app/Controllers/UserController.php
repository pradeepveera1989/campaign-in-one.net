<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author Pradeep
 */

namespace App\Controllers;

use Respect\Validation\Validator as v;
use App\Oauth2;

class UserController extends Controller
{
    protected $view;
    protected $query;
    public $result;
       
    /**
     * getUserSignUp function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUserSignUp($request, $response)
    {
        return $this->container->view->render($response, 'login/signup.twig');
    }
    
    /**
     * postUserSignUp function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postUserSignUp($request, $response)
    {

        #Get the params
        $signup = $request->getParams();
        #Load all the Params to Session
        $_SESSION['signup'] = $signup;
        $validation = $this->container->validator->validate($request, [
            #Check for Error in email and password
           'email' => v::noWhitespace()->notEmpty(),
           'password' => v::noWhitespace()->notEmpty()
        ]);

        //Password policy check
        $this->container->validator->validatePassword($signup['password']);
        if ($this->container->validator->failed()) {
            $signup['error'] = "password_policy";
            return $this->container->view->render($response, 'login/signup.twig', array('usr_signup' => $signup));
        }
        
        #Get the Company Id and validate
        $company = new \App\Models\Company($this->container->dbh);
        $domain = explode('@', $signup['email'])[1];
        $signup['company_id'] = $company->createCompany($domain);
        if ($signup['company_id'] <= 0) {
            $signup['error'] = "Email is not accepted, Please use your Company Email address";
            return $this->container->view->render($response, 'login/signup.twig', array('usr_signup' => $signup));
        }

        #Create User and validate User Id
        $user = new \App\Models\User($this->container->dbh);
        $user_details= $user->createUser($signup);
        if ($user_details['id'] <= 0) {
            $signup['error'] = "Invalid User detials";
            return $this->container->view->render($response, 'login/signup.twig', array('usr_signup' => $signup));
        }
        $signup['id'] = $user_details['id'];
        $signup['account_no'] = $company->getAccountNumberByCompanyId((int) $signup['company_id']);

        // Send the activation Link to User email Id
        $mail = new \App\Mailer\mail($signup);
        $status = $mail->sendMailAccountCreate();
        
        if (!$status) {
            $_SESSION['error']  = "Something went wrong in sending you activation Link";
        } else {
            $_SESSION['success']  = "Please check your inbox for activation link";
        }
        
        return $response->withRedirect($this->container->router->pathFor('auth.signin'));
    }

    
    /**
     * getUpdateUser function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateUser($request, $response)
    {
        $this->auth = new \App\Models\Authenticate($this->container->dbh);
        $params = $this->auth->getAuthIds();

        if ((empty($params)) || !is_array($params)) {
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        return $response->withRedirect($this->container->router->pathFor('user.updateById', $params));
    }
    

    /**
     * getUpdateUserById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function getUpdateUserById($request, $response)
    {
        
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['error'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $params['usr_id'] = $request->getAttribute('user_id');
        $params['acc_id'] = $request->getAttribute('acc_id');

        // Get the User Information from Model
        $user = new \App\Models\User($this->container->dbh);
        $user_details = $user->getUserDetailsById((int) $params['usr_id']);

        return $this->container->view->render(
            $response,
            'account/user_information/userupdate.twig',
            array('details' => $user_details)
        );
    }
    

    /**
     * postUpdateUserById function
     *
     * @param [type] $request
     * @param [type] $response
     * @return void
     */
    public function postUpdateUserById($request, $response)
    {

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['error'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        $params = $request->getParams();
        $params['user_id'] = $request->getAttribute('user_id');

        $user = new \App\Models\User($this->container->dbh);
        $result = $user->updateUserById($params);
        $user_details = $user->getUserDetailsById((int) $params['user_id']);

        if (is_bool($result) && $result == false) {
            $user_details['error'] = "Failed to Update Personal Information";
            return $this->container->view->render(
                $response,
                'account/user_information/userupdate.twig',
                array('details' => $user_details)
            );
        }
        $user_details['success'] = "Personal Information is Updated Successfully";

        return $this->container->view->render(
            $response,
            'account/user_information/userupdate.twig',
            array('details' => $user_details)
        );
    }


    public function getActivateUserById($request, $response)
    {
        $params = $request->getParams();
        $params['user_id'] = $request->getAttribute('user_id');
        $params['acc_id'] = $request->getAttribute('acc_id');
        $params['expire_time'] = $request->getAttribute('expire_time');
    
        if (empty($params) || (int)$params['user_id'] <= 0 || (int) $params['acc_id'] <= 0) {
            $_SESSION['error'] = "Invalid URL, Could not activate your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        if (!$this->checkExpireTime($params['expire_time'])) {
            $_SESSION['error'] = "Activation link is expired";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        // User Activation
        $user = new \App\Models\User($this->container->dbh);
        $result = $user->activateUser($params);
        
        if (!is_bool($result) || $result == false) {
            $_SESSION['error'] = "Something went wrong while activating your account ";
        } else {
            $_SESSION['success'] = "Your account is activated :)";
        }

        return $response->withRedirect($this->container->router->pathFor('auth.signin'));
    }
    

    // public function getUserDetialsById($request,  $response){
        
    //     $params['user_id'] = $request->getAttribute('user_id');
    //     # Get Company Id From Account id
    //     $model = new \App\Models\Model($this->container->dbh);
    //     $params['company_id'] = $model->getCompanyIdByAccountNumber($request->getAttribute('acc_id'));
    //     try{

    //         if(empty($params)){
                
    //             throw new Exception("Invalid empty User and account id");
    //             // Invalid User credentials
    //             #return $this->container->view->render($response, 'signin.twig');
    //         }
            
    //         $company = new \App\Models\Company($this->container->dbh);
    //         $user = new \App\Models\User($this->container->dbh);
    //         $this->user_id = $params['user_id'];
    //         $this->company_id = $params['company_id'];
    //         $acc_detials = $company->getCompanyDetailsById((int)$this->company_id);
    //         $user_detials = $user->getUserDetailsById($this->user_id);

    //         $details = [
                
    //             // Account Details
    //             "acc_id" =>$acc_detials["id"],
    //             "acc_name" => $acc_detials["name"],
    //             "acc_address" => $acc_detials["street"],
    //             "acc_postal_code" => $acc_detials["postal_code"],
    //             "acc_city" => $acc_detials["city"],
    //             "acc_country" => $acc_detials["country"],
    //             "acc_website" => $acc_detials["website"],
    //             "acc_phone" => $acc_detials["phone"],
    //             "acc_vat" => $acc_detials["vat"],
                
    //             // User Detials
    //             "user_id" => $user_detials["id"],
    //             "user_email" => $user_detials["email"],
    //             "user_first_name" => $user_detials["first_name"],
    //             "user_last_name" => $user_detials["last_name"],
    //             "user_salutation" => $user_detials["salutation"],
    //             "user_phone" => $user_detials["phone"],
    //             "user_status" => $user_detials["status"]
    //         ];
            
    //         return $this->container->view->render($response, 'useraccountdetials.twig', array('details' => $details));
            
    //     } catch (Exception $e) {
            
    //         $e->getMessage();
    //         die();
    //     }
              
    //     return true;
    // }
    
    
    // public function getBlockUserById($request, $response){
        
    //     $params['id'] = $request->getAttribute('user_id');
    //     #$params['company_id'] = $request->getAttribute('acc_id');
        
    //     try{
            
    //         if(empty($params)){

    //             throw new Exception("Invalid empty User and account id");
    //         }
            
    //         $auth = new \App\Models\Authenticate($this->container->dbh);
    //         $auth->blockuser($params);
    //         $message = [
                
    //             "success" => "User has been blocked"
    //         ];
    //         return $this->container->view->render($response, 'message.twig', array("message" => $message));
            
    //     } catch (Exception $e) {
            
    //         echo "Caught Exception : ". $e->getMessage();
    //     }
    // }
    
    
    // public function getBlockUser($request, $response){
  
    //     $auth = new \App\Models\Authenticate($this->container->dbh);
        
    //     $params = $auth->getAuthIds();

    //     if((empty($params))){
            
    //         return $response->withRedirect($this->container->router->pathFor('auth.signin'));
    //     }
        
    //     return $response->withRedirect($this->container->router->pathFor('auth.signin'), $params);
    // }
    
    
    // public function getUnBlockUserById($request, $response){
        
    //     $params['id'] = $request->getAttribute('user_id');
    //     try{
            
    //         if(empty($params)){

    //             throw new Exception("Invalid empty User and account id");
    //         }
            
    //         $auth = new \App\Models\Authenticate($this->container->dbh);
    //         $auth->unblockuser($params);
    //         $message = [
                
    //             "success" => "User has been unblocked"
    //         ];
    //         return $this->container->view->render($response, 'message.twig', array("message" => $message));
            
    //     } catch (Exception $e) {
            
    //         echo "Caught Exception : ". $e->getMessage();
    //     }
    // }
        
//     public function getActivateUserById($request, $response){

//         $params = $request->getParams();
//         $params['id'] = $request->getAttribute('user_id');

//         try{
            
//             if((empty($params)) && (!is_numeric($params['id'])) && ($params['id'] <= 0)){
                
//                 throw new Exception("Invalid empty User and account id");
//             }
            
//             //Oauth Token Validation
//             //This validation is needed only after the User signed-in
// //            $oauth2 = new \App\Oauth2\oauth2($this->container->dbh);
// //            $oauth_status = $oauth2->checkUserOauthToken($params['id']);
// //            var_dump($oauth_status);
// //
// //            if(!$oauth_status){
// //                $_SESSION['message'] = "Please Sign In Once Again";
// //                return $response->withRedirect($this->container->router->pathFor('auth.signin'));
// //            }
            
//             // User Activation
//             $user = new \App\Models\User($this->container->dbh);
//             $result = $user->activateUser($params);

//             if(!is_bool($result) || !$result){
//                 $_SESSION['message'] = "Something went wrong while activating your account :(";
//             }
            
//             $_SESSION['message'] = "Your account is activated :)";
//             return $response->withRedirect($this->container->router->pathFor('auth.signin'));
            
//         } catch (Exception $ex) {

//         }
//     }
}
