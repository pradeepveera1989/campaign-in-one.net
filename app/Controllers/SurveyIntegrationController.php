<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//namespace App\Controllers\CampaignSettings;

namespace App\Controllers;

use Slim\Http\UploadedFile;

/**
 * Description of SurveyIntegrationController
 *
 * @author Pradeep
 */
class SurveyIntegrationController extends Controller
{

    /*
     * getCampaignSurveyById
     *
     * @param   $request : Get all the requests
     *          $response : Get all response params
     *
     * @return :
     *          Get request to the params to From in DB through Models and
     *          redirect to Survey template.
     *          Empty params    :   redirect to survey.twig.
     *          Survey Details  :   redirect to survey_with_values.twig
     */
    public function getCampaignSurveyById($request, $response)
    {

        // Array of Required Parameters from URL
        $req_attrs = ['camp_id', 'acc_id', 'sury_id', 'ques_id'];

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $params = $request->getParams();
        foreach ($req_attrs as $attr) {
            $params[$attr] = $request->getAttribute($attr);
        }
        $s_obj = new \App\Models\Survey(
            $this->container->dbh,
            $params['acc_id'],
            $params['camp_id'],
            $params['sury_id'],
            $params['ques_id']
        );
        $survey_details['details'] = $s_obj->getSurvey($params);

        $survey_details["camp_id"] = $params["camp_id"];
        $survey_details["acc_id"] = $params["acc_id"];

        //redirect twig according to Survey Details
        if (empty($survey_details['details']['questions'])) {
            return $this->container->view->render(
                $response,
                '/campaign_settings/survey/survey.twig',
                array('survey' => $survey_details)
            );
        } else {
            return $this->container->view->render(
                $response,
                '/campaign_settings/survey/survey_with_values.twig',
                array('survey' => $survey_details)
            );
        }
    }

    /*
     * postCampaignSurveyById
     *
     * @param  $request : Get all the requests
     *         $response : Get all response params
     *
     * @return :
     *         Post request all the params to insert in DB through Models and
     *         redirect to Survey Update template.
     */
    public function postCampaignSurveyById($request, $response)
    {

        // Array of Required Parameters from URL
        $req_attrs = ['camp_id', 'acc_id', 'sury_id', 'ques_id'];

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        $params = $request->getParams();
        $params['uploaded_files'] = $request->getUploadedFiles();
        $params['image_dir'] = $this->container['upload_path'];
        $params['upload_url'] = $this->container['upload_url'];
        foreach ($req_attrs as $attr) {
            $params[$attr] = $request->getAttribute($attr);
        }
        $s_obj = new \App\Models\Survey(
            $this->container->dbh,
            $params['acc_id'],
            $params['camp_id'],
            $params['sury_id'],
            $params['ques_id']
        );
        $status = $s_obj->putSurvey($params);

        if (is_bool($status) && $status == true) {
            $survey_details['details'] = $s_obj->getSurvey($params);
        }

        $survey_details["camp_id"] = $params["camp_id"];
        $survey_details["acc_id"] = $params["acc_id"];

        //redirect twig according to Survey Details
        if (empty($survey_details['details']['questions'])) {
            return $this->container->view->render(
                $response,
                '/campaign_settings/survey/survey.twig',
                array('survey' => $survey_details)
            );
        } else {
            return $this->container->view->render(
                $response,
                '/campaign_settings/survey/survey_with_values.twig',
                array('survey' => $survey_details)
            );
        }
    }

    /*
     * deleteCampaignSurveyQuestionById
     *
     * @param  $request : Get all the requests
     *         $response : Get all response params
     *
     * @return :
     *          redirect to Survey Update template.
     */
    public function deleteCampaignSurveyQuestionById($request, $response)
    {

        // Array of Required Parameters from URL
        $req_attrs = ['camp_id', 'acc_id', 'sury_id', 'ques_id'];
        
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        foreach ($req_attrs as $attr) {
            $params[$attr] = $request->getAttribute($attr);
        }

        // Create Survey Object
        $s_obj = new \App\Models\Survey(
            $this->container->dbh,
            $params['acc_id'],
            $params['camp_id'],
            $params['sury_id'],
            $params['ques_id']
        );
        $status = $s_obj->deleteQuestionById($params);
        if (is_bool($status) &&  $status = false) {
            // Set the error Message;
        }
        // Redirect the page to getSurvey
        return $response->withRedirect($this->container->router->pathFor('getSurvey', $params));
    }

    /*
     * postCampaignSurveyQuestionById
     *
     * @param  $request : Get all the requests
     *         $response : Get all response params
     *
     * @return :
     *          redirect to Survey Display Template
     */
    public function postCampaignSurveyQuestionById($request, $response)
    {

        // Array of Required Parameters from URL
        $req_attrs = ['camp_id', 'acc_id', 'sury_id', 'ques_id'];

        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        // get all the fields
        $params = $request->getParams();
        $params['uploaded_files'] = $request->getUploadedFiles();
        $params['image_dir'] = $this->container['upload_path'];
        $params['upload_url'] = $this->container['upload_url'];
        foreach ($req_attrs as $attr) {
            $params[$attr] = $request->getAttribute($attr);
        }
        // Create Survey Object
        $s_obj = new \App\Models\Survey(
            $this->container->dbh,
            $params['acc_id'],
            $params['camp_id'],
            $params['sury_id'],
            $params['ques_id']
        );
        $status = $s_obj->updateQuestionById($params);
        if (is_bool($status) && $status != true) {
            // Error Message
        }
        // Create Survey Object
        return $response->withRedirect($this->container->router->pathFor('getSurvey', $params));
    }

    /*
     * getCampaignSurveyQuestionById
     *
     * @param  $request : Get all the requests
     *         $response : Get all response params
     *
     * @return :
     *          redirect to Survey Update template.
     */
    public function getCampaignSurveyQuestionById($request, $response)
    {

        // Array of Required Parameters from URL
        $req_attrs = ['camp_id', 'acc_id', 'sury_id', 'ques_id'];
        $sury = [];
        // Check the Authencity of the User
        if (!$this->getUserAuthenticity()) {
            $_SESSION['message'] = "Could not verify your account";
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        foreach ($req_attrs as $attr) {
            $sury[$attr] = $request->getAttribute($attr);
        }
        // Get the Survey Question from Model
        $s_obj = new \App\Models\Survey(
            $this->container->dbh,
            $sury['acc_id'],
            $sury['camp_id'],
            $sury['sury_id'],
            $sury['ques_id']
        );
        $sury['question'] = $s_obj->getSurveyQuestionById($sury);

        // Redirect the Question detials
        return $this->container->view->render(
            $response,
            '/campaign_settings/survey/survey_update.twig',
            array('survey' => $sury)
        );
    }
}
