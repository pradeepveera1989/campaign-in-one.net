<?php

/*
 * To Visualize all leads for the 'campaigns.cio_leads'
 * @author Pradeep
 */


class leadview {
    
    private $dbObject;
    
    public function __construct() {
        $this->table = "`cio.leads`";
        $this->dbObject = new db();
        $this->config = new config();
    }

    public function createLeadView($params){
        $lead_params = array();
        $return = 0;
        $operation = "insert";
        
        var_dump($this->config);
        die();
        
        if(empty($params)){
            
            return $return;
        }
        
        if(!$this->dbObject){
            
            return $return;
        }
        
        $lead_params = $this->mapParams($params);
        //Merge the log Created Params
        $lead_params = array_merge($lead_params, $this->getLogCreatedParams()); 
        //Merge Log Update Params
       // $lead_params = array_merge($lead_params, $this->getLogUpdatedParams()); 
        //Merge Campaign Id
        $lead_params = array_merge($lead_params, $this->getCampaignId());  
        //Merge Company Id
        $lead_params = array_merge($lead_params, $this->getCompanyId());   
        
        $this->dbObject->execute($this->table, $lead_params, "", $operation);
               
    }
    
    private function checkLeadTrackingRefForAccount(){
        
        
    }
    
    private function getLeadTrackingRef(){
        
        
    }
    
    private function getCampaignId(){
        
        return [
            "campaign_id" => 1
        ];
    }
    
    private function getCompanyId(){
        
        return [
            "company_id" => 1
        ];
    }
    
    private function generateTrackingCode(){
        
    }
    
    private function getLogCreatedParams(){
        
        $log_create= array(
            "created"=> date('Y-m-d H:i:s', time()),
            "created_by" => 0
        );
        return $log_create;
    }
    
    private function getlogUpdatedParams(){
        
        $log_update = array(
            "update" => date('Y-m-d H:i:s', time()),
            "updated_by" => 0
        );
        
        return $log_update;
    }
    
    private function mapParams($params){
        
        if(empty($params)){
            
            return false;
        }
        $lead_params = array();
        $mapping = array(
            "anrede" => "salutation",
            "vorname" => "first_name",
            "nachname" => "last_name",
            "email" => "email",
            "plz" => "postal_code",
            "telefon" => "phone",
            "ort" => "city",
            "strasse" => "street",
            "url" => "url",
            "country" => "country",
        );
        $count = count($params);

        foreach ($params as  $key => $value){
            foreach($mapping as $mkey => $mval){
                if($key === $mkey){
                    $lead_params[$mval] = $value;
                }
            }
        }
        return $lead_params;     
    }
}
