
        $(document ).ready(function() {
            $('.errorplz').hide();
            
            //Parse config.ini file 
                        
           // var autofill_postal_code = "<?= $config[Postal_Code][Autofill];?>";
           // console.log("autofill", autofill_postal_code);
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){                
                    city = checkZipCode(zip);
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyC5oWnvqIPHFPQF6Zp7ooEj0QxSAYhOwv0&components=country:DE';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }
           
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
            }); 
        }); 