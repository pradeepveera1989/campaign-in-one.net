var classcom__maileon__api__reports___unique_conversion =
[
    [ "toString", "classcom__maileon__api__reports___unique_conversion.html#a51a827c2fcdd159488ce5b4a2e9b0def", null ],
    [ "fromXML", "classcom__maileon__api__reports___unique_conversion.html#a25f7fb634b102b32e8a2ba39f7cf52f6", null ],
    [ "toCsvString", "classcom__maileon__api__reports___unique_conversion.html#a799dd0d8c52473648c0f5cf46055f8f6", null ],
    [ "toXML", "classcom__maileon__api__reports___unique_conversion.html#a39fe9b0d27d96668734ca9ced031b84b", null ],
    [ "$contactId", "classcom__maileon__api__reports___unique_conversion.html#a82a09b41075c2af61c15d67eb2115b06", null ],
    [ "$contactEmail", "classcom__maileon__api__reports___unique_conversion.html#a96fe4b418df5aa04416a8ec1acfb7cb1", null ],
    [ "$revenue", "classcom__maileon__api__reports___unique_conversion.html#a6918d9deb0e18bbd072e28f40e532b8b", null ],
    [ "$countTotal", "classcom__maileon__api__reports___unique_conversion.html#a704fb3f1fefa3152b56fe0a8b6b05e2b", null ],
    [ "$countUnique", "classcom__maileon__api__reports___unique_conversion.html#a6aad3e7dc1df20e49a22141b2df8cc8b", null ]
];