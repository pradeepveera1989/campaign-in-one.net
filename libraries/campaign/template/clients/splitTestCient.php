<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of splitTest-client
 *
 * @author Pradeep
 */
class splitTestClient
{
    //put your code here
    
    public function __construct($spTestName, $varients)
    {
        $this->spTestName = $spTestName;
        $this->varients = $varients;
        if (!$this->initialize()) {
            throw new \Exception("Could not initialize Split Test Functionalitiy for this Campaign");
        }
    }

    /**
     * @Function  initialize.
     *
     * @Parameters  -
     *
     * @Description Intializes all the required Class objects.
     *
     * @return Boolean
     *      TRUE    : If all the objects are initialized
     *      FALSE   : Something went wrong in intializing the Class Objects.
     */
    private function initialize()
    {
        if (!isset($this->spTestName) || !isset($this->varients)) {
            return false;
        }
        
        $this->spTestObj = new splitTest($this->spTestName, $this->varients);
        $this->config = new config('config.ini');
        $this->config_split = $this->config->getConfigSplitTest();
        $this->leads = new leads();
        $this->gfunc = new generalFunctions();
        
        if (!isset($this->spTestObj) || !isset($this->config) || !isset($this->config_split)) {
            return false;
        }
        if (!isset($this->leads) || !isset($this->gfunc)) {
            return false;
        }
        
        return true;
    }


    /**
     * @Function  getRedirectURL.
     *
     * @Parameters  -
     *
     * @Description Get the splitTest URL after validating following conditions
     *      1. If Wining Version is Specified
     *      2. StikyVersion based on Cookie
     *      3. StikyVersion based on IP
     *      4. Get the new SplitTest version
     *
     * @return String | Boolean
     *      String  : Redirect URL
     *      FALSE   : If SplitTest is not configured properly.
     */
    public function getRedirectURL($currentURL)
    {
        $this->dir = $currentURL;
        $redirectURL = "";
        $url_no_params = $this->gfunc->getCurrentURLNoParams();
        $url_params = $this->gfunc->getCurrentURLParams();
        $url_params = isset($url_params) ? "?".$url_params : "";
        
        if ($this->varients <= 0 || strlen($this->spTestName) == 0 || strlen($this->dir) <= 0 || $url_no_params == false) {
            return false;
        }
        
        if ($this->config_split['CreateVariants'] && !$this->createVarientFolder()) {
            return false;
        }

        // Check for Winning Version.
        if ($this->checkWinningVersion()) {
            $redirectURL = $url_no_params . $this->config->getConfigSplitTest()['Name'].'/'.$this->config->getConfigSplitTest()['EndSplitTest'] . $url_params;
        } elseif ($this->checkStikyVersionOnCookie()) {
            //Check for StikyVersionOnCookie
            $redirectURL = $this->gfunc->getCookieAsSplitVersion() . $url_params;
        } elseif ($this->checkStikyVersionOnIPAdress()) {
            //Check for StikyVersionOnIPAdress
            $final_url = $this->leads->getCampaignFinalURL(['ip'=>$_SERVER['REMOTE_ADDR'], 'url'=>$url_no_params])['final_url'];
            ;
            $final_url_no_params =  explode("?", $final_url)[0];
            
            if (filter_var($final_url_no_params, FILTER_VALIDATE_URL) == false) {
                $redirectURL = $url_no_params . $this->sp_link . $url_params;
            } else {
                $redirectURL = $final_url_no_params . $url_params;
            }
        } elseif ($this->createSplitTest()) {
            // Create SplitTest
            $redirectURL = $url_no_params . $this->sp_link . $url_params;
        }

        return $redirectURL;
    }


    /**
     * @Function  createSplitTest.
     *
     * @Parameters  -
     *
     * @Description Create a new SplitTest version
     *      1. Checks if the Table is present in DB else create new table
     *      2. Get a new SplitTest version.
     *
     * @return
     */
    private function createSplitTest()
    {
        if (!($this->spTestObj->tableExists())) {
            // Excutes only once while creating the splitTest
            $this->spTestObj->createTable();
            $sp_index = 1;
        } else {
            #Get the current value of split Test index
            $sp_index = $this->spTestObj->getCurrentIndex();
        }

        $sp_new_index = $this->spTestObj->getNewIndex($sp_index);
        $this->sp_link = $this->spTestName . '/' . $sp_new_index . '/index.php';
        
        return true;
    }


    /**
     * @Function  createVarientFolder.
     *
     * @Parameters  -
     *
     * @Description Creates different Varients as mentioned in ini file.
     *              and copies the parent campaign.
     *
     * @return Boolean
     *      TRUE    : Varients are created and copy is successfull.
     *      FALSE   : Something went wrong while creating Varient folder.
     */
    public function createVarientFolder()
    {
        $v = $this->varients;

        while ($v > 0) {
            $folder = $this->spTestName.'/'.$v.'/';
            if (!is_dir($folder)) {
                mkdir($folder, 0777, true);
            }
            
            $this->copyDirectory($this->dir, $folder, $this->spTestName);

            if (!$this->modifyConfigFile($folder.'config.ini')) {
                // something went wrong
                return false;
            }

            $v = $v - 1;
        }
        return true;
    }
    
    /* Function Name : copyDirectory
     *
     * Parameters :
     *    1. Source Path
     *    2. Destination Path
     *    3. Name of the splitTest
     *
     * Returns :
     *
     */
     
    public function copyDirectory($source, $destination, $spTestName)
    {
        
        # Creates destination directory
        if (!is_dir($destination)) {
            mkdir($destination, 0777, true);
        }
        $dir  = opendir($source);
        
        #Recursively copies all the files and folders.
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($source . '/' . $file)) {
                    if ($file !== $spTestName) {
                        $this->copyDirectory($source . '/' . $file, $destination . '/' . $file);
                    }
                } else {
                    copy($source . '/' . $file, $destination . '/' . $file);
                }
            }
        }
    }
    
    /* Function Name : modifyFile
     *
     * Parameters :
     *    1. config.ini file path for each Varient
     *
     * Returns :
     *    Success :  true
     *    Failure :  false
     */
    
    public function modifyConfigFile($file)
    {
        $filecontent = file_get_contents($file, true);
        $dir = getcwd();
        $dir .= '/'.$file;
        $status = true;
        
        if (isset($filecontent)) {
            $filecontent .= "\n\n[Varient] \n VarientFile = true \n";
        } else {
            var_dump("Unable to read the contents from config.ini from", $dir);
            $status = false;
        }
        if (!file_put_contents($file, $filecontent)) {
            var_dump("Unable to write the contents to config.ini at", $dir);
            $status = false;
        }
        
        return $status;
    }
    

    /**
     * @Function  checkStikyVersionOnCookie.
     *
     * @Description Checks the following
     *          1. Cookie value is similar to Current URL and also mataches the Splittest name
     *          2. StikyVersionOnCookie is True or False
     *          3. Trade mode is True or False.
     *
     * @return boolen
     *      True : Cookie present, StickyVersionOnCookie is True and Trademode is False
     *      False: If any of the above condition fail.
     */
    private function checkStikyVersionOnCookie()
    {
        $status = false;
        $url_no_params = $this->gfunc->getCurrentURLNoParams();
        $cookie_url = $this->gfunc->getCookieAsSplitVersion();
        $sticky_cookie = $this->config->getConfigSplitTest()['StikyVersionOnCookie'];

        // Check for TradeShowMode
        if ($this->config->getConfigTradeshowMode()) {
            return false;
        }
        
        if (!isset($cookie_url) || ($cookie_url == false) || $url_no_params == false) {
            return false;
        }
        // Check if the CookieURL is similar to current URL
        if (($cookie_url == $url_no_params) || (strstr($cookie_url, $url_no_params) == false) ||  (strpos($cookie_url, $this->spTestName) == false)) {
            return false;
        }
        // Check for StickVersionCookie
        if (isset($sticky_cookie) && ($sticky_cookie== true)) {
            $status = true;
        }

        return $status;
    }


    /**
     * @Function  checkStikyVersionOnIPAdress.
     *
     * @Description Checks the following
     *          1. Final URL is similar to Current URL and also mataches the Splittest name
     *          2. StikyVersionOnIPAdress is True or False
     *          3. Trade mode is True or False.
     *
     * @return boolen
     *      True : IPaddress is found, StikyVersionOnIPAdress is True and Trademode is False
     *      False: If any of the above condition fail.
     */
    private function checkStikyVersionOnIPAdress()
    {
        $status = false;
        $url_no_params = $this->gfunc->getCurrentURLNoParams();
        $final_url = $this->leads->getCampaignFinalURL(['ip'=>$_SERVER['REMOTE_ADDR'], 'url'=>$url_no_params])['final_url'];
        $final_url_no_params =  explode("?", $final_url)[0];

        $sticky_ip = $this->config->getConfigSplitTest()['StikyVersionOnIPAdress'];

        // Check for TradeShowMode
        if ($this->config->getConfigTradeshowMode()) {
            return false;
        }
        // Validate Final URL and URL
        if (($url_no_params == false) || filter_var($final_url_no_params, FILTER_VALIDATE_URL) == false) {
            return false;
        }

        // Validate Final URL
        if (($final_url_no_params == $url_no_params) || (strstr($final_url_no_params, $url_no_params) == false) || (strpos($final_url_no_params, $this->spTestName) == false)) {
            return false;
        }
        
        // Check for StickyVersionIPAdress
        if ((isset($sticky_ip)) && ($sticky_ip == true)) {
            $status = true;
        }
        
        return $status;
    }
    

    /**
     * @Function  checkWinningVersion.
     *
     * @Description Checks the following
     *      1. EndSplitTest is specified
     *      2. Verion of the Cmapaign has to be Parent
     *
     * @return boolen
     *      True : EndSplitTest is not empty and Verion of Campaign is Parent
     *      False: If any of the above condition fail.
     */
    private function checkWinningVersion()
    {
        $status = false;
        $split_final_version = filter_var($this->config->getConfigSplitTest()['EndSplitTest'], FILTER_SANITIZE_STRING);
        $parent = filter_var($this->config->getConfigSplitTest()['Version'], FILTER_SANITIZE_STRING);

        if (!empty($split_final_version) && $parent == "Parent") {
            $status = true;
        }
        
        return $status;
    }
}
