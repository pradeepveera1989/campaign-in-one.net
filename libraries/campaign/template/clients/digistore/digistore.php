<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of digistore
 *
 * @author Pradeep
 */
class digistore
{
    
    
    //put your code here
    public $email;
    public $product_id;
    private $api;
    private $config_digistore;
    private $digi;
    
    public function __construct()
    {
        if (!$this->initialize()) {
            throw new \Exception("Could not initialize Digistore for this Campaign");
        }
    }
    
    
    private function initialize()
    {
        $status = false;
        $config = new config('config.ini');
        $this->config_digistore = $config->getConfigDigistore();
        $this->config_mailinone = $config->getConfigMailInOne();
        if (empty($this->config_digistore) || empty($this->config_mailinone)) {
            return $status;
        }
        
        $this->digi = DigistoreApi::connect($this->config_digistore['digistore_api_key']);
        if (empty($this->digi)) {
            return $status;
        }
        $status = true;

        return $status;
    }
    
    
    
    private function get_product_id()
    {
        $product_id = [
            'product_id' => 0
        ];
        
        if (!isset($this->config_digistore['product_id'])) {
            return $product_id;
        }
        $product_id['product_id'] = $this->config_digistore['product_id'];
        
        return $product_id;
    }
    
    
    
    private function get_buyer_detials($params)
    {
        $buyer = [];
        if (empty($params) || !filter_var($params[$this->config_digistore['buyer']['email']], FILTER_VALIDATE_EMAIL)) {
            return $buyer;
        }
               
        $config_digi_buyer = $this->config_digistore['buyer'];
        
        foreach ($config_digi_buyer as $key => $value) {
            if (!empty($params[$value])) {
                $buyer[$key] = $params[$value];
            }
        }
        
        return $buyer;
    }
    
    
    
    private function get_payment_plan()
    {
        $payment_plan = [];
        
        $config_payment_plan = $this->config_digistore['payment_plan'];
        foreach ($config_payment_plan as $key => $value) {
            if (!empty($value)) {
                $payment_plan[$key] =  $value;
            }
        }
        return $payment_plan;
    }
    
    
    
    private function get_link_validity()
    {
        $validity = [
            'valid_until' => 0
        ];
        
        if (empty($this->config_digistore['valid_until'])) {
            $validity['valid_until'] = '24h';
        }
        
        return $validity;
    }
    
    
    
    public function get_digistore_buyurl($params)
    {
        $buy_url = "";
        
        if (empty($params) || empty($params['email'])) {
            return $buy_url;
        }
        $this->product_id = $this->get_product_id();
        $this->info_buyer = $this->get_buyer_detials($params);
        $this->info_payment = $this->get_payment_plan();
        $this->valid_until = $this->get_link_validity(); // expires two hours after creation
        $urls = array();
        $placeholders = array();
        $settings = array();

        $arguments = [
            '0' => $this->config_digistore['product_id'],
            '1' => $this->info_buyer,
            '2' => $this->info_payment,
            '3' => array(),
            '4' => array(),
            '5' => array(),
            '6' => array(),
            '7' => array()
         ];

        $data = $this->digi->__call("createBuyUrl", $arguments);
        if (!empty($data) && filter_var($data->url, FILTER_VALIDATE_URL)) {
            $buy_url  = $data->url;
        }
        
        return $buy_url;
    }
}
