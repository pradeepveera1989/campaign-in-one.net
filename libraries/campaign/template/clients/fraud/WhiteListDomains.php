<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of campaign
 *
 * @author Pradeep
 */

class WhiteListDomains
{
    public function __construct()
    {
        $this->table = "`cio.whitelist_domains`";
        $this->dbObject = new db();
    }

    /**
     * checkWhiteListDomainName function
     * Check whether the given domain name is present in whitelist Domain or not.
     *
     * @param string $domain_name
     * @return boolean
     */
    public function checkWhiteListDomainName(string $domain_name) : bool
    {
        $status = false;
        if (empty($domain_name) || !isset($domain_name)) {
            return $status;
        }

        $result = $this->getWhiteListDomainByName($domain_name);

        if (is_array($result) && !empty($result) && isset($result['id']) && (int) $result['id'] > 0) {
            $status = true;
        }

        return $status;
    }


    /**
     * getWhiteListDomainByName function
     * Get White List domain details by given domain name
     *
     * @param [type] $domain_name
     * @return array
     */

    private function getWhiteListDomainByName($domain_name) : array
    {
        $return = [];
        if (empty($domain_name) || !isset($domain_name) || empty($this->dbObject) || !isset($this->table)) {
            return $return;
        }

        $select = array('*');
        $where = "`domain_name` LIKE '$domain_name'";

        $result = $this->dbObject->getOne($select, $this->table, $where);

        if (is_array($result) && !empty($result)) {
            $return = $result;
        }

        return $return;
    }
}
