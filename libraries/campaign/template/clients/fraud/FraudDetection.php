<?php

/**
 * FraudDetection
 *
 * Verifies the Fraud leads based on White and Black List Domains.
 * Updates fraud email to 'cio.fraud' DB table
 * php version 7.2.10
 *
 * @category  FraudDetection
 * @package   FraudDetection
 * @author    Pradeep Veera <pradeep.veera@treaction.net>
 * @copyright 2015 treaction.net
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://treaction.net
 * @since     1.4.0
 */

 
class FraudDetection
{
    public $email;
    private $table;
    private $domain;
    private $type;
    private $ip;

    /**
     * __construct function
     */
    public function __construct()
    {
        $this->table = "`cio.fraud`";
        $this->setUpConfig();
        $this->setUpDB();
        $this->setUp();
    }

    private function setUp()
    {
        spl_autoload_register('Autoload::fraudloader');
        $this->wht_obj = new WhiteListDomains();
        $this->blk_obj = new BlackListDomains();
    }

    private function setUpDB()
    {
        $this->dbObject = new db();
    }

    private function setUpConfig()
    {
        $this->config = new config("config.ini");
    }


    /**
     * getDomainFromEmail function
     * Parse the Email address and return Domain Name
     *
     * @return bool
     */
    private function getDomainFromEmail() : bool
    {
        $status = false;
        if (!isset($this->email) || empty($this->email)) {
            return $status;
        }
        $domain = substr(strrchr($this->email, "@"), 1);
        if (is_string($domain) && !empty($domain) && isset($domain)) {
            $this->domain = $domain;
            $status = true;
        }

        return $status;
    }


    /**
     * validateEmailAddr function
     *
     * @param string $email
     * @return bool
     */
    private function isEmailAddrValid(string $email) :bool
    {
        $is_valid = false;
        if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
            $is_valid = true;
        }

        return $is_valid;
    }

    /**
     * getBrowserNameAndClientOs function
     * Returns Browser Name and Os of the Optin trying to fraud.
     *
     * @return array
     */
    private function getBrowserNameAndClientOs() :array
    {
        $browser = [];
        spl_autoload_register('Autoload::generalFunctionLoader');
        $func = new generalFunctions();
        if (empty($func) || empty($this->config)) {
            return $browser;
        }
        $conf_mapping = $this->config->getConfigMailInOne()['Mapping'];
        // Get Browser Details
        $result = $func->getBrowserDetails();
        if (is_array($result) && !empty($result)) {
            $browser[$conf_mapping['DeviceBrowser']] = $result[$conf_mapping['DeviceBrowser']];
            $browser[$conf_mapping['DeviceOs']] = $result[$conf_mapping['DeviceOs']];
        }

        return $browser;
    }


    /**
     * getTimeStamp function
     * Returns Current Timestamp using Date Lib
     *
     * @return void
     */
    private function getTimeStamp()
    {
        $time_stamp = date('Y-m-d H:i:s', time());
        return $time_stamp;
    }


    /**
     * getDomainCountForCampaign function
     *
     * @param string $domain
     * @return integer
     */
    private function getDomainCountForCampaign(string $domain, bool $today = false) : int
    {
        $count = -1;

        if (empty($domain) || !isset($domain) || !isset($this->dbObject)) {
            return $count;
        }

        $date = date('Y-m-d', time()); // Get today date.
        $sql = "SELECT count(*) FROM `cio.leads` WHERE `email` LIKE '%$domain%' ";
        $sql .= (is_bool($today) && $today) ? " AND `created` LIKE '%$date%'" : "";

        $result = $this->dbObject->getAll($sql);

        if (is_array($result) && (int)$result[0]["count(*)"] >= 0) {
            $count = (int)$result[0]["count(*)"];
        }

        return $count;
    }


    /**
     * getIpAddr function
     * Returns Ipaddress from $_SERVER['REMOTE_ADDR']
     *
     * @return bool
     */
    private function getIpAddr() : bool
    {
        $status = false;
        $ip_addr = $_SERVER['REMOTE_ADDR'];
        if (isset($ip_addr) && filter_var($ip_addr, FILTER_VALIDATE_IP)) {
            $this->ip = $_SERVER['REMOTE_ADDR'];
            $status = true;
        }
        return $status;
    }


    /**
     * getCampaignDetials function
     *
     * @return array
     */
    private function getCampaignDetials() : array
    {
        $camp_details = [];

        spl_autoload_register('Autoload::campaignloader');
        $camp_obj = new campaign();
        $details = $camp_obj->getCampaignDetailsByURL();
        if (is_array($details) && !empty($details)) {
            $camp_details = $details;
            $status = true;
        }

        return $camp_details;
    }


    /**
     * checkIpAddrOfLead function
     * Validates the IpAddr of the Lead.
     * Whether the IpAddr Optins reached maximum limit.
     *
     * @return boolean
     */
    private function isIpAddrLimitExceeded() :bool
    {
        $is_ip_exceeded = false;
        // Create lead Object
        spl_autoload_register('Autoload::leadsloader');
        $lead_obj = new leads();
        if (!isset($lead_obj) || !$this->getIpAddr()) {
            return $is_ip_exceeded;
        }
        // Get Config Settings
        $config_settings = $this->config->getConfigGeneralSettings();
        // Get campaign URL
        $camp_url = $this->getCampaignDetials()['url'];
        // Get IpAddr
        $duplicate_ip = $lead_obj->getLeadDetailsByIPAddress($this->ip, $camp_url, true);
        // Check for Duplicate IpAddr
        if (count($duplicate_ip) >= (int)$config_settings['MaximumOptInsPerIPandDay']) {
            $is_ip_exceeded = true;
        }

        return $is_ip_exceeded;
    }


    /**
     * checkFraudLimit function
     * Validates the FraudLimit for the Optins.
     *
     * @return bool
     */
    private function isFraudLimitExceeded() :bool
    {
        $is_exceeded = false;
        $count = 0;
        if (!isset($this->domain) && !isset($this->config)) {
            return $is_exceeded;
        }

        // Get the max limit of optins per day
        $max_limit = $this->config->getMaxUnknownDomainsOptinsPerDay();
        $count = $this->getDomainCountForCampaign($this->domain, true);
        $is_exceeded = ($count>= 0 && $count < $max_limit) ? false : true;
        
        return $is_exceeded;
    }


    /**
     * checkBlackListDomain function
     * Validates whether domain is in Blacklist.
     *
     * @return boolean
     */
    private function isBlackListDomain() :bool
    {
        $status = false;
        if (!isset($this->domain) || !isset($this->blk_obj)) {
            return $status;
        }
        // Check for Blacklist Domain
        $is_blk_domain = $this->blk_obj->checkBlackListDomainName($this->domain);
        if (is_bool($is_blk_domain) && $is_blk_domain) {
            $status = $is_blk_domain;
        }

        return $status;
    }

    /**
     * getFraudType function
     *
     * @return string
     */
    private function getFraudType() : string
    {
        $status = false;
        // BlackList Domain
        if ($this->isBlackListDomain()) {
            $this->type = "Blacklist Domain";
            $status = true;
        } elseif ($this->isEmailAddrFraud()) {
            $this->type = "Fraud Email";
            $status = true;
        } elseif ($this->isFraudLimitExceeded()) {
            $this->type = "Fraud Limit";
            $status = true;
        } elseif ($this->isIpAddrLimitExceeded()) {
            $this->type = "IP Block";
            $status = true;
        }

        return $status;
    }

    /**
     * isEmailAddrFraud function
     * Verifies if the Email addr is fraud.
     * example : joshiksuperstar+LOI@gmail.com
     *           joshiksuperstar+14M@gmail.com
     *           joshiksuperstar+LB4@gmail.com
     * @return boolean
     */
    private function isEmailAddrFraud() :bool
    {
        $is_fraud = true;
        $time_stamp = date('Y-m-d', time());
        if (!isset($this->dbObject) || !isset($this->email)) {
            return $is_fraud;
        }
        $parse_email = explode('@', $this->email);
        if (empty($parse_email) || !isset($parse_email[0]) || !isset($parse_email[1])) {
            return $is_fraud;
        }
        $check_plus_sign = explode('+', $parse_email[0]);
        if (count($check_plus_sign) == 1) {
            // $parse_email[0] does not have plus sign.
            $is_fraud = false;
        } else {
            // $parse_email[0] does have plus sign.
            $sql = "SELECT count(*) FROM `cio.leads` WHERE `email` LIKE '%$check_plus_sign[0]%' ";
            $sql .= "AND `created` LIKE '%$time_stamp%'";
            $result = $this->dbObject->getAll($sql);
            if (is_array($result) && (int) $result[0]['count(*)'] < 1) {
                $is_fraud = false;
            }
        }
        return $is_fraud;
    }

    /**
     * insertFraudDetialsToDB function
     *
     * @return void
     */
    private function insertFraudDetialsToDB() : bool
    {
        $status = false;

        if (!isset($this->dbObject) || !isset($this->email) || !isset($this->domain)) {
            return $status;
        }
        if (!isset($this->type) || !$this->getFraudType()) {
            return $status;
        }
        if (!$this->getIpAddr()) {
            return $status;
        }

        $browser = $this->getBrowserNameAndClientOs();
        $time_stamp = $this->getTimeStamp();
        $camp_id = $this->getCampaignDetials()['id'];
        $conf_mapping = $this->config->getConfigMailInOne()['Mapping'];

        $value = [
            'type'  => $this->type,
            'email' => $this->email,
            'time_stamp' => $time_stamp,
            'campaign_id' => $camp_id,
            'domain'    => $this->domain,
            'ip'    => $this->ip,
            'device_browser'   => $browser[$conf_mapping['DeviceBrowser']],
            'device_os' => $browser[$conf_mapping['DeviceOs']]
        ];

        $status = $this->dbObject->execute($this->table, $value, "", "insert");

        return $status;
    }

    /**
     * checkFraudDomain function
     *
     * @param string $email client email id
     *
     * @return bool
     */
    public function isFraudDomain(string $email) : bool
    {
        $is_fraud = true;
        if (!$this->isEmailAddrValid($email) || !$this->getDomainFromEmail()) {
            return $is_fraud;
        }
        if (!isset($this->wht_obj) ||!isset($this->domain) || !isset($this->config)) {
            return $is_fraud;
        }
        // Check if the Domain is in whitelist.
        $wht_lst_status = $this->wht_obj->checkWhiteListDomainName($this->domain);
        if (is_bool($wht_lst_status) && $wht_lst_status) {
            $is_fraud = false;
        } else {
            // Check if the Email is fraud dectected.
            if (!$this->getFraudType()) {
                $is_fraud = false;
            } else {
                // Email is dectected as fraud.
                $this->insertFraudDetialsToDB();
                $is_fraud = true;
            }
        }

        return $is_fraud;
    }
}
