<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of survey
 *
 * @author Pradeep
 */
class survey
{
    public $table;

    public function __construct(int $survey_id)
    {
        $this->table = "`cio.survey`";
        $this->dbObject = new db();

        if ((int)$survey_id > 0) {
            $this->survey_id = $survey_id;
        }
    }


    /**
     * getSurveyDescription function
     *
     * @return array
     */
    public function getSurveyDescription(): array
    {
        $return = [];
        if (empty($this->survey_id)) {
            die("getSurveyDescription");
            return $return;
        }

        $return = $this->getSurveyDetialsById();

        return $return;
    }


    /**
     * isSurveyOnline function
     *
     * @return integer
     */
    public function isSurveyOnline():int
    {
        $s_online = 0;

        if (empty($this->survey_id)) {
            return $s_online;
        }

        $s_detials = $this->getSurveyDetialsById();
        $s_online = $s_detials['survey_is_online'];

        return $s_online;
    }


    public function getSurveyLayoutInfo()
    {
    }


    /**
     * getSurveyDetialsById function
     *
     * @return array
     */
    private function getSurveyDetialsById() :array
    {
        $return = [];

        if (empty($this->survey_id)) {
            die("getSurveyDetialsById");
            return $return;
        }

        $select = array('*');
        $where = "`id` = '" .$this->survey_id."'";
        $table = $this->table;
        $return = $this->dbObject->getOne($select, $table, $where);

        return $return;
    }


    /**
     * getSurveyQuestionById function
     * Get the Survey Question from 'cio.questions' based on question_id.
     *
     * @param integer $ques_id
     * @return array
     */
    public function getSurveyQuestionById(int $ques_id):array
    {
        $question = [];
        if ($ques_id <= 0) {
            return $question;
        }

        $select = array('*');
        $where = "`id` = '" .$ques_id."'";
        $table = '`cio.questions`';
        $question = $this->dbObject->getOne($select, $table, $where);

        return $question;
    }
}
