<?php

/**
 * getSurvey class
 * Get all the survey questions from DB.
 *
 */
class getSurvey extends survey
{
    public $table = "`cio.questions`";
    /**
     * __construct function
     *
     * @param integer $survey_id
     */
    public function __construct(int $survey_id)
    {
        parent::__construct($survey_id);
        $this->table = "`cio.questions`";
    }


    /**
     * getSurveyQuestions function
     * 1. Get the Survey questions from DB for given Survey id.
     * 2. Empty array is returned if survey id is missing.
     *
     * @return array
     *
     */
    public function getSurveyQuestions():array
    {
        $questions = [];
        if (empty($this->survey_id)) {
            die("No survey id at getSurveyQuestions");
            return $questions;
        }

        $sql = "SELECT `cio.questions`.`id` As question_id, `cio.survey`.`layout_status_id`, `cio.questions`.`cio.survey_id`, `cio.questions`.`cio.context_id`,`cio.questions`.`cio.question_types_id` As question_type_id, "
        . "`cio.questions`.`question_question_txt`, `cio.questions`.`question_hint_txt` ,`cio.questions`.`question_json_representation` ,"
        . "`cio.question_types`.`id`,  `cio.question_types`.`question_type_html_tag`,  `cio.question_types`.`question_type_html_template`, "
        . "`cio.question_types`.`question_type_frontend_hint`,  `cio.question_types`.`question_type_description`\n"
        . " FROM `cio.questions`"
        . " INNER JOIN `cio.question_types` ON `cio.questions`.`cio.question_types_id` = `cio.question_types`.`id`\n"
        . " INNER JOIN `cio.survey` ON `cio.questions`.`cio.survey_id` = `cio.survey`.`id`\n"
        . " WHERE `cio.questions`.`cio.survey_id`= " . $this->survey_id;

        $result = $this->dbObject->getAll($sql);

        // Parsing result.
        if (!empty($result)) {
            $questions = $result;
            $i = 0;
            // Twig missing Json decode functionality
            // Hence Json string is converted to Array
            while ($i < sizeof($result)) {
                if (empty($result[$i]['question_json_representation'])) {
                    continue;
                }
                $questions[$i]['question_json_representation'] = (array) json_decode($result[$i]['question_json_representation'], true);
                $i++;
            }
        }

        // Sort the question by Id(Not Id from Database but Id defiend by User)
        usort($questions, array($this, "usortQuestionsById"));

        return $questions;
    }

    /*
     * usortQuestionsById
     *
     * @param  $a : Question Array First
     *         $b : Question Array Second
     *
     * @return : Interger
     *      1     : if 1st array element is less than 2nd array element
     *      -1    : if 1st array element is not less than 2nd array element
     */

    private function usortQuestionsById($a, $b)
    {
        $x = intval($a['question_json_representation']['id']);
        $y = intval($b['question_json_representation']['id']);

        // if x is less than y then it returns -1
        // else it returns 1
        if ($x < $y) {
            return -1;
        } else {
            return 1;
        }
    }


    /**
     * getSurveyQuestion function
     *
     * @param integer $q_id
     * @return array
     */
    public function getSurveyQuestionDetials(int $q_id) : array
    {
        $q_details = [];

        if ($q_id <= 0) {
            die("getSurveyQuestionDetials");
            return $q_details;
        }

        $select = ['*', '`cio.questions`.`id` As question_id' ];
        $table = $this->table . " INNER JOIN `cio.question_types` ON `cio.questions`.`cio.question_types_id` = `cio.question_types`.`id`";
        $where = "`cio.questions`.`id` = $q_id";

        $result = $this->dbObject->getOne($select, $table, $where);

        if (is_array($result) && !empty($result)) {
            $q_details = $result;
        }

        return $q_details;
    }
}
