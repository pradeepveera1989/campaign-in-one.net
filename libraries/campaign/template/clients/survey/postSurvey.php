<?php

/**
 * postSurvey class
 * post all the Survey questions with answers to DB.
 *
 *
 */
class postSurvey extends survey
{
    public $camp_id;
    public $custom_field_type;
    private $cus_f_id;
    private $lead_id;
    public $table;

    /**
     * __construct function
     *
     * @param integer $survey_id
     */
    public function __construct($survey_id, $camp_id)
    {
        parent::__construct($survey_id);
        $this->table = "`cio.answers`";
        $this->camp_id = $camp_id;
        $this->custom_field_type = "Survey";
    }


    /**
     * postSurveyAnswers function
     *
     * @param array $s_ans
     * @return boolean
     */
    public function postSurveyAnswers(array $s_ans) : bool
    {
        $status = false;
        if (empty($this->survey_id) || empty($s_ans)) {
            die("postSurveyAnswers 1");
            return $status;
        }
        // Check for DB Connection
        if (empty($this->dbObject)) {
            die("postSurveyAnswers 2");
            return $status;
        }

        spl_autoload_register('Autoload::leadsLoader');
        $leadObj = new leads();
        $ld_cdta_obj = new LeadCustomData();

        // Get Custom feild id
        $this->cus_f_id = $this->getSurveyCustomFieldId();

        // Get Lead id
        $this->lead_id = $leadObj->getLeadDetailsByEmail($_SESSION['email'])['id'];
        unset($_SESSION['email']);

        // Get Insert query
        $ins_qry = $this->getInsertQuery($s_ans);

        // call Lead custom data for multi insert.
        $result = $ld_cdta_obj->insertMultiRowsCustomData($ins_qry);
        
        if (is_bool($result) && $result == true) {
            $status = $result;
        }

        return $status;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getSurveyCustomFieldId()
    {
        $cus_id = 0;
        if ((int)$this->camp_id <= 0 || empty($this->custom_field_type)) {
            die("getSurveyCustomFieldId");
            return $cus_id;
        }

        spl_autoload_register('Autoload::leadsLoader');
        $cus_Obj = new LeadCustomField();

        $cus_data = $cus_Obj->getCustomField($this->camp_id, $this->custom_field_type);
        if (is_array($cus_data) && empty($cus_data)) {
            $cus_val = $this->getLeadCustomFieldValues();
            $cus_id = $cus_Obj->insertCustomLeadField($cus_val);
        } else {
            $cus_id = $cus_data['id'];
        }

        return $cus_id;
    }

    /**
     * getLeadCustomFieldValues function
     *
     * @return void
     */
    private function getLeadCustomFieldValues()
    {
        $cus_val = [];
        if ((int) $this->camp_id <= 0 || !isset($this->custom_field_type)) {
            return $cus_val;
        }

        $cus_val = [
            'campaign_id' => $this->camp_id,
            'type'  => $this->custom_field_type,
            'datatype'  => "string",
            'regex' => ""
        ];

        return $cus_val;
    }

    /**
     * getInsertQuery function
     *
     * @param array $answers
     * @return string
     */
    public function getInsertQuery(array $answers): string
    {
        $ins_qry = "";
        $count = 0;
        if (empty($answers)) {
            return $ins_qry;
        }

        $gtSurvy_Obj = new getSurvey($this->survey_id);
        # Query Statment.
        $ins_qry = "INSERT INTO `cio.leads_customdata` (`lead_customfield_id`, `lead_id`, `cio.questions_id`, `name`, `type`, `value`) VALUES";
        foreach ($answers as $answer) {
            # Valdiate params
            if (!is_numeric($answer["question_id"]) || !isset($answer["lead_answer"])) {
                die("continue 1");
                continue;
            }
            # Get Question details
            $question =  $gtSurvy_Obj->getSurveyQuestionDetials((int) $answer['question_id']);

            if (!is_array($question) || empty($question)) {
                continue;
            }

            $lead_id = $this->lead_id;
            $lead_customfield_id = $this->cus_f_id;
            $question_id = $question['question_id'];
            $name = $question['question_question_txt'];
            $type = $question['question_type_html_tag'];
            $value_stmt .= $count > 0 ? ',': '';
            if ($type != "checkbox") {
                $value = $answer['lead_answer'];
                $value_stmt .= " ('".$lead_customfield_id."' ,'".$lead_id."','".$question_id."','".$name."','".$type."','".$value."') ";
            } else {
                $count_chx_box = 0;
                foreach ($answer['lead_answer'] as $chck_answer) {
                    $value_stmt .= $count_chx_box > 0 ? ',': '';
                    $value_stmt .= " ('".$lead_customfield_id."' ,'".$lead_id."','".$question_id."','".$name."','".$type."','".$chck_answer."') ";
                    $count_chx_box ++;
                }
            }

            $count++;
        }
        $ins_qry .= $value_stmt;
        $ins_qry .= ';';

        return $ins_qry;
    }
}
