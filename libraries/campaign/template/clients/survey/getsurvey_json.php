<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require "../../vendor/autoload.php";
session_start();

$config = new config('config.ini');
$config_survey = $config->getConfigSurvey();
$email = "";
// // Validate EMail.
// if(isset($_GET['email']) && filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)){
//     $_SESSION['email']= $_GET['email'];
// }

if ($config_survey['Status']) {
    $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .'/'.  explode('/', $_SERVER['REQUEST_URI'])[1];

    if (filter_var($url, FILTER_VALIDATE_URL)) {
        $survey_id = getSurveyIdFromURL($url);

        if ($survey_id > 0) {
            $survey = getSurveyFromId($survey_id) ;
            echo json_encode($survey);
        } else {
            echo "No Survey integrated to this Campaign";
        }
    }
}

/**
 * getSurveyIdFromURL function
 *
 * @param string $url
 * @return integer
 */
function getSurveyIdFromURL(string $url):int
{
    $s_id = 0;

    if (empty($url)) {
        die("getSurveyIdFromURL");
        return $s_id;
    }

    spl_autoload_register('Autoload::campaignLoader');
    $c_obj = new campaign();
    $camp_detials = $c_obj->getCampaignDetailsByURL();

    if (is_array($camp_detials) && !empty($camp_detials)) {
        $s_id = (int)$camp_detials["cio.survey_id"];
    }

    return $s_id;
}


/**
 * getSurveyFromId function
 *
 * @param integer $s_id
 * @return array
 */
function getSurveyFromId(int $s_id): array
{
    $survey = [];
    if ($s_id <= 0) {
        return $survey;
    }

    spl_autoload_register('Autoload::surveyLoader');
    $s_obj = new getSurvey($s_id);
    $result = $s_obj->getSurveyQuestions();

    if (is_array($result) && !empty($result)) {
        $survey = $result;
    }
    
    return $survey;
}
