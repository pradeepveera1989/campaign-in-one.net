<?php

/*
* maileonApiClient.php
*
* MailonApiClient used to create contact service or transactions.
*   1. Inserts or updates a contact data
*   2. Creates a contact Event.
*/
#require '../../vendor/autoload.php';
class maileonApiClient
{
    public $mailinone;
    public $doi;
    public $config;
    public $custom_params;
    public $stnd_params;
    public $hubspot_contact_id;
    public $email;

    
       
    
    public function __construct()
    {
        $config = new config("config.ini");
        $this->config_mailinone = $config->getConfigMailInOne();
        $this->config_mailinone_event = $config->getConfigMailInOneEvent();
        $this->getConfigParams();
    }

    /**
     * @Function  getConfigParams.
     *
     * @Description create config parameter required for Maileon
     *
     * @return void
     *
     */
     
    private function getConfigParams()
    {
        try {
            if (empty($this->config_mailinone)) {
                throw new Exception("Could not Load the Mailinone parameters");
            }
            
            $this->mailinone = [
                'BASE_URI' => 'https://api.maileon.com/1.0',
                'API_KEY' => $this->config_mailinone["Mapikey"],
                'THROW_EXCEPTION' => 'FALSE',
                'TIMEOUT' => 60,
                'DEBUG' => 'FALSE' // NEVER enable on production
            ];
            $this->doi = $this->config_mailinone["DOIKey"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * @Function  createContactService.
     *
     * @Description Initiate Contact Service to insert or update a contact in Maileon
     *
     * @return void
     *
     */
    private function createContactService()
    {
        try {
            if (count($this->mailinone) == 0) {
                throw new Exception("Maileon: Invalid Config Parameters " . PHP_EOL);
            }
            
            $this->contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
            $this->contact_service->setDebug(false);
            
            com_maileon_api_contacts_Permission::init();
            com_maileon_api_contacts_SynchronizationMode::init();
           
            $newContact = new com_maileon_api_contacts_Contact();
            $newContact->email = $this->email;
            $newContact->anonymous = false;
            $newContact->permission = $this->getPermissionForContact();

            foreach ($this->stnd_params as $key => $value) {
                $newContact->standard_fields[$key] = $value;
            }
            
            foreach ($this->custom_params as $key => $value) {
                if ($this->checkCustomField($key)) {
                    $newContact->custom_fields[$key] = $value;
                }
            }
  
            foreach ($this->constant_params as $key => $value) {
                if ($this->checkCustomField($key)) {
                    $newContact->custom_fields[$key] = $value;
                }
            }
            
            foreach ($this->url_params as $p) {
                $l = explode("=", $p);
                if ($this->checkCustomField($l[0])) {
                    $newContact->custom_fields[$l[0]] = $l[1];
                }
            }
            $doi_status = $this->getDOIStatus();

            $this->response = $this->contact_service->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', $doi_status['doi'], $doi_status['doiPlus'], $this->doi);

            if (!($this->response->isSuccess())) {
                $this->response = false;
                throw new Exception("Maileon: Contact could not be created or updated ".PHP_EOL);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function intiateMarketingAutomation()
    {
        $program_id = $this->config_mailinone["MarketingAutomationId"];
        $email = $this->email;
        if (!is_numeric($program_id) && $program_id < 0) {
            return false;
        }
        $markt_automate = new com_maileon_api_marketingautomation_MarketingAutomationService($this->mailinone);
        $markt_automate->setDebug(false);
        $status = $markt_automate->startMarketingAutomationProgram($program_id, $email);
        return $status;
    }
    
    public function DOIMarketingAutomation($email)
    {
        $program_id = $this->config_mailinone["MarketingAutomationId"];
        if (!is_numeric($program_id) && $program_id < 0) {
            return false;
        }
        $markt_automate = new com_maileon_api_marketingautomation_MarketingAutomationService($this->mailinone);
        $markt_automate->setDebug(false);
        $status = $markt_automate->startMarketingAutomationProgram($program_id, $email);

        return $status;
    }

    /**
  * @Function  createTransaction.
  *
  * @Description Verifies the config and throw an exception if the config is missing.
  *              Initiates the transaction service with respective email adress.
  *
  * @return void
  *
  */
    
    private function createTransaction()
    {
        try {
            if (count($this->mailinone) == 0) {
                throw new Exception("Maileon: Invalid Config Parameters ".PHP_EOL);
            }
            
            $transactionsService = new com_maileon_api_transactions_TransactionsService($this->mailinone);
            $transactionsService->setDebug(false);
            $transaction = new com_maileon_api_transactions_Transaction();
            $transaction->contact = new com_maileon_api_transactions_ContactReference($this->contact_service);
            $transaction->type = $this->type;
            $transaction->contact->email = $this->email;

            $transactions = array($transaction);
            foreach ($this->event_params as $key => $value) {
                $transaction->content[$key] = $value;
            }
            
            $response = $transactionsService->createTransactions($transactions, true, false);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
    * @Function  getContactStatus.
    *
    * @Description Verifies the Maileon where the contact is present or
    *              not based on Email address of the contact.
    *
    * @return contact details on success
    */
        
    private function getContactStatus($email)
    {
        $return = false;
        $contact_service = new com_maileon_api_contacts_ContactsService($this->mailinone);
        $contact_service->setDebug(false);
        $getContact = $contact_service->getContactByEmail($email);
        if (!($getContact->getResult()->id) || $getContact->getStatusCode() != 200) {
            return $return;
        }
        $obj_result = $getContact->getResultXML();   // Convert the Result to XML
        $return = (array)$obj_result;                // Convert OBject to Array
        
        return $return;
    }

    
    /**
     * @Function  getEventParams.
     *            -  Maps the Standard Params to Config ini params
     *
     * @param array $param
     * @return array
     */
    private function getEventParams($params)
    {
        $event_params = $this->config_mailinone_event;
        $event_params = [];
        
        // Check for event type
        if (empty($this->getEventType())) {
            return [];
        }

        // Mapping the event params from ini file
        foreach ($this->config_mailinone_event[EventMapping] as $key => $value) {
            $event_params[$key] = $params[$value];
        }
        
        return $event_params;
    }
    

    /**
     * @Function  getEventType.
     *
     * @param array $param
     * @return param event type
     */
    private function getEventType()
    {
        return $this->config_mailinone_event[ContactEvent];
    }
  
  
    /**
    * @Function  createEventPartnerLead.
    *
    * @Description Intiate the contact event for the Partner Lead.
    *              1. Inserts the contact in the system if the contact is missing.
    *              2. Intiate the tranaction service with Partner contact Email adress
    *
    * @return array of parameter
    *
    */
    public function createEvent($params)
    {
        $this->type = $this->getEventType($params);
        try {
            // Contact needs to be updated all the time.
            $this->insertContact($params);
            if (!$this->response->isSuccess()) {
                return false;
            }
            $this->type = $this->getEventType($params);
            $this->event_params = $this->getEventParams($params);
            if (!empty($this->event_params)) {
                $this->createTransaction();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        
        return true;
    }

  
    /**
    * @Function  getStandardParams.
    *
    * @Description Maps the Standard parameters required to create a contact in Maileon
    *
    * @return array of parameter
    *
    */
    private function getStandardParams($param)
    {
        $return = false;
        
        $stand_parms = $this->config_mailinone[Mapping];
        
        if (empty($param[$stand_parms[EMAIL]])) {
            return $return;
        }
        $this->email = $param[$stand_parms[EMAIL]];
        
        return array(
            "SALUTATION" => $param[$stand_parms[SALUTATION]],
            "FIRSTNAME" => $param[$stand_parms[FIRSTNAME]],
            "LASTNAME" => $param[$stand_parms[LASTNAME]],
            "ADDRESS" => $param[$stand_parms[ADDRESS]],
            "ZIP" => $param[$stand_parms[ZIP]],
            "CITY" => $param[$stand_parms[CITY]]
        );
    }

    private function checkCustomField($field)
    {
        if ((!$this->contact_service) && !empty($field)) {
            return false;
        }
        
        $getCustomFields = $this->contact_service->getCustomFields(); #Get all the Custom fields
        $this->custom_fields = array_keys($getCustomFields->getResult()->custom_fields);
       
        # Return true if the contact field is present
        $serach_index = array_search($field, $this->custom_fields);

        if (is_numeric($serach_index) && $serach_index >= 0) {
            return true;
        }
        
        # if the contact field is not present
        return false;
    }
    
    
    private function getCustomParams($params)
    {
        $mapping_parms = $this->config_mailinone[Mapping];
        $pv = array_values($params);                # Get the values of parms
        $pk = array_keys($params);                  # Get the keys of parms
        $return = array();
        $i = 0;
        
        while ($i <= count($pk)) {
            $search_value = array_search($pk[$i], $mapping_parms);  # Search the key value in Mapping parms
            $not_standard = array_search($pv[$i], $this->standard); # Search the key value in Standard parms
            if (!empty(($search_value)) && empty($not_standard)) {
                $return[$search_value] = $pv[$i];                   # Custom keys shoud not be present in Standard parms
            }                                                       # but should be present in Mapping prams
       
            $i++;
        }

        return $return;
    }
    
    private function getConstantParams()
    {
        $constant_params = $this->config_mailinone[Constants];
               
        return $constant_params;
    }
    
    private function getURLParams($params)
    {
        if (empty($params["url_params"])) {
            return false;
        }
        return explode("&", $params["url_params"]);
    }
    
    public function buildURL($params)
    {
        $parameter = '?';
        $feld = array(
           'typ',
           'segment',
           'quelle',
           'trafficsource',
           'partner',
           'ip',
           'datum'
        );
        foreach ($params as $key => $param) {
            if (in_array($key, $feld)) {
                $parameter .= $key .'='.$param.'&';
            }
        }
        return $parameter;
    }

    public function getURLWithOutParams($params)
    {
        if (empty($params['url'])) {
            return false;
        }
        $url = explode('?', $params['url']);
        $new_url = $url[0]. '/v1.0.0/' .$url[1] ;
        return $new_url;
    }
   
    /**
    * @Function  insertContact.
    *
    * @Description inserts the contact data to the by creating contact service.
    *              Maps the Standard and Custom parameters of the contact.
    *              Intiates the contact service only if there are standard parameters.
    *
    * @return response data of contact service.
    *
    */
    public function insertContact($params)
    {
        try {
            if (empty($params)) {
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);
            }

            $this->stnd_params = $this->getStandardParams($params);
            $this->custom_params = $this->getCustomParams($params);
            $this->constant_params = $this->getConstantParams();
            //$this->url_params = $this->getURLParams($params);
            #$this->url = $this->getURLWithOutParams($params);
            $this->url = $params["url"].'v1.0.0/'.$this->buildURL($params);
            $this->campaign = $params["campaign_name"];
            if (!$this->stnd_params) {
                throw new Exception("Maileon: Invalid Contact Parameters ".PHP_EOL);
            }
            $this->createContactService();
            $return = $this->response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $return;
    }

    /**
     * @Function  getDOIStatus
     *
     * @Description
     *  - If the Permission is DOI -> doi is TRUE
     *  - If the Permission is DOIPLUS  -> doiPlus is TRUE
     *
     * @return status of DOI
     *
     */
    private function getDOIStatus()
    {
        $doi = [
            "doi"       =>  true,
            "doiPlus"   =>  false
        ];
         
        $permission = $this->getPermissionForContact();
        if ($this->config_mailinone['TypeOfIntegration'] == "ContactEvent") {
            $doi = [
                "doi"       =>  true,
                "doiPlus"   =>  true
            ];
        } elseif ($this->config_mailinone['TypeOfIntegration'] == "DOI") {
            $doi = [
                "doi"       =>  true,
                "doiPlus"   =>  false
            ];
        } else {
            if (empty($permission)) {
                return $doi;
            }

            $doi['doiPlus'] = ($permission->code == 2 || $permission->code == 4 || $permission->code == 5)? true : false;
        }

        return $doi;
    }
    
    
    /**
     * @Function  getPermission
     *
     * @Description Returns the Configured Permission from the ini file.
     *  - Permission is always 'DOI' for TypeofIntegration DOI & DOIXorContactEvent
     *  - Return the Permission value for other Integration type.
     *
     * @return Permission
     *
     */
    private function getPermissionForContact()
    {
        $result;
        $set_permission = $this->config_mailinone['SetPermission'];
        $typ_of_integration = $this->config_mailinone['TypeOfIntegration'];
        $contact_status = false;
        $permission = [
           0,
           com_maileon_api_contacts_Permission::$NONE,
           com_maileon_api_contacts_Permission::$SOI,
           com_maileon_api_contacts_Permission::$COI,
           com_maileon_api_contacts_Permission::$DOI,
           com_maileon_api_contacts_Permission::$DOI_PLUS,
           com_maileon_api_contacts_Permission::$OTHER
        ];
        
        if (!empty($this->email)) {
            $contact_status = $this->getContactStatus($this->email);
        }
    
        // Validating the given Permission
        if (empty($set_permission) || !in_array($set_permission, $permission)) {
            $result = $permission[1];
        }

        // Check the Type of Integration
        if (($typ_of_integration == "DOIXorContactEvent")) {
            $result = (is_bool($contact_status) && ($contact_status == false))? $permission[1] : $permission[5];
        } elseif ($typ_of_integration == "ContactEvent") {
            $result = $permission[5];
        } else {
            $result = $permission[$set_permission];
        }
            
        return $result;
    }

    
    /**
     * @Function  IntegrateContactToMailInOne.
     *
     * @Description Integrates the Contact To Mail-In-One
     *
     * 'DOI'                    : If the contact is not Active -> Insert Contact with DOIKey.
     * 'ContactEvent'           : Call the contact event
     * 'DOIXorContactEvent'     : If the contact is active -> Create Event; If the contact is not active -> Just create DOI
     * 'MarketingAutomationById': Just call the MarketAutomation By Id.
     *
     *  - If ContactCreate in Ini file.
     *
     * @return Response of the Integration
     *
     */
    public function integrateContactToMailInOne($params)
    {
        $response;

        $permission = [0,1,2,3,4,5,6];
        $contact_create = $this->config_mailinone['CreateContact'];
        $type_of_integration = $this->config_mailinone['TypeOfIntegration'];
       
        if (empty($this->config_mailinone['TypeOfIntegration']) || empty($params)) {
            return $response;
        }

        // Get the Details of the contact
        $contact_status = $this->getContactStatus($params[$this->config_mailinone['Mapping']['EMAIL']]);
        $contact_not_present = (is_bool($contact_status) && ($contact_status == false));
        $contact_present_inactive = (!empty($contact_status['permission']) && array_key_exists($contact_status['permission'], $permission) && ($contact_status['permission'] == 1));
        $contact_present_active =  (!empty($contact_status['permission']) && array_key_exists($contact_status['permission'], $permission) && ($contact_status['permission'] > 1));
        
        // If the Contact is not Present and CreateContact is False
        if ($contact_not_present && !($contact_create)) {
            return $response;
        }

        if ($type_of_integration == 'DOI' && (($contact_not_present) || ($contact_present_inactive) || ($contact_present_active))) {
            $response = $this->insertContact($params);
        } elseif ($type_of_integration == 'ContactEvent') {
            $response = $this->createEvent($params);
        } elseif ($type_of_integration == 'DOIXorContactEvent') {

            // If the Contact is not Present or Inactive
            if ($contact_not_present || $contact_present_inactive) {
                $response = $this->insertContact($params);
            } else {
                $response =  $this->createEvent($params);
            }
        } elseif ($type_of_integration == 'MarketingAutomation') {
            if ($contact_not_present || $contact_present_inactive) {
                $response = $this->insertContact($params);
                if (!($this->response->isSuccess())) {
                    return $this->response;
                }
            }

            $response = $this->DOIMarketingAutomation($params[$this->config_mailinone['Mapping']['EMAIL']]);
        }

        return $response;
    }
}
