<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of generalSettings
 *
 * @author Pradeep
 */
class generalFunctions
{

    //put your code here
    public $get_params;
    public $config;

    public function __construct()
    {
        $this->config = new config("config.ini");
    }

    public function checkUTMParamsInPost($post_params)
    {
        if (empty($post_params)) {
            return false;
        }

        if (array_key_exists("trafficsource", $post_params) && empty($post_params["trafficsource"])) {
            $post_params["trafficsource"] = $_SESSION['trafficsource'];
        }

        //utm_term
        if (array_key_exists("utm_term", $post_params) && empty($post_params["utm_term"])) {
            $post_params["utm_term"] = $_SESSION['utm_term'];
        }

        //utm_name
        if (array_key_exists("utm_name", $post_params) && empty($post_params["utm_name"])) {
            $post_params["utm_name"] = $_SESSION['utm_name'];
        }

        //utm_medium
        if (array_key_exists("utm_content", $post_params) && empty($post_params["utm_content"])) {
            $post_params["utm_content"] = $_SESSION['utm_content'];
        }

        //utm_medium
        if (array_key_exists("utm_medium", $post_params) && empty($post_params["utm_medium"])) {
            $post_params["utm_medium"] = $_SESSION['utm_medium'];
        }

        //utm_source
        if (array_key_exists("utm_source", $post_params) && empty($post_params["utm_source"])) {
            $post_params["utm_source"] = $_SESSION['utm_source'];
        }

        //utm_source
        if (array_key_exists("utm_campaign", $post_params) && empty($post_params["utm_campaign"])) {
            $post_params["utm_campaign"] = $_SESSION['utm_campaign'];
        }

        return $post_params;
    }

    public function checkSession()
    {
        if (empty($_SESSION)) {
            return false;
        }
        return true;
    }


    /**
     * @Function  setCookieAsEmail.
     *
     * @Parameters  $email : Email of the User
     *
     * @Description Checks the following
     *      1. Check if the Email is Valid or not.
     *      2. Removes the Previous set Cookie.
     *      3. Sets a new Cookie as Email.
     *
     * @return void
     *      Set a cookie with current URL.
     */
    public function setCookieAsEmail($email)
    {
        if (!isset($email) || filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
            return false;
        }
        
        if (isset($_COOKIE["cookie"]["email"])) {
            setcookie("cookie[email]", "", strtotime('-30 days'), "/");
        }
        
        setcookie("cookie[email]", $email);
    }
    
    
    /**
     * @Function  setCookieAsSplitVersion.
     *
     * @Parameters  $url_no_params : Current URL of the Campaign with no parameters
     *
     * @Description Checks the following
     *          1. Validate the URL
     *          2. Removes the Cookie if present before
     *          3. Set a new Cookie with new URL
     *
     * @return void
     *      Set a cookie with current URL.
     */
    public function setCookieAsSplitVersion($url_no_params)
    {
        if (isset($_COOKIE["cookie"]["splitversion"])) {
            setcookie("cookie[splitversion]", "", strtotime('-30 days'), "/");
        }
        
        setcookie("cookie[splitversion]", $url_no_params, strtotime('+30 days'), "/");
    }


    /**
     * @Function  setCookieAsSplitVersion.
     *
     * @Parameters  -
     *
     * @Description
     *          1. Get the current URL using $_SERVER functionality
     *          2. Removes the parameters attached to URL
     *          3. Validates the URL
     *
     * @return String | Boolean
     *      String : URL with no parameters
     *      FALSE  : URL is not formulated.
     */
    public function getCurrentURLNoParams()
    {
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $url_no_params = explode("?", $url)[0];
        
        if (filter_var($url_no_params, FILTER_VALIDATE_URL) == false) {
            return false;
        }
        
        return $url_no_params;
    }

    
    /**
     * @Function  getCurrentURLParams.
     *
     * @Parameters  -
     *
     * @Description
     *     1. Get the current URL using $_SERVER functionality
     *     2. Parse URL to get the parameters
     *
     * @return String | NULL
     *      String : URL parameters
     */
    public function getCurrentURLParams()
    {
        $url_params= "";
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        
        if (filter_var($url, FILTER_VALIDATE_URL) == false) {
            return $url_params;
        }
        
        $url_params = explode("?", $url)[1];
        
        return $url_params;
    }
    
    public function getUTMParsamFromURL()
    {
        if (empty($_GET)) {
            return false;
        }
        foreach ($_GET as $key => $value) {
            $_SESSION[$key] = $value;
        }

        return true;
    }

    
    /**
     * @Function  getCampaignNameFromURL.
     *
     * @Parameters  Parameters obtained from the Campaign
     *
     * @Description
     *     1. Get the current URL using $_SERVER functionality
     *     2. Parse URL to get the parameters
     *
     * @return String | NULL
     *      String : URL parameters
     */
    public function getCampaignNameFromURL($params)
    {
        if (!isset($params["url"]) || filter_var($params["url"], FILTER_VALIDATE_URL) == false) {
            return false;
        }

        $segments = parse_url($params["url"]);
        $segments = explode('/', trim(parse_url($params["url"], PHP_URL_PATH), '/'));

        # If no folder is found in the URL return Default campaign Name
        if (empty($segments[0])) {
            # Treaction is the default campaign
            return "Treaction";
        }

        return $segments[0];
    }


    /**
     * @Function  redirectPage.
     *
     * @Parameters  $url : URL of the page to be redirected.
     *
     * @Description
     *     1. Validates the Format of the URL
     *     2. Redirects Page Using Javascript
     *
     * @return void.
     */
    public function redirectPage($url)
    {
        if (!isset($url) || filter_var($url, FILTER_VALIDATE_URL) == false) {
            return false;
        }
        echo '<script type="text/javascript">';
        echo "window.top.location='" . $url . "'";
        echo '</script>';
    }

    
    /**
     * @Function  clearSession.
     *
     * @Parameters  -
     *
     * @Description Destroys all the created Sessions.
     *
     * @return void.
     */
    public function clearSession()
    {
        session_destroy();
    }

    
    /**
     * EncriptDataValuePair: Input type(Type of encryption) and Associative array
     *
     * @param type $params
     * @return An array of encrypted (value=pair).
     */
    public function encriptDataValuePair($type, $array)
    {
        if (empty($type) or empty($array)) {
            return [];
        }
        switch ($type) {
            case Base64:
                foreach ($array as $k => $v) {
                    $a = [$k => $v];
                    $b = http_build_query($a);
                    $c = base64_encode($b);
                    $sd .= '&' . $c;
                }
                break;
            case MD5:
                foreach ($array as $k => $v) {
                    $a = [$k => $v];
                    $b = http_build_query($a);
                    $c = md5($b);
                    $sd .= '&' . $c;
                }
                break;
        }
        return $sd;
    }

    /**
     * EncriptDataValue: Input type(Type of encryption) and  array
     *
     * @param type $params
     * @return An array of encrypted values.
     */
    public function encriptDataValue($type, $array)
    {
        if (empty($type) or empty($array)) {
            return [];
        }
        $sd = '';
        switch ($type) {
            case Base64:
                foreach ($array as $a) {
                    $a = base64_encode($a);
                    $sd .= '&' . $a;
                }
                break;
            case MD5:
                foreach ($array as $a) {
                    $a = md5($a);
                    $sd .= '&' . $a;
                }
                break;
        }

        return $sd;
    }

    /**
     * senddata: Input variable $params
     *
     * @param type $params
     * @return An array of encrypted value or value=pair according to requirement.
     */
    public function senddata($params)
    {
        if (empty($params)) {
            return [];
        }
        $config_mailinone = $this->config->getConfigMailInOne();
        $mapping = $config_mailinone['Mapping'];
        $generalSettings = $this->config->getConfigGeneralSettings();
        $encriptionType = $generalSettings[SendDataEncryption]; #type
        $sendDataParameter = array_flip($this->config->getConfigSendDataParameter());
        $intermediateArray = array_flip(array_intersect_key($mapping, $sendDataParameter));
        $sendData = array_intersect_key($params, $intermediateArray);
        $arrayValues = array_values($sendData);
        $encriptionDataFormat = $generalSettings[SendDataFormat];

        switch ($encriptionDataFormat) {
            case values:
                $result = $this->encriptDataValue($encriptionType, $arrayValues);
                break;
            case valuesPairs:
                $result = $this->encriptDataValuePair($encriptionType, $sendData);
                break;
        }
        return $result;
    }


    /**
     * @Function  getCookieAsSplitVersion.
     *
     * @Description Check the cookie value of splitversion and return if present
     *
     * @return boolen | string
     *      Boolean : if cookie is
     *
     */
    public function getCookieAsSplitVersion()
    {
        $cookie = filter_var($_COOKIE["cookie"]["splitversion"], FILTER_SANITIZE_URL);
        if (!isset($cookie) || filter_var($cookie, FILTER_VALIDATE_URL) == false) {
            return false;
        }
        
        return $cookie;
    }


    /**
     * getBrowserDetails function
     * User WhichBrowser Library to get the Details of the client Browser
     *
     * @return array
     */
    public function getBrowserDetails() :array
    {
        $b_detials = [];
        $http_user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (!isset($http_user_agent) && empty($http_user_agent)) {
            return $b_detials;
        }
        $device = new WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);
        if (!isset($device) || empty($device) || empty($this->config)) {
            return $b_detials;
        }
        // Get config Mapping
        $conf_mapping = $this->config->getConfigMailInOne()['Mapping'];
        if ($device->isType('mobile', 'tablet') && empty($device->browser->name)) {
            if ($device->os->name === "Android") {
                $browser_name = $device->browser->name ? $device->browser->name : "Chrome";
            } elseif ($device->os->name === "iOS") {
                $browser_name = $device->browser->name ? $device->browser->name : "Safari";
            } else {
                $browser_name = $device->browser->name ? $device->browser->name : "Chrome";
            }
        } else {
            $browser_name = $device->browser->name;
        }
        $b_detials[$conf_mapping['DeviceType']] = $device->device->type;
        $b_detials[$conf_mapping['DeviceBrowser']] = isset($browser_name) ? $browser_name : "";
        $b_detials[$conf_mapping['DeviceBrowserVersion']] = $device->browser->version->value;
        $b_detials[$conf_mapping['DeviceOs']] = $device->os->name;
        $b_detials[$conf_mapping['DeviceOsVersion']] = $device->os->version->value;

        return $b_detials;
    }
}
