<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mail
 *
 * @author Pradeep
 */


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail
{
    public $mail = "";
    public $password = "EzYS%*m\$wnLV.CVtpm'y";                    // SMTP password

    /**
     * __construct function
     */
    public function __construct()
    {
        $this->config = new config("config.ini");
        $this->config_mail = $this->config->getConfigEmail();
        $this->mail = new PHPMailer(true);
    }


    /**
     * getServerSettings function
     *
     * @return boolean
     */
    public function getServerSettings() : bool
    {
        $status = false;
        
        if (!empty($this->mail) && isset($this->mail) && is_array($this->config_mail) && !empty($this->config_mail)) {
            $status = true;
            $this->mail->SMTPDebug = $this->config_mail['smpt_debug'];
            // Set mailer to use SMTP
            $this->mail->isSMTP();
            $this->mail->Host       = $this->config_mail['host'];
            $this->mail->SMTPAuth   = $this->config_mail['smpt_auth'];
            $this->mail->Username   = $this->config_mail['username'];
            $this->mail->Password   = $this->password; //$this->config_mail['password'];
            $this->mail->SMTPSecure = $this->config_mail['smpt_secure'];
            $this->mail->Port       = $this->config_mail['port'];
        }
        return $status;
    }

    
    /**
     * getRecipients function
     *
     * @return boolean
     */
    public function getRecipients() :bool
    {
        $status = false;
        $send_recipients = explode(',', $this->config_mail['ForwardEmailRecipient']);

        if (!empty($this->mail) || isset($this->mail)) {
            $status = true;
            $this->mail->setFrom('cio@treaction.net', 'CIO Client');
            foreach ($send_recipients as $recipient) {
                if (!empty($recipient) && isset($recipient)) {
                    $this->mail->addAddress($recipient);
                }
            }
        }
        return $status;
    }


    /**
     * getContent function
     *
     * @return boolean
     */
    public function getContent() : bool
    {
        $status = false;

        if (!empty($this->mail) || isset($this->mail)) {
            $status = true;
            $this->mail->isHTML(true);                                  // Set email format to HTML
            $this->mail->Subject = $this->getMailSubject();
            $this->mail->Body    = $this->getMailBody();
        }
        return $status;
    }


    /**
     * getMailBody function
     *
     * @return string
     */
    public function getMailBody() : string
    {
        return $this->getMailSubject() . "\n"
            . "Anrede:" . $this->recipient_anrede . "\n"
            . "Name:" . $this->recipient_name . "\n"
            . "Vorname:" . $this->recipient_vorname . "\n"
            . "Telefon:" . $this->recipient_tel . "\n"
            . "Email:" . $this->recipient_email . "\n";
    }


    /**
     * getMailSubject function
     *
     * @return string
     */
    public function getMailSubject() : string
    {
        $subject = "No Subject";

        if (is_array($this->config_mail) && isset($this->config_mail['ForwardEmailSubject'])) {
            $subject = $this->config_mail['ForwardEmailSubject'];
        }
        return $subject;
    }


    /**
     * parseRecipientDetails function
     *
     * @param array $recipient_details
     * @return bool
     */
    public function parseRecipientDetails(array $recipient_details) : bool
    {
        $status = false;
        $this->config_mailon = $this->config->getConfigMailInOne()['Mapping'];

        if (is_array($this->config_mailon) && !empty($this->config_mailon) && isset($recipient_details[$this->config_mailon['EMAIL']])) {
            $status = true;
            $this->recipient_email      = $recipient_details[$this->config_mailon['EMAIL']] ;
            $this->recipient_name       = isset($recipient_details[$this->config_mailon['LASTNAME']])     ? $recipient_details[$this->config_mailon['LASTNAME']]    : "";
            $this->recipient_vorname    = isset($recipient_details[$this->config_mailon['FIRSTNAME']])    ? $recipient_details[$this->config_mailon['FIRSTNAME']] : "";
            $this->recipient_tel        = isset($recipient_details[$this->config_mailon['Telefon']])      ? $recipient_details[$this->config_mailon['Telefon']]     : "";
            $this->recipient_anrede     = isset($recipient_details[$this->config_mailon['SALUTATION']])   ? $recipient_details[$this->config_mailon['SALUTATION']]  : "";
        }

        return $status;
    }


    /**
     * sendMail function
     *
     * @param array $recipient_details
     * @return boolean
     */
    public function sendMail(array $recipient_details) : bool
    {
        $status = false;
        if (!is_array($recipient_details) || empty($recipient_details)) {
            return $status;
        }

        if (!$this->parseRecipientDetails($recipient_details)) {
            return $status;
        }

        if (!$this->getServerSettings()) {
            return $status;
        }

        if (!$this->getRecipients()) {
            return $status;
        }

        if (!$this->getContent()) {
            return $status;
        }

        $result = $this->mail->send();

        if (is_bool($result) && $result) {
            $status = $result;
        }

        return $status;
    }
}
