<?php

class LeadCustomData
{
    public $id ;
    public $table;

    /**
     * __construct function
     */
    public function __construct()
    {
        $this->dbObject = new db();
        $this->table = "`cio.leads_customdata`";
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @return array
     */
    public function getLeadCustomDatabyId(int $id) : array
    {
    }

    /**
     * insertLeadCustomData function
     *
     * @param array $ins_value
     * @return id
     */
    public function insertLeadCustomData(array $ins_value) :id
    {
        $lead_custom_id = 0;
        if (empty($ins_value)) {
            return $lead_custom_id;
        }

        $value = $ins_value;
        $where = [];
        $operation = "insert";
        $result = $this->dbObject->execute($this->table, $ins_value, $where, $operation, false);

        if (is_bool($result) && $result == true) {
            $lead_custom_id = $this->dbObject->getLastInsertId();
        }

        return $lead_custom_id;
    }

    /**
     * insertMultiRowsCustomData function
     *
     * @param string $ins_query
     * @return boolean
     */
    public function insertMultiRowsCustomData(string $ins_query) :bool
    {
        $status = false;
        if (empty($ins_query) || !isset($this->dbObject)) {
            die("insertMultiRowsCustomData");
            return $status;
        }

        $result = $this->dbObject->insertMultiRows($ins_query);

        if (is_bool($result) && $result == true) {
            $status = $result;
        }

        return $status;
    }


    /**
     * getInsertQueryMultiRow function
     *
     * @param array $cus_fields
     * @param integer $lead_id
     * @param integer $lead_customfield_id
     * @return string
     */
    public function getInsertQueryMultiRow(array $cus_fields, int $lead_id, int $lead_customfield_id):string
    {
        $query = "";
        $i = 0;
        if (empty($cus_fields) || $lead_id <= 0 || $lead_customfield_id <= 0) {
            return $ins_query;
        }
        $count = count($cus_fields);
        $type = "equals";
        #query statement
        $query = "INSERT INTO `cio.leads_customdata` (`lead_customfield_id`, `lead_id`, `cio.questions_id`, `name`, `type`, `value`, `created_at`, `updated_at`) VALUES";
        foreach ($cus_fields as $key => $value) {
            $question_id = null;
            $ins_query .= $i > 0 ? ',': '';
            $date_time = date("Y-m-d h:i:sa");
            $ins_query .= "('".$lead_customfield_id."' ,'".$lead_id."' ,'".$question_id."' ,'".$key."' ,'".$type."' ,'".$value."', '".$date_time."', '".$date_time."')";
            $i++;
        }
        $query .= $ins_query;
        $query .= ';';

        return $query;
    }
}
