<?php

/*
 * To Visualize all leads for the 'campaigns.cio_leads'
 * @author Pradeep
 */
require "vendor/autoload.php";

class leads
{
    public $dbObject;
    private $table;
    
    public function __construct()
    {
        $this->table = "`cio.leads`";
        $this->dbObject = new db();
        $this->config = new config("config.ini");
    }

    /**
     * createLead:
     *
     * create a lead in the database table by creating a new tracking number
     *
     * @param type $params
     * @return boolean|string
     */
    public function createLead($params)
    {
        $lead_params = array();
        $return = 0;
        $operation = "insert";
        $config_lead = $this->config->getConfigLeadView();
        $config_mailinone = $this->config->getConfigMailInOne();
        
        if (empty($params)) {
            return $return;
        }
        # Return false if no databse object is found
        if (!$this->dbObject) {
            return $return;
        }
 
        // Get the SplitVersion Params
        $lead_params = $this->getSplitVersionParams($params);
        //Get the Campaign URL
        $params[$config_mailinone[Mapping][url]] = $this->getCampaignURL($params);
        // Maping the HTML parameter to table column fields
        $lead_params = array_merge($lead_params, $this->mapParams($params));
        //Merge the log Created Params
        $lead_params = array_merge($lead_params, $this->getLogCreatedParams());
        //Merge Log Update Params
        $lead_params = array_merge($lead_params, $this->getLogUpdatedParams());
        //Merge Campaign Id
        $lead_params = array_merge($lead_params, $this->getCampaignId($params));
        //Merge Company Id
        $lead_params = array_merge($lead_params, $this->getCompanyId($params));
        //Merge Account Id Which is similar to Company Id in 'cio.company'
        $lead_params = array_merge($lead_params, $this->getCompanyId($params));
        // Merge LeadTracking Information
        $lead_params = array_merge($lead_params, $this->getLeadTrackingRef());
        // UTM Parameters
        $lead_params = array_merge($lead_params, $this->getUTMParamters($params));
        // Device Type
        $lead_params = array_merge($lead_params, $this->getDeviceType($params));
        // Affiliate Id and AffiliateSub Id
        $lead_params = array_merge($lead_params, $this->getAffiliateIds($params));
        // Target group
        $lead_params = array_merge($lead_params, $this->getTargetGroup($params));
        // Insert the Lead to Database
        $status = $this->dbObject->execute($this->table, $lead_params, "", $operation);

        if (!$status) {
            return $return;
        }
        
        $return = $lead_params[$config_mailinone[Mapping][LeadReference]];

        return $return;
    }


   
    /**
     * getCampaignId
     * Campaign Id of the campaign from the database
     *
     * @param array $params
     * @return array
     */
    private function getCampaignId(array $params): array
    {
        $campaign_id = 1;

        if (empty($params['url']) || empty($this->dbObject)) {
            return [
                "campaign_id" => $campaign_id
             ];
        }
        
        spl_autoload_register('Autoload::splitTestLoader');
        $camp = new campaign();
        $id = $camp->getCampaignDetailsByURL()['id'];

        if (isset($id) && (int) $id > 0) {
            $campaign_id = $id;
        }

        return [
            "campaign_id" => $campaign_id
        ];
    }
    
 
    /**
     * getSplitVersionParams function
     * Get the Parameters for SplitTest
     *
     * @param array $params
     * @return array
     */
    private function getSplitVersionParams(array $params): array
    {
        $split_params = [
            "final_url" => "",
            "split_version" => ""
        ];
        
        $config_mailinone = $this->config->getConfigMailInOne();
        $config_split = $this->config->getConfigSplitTest();
        
        if (empty($params)) {
            return $split_params;
        }
        
        $split_params["final_url"]      = $params[$config_mailinone[Mapping][url]]  ? $params[$config_mailinone[Mapping][url]]   : " ";
        $split_params["split_version"]  = $config_split[Version] ? $config_split[Version] : " ";
        
        return $split_params;
    }
    
    
    /**
     * getDeviceType function
     * Get the Device parameters of the Lead
     *
     * @param array $params
     * @return array
     */
    private function getDeviceType(array $params) :array
    {
        $device = [
            "device_type" => "",
            "device_browser" => "",
            "device_browser_version" => "",
            "device_os" => "",
            "device_os_version" => ""
            ];
        
        $config_mailinone = $this->config->getConfigMailInOne();
        
        if (empty($params)) {
            return $device;
        }
        
        return [
            "device_type" => $params[$config_mailinone[Mapping][DeviceType]],
            "device_browser" => $params[$config_mailinone[Mapping][DeviceBrowser]],
            "device_browser_version" => $params[$config_mailinone[Mapping][DeviceBrowserVersion]],
            "device_os" => $params[$config_mailinone[Mapping][DeviceOs]],
            "device_os_version" => $params[$config_mailinone[Mapping][DeviceOsVersion]],
            "ip" => $params[$config_mailinone[Mapping][ip_adresse]]
        ];
    }
    

    /**
     * getCompanyId:
     *
     * Company Id from the company present in the URL
     *
     * @param type $params
     * @return boolean|string
     */
    private function getCompanyId($params)
    {
        $company_id = 1;
        $account = "";
        if (empty($params)) {
            return [
                "company_id" => $company_id,
            ];
        }
        
        $select = array('company_id');
        $where = "url LIKE '".$params['url']."'";
        $table = "`cio.campaign`";
        $result = $this->dbObject->getOne($select, $table, $where);

        if (empty($result)) {
            return [
                "company_id" => $company_id,
            ];
        }
        
        $this->company_id = $result['company_id'];
        
        return [
            "company_id" => $this->company_id
        ];
    }
    
    
    
    /**
     * getCampaignURL:
     *
     * Campaign is first folder after domain, Client is Sub-Domain
     *
     * @param type $params
     * @return boolean|string
     */
    private function getCampaignURL($params)
    {
         
        // Get Config Mail-in-one Mapping
        $config_mailinone = $this->config->getConfigMailInOne();
        $trucated_url = "";
        if (empty($params[$config_mailinone[Mapping][url]])) {
            // return Null if no URL is present
            return $trucated_url;
        }
        // explode the https and address part.
        $url_address = explode('//', $params[$config_mailinone[Mapping][url]]);
        // explode other folders present in URL
        $url_folders = explode('/', $url_address[1]);
        // check if subfolder is present or not
        if (isset($url_folders[1])) {
            // append the url with domain with subfolder
            $url_address[1] = $url_folders[0].'/'.$url_folders[1];
        } else {
            // Consider only the url
            $url_address[1] = $url_folders[0];
        }
        // Append all the url parts.
        $trucated_url = $url_address[0].'//'.$url_address[1];

        return $trucated_url;
    }
    
    
    
    /*
     * getLeadTrackingRef:
     *
     * Campaign is first folder after domain, Client is Sub-Domain
     *
     * @param type $params
     * @return boolean|string
     */
    private function getLeadTrackingRef()
    {
        $lead_ref = "";
        $campaign_counter = 0;
        $config_mailinone = $this->config->getConfigMailInOne();
        // Get the Account and Campaign value
        $lead_config = $this->config->getConfigLeadView();
        
        if (!empty($this->company_id)) {
            $account_id = $this->company_id;
        }
        
        // Could not read the config.ini file
        if (empty($lead_config)) {
            return [
                "account" => $account_id,
                "campaign" => "",
                $config_mailinone[Mapping][LeadReference] => "",
            ];
        }
       
        // Generate the Counter
        #$account = $lead_config['Account'];
        $campaign = $lead_config["Campaign"];
        $select = array('lead_reference');
        $where = "account = $account_id AND campaign LIKE '$campaign' ORDER BY `id` DESC";
        $table = "`cio.leads`";
        $result = $this->dbObject->getOne($select, $table, $where);
        #        $count = $this->dbObject->getOne($this->table, ["account = $account_id", "campaign = $campaign"]);
        $old_lead_ref = $result['lead_reference'];
        
        // Could not get the Count of campaigns
        if (is_null($old_lead_ref)) {
            $campaign_counter = $campaign_counter + 1;
            $lead_ref = $account_id.'-'.$campaign.'-'.sprintf("%'.08d\n", $campaign_counter);
            return [
                "account" => $account_id,
                "campaign" => $campaign,
                $config_mailinone[Mapping][LeadReference] => $lead_ref,
            ];
        }
        
        // Get the Campaign count from old Lead refernce
        $old_lead_ref = $result['lead_reference'];
        $lead_buffer = explode('-', $old_lead_ref);
        $campaign_counter = (int)$lead_buffer[2];
        
        // increment the counter
        $campaign_counter = $campaign_counter + 1;
        // Generate lead refeence tracking number
        $lead_ref = $account_id.'-'.$campaign.'-'.sprintf("%'.08d\n", $campaign_counter);

        return  [
            "account" => $account_id,
            "campaign" => $campaign,
            $config_mailinone[Mapping][LeadReference] => $lead_ref,
        ];
    }
       
    
    /**
     * getCampaignURL:
     *
     * Campaign is first folder after domain, Client is Sub-Domain
     *
     * @param type $params
     * @return boolean|string     /
     */
    public function getUTMParamters($params)
    {
        if (empty($params)) {
            return false;
        }
        $config_mailinone = $this->config->getConfigMailInOne();
        $utm_params = array();
        
        if (array_key_exists($config_mailinone[Mapping][trafficsource], $params) && !empty($params[$config_mailinone[Mapping][trafficsource]])) {
            $utm_params[$config_mailinone[Mapping][trafficsource]] = $params[$config_mailinone[Mapping][trafficsource]];
        }
        if (array_key_exists($config_mailinone[Mapping][utm_term], $params) && !empty($params[$config_mailinone[Mapping][utm_term]])) {
            $utm_params['utm_parameters'] = 'utm_term='.$params[$config_mailinone[Mapping][utm_term]].'&';
        }
        if (array_key_exists($config_mailinone[Mapping][utm_name], $params) && !empty($params[$config_mailinone[Mapping][utm_name]])) {
            $utm_params['utm_parameters'] .= 'utm_name='.$params[$config_mailinone[Mapping][utm_name]].'&';
        }
        if (array_key_exists($config_mailinone[Mapping][utm_content], $params) && !empty($params[$config_mailinone[Mapping][utm_content]])) {
            $utm_params['utm_parameters'] .= 'utm_content='.$params[$config_mailinone[Mapping][utm_content]].'&';
        }
        if (array_key_exists($config_mailinone[Mapping][utm_medium], $params) && !empty($params[$config_mailinone[Mapping][utm_medium]])) {
            $utm_params['utm_parameters'] .= 'utm_medium='.$params[$config_mailinone[Mapping][utm_medium]].'&';
        }
        if (array_key_exists($config_mailinone[Mapping][utm_source], $params) && !empty($params[$config_mailinone[Mapping][utm_source]])) {
            $utm_params['utm_parameters'] .= 'utm_source='.$params[$config_mailinone[Mapping][utm_source]].'&';
        }
        if (array_key_exists($config_mailinone[Mapping][utm_campaign], $params) && !empty($params[$config_mailinone[Mapping][utm_campaign]])) {
            $utm_params['utm_parameters'] .= 'utm_campaign='.$params[$config_mailinone[Mapping][utm_campaign]];
        }

        return $utm_params;
    }
    
    /**
     * getAffiliateIds function
     * Get Affiliate Id and SubId from the params array
     *
     * @param array $params
     * @return array
     */
    private function getAffiliateIds(array $params) : array
    {
        $aff_ids = [
            "affiliateID" => "",
            "affiliateSubID" =>  ""
        ];
        $config_mailinone = $this->config->getConfigMailInOne()['Mapping'];

        if (empty($params) || !isset($params[$config_mailinone['affiliateID']])) {
            die("Null returned");
            return $aff_ids;
        }

        $aff_ids['affiliateID'] = (!empty($params[$config_mailinone['affiliateID']])  && isset($params[$config_mailinone['affiliateID']])) ? $params['affiliateID']:"";
        $aff_ids['affiliateSubID'] = (!empty($params[$config_mailinone['affiliateSubID']])  && isset($params[$config_mailinone['affiliateSubID']])) ? $params[$config_mailinone['affiliateSubID']]:"";

        return $aff_ids;
    }
    

    /**
     * getTargetGroup function
     *
     * @param array $params
     * @return array
     */
    private function getTargetGroup(array $params) : array
    {
        $tgroup =[
            "targetgroup" => ""
        ];

        $config_mailinone = $this->config->getConfigMailInOne()['Mapping'];

        if (empty($params) || !isset($params[$config_mailinone['targetgroup']])) {
            return $tgroup;
        }

        $tgroup["targetgroup"] = (!empty($params[$config_mailinone['targetgroup']]) && isset($params[$config_mailinone['targetgroup']])) ? $params[$config_mailinone['targetgroup']] : "";
  
        return $tgroup;
    }

    /*
     * getLogCreatedParams:
     *
     * Returns the Log created date and User
     *
     * @param type $params
     * @return boolean|string
     */
    private function getLogCreatedParams()
    {
        $log_create= array(
            "created"=> date('Y-m-d H:i:s', time()),
            "created_by" => 0
        );
        return $log_create;
    }
    
    
    /*
     * getlogUpdatedParams:
     *
     * Returns the Log Updated date and User
     *
     * @param type $params
     * @return boolean|string
     */
    private function getlogUpdatedParams()
    {
        $log_update = array(
            "updated" => date('Y-m-d H:i:s', time()),
            "updated_by" => 0
        );
        
        return $log_update;
    }


    /*
     * getCampaignFinalURL:
     *
     * User for Sticky Version of Campaign Version based on IP address
     * Returns the Campaign Details for Ip address
     *
     * @param type $params
     * @return boolean|string
     */
    public function getCampaignFinalURL($params)
    {
        if (!isset($params) || (filter_var($params['url'], FILTER_VALIDATE_URL) == false) || (filter_var($params['ip'], FILTER_VALIDATE_IP) == false)) {
            return false;
        }
        // Trucate the Last character of URL if '/' is found.
        if (substr($params['url'], -1) == "/") {
            $params['url'] = substr_replace($params['url'], "", -1);
        }

        $select = array("final_url");
        $where = "ip LIKE '".$params['ip']."' AND `url` LIKE '".$params['url']."'  ORDER BY `id` DESC";
        $table = "`cio.leads`";
        $result = $this->dbObject->getOne($select, $table, $where);
        
        return $result;
    }
    
       

    /**
     * checklead function
     * Checks the Lead email id with "cio.leads" table
     *
     * @param array $params
     * @return boolean
     */
    public function checklead(array $params): bool
    {
        $lead_status = false;
        $config_settings = $this->config->getConfigGeneralSettings();
        $config_lead = $this->config->getConfigLeadView();
        $config_mailinone = $this->config->getConfigMailInOne();

        if (empty($params) || empty($params[$config_mailinone[Mapping][EMAIL]])
            || empty($params[$config_mailinone[Mapping][ip_adresse]])) {
            return $lead_status;
        }
        
        $url = $this->getCampaignURL($params);
        $chck_duplicate_lead = $this->getLeadByEmailForCampaign($params[$config_mailinone[Mapping][EMAIL]], $url, true);

        if (empty($chck_duplicate_lead)) {
            $lead_status = true;
        }

        $current_date = date("Y-m-d");
        $lead_date = date('Y-m-d', strtotime($chck_duplicate_lead["created"]));
        if (isset($chck_duplicate_lead["created"]) && $current_date != $lead_date) {
            $lead_status = true;
        }

        return $lead_status;
    }
    

    /**
     * getLeadByEmailForCampaign function
     *
     * @param string $email
     * @param string $url
     * @return array
     */
    private function getLeadByEmailForCampaign(string $email, string $url, bool $today) : array
    {
        $lead_details = [];
        $date = date('Y-m-d', time());

        if (empty($email) || empty($url) || !isset($email) || !isset($url)) {
            return $lead_details;
        }

        $select = array('*');
        $where = "url LIKE '$url' AND email LIKE '$email'";
        $where .= (is_bool($today) && $today)? " AND `created` LIKE '%$date%' ":""; // For today's date
        $where .= " ORDER BY `id` DESC";
        $table = "`cio.leads`";
        $result = $this->dbObject->getOne($select, $table, $where);

        if (is_array($result) && !empty($result)) {
            $lead_details = $result;
        }

        return $lead_details;
    }
   

    /**
     * mapParams function
     * Necessary maping is done with MIO params and cio.leads table for all the Parameters
     *
     * @param array $params
     * @return array
     */
    private function mapParams(array $params): array
    {
        $lead_params = [];
        
        if (empty($params)) {
            return $lead_params;
        }

        $config_mailinone = $this->config->getConfigMailInOne();
        $mapping = array(
            $config_mailinone[Mapping][SALUTATION] => "salutation",
            $config_mailinone[Mapping][FIRSTNAME] => "first_name",
            $config_mailinone[Mapping][LASTNAME] => "last_name",
            $config_mailinone[Mapping][EMAIL] => "email",
            $config_mailinone[Mapping][ZIP] => "postal_code",
            $config_mailinone[Mapping][Telefon] => "phone",
            $config_mailinone[Mapping][CITY] => "city",
            $config_mailinone[Mapping][ADDRESS] => "street",
            $config_mailinone[Mapping][url] => "url",
            "country" => "country",
        );
        $count = count($params);

        foreach ($params as $key => $value) {
            foreach ($mapping as $mkey => $mval) {
                if ($key === $mkey) {
                    $lead_params[$mval] = $value;
                }
            }
        }
        
        return $lead_params;
    }
    
    
    
    /*
     * checkEmailDomainInExcludedList:
     *
     * Verify the current email or emaildomain with excluded list of domains in the config ini
     *
     * @param type $params
     *
     * @return boolean
     *      'True' = If Domain is found in Excluded list
     *      'False' = If Domain is not found in Excluded List
     */
    public function checkEmailDomainInExcludedList($params)
    {
        $config_mailinone = $this->config->getConfigMailInOne();
        $config_exluded_domains = $this->config->getConfigGeneralSettings()['ExcludeDomainsLeadTracking'];
        $return = false;

        if (empty($params) || empty($params[$config_mailinone['Mapping']['EMAIL']]) || empty($config_exluded_domains)) {
            return $return;
        }

        $email_domain = explode('@', trim($params[$config_mailinone['Mapping']['EMAIL']]))[1];
        $exluded_domains = explode(',', trim($config_exluded_domains));
        
        if (empty($email_domain) || empty($exluded_domains)) {
            return $return;
        }

        $validation = (in_array($params[$config_mailinone['Mapping']['EMAIL']], $exluded_domains) || in_array($email_domain, $exluded_domains));
        $return = $validation ? true : false;

        return $return;
    }


    /**
     * getLeadDetailsByEmail function
     *
     * @param string $email
     * @return array
     */
    public function getLeadDetailsByEmail(string $email) : array
    {
        $lead_details = [];
        if (empty($email) || !isset($this->table) || !isset($this->dbObject)) {
            return $lead_details;
        }

        $select = ['*'];
        $where = "`email` LIKE '$email'";
        $result = $this->dbObject->getOne($select, $this->table, $where);

        if (is_array($result) && !empty($result)) {
            $lead_details = $result;
        }

        return $lead_details;
    }


    /**
     * getLeadDetailsByIPAddress function
     *
     * @param string $ip_addr
     * @return array
     */
    public function getLeadDetailsByIPAddress(string $ip_addr, string $url = null, bool $created = false) : array
    {
        $lead_details = [];
 
        if (empty($ip_addr) || !isset($ip_addr) || !isset($this->dbObject)) {
            return $lead_details;
        }

        $sql = "SELECT * FROM `cio.leads` WHERE `ip` LIKE '$ip_addr'";
        // Campaign URL
        if (isset($url) && !empty($url)) {
            $sql .= " AND `url` LIKE '$url' ";
        }
        // Created
        if (is_bool($created) && $created) {
            $today = date('Y-m-d', time());
            $sql .= "AND `created` LIKE '%$today%' ";
        }

        $result = $this->dbObject->getAll($sql);

        if (is_array($result) && !empty($result)) {
            $lead_details = $result;
        }

        return $lead_details;
    }
}
