<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of campaign
 *
 * @author Pradeep
 */
class campaign
{
    //put your code here
    public $campaign_name;
    public $campaign_id;
    public $url;
    public $conn;

    public function __construct()
    {
        $this->url = $this->getCampaignURL();
        $this->table = "`cio.campaign`";
        $this->dbObject = new db();
    }


    /**
     * getCampaignURL function
     *
     * @return void
     */
    private function getCampaignURL()
    {
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/'.explode('/', $_SERVER['REQUEST_URI'])[1];
        return $url;
    }

    
    /**
     * getCampaignDetailsByURL function
     *
     * @return array
     */
    public function getCampaignDetailsByURL():array
    {
        $return = [];
        if (!filter_var($this->url, FILTER_VALIDATE_URL) || empty($this->table)) {
            die("Invalid URL passed");
            return $return;
        }

        $select = array('*');
        $where = "url LIKE '".$this->url."'";
        $table = $this->table;

        $result = $this->dbObject->getOne($select, $table, $where);

        if (is_array($result) && !empty($result)) {
            $return = $result;
        }

        return $return;
    }

    /*
     * getCampaignFinalURL:
     *
     * User for Sticky Version of Campaign Version based on IP address
     * Returns the Campaign Details for Ip address
     *
     * @param type $params
     * @return boolean|string
     */
    public function getCampaignFinalURL($params)
    {
        if (!isset($params) || (filter_var($params['url'], FILTER_VALIDATE_URL) == false) || (filter_var($params['ip'], FILTER_VALIDATE_IP) == false)) {
            return false;
        }
        // Trucate the Last character of URL if '/' is found.
        if (substr($params['url'], -1) == "/") {
            $params['url'] = substr_replace($params['url'], "", -1);
        }

        $select = array("final_url");
        $where = array("ip LIKE '".$params['ip']."' AND `url` LIKE '".$params['url']."'  ORDER BY `id` DESC");
        $table = "`cio.leads`";
        $result = $this->dbObject->getOne($select, $table, $where);
        
        return $result;
    }
}
