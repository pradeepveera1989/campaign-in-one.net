<?php


require "vendor/phpmailer/phpmailer/src/PHPMailer.php";
require "vendor/phpmailer/phpmailer/src/Exception.php";

// Composer Autoload
require "vendor/autoload.php";
session_start();

// Get the Config file path
$config_path = "config.ini";
$conf = new config($config_path);
$config = $conf->getAllConfig();
$config_js = $conf->getAllConfigForJs();

spl_autoload_register('Autoload::generalFunctionLoader');
$func = new generalFunctions();
$func->getUTMParsamFromURL($_GET);
$single_submit_text = $config['GeneralSetting']['SingleSubmitErrorTxt'];
$url_no_params = $func->getCurrentURLNoParams();


// Set traffic source cookie for Webgains
$webgains_cookie = $_GET['trafficsource'];

// Page counter
$counter_status = $conf->getConfigCounterStatus();
if ($counter_status) {

    $counter_params = $conf->getConfigCounter();
    $counter = new counter();
    $url = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER[HTTP_HOST] . "/" . explode('/', $_SERVER['REQUEST_URI'])[1];
    $counter_params['url'] = $url;
    $current_count_value = $counter->getCounterValue($url);
    $current_end_date = $counter->getCounterEndDate($url);

    if (is_bool($current_count_value) && $current_count_value == false) {
        // URL is missing ,Insert new URL
        $status = $counter->insertCounterValue($counter_params);
        $status = $counter->insertCounterEndDate($counter_params);
    } elseif ($current_count_value == 2 && $counter_params['auto_refill']) {
        // refill the counter after check the date.
        $counter_expire_date = strtotime($counter_params['expire']);
        $current_date = strtotime(date("Y-m-d"));
        if ($counter_expire_date > $current_date) {
            $new_counter_value = $counter_params['max_offers'] - $counter_params['sold_offers'];
            $counter->updateCounterValue($url, $new_counter_value);
        }
    } elseif (((strtotime($current_end_date) - strtotime(date('Y-m-d H:i:s'))) <= 9500) && $counter_params['auto_refill']) {
        // Update the latest Expire date for counter
        $counter->updateCounterEndDate($counter_params);
        $current_end_date = $counter->getCounterEndDate($url);
    }
}

//Split Test
if ($conf->getConfigSplitTestStatus()) {

    // Get the SplitTest Enabled at Parent Folder
    $splitTestEnableAtURL = "https://" . $_SERVER[HTTP_HOST] . "/" . explode('/', $_SERVER['REQUEST_URI'])[1] . "/";
    // Requested URL
    $requestURL = "https://" . $_SERVER[HTTP_HOST] . parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

    // Proceed if both are equal
    if (($splitTestEnableAtURL == $requestURL) || ($splitTestEnableAtURL.'index.php' == $requestURL)) {
        spl_autoload_register('Autoload::splitTestLoader');
        $currentURL = getcwd();
        $spTest = new splitTestClient($config[Splittest][Name], $config[Splittest][NumberOfVariants]);
        $redirect_url = $spTest->getRedirectURL($currentURL);
        if (filter_var($redirect_url, FILTER_VALIDATE_URL) == true) {
            $func->redirectPage($redirect_url);
        }
    }
}

// set Campaign cookie
if (!$conf->getConfigTradeshowMode()) {
    if (!empty($url_no_params) || filter_var($url_no_params, FILTER_VALIDATE_URL) != false) {
        $func->setCookieAsSplitVersion($url_no_params);
    }
}

// Check for Webinais
if ($conf->getConfigwebinaris()['status'] && empty($_POST[$config[MailInOne][Mapping][WebinarDate]])) {
    spl_autoload_register('Autoload::webinarisLoader');
    $webinaris = new webinaris();
    // Get the Webinar Dates to show on index.php
    $webinarisDates = $webinaris->getDates();
}


//Image upload
if ($conf->getConfigImageUploadStatus()) {
    $image = new Bulletproof\Image($_FILES);
    $image->setLocation('uploads'); // path for images folder
    if ($image["pictures"]) {
        $upload = $image->upload();
    }
}

// Survey Before Lead Registration
if ($conf->getConfigSurvey()['Status'] && empty($_SESSION['survey'])) {
    $config_survy = $conf->getConfigSurvey();
    // Check the page flow
    if($config_survy['Flow'] == "BeforeLeadRegistration"){
        $success_page = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER[HTTP_HOST] . "/" . explode('/', $_SERVER['REQUEST_URI'])[1].'/'.$config_survy['url'];
    }
    // Redirect the Page
    $func->redirectPage($success_page);
}

// Track Customer Journey
if ($conf->getConfigTrackCustomerJourney() && (isset($_GET['trafficsource']) || $_SERVER['HTTP_REFERER'])) {
    $ts = $_GET['trafficsource'];
    $ref = $_SERVER['HTTP_REFERER'];
    spl_autoload_register('Autoload::trackLoader');
    $track_cus_jry = new TrackCustomer();
    $s = $track_cus_jry->createTouchPoint($ts, $ref);
}

// Post Request
if (isset($_POST[$config[MailInOne][Mapping][EMAIL]])) {

    $params = array();
    $email = $_POST[$config[MailInOne][Mapping][EMAIL]];

    spl_autoload_register('Autoload::fraudLoader');
    $fraud_obj = new FraudDetection();
    $is_fraud = $fraud_obj->isFraudDomain($email);
  
    if (is_bool($is_fraud) && $is_fraud) {
        $_SESSION['error_msg'] = $config['ErrorHandling']['ErrorMsgDomainsNotAcceptedErrorMessage'];
    } else {
        // Email Validation
        $validate = new emailValidatorApiClient($email);
        $validate->curlRequest();
        $resp = $validate->curlResponce();
        if (is_bool($resp) && !$resp) {
            $_SESSION['error_msg'] = $config['ErrorHandling']['ErrorMsgEmail'];
        }
    }
    if (($is_fraud || $invalid_email) && isset($_SESSION['error_msg'])) {
        $_SESSION['data'] = $_POST;
        $_SESSION['error'] = "email";
        $func->redirectPage($url);
        exit();
    }

    // clearing session data
    $_SESSION['error'] = "";
    $_SESSION['data'] = "";
    $_SESSION['error_msg'] = "";

    // Map the POST params to Maileon Params
    $maileon_params = $config[MailInOne][Mapping];

    foreach ($maileon_params as $mp) {
        $params[$mp] = $_POST[$mp];
    }

    //Check for Test mode
    if ($_POST["test_mode"]) {
        $test_mode = $_POST["test_mode"];
    }

    // Adding image path to params and sending information to DB table
    if (isset($upload) && $conf->getConfigImageUploadStatus()) {
        $uploadPath = $upload->getFullPath(); // uploads path in database
        $url = $params[$config[MailInOne][Mapping][url]];
        $chopurl = chop($url, "index.php");
        $uploadimagepath = $chopurl . $uploadPath;
        $params[$maileon_params[uploadPath]] = $uploadimagepath;
    }

    // Check for UTM Params and load from SESSION
    if ($func->checkSession()) {
        $params = $func->checkUTMParamsInPOST($params);
    }

    // Get the Date, IP, URL
    $params[$config[MailInOne][Mapping][Datum]] = date('d.m.Y \\u\\m h:i:s a', time());
    $params[$config[MailInOne][Mapping][ip_adresse]] = $_SERVER['REMOTE_ADDR'];
    $params["url_params"] = parse_url($params[$config[MailInOne][Mapping][url]])["query"];

    spl_autoload_register('Autoload::leadsLoader');
    $lead = new leads();
    $lead_status = $lead->checklead($params);
    if (is_bool($lead_status) && $lead_status == false && !$test_mode && !$conf->getConfigTradeshowMode()) {
        $_SESSION['data'] = $_POST;
        $_SESSION['error'] = "email";
        $_SESSION['error_msg'] = $config[ErrorHandling][ErrorMsgDuplicate];
        $url = $_POST[$config[MailInOne][Mapping][url]] . '#form';
        $func->redirectPage($url);
    } else {
        // Returns the lead reference after creating the Lead
        $_SESSION['email'] = $params[$config[MailInOne][Mapping][EMAIL]];
        spl_autoload_register('Autoload::leadsLoader');

        // User Device Type
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $browser = $func->getBrowserDetails();
            foreach ($browser as $key => $value) {
                $params[$key] = $value;
            }
        }

        // Set Cookie
        if (!$test_mode && !$conf->getConfigTradeshowMode()) {
            $func->setCookieAsEmail($params[$config[MailInOne][Mapping][EMAIL]]);
        }

        // Keep URL Parameters
        $success_page = $config[GeneralSetting][SuccessPage];
        if ($conf->getConfigKeepURLParms() && !empty($params["url_params"])) {
            $success_page .= $params["url_params"];
        }

        // Update the Counter Value
        // Counter does not modify in Test mode
        if ($counter_status && !$test_mode) {
            $new_counter_value = $current_count_value - 1;
            $resp = $counter->updateCounterValue($url, $new_counter_value);
        }

        // Assignment Manager
        if ($conf->getConfigAssignmentManagerStatus()) {
            $assignmentmanager = new assignmentManager();
            $vertriebsmitarbeiter = $assignmentmanager->getAssignmentManager(
                $params[$config[MailInOne][Mapping][ZIP]],
                $config[AssignmentManager][Vertriebsmitarbeiter]
            );
            $vertriebsmitarbeiterid = $assignmentmanager->getAssignmentManagerId();
            $params[$config[MailInOne][Mapping][Vertriebsmitarbeiter]] = $vertriebsmitarbeiter;
            $params[$config[MailInOne][Mapping][Vertriebsmitarbeiterid]] = $vertriebsmitarbeiterid;
        }

        // Update LeadView for CIO Client
        if ($conf->getConfigUpdateLeadView()) {
                //  Create Lead
            if (!$lead->checkEmailDomainInExcludedList($params)) {
                $params[$config[MailInOne][Mapping][LeadReference]] = $lead->createLead($params);
            }

            //  Generate Lead Reference Number
            if ($params[$config[MailInOne][Mapping][LeadReference]] != 0) {
                $lead_reference = '&lead_reference=' . $params[$config[MailInOne][Mapping][LeadReference]];
                $success_page .= trim($lead_reference);
            }

            // Lead Custom Fields
            $lead_cus_fields = $conf->getConfigLeadCustomFields();
            if (count($lead_cus_fields) > 0) {
                    //  Get Campaign Detials
                spl_autoload_register('Autoload::campaignLoader');
                $camp_obj = new campaign();
                $camp_detail = $camp_obj->getCampaignDetailsByURL();

                //  Get Lead Custom Field detials
                spl_autoload_register('Autoload::leadsLoader');
                $lead_field_obj = new LeadCustomField();
                $lead_field_id = $lead_field_obj->getCustomField((int)$camp_detail['id'], "CustomField")['id'];

                //  Check for Custom Field
                if (is_null($lead_field_id) || (int)$lead_field_id <= 0) {
                    // Insert a new Custom Field
                    $ins_str = $lead_field_obj->getLeadCustomFieldValues(
                        (int)$camp_detail['id'],
                        "CustomField",
                        "string"
                    );
                    $lead_field_id = $lead_field_obj->insertCustomLeadField($ins_str);
                }

                //  Get Lead Details
                $lead_detail = $lead->getLeadDetailsByEmail($_SESSION['email']);

                //  Insert new Lead Custom Data.
                $lead_data_obj = new LeadCustomData();
                $ins_query = $lead_data_obj->getInsertQueryMultiRow(
                    $lead_cus_fields,
                    (int)$lead_detail['id'],
                    (int)$lead_field_id
                );
                $status = $lead_data_obj->insertMultiRowsCustomData($ins_query);
            }

            // if ($conf->getConfigImageUploadStatus()) {
                //     $cf = new customLeads();
                //     $cf->uploadCustomField($params);
                // }
        }

        // Update Touch Points to DB
        if ($conf->getConfigTrackCustomerJourney()) {
            spl_autoload_register('Autoload::trackLoader');
            $track_cus_jry = new TrackCustomer();
            // Get the Lead Detials
            if (empty($lead_detail) || !isset($lead_detail['id']) || !is_numeric($lead_detail['id'])) {
                $lead_detail = $lead->getLeadDetailsByEmail($_SESSION['email']);
            }
                
            $track_cus_jry -> insertTouchPointsToDB($lead_detail['id']);
        }

        //Hubspot Configuration Sync
        if ($conf->getConfigHubspotStatus() && !$lead->checkEmailDomainInExcludedList($params)) {
            spl_autoload_register('Autoload::hubspotLoader');
            $hubspot = new hubspotApiClient();
            $params[$config[MailInOne][Mapping][HubspotContactID]] = $hubspot->createContact($params);
            $params[$config[MailInOne][Mapping][HubspotDealID]] = $hubspot->createDeal($params);
            $hubspot->associateDealWithContact($params);
        }

        // Forward Email
        if ($conf->getConfigForwardEmail() && !$lead->checkEmailDomainInExcludedList($params)) {
            spl_autoload_register('Autoload::mailLoader');
            $mail = new Mail();
            $mail->sendMail($params);
            // $mail = new forwardMail();
                // $s = $mail->forwardEmail($params);
        }

        // Maileon Configuration Sync
        if ($conf->getConfigMailInOneStatus()) {
            spl_autoload_register('Autoload::maileonLoader');
            $maileon = new maileonApiClient();
            $maileon->IntegrateContactToMailInOne($params);
        }

        //webinaris
        $webinar = $conf->getConfigwebinaris();
        if ($webinar[status]) {
            spl_autoload_register('Autoload::webinarisLoader');
            $webinaris = new webinaris();
            $sendDatawebinaris = $webinaris->sendDataWebinaris($params);
            $webinarisDates = $webinaris->getDates();
        }

        //DigiStore Integration
        $config_digi = $conf->getConfigDigistore();
        if ($config_digi['sync']) {
            spl_autoload_register('Autoload::digistoreLoader');
            $dgstore = new digistore();
            $digi_buyurl = $dgstore->get_digistore_buyurl($params);
                
            // Update SuccessPage if the digiStore URL is obtained.
            $_SESSION['digistore_buyurl'] = filter_var($digi_buyurl, FILTER_VALIDATE_URL)? $digi_buyurl : " ";
        }

        //sendData
        if ($conf->getConfigSendData()) {
            $sendData = $func->sendData($params);
            if (!empty($sendData)) {
                $success_page .= $sendData;
            }
        }

        // Survey After Lead Registration
        if ($conf->getConfigSurvey()['Status']) {
            $config_survy = $conf->getConfigSurvey();

            // Check the page flow
            if ($config_survy['Flow'] == "AfterLeadRegistration") {
                $success_page = $config_survy['url'];
            } elseif ($config_survy["Flow"] == "BeforeLeadRegistration" && !empty($_SESSION['survey'])) {
                $sur_ans = $_SESSION['survey']['lead_ans'];
                $s_id    = $_SESSION['survey']['survey_id'];
                $c_id    = $_SESSION['survey']['campaign_id'];
                unset($_SESSION['survey']);

                spl_autoload_register('Autoload::surveyLoader');
                $s_Obj = new postSurvey($s_id, $c_id);
                $status = $s_Obj->postSurveyAnswers($sur_ans);

                if (is_bool($status) && $status == true) {
                    $success_page = $config['GeneralSetting']['SuccessPage'];
                } else {
                    $success_page = $config['GeneralSetting']['ErrorPage'];
                }
            }
        }
        // Redirect the Page
        $func->redirectPage($success_page);
    }
    //}
}
