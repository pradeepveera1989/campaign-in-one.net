<?php
require 'cio.php';

# Facebook Pixel URL
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
    
  <title>cio | master</title>
    
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/header-animation.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet"> 
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  
  <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
    <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
    <script type="text/javascript" src="vendor/bxslider/src/js/jquery.bxslider.js "></script>
    <script type="text/javascript" src="vendor/google/maps.js"></script>
    <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
    <script type="text/javascript">
        var countdown_status = "<?= $counter_status; ?>";
        var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
		var countdown_expire_period = "<?= $config[GeneralSetting][MaximumCountdownperiod]; ?>";
		var countdown_end_date = "<?= $current_end_date ?>";
		var single_submit = "<?= $config[GeneralSetting][SingleSubmit]; ?>";
		var webgains_cookie = "<?= $webgains_cookie ?>";
        var current_stocks = "<?= $current_count_value; ?>";
        var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
        var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
        var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
        var validate_telefon = "<?= $config[Telefon][Status]; ?>";
        var single_submit_text = "<?= $single_submit_text ?>"
        var cookie = "<?= $cookie; ?>";
        var fb_events = "<?= $config[TrackingTools][FacebookEvent] ?>";
        var fb_event = (fb_events.split(",")); // Split the events to array
        var fb_curreny = "<?= $config[TrackingTools][FacebookCurrency] ?>";
        var fb_value = "<?= $config[TrackingTools][FacebookValue] ?>";
    </script>

        <script language="JavaScript" type="text/javascript" src="cio.js"></script>          

        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?> 
    
</head>
    
    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once('clients/tracking/outbrain/outbrain_index.php') : " "; ?>

    <body>
        <!-- Web Gains Tracking -->
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_index.php' : " "; ?>

        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_index.php' : ' ' ;?>  
        

<!-- HEADER -->
<header class="masthead text-white text-center">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 mx-auto">
                <!-- STAR BG -->
                <div id='stars'></div>
                <div id='stars2'></div>
                <div id='stars3'></div>
                <!-- /. STAR BG --> 
                <div id='title'>
                    <h1>CAMPAIGN-IN-ONE</h1>
                    <h2>WE TACKLE YOUR TARGET GROUP</h2>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ./HEADER --> 
        
<!-- COUNTDOWN -->
<section>
    <div class="countdown">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <span style="letter-spacing: 3px; font-family: 'Titillium Web', sans-serif; color:#fff;" class="countdown-nav">OFFER ENDS IN</span>
                    <span style="letter-spacing: 3px; font-family: 'Titillium Web', sans-serif; color:#fff;" class="countdown-nav" id="demo"></span>
                </div> 
            </div>
        </div>   
    </div>  
</section>   
<!-- ./ COUNTDOWN --> 
    
<!-- OPTIN -->
<section class="optin">
    <div class="container">
        <div class="row"> 
            <div class="col-lg-6">
                <h1>CONVERSION OPTIMISATION</h1>
                <p>Landing pages are specialized webpages that are designed solely for one purpose. The user needs to understand on the first glance the benefits of your offer. Landing pages encourage a user to perform a clear call-to-action (CTA) e.g. “buy now”, “download” etc.</p>
                <p>From the beginning we focus all aspects of conversion optimization. Our experience from several thousand campaigns enables us to generate more online business for you.</p>   
            </div>
            <div class="col-lg-6">   
                <!-- FORM BEGIN -->
                <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                    <input type="hidden" name="contactid" value="<?= $contactid ?>">
                    <input type="hidden" name="checksum" value="<?= $checksum ?>">
                    <input type="hidden" name="mailingid" value="<?= $mailingid ?>">
                    <!-- INPUT ANREDE FRAU & HERR -->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div style="width:100%;"class="btn-group btn-group-toggle" data-toggle="buttons">  
                                <label style="cursor:pointer; width:100%;" class="btn btn-outline-dark">
                                    <input class="sr-only" style="color:#fff; ;background:transparent;" value="Frau" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option1" autocomplete="off" required> Mrs
                                </label>
                                <label style="cursor:pointer; width:100%;" class="btn btn-outline-dark">
                                    <input class="sr-only" style=" color:#fff; ;background:transparent;" value="Herr" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" id="option2" autocomplete="off" required> Mr
                                </label>
                            </div>
                        </div>       
                    </div>                       
                    <!-- ./ INPUT ANREDE FRAU & HERR -->   
  
                    <div class="form-row">
                        <div class="form-group col-md-6">    
                            <!-- INPUT VORNAME -->
                            <input style="color:#000;background:transparent;" value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data'][$config[MailInOne][Mapping][FIRSTNAME]])); ?>" class="btn-outline-dark input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="Firstname" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>                                     
                            <!-- ./INPUT VORNAME --> 
                        </div>
                        <div class="form-group col-md-6">
                            <!-- INPUT NACHNAME -->
                            <input style="color:#000; ;background:transparent;" value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data'][$config[MailInOne][Mapping][LASTNAME]])); ?>" class="btn-outline-dark input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="Lastname" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>                                
                            <!-- ./INPUT NACHNAME --> 
                        </div>
                    </div> 
                    <div class="form-row">
                        <div class="form-group col-md-6">   
                            <!-- INPUT TELEFON -->
                            <input style="color:#000;background:transparent;" value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="btn-outline-dark input-fields form-control" id="telefon-mobile"  placeholder="Telephone" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >
                            <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden>                  
                            <!-- ./INPUT TELEFON --> 
                        </div>
                        <div class="form-group col-md-6">
                            <!-- INPUT EMAIL --> 
                            <input style="color:#000; ;background:transparent;" value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data'][$config[MailInOne][Mapping][EMAIL]])); ?>" class="btn-outline-dark input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="Email address" type="email" required 
                                   <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                                       
                            <!-- ./INPUT EMAIL --> 
                        </div>
                    </div>
                    
                     <!--Hidden Fields-->   
                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">URL</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                    </div>
                                </div>                                        

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Source</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>" >
                                    </div>
                                </div>  

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                    </div>
                                </div>      

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Term</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                    </div>
                                </div>                             

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Content</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                    </div>
                                </div>                              

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                    </div>
                                </div>    

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                    </div>
                                </div>                                               

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Quelle</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Typ</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Segment</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" ame="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <label class="col-sm-2 control-label" for="">Test Mode</label>
                                    <div class="col-sm-10">
                                        <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                    </div>
                                </div>
                    
                    <div class="card" style="margin-bottom:15px;">
                        <div class="card-header">
                            &nbsp;&nbsp;
                            <input class="form-check-input" type="checkbox" id="gridCheck" required>
                            <label class="form-check-label" for="gridCheck">
                                <small>I, hereby accept the<a href="#"> GTC</a> and <a href="#">Data protection</a></small>
                            </label>
                        </div>
                    </div>  
                    <div class="form-row">
                        <div class="form-group col-md-12">     
                            <input style="cursor: pointer; background-color: #64a676; border-color: #64a676;" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="SUBMIT" name="submit" style="width: 100%;">   
                        </div>
                    </div> 
                    
                    
                    <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                     <div class="alert alert-danger" role="alert"><span class="p-light erroremail"  style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span></div>
                     <?php } ?>    
                    
                    
                    
                    <?php if (isset($response) && $response->isSuccess()) { ?>
                    <div class="alert alert-success fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Subscription successful</strong>
                    </div>
                    
                    <?php } elseif (isset($warning)) { ?>
                    <div class="alert alert-warning fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                        <?= $warning['message'] ?>
                    </div>
                    
                    <?php } elseif (isset($response)) { ?>
                    <div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Subscription failed</strong>
                    </div>
                    <?php } ?>
                </form> 
                <!-- ./ FROM END -->
            </div>          
        </div>  
    </div>
</section>
<!-- ./OPTIN -->
    
<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                <ul class="list-inline mb-2">
                    <li class="list-inline-item">
                        <a href="#">Terms of Use</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item">
                        <a href="#">Privacy Policy</a>
                    </li>
                </ul>
                <p style="color:#c5c5c5!important;" class="text-muted small mb-4">&copy; Campaign-In-One. All Rights Reserved.</p>
            </div>
            <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item mr-3">
                        <a href="#">
                            <i class="fab fa-facebook fa-2x fa-fw"></i>
                        </a>
                    </li>
                    <li class="list-inline-item mr-3">
                        <a href="#">
                            <i class="fab fa-twitter-square fa-2x fa-fw"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">
                            <i class="fab fa-instagram fa-2x fa-fw"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
    
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
</body>
</html>