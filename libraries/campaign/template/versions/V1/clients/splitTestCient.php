<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of splitTest-client
 *
 * @author Pradeep
 */
class splitTestClient {
//put your code here
    
    public function __construct($spTestName, $varients){
        
        $this->spTestName = $spTestName;
        $this->varients = $varients;
        $this->spTestObj = new splitTest($spName, $varients);
        $this->config = new config('config.ini');
        $this->config_split = $this->config->getConfigSplitTest();
        $this->leads = new leads();
    }
    
    
    public function getRedirectURL($currentURL){
       
        $this->dir = $currentURL;
        $redirectURL ;
        if($this->varients <= 0 && strlen($this->spTestName) == 0 && strlen($this->dir) <= 0){
            
            return false;
        }
        
        if($this->config_split['CreateVariants'] && !$this->createVarientFolder()){
    
            return false;
        }
        
        $url_parts = explode('?', $_SERVER['REQUEST_URI'], 2); 
        $php_parts = explode('/', $_SERVER['REQUEST_URI']);
        $index = $php_parts[sizeof($php_parts) - 1];
        array_pop($php_parts);
        $link = implode($php_parts, "/");    

        
        #Check for StikyVersionOnCookie
        if($this->config->getConfigSplitTest()['StikyVersionOnCookie'] && $_COOKIE["cookie"]["splitversion"]){
            
            $redirectURL = "https://" . $_SERVER['HTTP_HOST'] . $link .'/'. $_COOKIE["cookie"]["splitversion"].(empty($url_parts[1]) ? "": "?".$url_parts[1]);
            
        }#Check for StikyVersionOnIPAdress
        else if($this->config->getConfigSplitTest()['StikyVersionOnIPAdress'] && $this->leads->getCampaignByIPAddress($_SERVER['REMOTE_ADDR'])['final_url']){
           
            $campaign_url = $this->leads->getCampaignByIPAddress($_SERVER['REMOTE_ADDR'])['final_url'];
            $redirectURL = $campaign_url . (empty($url_parts[1])? "": "?".$url_parts[1]);
            
        }#Create SplitTest
        else if($this->createSplitTest()){
           # Generate a new validate link.
                
           $redirectURL = "https://" . $_SERVER['HTTP_HOST'] . $link . "/" . $this->sp_link .(empty($url_parts[1]) ? "": "?".$url_parts[1]);  
        }
        #If redirect URL is empty 
        if(empty($redirectURL)){
            
            $redirectURL = "https://" . $_SERVER['HTTP_HOST'] . $link .(empty($url_parts[1]) ? "": "?".$url_parts[1]);
        }
        
        return $redirectURL;
    }
    
    public function createSplitTest(){
        
        if(empty($this->spTestObj)){
            
            return false;
        }
        
        if(!($this->spTestObj->tableExists())){

			# Excutes only once while creating the splitTest
			$this->spTestObj->createTable();
			$sp_index = 1;            
        }else {
            
			#Get the current value of split Test index
			$sp_index = $this->spTestObj->getCurrentIndex();
		}	

		$sp_new_index = $this->spTestObj->getNewIndex($sp_index);
        $this->sp_link = $this->spTestName . '/' . $sp_new_index . '/index.php';
        
        return true;         
    }
    
    public function createVarientFolder(){
        
        $v = $this->varients;

        while($v > 0){
            
			$folder = $this->spTestName.'/'.$v.'/';
			if(!is_dir($folder)){
                
				mkdir( $folder, 0777, true);
			}			
            
			$this->copyDirectory( $this->dir, $folder, $this->spTestName);

			if(!$this->modifyConfigFile($folder.'config.ini')){
				
				#something went wrong
				return false;
			}			

            $v = $v - 1;
        }
        return true;
    }
    
	/* Function Name : copyDirectory
	 *
	 * Parameters : 
	 *    1. Source Path
	 *    2. Destination Path
	 *    3. Name of the splitTest
	 *
     * Returns : 
     *   
     */
	 
	function copyDirectory($source, $destination, $spTestName){
        
		# Creates destination directory 
		if(!is_dir($destination)){
            
			mkdir( $destination, 0777, true);
		}		
		$dir  = opendir($source);
		
		#Recursively copies all the files and folders.
		while(false !== ($file = readdir($dir))){
            
			if (( $file != '.' ) && ( $file != '..' )) {
                
                if ( is_dir($source . '/' . $file) ) {
                    
                    if($file !== $spTestName){

						$this->copyDirectory($source . '/' . $file, $destination . '/' . $file);	
					}
				}
				else {
                    
					copy($source . '/' . $file,$destination . '/' . $file);
				}
			}	
		}			
	}
	
	/* Function Name : modifyFile
	 *
	 * Parameters : 
	 *    1. config.ini file path for each Varient
	 *
     * Returns :  
	 *	  Success :  true
	 *    Failure :  false
     */	
	
	function modifyConfigFile($file){
        
   
		$filecontent = file_get_contents($file, true);
		$dir = getcwd();
		$dir .= '/'.$file;
		$status = true;
		
		if(isset($filecontent)){
            
			$filecontent .= "\n\n[Varient] \n VarientFile = true \n";
		}else {
            
			var_dump("Unable to read the contents from config.ini from", $dir);
			$status = false;
		}
		if(!file_put_contents($file, $filecontent)){
            
			var_dump("Unable to write the contents to config.ini at", $dir);
			$status = false;
		}
        
		return $status;
	}    
}
