<?php

/*
 * To Visualize all leads for the 'campaigns.cio_leads'
 * @author Pradeep
 */


class leads {
    
    private $dbObject;
    
    public function __construct() {
        $this->table = "`cio.leads`";
        $this->dbObject = new db();
        $this->config = new config("config.ini");
    }

    /**
     * createLead:
     * 
     * create a lead in the database table by creating a new tracking number 
     * 
     * @param type $params
     * @return boolean|string     
     */   
    public function createLead($params){
        $lead_params = array();
        $return = 0;
        $operation = "insert";
        $config_lead = $this->config->getConfigLeadView();
        $config_mailinone = $this->config->getConfigMailInOne();
        
        if(empty($params)){
            
            return $return;
        }
        # Return false if no databse object is found
        if(!$this->dbObject){
            
            return $return;
        }
 
        // Get the SplitVersion Params
        $lead_params = $this->getSplitVersionParams($params);
        //Get the Campaign URL
        $params[$config_mailinone[Mapping][url]] = $this->getCampaignURL($params);
        // Maping the HTML parameter to table column fields
        $lead_params = array_merge($lead_params, $this->mapParams($params)); 
        //Merge the log Created Params
        $lead_params = array_merge($lead_params, $this->getLogCreatedParams()); 
        //Merge Log Update Params
        $lead_params = array_merge($lead_params, $this->getLogUpdatedParams()); 
        //Merge Campaign Id
        $lead_params = array_merge($lead_params, $this->getCampaignId($params));  
        //Merge Company Id
        $lead_params = array_merge($lead_params, $this->getCompanyId($params));   
        //Merge Account Id Which is similar to Company Id in 'cio.company'
        $lead_params = array_merge($lead_params, $this->getCompanyId($params));      
        // Merge LeadTracking Information
        $lead_params = array_merge($lead_params, $this->getLeadTrackingRef());
        // UTM Parameters
        $lead_params = array_merge($lead_params, $this->getUTMParamters($params));
        // Device Type
        $lead_params = array_merge($lead_params, $this->getDeviceType($params));        
        
        // Insert the Lead to Database
        $status = $this->dbObject->execute($this->table, $lead_params, "", $operation);
        
        if(!$status){
            return $return;
        }
        $return = $lead_params[$config_mailinone[Mapping][LeadReference]];

        return $return;
    }   


    /**
     * getCampaignId:
     * 
     * Campaign Id of the campaign from the database 
     * 
     * @param type $params
     * @return boolean|string     
     */        
    private function getCampaignId($params){
        
        $campaign_id = 1;

        if(empty($params['url']) || empty($this->dbObject)){
            
            return [
                "campaign_id" => $campaign_id
             ];
        }
        
        $select = array('id','account');
        $where = array("url LIKE '".$params['url']."'");
        $table = "`cio.campaign`";
        $result = $this->dbObject->getOne($select, $table, $where);
        
        if(empty($result['id'])){
            
            return [
                "campaign_id" => $campaign_id
            ];
        }
        $this->campaign_id = $result['id'];
        
        return [
            "campaign_id" => $this->campaign_id
        ];
    }
    
    /**
     * getSplitVersionParams:
     * 
     * Get the Parameters for SplitTest
     * 
     * @param type $params
     * @return Array     
     */     
    private function getSplitVersionParams($params){
        
        $split_params = [
            "final_url" => "",
            "split_version" => ""
        ];
        
        $config_mailinone = $this->config->getConfigMailInOne();
        $config_split = $this->config->getConfigSplitTest();       
        
        if(empty($params)){
            
            return $split_params;
        }
        
        $split_params["final_url"]      = $params[$config_mailinone[Mapping][url]]  ? $params[$config_mailinone[Mapping][url]]   : " ";
        $split_params["split_version"]  = $config_split[Version] ? $config_split[Version] : " ";
        
        return $split_params;
    }
    

    /**
     * getCampaignId:
     * 
     * Get the Device parameters of the Lead
     * 
     * @param type $params
     * @return Array     
     */        
    private function getDeviceType($params){   
        
        $device = [
            "device_type" => "",
            "device_browser" => "",
            "device_browser_version" => "",
            "device_os" => "",
            "device_os_version" => ""
            ];
        
        $config_mailinone = $this->config->getConfigMailInOne();
        
        if(empty($params)){
            return $device;
        }
        
        return [
            "device_type" => $params[$config_mailinone[Mapping][DeviceType]],
            "device_browser" => $params[$config_mailinone[Mapping][DeviceBrowser]],
            "device_browser_version" => $params[$config_mailinone[Mapping][DeviceBrowserVersion]],
            "device_os" => $params[$config_mailinone[Mapping][DeviceOs]],
            "device_os_version" => $params[$config_mailinone[Mapping][DeviceOsVersion]],
            "ip" => $params[$config_mailinone[Mapping][ip_adresse]]
        ];
    }
    

    /**
     * getCompanyId:
     * 
     * Company Id from the company present in the URL 
     * 
     * @param type $params
     * @return boolean|string     
     */    
    private function getCompanyId($params){
        
        $company_id = 1;      
        $account = "";
        if(empty($params)){
            
            return [
                "company_id" => $company_id,
            ];
        }
        
        $select = array('company_id');
        $where = array("url LIKE '".$params['url']."'");
        $table = "`cio.campaign`";
        $result = $this->dbObject->getOne($select, $table, $where);  

        if(empty($result)){
            
            return [
                "company_id" => $company_id,
            ];            
        }
        
        $this->company_id = $result['company_id'];
        
        return [
            "company_id" => $this->company_id
        ];          
    }
    
    
    
    /**
     * getCampaignURL:
     * 
     * Campaign is first folder after domain, Client is Sub-Domain 
     * 
     * @param type $params
     * @return boolean|string     
     */
    private function getCampaignURL($params){
         
        // Get Config Mail-in-one Mapping
        $config_mailinone = $this->config->getConfigMailInOne();
        $trucated_url = "";
        if(empty($params[$config_mailinone[Mapping][url]])){
            // return Null if no URL is present
            return $trucated_url;
        }
        // explode the https and address part.
        $url_address = explode('//', $params[$config_mailinone[Mapping][url]]);
        // explode other folders present in URL
        $url_folders = explode('/', $url_address[1]);
        // check if subfolder is present or not
        if(isset($url_folders[1])){
            // append the url with domain with subfolder
            $url_address[1] = $url_folders[0].'/'.$url_folders[1];
        }else{
            // Consider only the url
            $url_address[1] = $url_folders[0];
        }
        // Append all the url parts.
        $trucated_url = $url_address[0].'//'.$url_address[1];

        return $trucated_url;
    }
    
    
    
    /*
     * getLeadTrackingRef:
     * 
     * Campaign is first folder after domain, Client is Sub-Domain 
     * 
     * @param type $params
     * @return boolean|string    
     */
    private function getLeadTrackingRef(){
        
        $lead_ref = "";
        $campaign_counter = 0;
        $config_mailinone = $this->config->getConfigMailInOne();
        // Get the Account and Campaign value
        $lead_config = $this->config->getConfigLeadView();
        
        if(!empty($this->company_id)){
            $account_id = $this->company_id;
        }
        
        // Could not read the config.ini file
        if(empty($lead_config)){
            return [
                "account" => $account_id,
                "campaign" => "",
                $config_mailinone[Mapping][LeadReference] => "",
            ];
        }
       
        // Generate the Counter
        #$account = $lead_config['Account'];
        $campaign = $lead_config["Campaign"];
        $select = array('lead_reference');
        $where = array("account = $account_id AND campaign LIKE '$campaign' ORDER BY `id` DESC");
        $table = "`cio.leads`";        
        $result = $this->dbObject->getOne($select, $table, $where);
#        $count = $this->dbObject->getOne($this->table, ["account = $account_id", "campaign = $campaign"]); 
        $old_lead_ref = $result['lead_reference'];
        
        // Could not get the Count of campaigns
        if(is_null($old_lead_ref)){
            
            $campaign_counter = $campaign_counter + 1;       
            $lead_ref = $account_id.'-'.$campaign.'-'.sprintf("%'.08d\n", $campaign_counter);
            return [
                "account" => $account_id,
                "campaign" => $campaign,
                $config_mailinone[Mapping][LeadReference] => $lead_ref,
            ];
        }
        
        // Get the Campaign count from old Lead refernce
        $old_lead_ref = $result['lead_reference'];
        $lead_buffer = explode('-', $old_lead_ref);
        $campaign_counter = (int)$lead_buffer[2];
        
        // increment the counter
        $campaign_counter = $campaign_counter + 1;       
        // Generate lead refeence tracking number
        $lead_ref = $account_id.'-'.$campaign.'-'.sprintf("%'.08d\n", $campaign_counter);

        return  [
            "account" => $account_id,
            "campaign" => $campaign,
            $config_mailinone[Mapping][LeadReference] => $lead_ref,
        ];
        
    }
       
    
    /**
     * getCampaignURL:
     * 
     * Campaign is first folder after domain, Client is Sub-Domain 
     * 
     * @param type $params
     * @return boolean|string     /
     */    
    public function getUTMParamters($params){
        
        if(empty($params)){
            
            return false;
        }
        $config_mailinone = $this->config->getConfigMailInOne();
        $utm_params = array();
        
        if(array_key_exists($config_mailinone[Mapping][trafficsource], $params) && !empty($params[$config_mailinone[Mapping][trafficsource]])){
            
            $utm_params[$config_mailinone[Mapping][trafficsource]] = $params[$config_mailinone[Mapping][trafficsource]];
        }
        if(array_key_exists($config_mailinone[Mapping][utm_term], $params) && !empty($params[$config_mailinone[Mapping][utm_term]])){
            
            $utm_params['utm_parameters'] = 'utm_term='.$params[$config_mailinone[Mapping][utm_term]].'&';
        }
        if(array_key_exists($config_mailinone[Mapping][utm_name], $params) && !empty($params[$config_mailinone[Mapping][utm_name]])){
            
            $utm_params['utm_parameters'] .= 'utm_name='.$params[$config_mailinone[Mapping][utm_name]].'&';
        }        
        if(array_key_exists($config_mailinone[Mapping][utm_content], $params) && !empty($params[$config_mailinone[Mapping][utm_content]])){
            
            $utm_params['utm_parameters'] .= 'utm_content='.$params[$config_mailinone[Mapping][utm_content]].'&';
        } 
        if(array_key_exists($config_mailinone[Mapping][utm_medium], $params) && !empty($params[$config_mailinone[Mapping][utm_medium]])){
            
            $utm_params['utm_parameters'] .= 'utm_medium='.$params[$config_mailinone[Mapping][utm_medium]].'&';
        } 
        if(array_key_exists($config_mailinone[Mapping][utm_source], $params) && !empty($params[$config_mailinone[Mapping][utm_source]])){

            $utm_params['utm_parameters'] .= 'utm_source='.$params[$config_mailinone[Mapping][utm_source]].'&';
        }      
        if(array_key_exists($config_mailinone[Mapping][utm_campaign], $params) && !empty($params[$config_mailinone[Mapping][utm_campaign]])){

            $utm_params['utm_parameters'] .= 'utm_campaign='.$params[$config_mailinone[Mapping][utm_campaign]];
        }

        return $utm_params;
    }
    
    
    /*
     * getLogCreatedParams:
     * 
     * Returns the Log created date and User
     * 
     * @param type $params
     * @return boolean|string    
     */    
    private function getLogCreatedParams(){
        
        $log_create= array(
            "created"=> date('Y-m-d H:i:s', time()),
            "created_by" => 0
        );
        return $log_create;
    }
    
    
    /*
     * getlogUpdatedParams:
     * 
     * Returns the Log Updated date and User
     * 
     * @param type $params
     * @return boolean|string    
     */       
    private function getlogUpdatedParams(){
        
        $log_update = array(
            "updated" => date('Y-m-d H:i:s', time()),
            "updated_by" => 0
        );
        
        return $log_update;
    }


    /*
     * getCampaignByIPAddress:
     * 
     * User for Sticky Version of Campaign Version based on IP address
     * Returns the Campaign Details for Ip address
     * 
     * @param type $params
     * @return boolean|string    
     */          
    public function getCampaignByIPAddress($ip){
        
        if(empty($ip)){
            
            return false;
        }

        $select = array("*");
        $where = array("ip LIKE '".$ip."'");
        $table = "`cio.leads`";
        $result = $this->dbObject->getOne($select, $table, $where); 
        
        return $result;
    }
    
    
    /*
     * checklead:
     * 
     * Checks the Lead email id with "cio.leads" table
     * 
     * @param type $params
     * @return boolean|string    
     */        
    public function checklead($params){

        if(empty($params) || empty($params["email"])){
            
            return false;
        }
        
        $config_lead = $this->config->getConfigLeadView();
        $config_mailinone = $this->config->getConfigMailInOne();
        
        $url = $this->getCampaignURL($params);
        $email = $params[$config_mailinone[Mapping][EMAIL]];
        $campaign = $config_lead['Campaign'];
        $select = array('id','created');
        $where = array("url LIKE '$url' AND email LIKE '$email' AND campaign LIKE '$campaign'  ORDER BY `id` DESC");
        $table = "`cio.leads`";        
        $result = $this->dbObject->getOne($select, $table, $where);

        return $result;
    }
    
   
    
    /*
     * mapParams:
     * 
     * Necessary maping is done with MIO params and cio.leads table for all the Parameters
     * 
     * @param type $params
     * @return boolean|string    
     */     
    private function mapParams($params){
        
        if(empty($params)){
            
            return false;
        }
        $config_mailinone = $this->config->getConfigMailInOne();
        $lead_params = array();
        $mapping = array(
            $config_mailinone[Mapping][SALUTATION] => "salutation",
            $config_mailinone[Mapping][FIRSTNAME] => "first_name",
            $config_mailinone[Mapping][LASTNAME] => "last_name",
            $config_mailinone[Mapping][EMAIL] => "email",
            $config_mailinone[Mapping][ZIP] => "postal_code",
            $config_mailinone[Mapping][Telefon] => "phone",
            $config_mailinone[Mapping][CITY] => "city",
            $config_mailinone[Mapping][ADDRESS] => "street",
            $config_mailinone[Mapping][url] => "url",
            "country" => "country",
        );
        $count = count($params);

        foreach ($params as  $key => $value){
            foreach($mapping as $mkey => $mval){
                if($key === $mkey){
                    $lead_params[$mval] = $value;
                }
            }
        }
        
        return $lead_params;     
    }
}

