/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    // HotFix for Safari
    // Problem with Date function in Safari
    // Convert the CountDownEndDate to TimeStamp
    var countdown_date = countdown_end_date.split("-");
    var countDownDate = new Date(countdown_date.join('/')).getTime();

    var currentDate = new Date();

    if (currentDate < countDownDate) {
        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = days + " DAYS " + hours + " HOURS " +
                    minutes + " MINUTES " + seconds + " SECONDS ";

            // If the count down is finished, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = countdown_expire_message;
            }
        }, 1000);
    }else {
        disableContactForm(countdown_expire_message);
        disableCampaign();
        }

    $('.close').click(function () {
        $('#modal-video').hide();
        $('#modal-video iframe').attr("src", jQuery("#modal-video iframe").attr("src"));
    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll > 660) {
            $(".nav-bottom").addClass("change"); // you don't need to add a "." in before your class name
        } else {
            $(".nav-bottom").removeClass("change");
        }
    });

    // Error messages
    $('.errorplz').hide();
    $('.errortelefon').hide();
    $('.errorimage').hide();

    console.log("Cookie status",$.cookie('cookie[email]') && single_submit && !trade_show_mode);

    //Cookie 
    if ($.cookie('cookie[email]') && single_submit && !trade_show_mode) {
        disableContactForm(single_submit_text);
    }


    // Countdown counter
//    if (countdown_status != 1 || current_stocks <= 0) {
//        disableContactForm(countdown_expire_message);
//    }


    function disableContactForm(err_msg) {
        $("input.input-fields").prop('disabled', true);
        $("input[type=radio]").prop('disabled', true);
        $("input[type=checkbox]").prop('disabled', true);
        $("span.countdown-nav").css('display', 'none');
        $("span.countdown-nav:last-child").replaceWith("<span style='color:#fff;font-size:16;text-transform:uppercase'>" + err_msg + "</span>");
    }

    function disableCampaign(){
        $('body').css('opacity', campaign_expire_transparency);
        $('html').append("<p style='color:"+campaign_expire_bgcolor+"; position:absolute; top:50px; left:43%;'>"+countdown_expire_message+"</p>");
    }

    // Hide the Ort field but allow the validation of PLZ
    if (parseInt(hide_ort_field) >= 1 && hide_ort_field) {
        $('#form-group-ort').hide();
    }


    // clint side image verification
    $('#uploadImage').blur(function () {
        var ext = $('#uploadImage').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            $('.errorimage').show();
        } else {
            $('.errorimage').hide();
        }
    });


    /* JQuery Check for Empty fields 
     * Parameters : 
     * Returns : Boolean
     *    TRUE  : If any of the Input field is empty 
     *    FALSE : If no of the Input field is empty in the form 
     */
    function checkEmptyFieldSubmit(){
        var status = false;
        var fields = {
            "strasse"   :$("input[name='"+mapping_strasse+"']").val(),
            "telefon"   :$("input[name='"+mapping_telefon+"']").val(),
            "vorname"   :$("input[name='"+mapping_vorname+"']").val(),
            "nachname"  :$("input[name='"+mapping_nachname+"']").val(),
            "ort"       :$("input[name='"+mapping_ort+"']").val(),
            "plz"       :$("input[name='"+mapping_plz+"']").val(),
            "email"     :$("input[name='"+mapping_email+"']").val(),
        };

        // Loops to all the fields
        $.each( fields, function( key, value ){
            if($.trim(value).length <= 0){
                status = true;
            }
        });        
        
        return status;
    }

    /* JQuery submit event for the postal code
     * Parameters : 
     * Returns : 
     *    1. check the postal code and accordingly display the error message for postal code.
     */
    form = $('.check-submit-form');
    form.submit(function (e) {
        //e.preventDefault();

        var form = $(this);
        var readyforsubmit = true;
        var plz = $('.plz');
        //var ort = $('.ort');
        var email = $('.email');
        if (autofill_postal_code) {
            if (plz.hasClass("wrongplz")) {
                $('.errorplz').show();
                return false;
            }
        }
        // Check for Empty fields Submit
        var checksubmit = checkEmptyFieldSubmit();
        if((checksubmit)){
            return false;
        }
        
        // Check for Autofill Validation
        if (address_validation) {
            if ($('.erroradress').css('display') !== 'none') {
                return false;
            }
        }
        // Check for Block Email Validation
        if (block_free_mailer) {
            if ($('.error_blocker_email').css('display') !== 'none') {
                return false;
            }
        }
        // Check for Telefon Validation
        if (validate_telefon) {
            if ($('.errortelefon').css('display') !== 'none') {
                return false;
            }
        }

        if (image_upload) {
            if ($('.errorimage').css('display') !== 'none') {
                return false;
            }
        }
        form.find("input[type='submit']").prop('disabled', true);
    });
});
