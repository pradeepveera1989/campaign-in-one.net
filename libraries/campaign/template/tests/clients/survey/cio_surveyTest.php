<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cio_surveyTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;

class cio_surveyTest extends TestCase{
    //put your code here
    public function setUp() {
        $this->campaign_id = 2;
        $this->survey = new cio_survey($this->campaign_id);
    }          
    
    public function testGetSurveryFormatQuestionTypeInputfield(){
      
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "inputfield", 'id' => "0", 'question' => "Test"]));    // Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "NotInputField", 'id' => "1", 'question' => "Test"])); // question_type is not Inputfield 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "inputfield", 'id' => "-1", 'question' => ""]));       // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "inputfield", 'id' => "1", 'question' => " "]));       // Question is null 
    }
    
    public function testGetSurveryFormatQuestionTypeSlider(){
        
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(['question_type' => "slider", 'id' => "0", 'question' => "Test", 'answer_options_id' => "1"] ));    // Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(['question_type' => "NotSlider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "1"])); // question_type is not Inputfield 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(['question_type' => "slider", 'id' => "-1", 'question' => "", 'answer_options_id' => "1"]));       // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(['question_type' => "slider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "0"]));    // Question is null 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "-3"]));//AnserId is -3     
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(['question_type' => "slider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "asdf"])); 
    }    
    
    public function testGetSurveryFormatQuestionTypeMultiChoice(){

        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "0", 'question' => "Test", 'answer_options_id' => "1"] ));// Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "NotSlider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "1"])); // question_type  
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "-1", 'question' => "", 'answer_options_id' => "1"]));    // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "0"])); // AnserId is 0 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "-3"]));//AnserId is -3   
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "asdf"]));//AnserId        
    }    
    
    public function testGetSurveryFormatQuestionTypeSingleChoice(){

        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' => "0", 'question' => "Test", 'answer_options_id' => "1"] ));// Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "NotSlider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "1"])); // question_type  
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' =>"-1", 'question' => "", 'answer_options_id' => "1"]));    // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' =>"1", 'question' => "Test", 'answer_options_id' => "0"])); // AnserId is 0 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "-3"]));//AnserId is -3     
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "asdf"]));//AnserId        
    }

    public function testSearchQuestionIdInSurvey(){

        $this->assertFalse($this->survey->searchQuestionIdInSurvey($id="0"));
        $this->assertFalse($this->survey->searchQuestionIdInSurvey($id="Test"));
        $this->assertFalse($this->survey->searchQuestionIdInSurvey($id="-2"));
    }    
          
    public function testGetRangeValuesForSlider(){
        $expected = [
            'min_value' => 0,
            'max_value' => 0,
            'value' => 0
        ];
        
        $this->assertEquals($this->survey->getRangeValuesForSlider(array()), $expected);
        $this->assertEquals($this->survey->getRangeValuesForSlider(['question_type'=> "Test", "slider"=> array()]), $expected);
        $this->assertEquals($this->survey->getRangeValuesForSlider(['question_type'=> "", "slider"=> array()]), $expected);
    }
    
    public function testpostSurveyByCampaignId(){
        
        $this->assertEquals($this->survey->postSurveyByCampaignId(array()), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => "Test", 'id'=>"23"], 1 => ['question_type' => "Test", 'id'=>"23"]]), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => " ", 'id'=>"23"], 1 => ['question_type' => "Test", 'id'=>"23"]]), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => "Test", 'id'=>" "], 1 => ['question_type' => "Test", 'id'=>"23"]]), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => "Test", 'id'=>"23"], 1 => ['question_type' => " ", 'id'=>"23"]]), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => "Test", 'id'=>"23"], 1 => ['question_type' => "Test", 'id'=>" "]]), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => "Test", 'id'=>"-3"], 1 => ['question_type' => "Test", 'id'=>"23"]]), []);
        $this->assertEquals($this->survey->postSurveyByCampaignId(['lead_reference' => "", 0 =>['question_type' => "Test", 'id'=>"23"], 1 => ['question_type' => "Test", 'id'=>"0"]]), []);
    }
    
    public function testinsertAnswersToDatabase(){
        
        $this->assertFalse($this->survey->insertAnswersToDatabase(array()));
        $this->assertFalse($this->survey->insertAnswersToDatabase(['value'=> "", 'question_id' => "12", 'question_type'=>"Test"]));
        $this->assertFalse($this->survey->insertAnswersToDatabase(['value'=> "Test", 'question_id' => "0", 'question_type'=>"Test"]));
        $this->assertFalse($this->survey->insertAnswersToDatabase(['value'=> "Test", 'question_id' => "2", 'question_type'=>" "]));
    }
}
