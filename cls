[33mcommit 4fc563a6a823739ac47b04df52542f565a1c670d[m[33m ([m[1;36mHEAD -> [m[1;32mintegrations[m[33m, [m[1;31morigin/integrations[m[33m)[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Thu Sep 19 15:30:59 2019 +0200

    copy campaign when revised

[33mcommit af90eb6aadd35bb0499d7469e774271aed1ed7c1[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Thu Sep 19 13:15:41 2019 +0200

    few new UIs are added

[33mcommit b75ef19ad26082c0b591cdc9a5c00ec9e3f6543e[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Thu Sep 12 17:14:31 2019 +0200

    few new sections added

[33mcommit ca793a67d0aa824415e8421174f1862927abf7e1[m[33m ([m[1;31morigin/development[m[33m, [m[1;32mdevelopment[m[33m)[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Sep 10 17:40:30 2019 +0200

    clean code for all controllers and few changes in Account level

[33mcommit eb7fc64d3ad3ae6805cf27288c1d91ad799d90ce[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Mon Sep 9 12:30:18 2019 +0200

    working campaign revise and activate

[33mcommit 2de62965fff120ec43304122817b13e263024fe2[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Mon Sep 9 10:17:46 2019 +0200

    some account level and campaign level settings are updated

[33mcommit 5e82d56f49989e9f775078e931fb73ffaf3a66c3[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Wed Sep 4 11:00:47 2019 +0200

    campaign level settings

[33mcommit 0f0af587199d11638a06b5c51e20ef5878023660[m[33m ([m[1;32mDevelopmentForSettings[m[33m)[m
Merge: 1ada6a5 ed8b9ad
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 11:36:46 2019 +0200

    Merge branch 'development' into DevelopmentForSettings

[33mcommit 1ada6a5f53142eed332b69cd17b901448ed88176[m[33m ([m[1;31morigin/DevelopmentForSettings[m[33m)[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 11:34:06 2019 +0200

    Merge conflicts

[33mcommit ed8b9adb4f331f25f35fbecd2ec9e0dc7b2483c9[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 11:29:02 2019 +0200

    Merge conflicts from Development branch

[33mcommit b77f461bd59a4f40d4abdc82b2191e184f64f68f[m
Merge: 7f3ce06 39eb479
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 11:02:29 2019 +0200

    Merge branch 'DevelopmentForSettings' of https://bitbucket.org/pradeepveera1989/campaign-in-one.net into DevelopmentForSettings

[33mcommit 7f3ce06de49cd24544c8c61808278ccfe7db6c91[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 10:48:24 2019 +0200

    small changes

[33mcommit 2405faa44c4b1e5702fecabe43e8c5dc18a8f6b9[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 10:12:08 2019 +0200

    added salt and peper for password

[33mcommit eff85fa9f271b1b4c6093e0458ee1de980ec567c[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Wed Aug 14 14:56:16 2019 +0200

    Fulling working Settings

[33mcommit e12d5a30828cce90700ef69dda01523b8dc35290[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 6 16:28:52 2019 +0200

    fully working settings menu

[33mcommit 0d8741aa3bee324a320b004a78e40e61c70da392[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 6 12:24:27 2019 +0200

    working general settings

[33mcommit 564f4faa32040c73cadfaa9e351b090051077369[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Fri Aug 2 10:19:03 2019 +0200

    changes in settings UI

[33mcommit 39eb47942c6344cfb323c44157d907706c202e63[m
Merge: 41d4429 4506e87
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 10:53:03 2019 +0200

    Merge branch 'development' of https://bitbucket.org/pradeepveera1989/campaign-in-one.net into DevelopmentForSettings

[33mcommit 41d44296f6ed1af5e51c21750b7d25ec7cade6a8[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 10:48:24 2019 +0200

    small changes

[33mcommit f8dd8496c56467319c5e52196e26a24d8a817dbe[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 20 10:12:08 2019 +0200

    added salt and peper for password

[33mcommit fa440627b8839cfb2cf807584c4382fdc856139e[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Wed Aug 14 14:56:16 2019 +0200

    Fulling working Settings

[33mcommit 87bb16d10b5e6e735d7c64efa5eaf8ae4cb16b05[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 6 16:28:52 2019 +0200

    fully working settings menu

[33mcommit 6e3f0b946df090b638b62a5aa349f0819ba791b7[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Tue Aug 6 12:24:27 2019 +0200

    working general settings

[33mcommit aa42c626a3a33bdd809a47415eb32568f236131c[m
Author: Karri Aravind Reddy <aravind.karri@treaction.net>
Date:   Fri Aug 2 10:19:03 2019 +0200

    changes in settings UI

[33mcommit 4506e8765dc7339ba26f8b5635a0410a21791dda[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 14:18:43 2019 +0200

    text file deleted

[33mcommit c9f0f93fe7f49d845721fc6f52b94157ce598c05[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 14:16:43 2019 +0200

    test

[33mcommit 8ef773b54a647e799ef20956074aa602769bfabf[m
Merge: 9732f62 3c7edf1
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 14:15:11 2019 +0200

    Merge branch 'development' of https://bitbucket.org/pradeepveera1989/campaign-in-one.net into development

[33mcommit 9732f62d3e3eb32ff96ae31a669615fded70517b[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 14:04:58 2019 +0200

    rebase master

[33mcommit da83bf06dad3b948d0c4b05362bd081d87c8bf63[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Apr 9 11:12:10 2019 +0200

    Completed Survey with Client template

[33mcommit 7193c92971a09130125a30e909ed6839f9cca2b6[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 13:30:28 2019 +0200

    Checking development branch

[33mcommit 4d97c78ab8923fcb78b983325cca60b43ccaf34b[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 13:46:23 2019 +0200

    resolving comming

[33mcommit 3c7edf171ef00504889aa5da39d39758abc223cc[m
Merge: b127531 6a3fc1d
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 13:52:44 2019 +0200

    fixing merge conflicts

[33mcommit b127531ec7cfb5b066c8cc76d08dfa3efaf5bcfe[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 13:30:28 2019 +0200

    Checking development branch

[33mcommit 12bc6d475442bb35fee72f0f69b34793637504e2[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 13:46:23 2019 +0200

    resolving comming

[33mcommit 6a3fc1dab243715d3373bb049c7ec49844dd9fa8[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 13:30:28 2019 +0200

    Checking development branch

[33mcommit 66ff0a6607d1da2c35a4f207a13d8dd294e974e4[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 23 12:02:52 2019 +0200

    WEB-2241 : Ablaufzeit für neue Konten

[33mcommit 24971c0bbc6c66c7051c95db2bb2e3a3d880aa17[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 22 17:36:19 2019 +0200

    WEB-2241 : Ablaufzeit für Passwort-Reset

[33mcommit 731bd36833e0119bf857d339463b22847c2eda29[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 22 09:47:21 2019 +0200

    Added new Unit Test cases for Company.php

[33mcommit 4dbae7412c1dc716950ad14099ba4ff067ec6780[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jul 19 16:57:57 2019 +0200

    fixed bug with survey range and layout

[33mcommit 224cc28eaa212c67496fcbafe8ff39e1fdf6b2ee[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jul 19 13:27:27 2019 +0200

    Integrated Survey Layout functionality (Single page or Carosal)

[33mcommit d0d8779674d16dbd42918ed280190a83f1cdc523[m[33m ([m[1;32mlist[m[33m)[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Thu Jul 18 11:37:38 2019 +0200

    Suvey layout functionality added to DB

[33mcommit 6ecd6f3146adc46d12ad0df1a0bb14b3d647bcaf[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jul 17 15:32:18 2019 +0200

    removed revise option for draft campaigns

[33mcommit d0f180e9438d2cca1538d5fa0baba2fdc007ac35[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jul 17 11:10:43 2019 +0200

    Updated Master Campaign Libraires

[33mcommit f9c362c50ebceafc4f183f719f5144f5352cf38f[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jul 17 11:08:49 2019 +0200

    Fixed bugs related to Survey Versioning

[33mcommit 6a745eca48b1d73f8c4485de8606d569c0bed0ef[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 16 15:10:38 2019 +0200

    Modified versioning for Survey

[33mcommit 11734886a165cbccf863c25b04264acb34b387ed[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 16 11:43:43 2019 +0200

    removing untracked files

[33mcommit a693b63379c50823e83767902167c91a0275aae8[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 16 11:35:10 2019 +0200

    append to last commit

[33mcommit 41b35ec264f40f5b7dfa92f9e2785a36fa1cd450[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 16 11:24:56 2019 +0200

    append last commit

[33mcommit e80ae8950b348c16c79154622670dd2807285d66[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 16 11:20:51 2019 +0200

    Updated campaign to Master client version

[33mcommit c5798e58f11c124be00c0173a92ae48775a79a41[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jul 12 10:37:42 2019 +0200

    added few comments

[33mcommit 593faf00fb3afd59baeba2905c8c9166dee2a318[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Thu Jul 11 08:58:11 2019 +0200

    fixed bug related to hubspot twig

[33mcommit 0dbd5bd421c300ea8685e0f793111471fed8486e[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jul 9 12:43:11 2019 +0200

    fixed issue with image upload in Survey

[33mcommit 7ddcb69964ec5d65f3989f32a37a13837ccb431b[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 8 21:02:44 2019 +0200

    Campaign PHP Unit Test cases

[33mcommit 7ba3faba8675c1078238503df106a9b22d86c47b[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 8 15:06:10 2019 +0200

    Integrated Campaign versioning with campaign settings

[33mcommit 0fb0dc6fdfd42a62b1978bda73aa9e06f587bd3c[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jul 5 14:18:43 2019 +0200

    formated campiagn class

[33mcommit 92c1630d2c7faf196dd97f96712f8b47dd2d611e[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jul 5 13:18:43 2019 +0200

    refractored Campaign class

[33mcommit 40afc756ec0277825b4e2794ae3c6e5b2bbc9701[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jul 3 16:42:48 2019 +0200

    bugs related to MPP

[33mcommit fc812a47b2f252e4c15e8da17a87ba9fda176bde[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jul 3 14:04:24 2019 +0200

    Added right answer for survey slider

[33mcommit 405699f69da4e1e8fd5052ed0cfce3beeca2e63e[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jul 3 12:12:22 2019 +0200

    Fixed menu for Campaign Settings and Lazy loading for leads

[33mcommit 1cdc9747d97b21f05021734d958930ead552acf2[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 1 16:54:06 2019 +0200

    small bugs

[33mcommit 7d61cae76c5b1fe6a29bb6f003db0572f04bbb61[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 1 12:05:05 2019 +0200

    Modified Lead detail view

[33mcommit b7cd91b942ade2db7b48ae3f377cf9c285db9e00[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jul 1 09:48:16 2019 +0200

    bugs in Campaign view

[33mcommit 4f4091523aa04d5ce8f67d5dc2deacb8243f07b5[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jun 28 10:17:26 2019 +0200

    Added custom mectronic css folder

[33mcommit f0aca665070d8541b6eba21faac187a97e88f7f0[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Thu Jun 27 19:22:00 2019 +0200

    disabled Survey on cio template

[33mcommit 8cd8744fe84cad926e788650b522755b205ec815[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Thu Jun 27 19:06:20 2019 +0200

    Fixed small Frontend bugs

[33mcommit 8cf4bbb3bb6ee2fe98a5c38c2eaf8b9026d0f4ab[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Thu Jun 27 12:45:17 2019 +0200

    Added Campaign views with dynamic Filter by Campaign

[33mcommit 8ea9e6ef51bd1d96ea78021ead755db6daf49976[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jun 26 15:40:59 2019 +0200

    Added Error / success messages for campaigns

[33mcommit ec7a8cca496cfd3626efa4097a5dd87f3b976ede[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jun 26 12:25:00 2019 +0200

    Removed actions button from Campaigns twig

[33mcommit e51fb349beae2b3ec3549fbb63c1c2f319f59ecd[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 25 17:05:11 2019 +0200

    Modified User Update functionality with Error/Success messages

[33mcommit e8cbbd6ebe0a7dfba429dc8676ca6e10a9694173[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 25 16:16:09 2019 +0200

    Added Error/Success msgs for Account Update

[33mcommit 9920f00292a949034cf3964a71566c8dd8ea97f5[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 25 13:51:59 2019 +0200

    Added error messages for User sign in and sign out

[33mcommit 05e874aff8f8137bfe59650a6211a81dbdfdc583[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jun 24 20:25:28 2019 +0200

    Modified User sign up functionality

[33mcommit 0c5889fdbb1da25bad5558fd5b6acc0293024ae5[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jun 24 14:17:43 2019 +0200

    Fixed bugs for forgotpassword functionality

[33mcommit 9d558dc2917b6aea73f27fc947e3300654a0f706[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jun 19 09:22:33 2019 +0200

    Clean up  Survey and Added robot.txt

[33mcommit 5ee46c2ecfd366fe37fe14a7c8aaf03b5e14a2ca[m
Merge: 32282d5 e6a4759
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 18 13:26:33 2019 +0200

    Created development branch

[33mcommit e6a47599bc4c56b0632df3453c3f486ae7637de5[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jun 14 16:13:34 2019 +0200

    Fixed small bugs related to twig files

[33mcommit 3676d997edb6c8efa695bde29af5a1e0a04651d5[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jun 14 15:04:55 2019 +0200

    Added date picker to dashboard leads

[33mcommit c31e12b25ca6eb248cdfd2d96b2a553a747fd4fb[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jun 14 11:39:21 2019 +0200

    Renamed home.twig to dashboard.twig

[33mcommit 2ee940959676ae205f5b7e4abcc48587e58f9603[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Jun 14 11:05:03 2019 +0200

    Dashboard with PIE and Bar charts

[33mcommit 1d221001deb320b9e1a1d1c1f70782a35420226d[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Thu Jun 13 17:21:49 2019 +0200

    Dash board with charts

[33mcommit ef6d7d3f5f4d4d599c7c0950c29fb46ca09232a6[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jun 12 17:03:09 2019 +0200

    Dashboard with Leads and Campaigns - Part 1

[33mcommit c01948187215d7a830ae1bc92bb1b08365c25f7a[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Wed Jun 12 09:20:56 2019 +0200

    Deleted Backup files from Bitbucket

[33mcommit 9cd4f84665cbde2fba71a2e91b8d845d015c4a5d[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 11 17:21:07 2019 +0200

    Small issue found in update survey

[33mcommit f61873a29c90d195d7310dfdfb3808525b2e6036[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 11 17:19:55 2019 +0200

    Update Suvery integrated with new Frontend

[33mcommit af465129a8f6b1d4c8122b65d2bd362aa235d8a6[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 11 16:07:48 2019 +0200

    Create Survey integrated with new theme

[33mcommit 7e14466dafa4413e90649a6d1ab6261fb5ef123c[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 11 12:39:08 2019 +0200

    Completed with Hubspot Integration

[33mcommit 89af01d8dcbcd9ea74dd6f1723d69cab14eaa84b[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jun 11 12:12:13 2019 +0200

    Completed Mail-In-One Integration

[33mcommit 0d93859a3f24560cb8f7b622ada36695161c5e25[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jun 10 11:25:03 2019 +0200

    Deleted non used files

[33mcommit 1e91282f4cd68ce06254fd9bf6bf904717c3b663[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Mon Jun 10 11:01:43 2019 +0200

    Changed to new Frontend UI

[33mcommit 32282d53fa49684ae8caae7af3a7ac8736023c39[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Apr 9 11:12:10 2019 +0200

    Completed Survey with Client template

[33mcommit df921b1d3b925b40ead435e8769d9eab2fa0f11b[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Mar 19 11:52:37 2019 +0100

    Integrated with Survey Backend and Super admin feature

[33mcommit 71b7969adba1680f7f622eec3ea06f977650aa99[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jan 22 16:43:00 2019 +0100

    Latest changes

[33mcommit f0a6fb026928a2a38924f115734194ce242505bf[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Jan 22 16:42:13 2019 +0100

    Latest changes

[33mcommit 455518b04177ab7803c808355e7bc6728d6ee076[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Fri Oct 19 12:30:25 2018 +0200

    MVP Login Campaign-In-One User Management

[33mcommit ed462b05af358a0c1f9a01706641bd8227e7b3af[m
Author: Pradeep <pradeep.veera@treaction.de>
Date:   Tue Sep 25 10:06:23 2018 +0200

    Front end tool with basic functionility

[33mcommit de810fb0a926e0f43537f5a4d7a0e3cd72621eb2[m[33m ([m[1;31morigin/deployment[m[33m)[m
Author: Pradeep Veera <pradeep.veera@treaction.de>
Date:   Fri Sep 21 07:03:30 2018 +0000

    Initial commit
