<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Authentication
$app->get('/', 'AuthController:getSignIn')->setName('auth.signin');
$app->post('/', 'AuthController:postSignIn');
$app->get('/signout', 'AuthController:getSignOut')->setName('auth.signout');
$app->get('/dashboard', 'AuthController:getDashboard')->setName('auth.home');
$app->get('/account/{acc_id}/switch', 'AccountController:getSwitchCustomer');

// Accound Details : User + Company
$app->get('/account/{acc_id}/user/{user_id}/read', 'UserController:getUserDetialsById')
->setName('acc.details');

// User Reset Password
$app->get('/resetpassword', 'AuthController:getUserResetPassword')->setName('auth.resetpassword');
$app->get(
    '/account/{acc_id}/user/{user_id}/resetpassword[/{expire_time}]',
    'AccountController:getUserResetPasswordById'
)->setName('auth.resetpasswordbyId');

$app->post(
    '/account/{acc_id}/user/{user_id}/resetpassword[/{expire_time}]',
    'AccountController:postUserResetPasswordById'
);

// User Forgot Password
$app->get('/forgotpassword', 'AuthController:getForgotPassword')->setName('auth.forgotpassword');
$app->post('/forgotpassword', 'AuthController:postForgotPassword');

//Block User
//$app->get('/blockuser', 'UserController:getBlockUser')->setName('user.blockuser');
$app->get('/account/{acc_id}/user/{user_id}/blockuser', 'UserController:getBlockUserById')
->setName('user.blockUserById');

//Unblock User
//$app->get('/ubblockuser', 'UserController:getUnBlockUser')->setName('user.unblockuser');
$app->get('/account/{acc_id}/user/{user_id}/unblockuser', 'UserController:getUnBlockUserById')
->setName('user.UnBlockBlockUserById');

// User Sign Up Page.
$app->get('/usersignup', 'UserController:getUserSignUp')->setName('user.signup');
$app->post('/usersignup', 'UserController:postUserSignUp');

// User Update detials
$app->get('/account/userupdate', 'UserController:getUpdateUser')->setName('user.update');
$app->get('/account/{acc_id}/user/{user_id}/update', 'UserController:getUpdateUserById')->setName('user.updateById');
$app->post('/account/{acc_id}/user/{user_id}/update', 'UserController:postUpdateUserById');

//Activate User account
$app->get('/account/{acc_id}/user/{user_id}/activateuser[/{expire_time}]', 'UserController:getActivateUserById')
->setName('user.ActivateUserById');

// Account Sign Up Page.
$app->get('/accountsignup', 'AccountController:getAccountSignUp')->setName('acc.signup');
$app->post('/accountsignup', 'AccountController:postAccountSignUp');

//Account read
$app->get('/account/{acc_id}/read', 'AccountController:getAccountDetialsById')->setName('acc.read');

//Account block    - post  - AccountController:block
$app->get('/account/{acc_id}/block', 'AccountController:getBlockAccountById')->setName('acc.block');

//Account unblock  - post  - AccountController:unblock
$app->get('/account/{acc_id}/unblock', 'AccountController:getUnBlockAccountById')->setName('acc.unblock');

//Account update   - post  - AccountController:update
$app->get('/accountupdate', 'AccountController:getUpdateAccount')->setName('acc.update');
$app->get('/account/{acc_id}/update', 'AccountController:getUpdateAccountById')->setName('acc.updateById');
$app->post('/account/{acc_id}/update', 'AccountController:postUpdateAccountById');


// #########################################################################################################################################################

//Campaigns
// Active Campaigns
$app->get('/account/campaigns/active', 'CampaignController:getCampaignsByStatusActive')
->setName('camp.active');

$app->get('/account/{acc_id}/campaigns/active', 'CampaignController:getActiveCampaignsById')
->setName('camp.activeById');

// Draft Campaigns
$app->get('/account/campaigns/draft', 'CampaignController:getCampaignsByStatusDraft')
->setName('camp.draft');

$app->get('/account/{acc_id}/campaigns/draft', 'CampaignController:getDraftCampaignsById')
->setName('camp.draftById');

// Archive Campaigns
$app->get('/account/campaigns/archive', 'CampaignController:getCampaignsByStatusArchive')
->setName('camp.archive');

$app->get('/account/{acc_id}/campaigns/archive', 'CampaignController:getArchiveCampaignsById')
->setName('camp.archiveById');

//Reactivate Campaign
$app->get('/account/{acc_id}/campaigns/{camp_id}/reactivate', 'CampaignController:getCampaignReactivateById')
->setName('camp.reactivateById');

//Copy Campaign
$app->get('/account/{acc_id}/campaigns/{camp_id}/copy', 'CampaignController:getCampaignCopyById')
->setName('camp.copyById');

// Create Campaign
$app->get('/account/campaigns/create', 'CampaignController:createCampaign')
->setName('camp.create');

$app->get('/account/{acc_id}/campaigns/create', 'CampaignController:getcreateCampaignById')
->setName('camp.createById');

$app->post('/account/{acc_id}/campaigns/create', 'CampaignController:postcreateCampaignById');

// Delete Campaign Data
$app->get('/account/{acc_id}/campaigns/{camp_id}/delete', 'CampaignController:getCampaignDeleteById')
->setName('camp.deleteById');

// Update Campaign Data
$app->get('/account/{acc_id}/campaigns/{camp_id}/update', 'CampaignController:getCampaignUpdateById')
->setName('camp.updateById');
$app->post('/account/{acc_id}/campaigns/{camp_id}/update', 'CampaignController:postCampaignUpdateById');

// Activate Campaign
$app->get('/account/{acc_id}/campaigns/{camp_id}/activate', 'CampaignController:getCampaignActivateById')
->setName('camp.activateById');

// Archived Campaign
$app->get('/account/{acc_id}/campaigns/{camp_id}/archive', 'CampaignController:getCampaignArchivedById')
->setName('camp.archiveById');


// #########################################################################################################################################################

// Lead Tracking
$app->get('/account/leads', 'LeadController:getAllLeads')->setName('leads.active');
$app->get('/account/{acc_id}/leads', 'LeadController:getLeadsByAccountId')->setName('leads.LeadsByAccountId');
$app->get('/account/{acc_id}/campaigns/{camp_id}/leads', 'LeadController:getLeadsByCampaignId')->setName('leads.LeadsByCampaignId');
$app->get('/account/{acc_id}/lead/{lead_id}/deactivate', 'LeadController:deactivateLeadById')
->setName('leads.deactivateLeadById');
$app->get('/account/{acc_id}/lead/{lead_id}/reactivate', 'LeadController:reactivateLeadById')
->setName('leads.reactivateLeadById');
$app->get('/account/{acc_id}/lead/{lead_id}/detail', 'LeadController:getDetailLeadViewById')
->setName('leads.detailLeadViewById');


// #########################################################################################################################################################

// Campaign Integrations
    //Survey
$app->get('/account/{acc_id}/campaigns/{camp_id}/survey', 'SurveyIntegrationController:getCampaignSurveyById')
->setName('getSurvey');

$app->post('/account/{acc_id}/campaigns/{camp_id}/survey', 'SurveyIntegrationController:postCampaignSurveyById');

$app->get(
    '/account/{acc_id}/campaigns/{camp_id}/survey/{sury_id}/{ques_id}/delete',
    'SurveyIntegrationController:deleteCampaignSurveyQuestionById'
);

$app->get(
    '/account/{acc_id}/campaigns/{camp_id}/survey/{sury_id}/{ques_id}/update',
    'SurveyIntegrationController:getCampaignSurveyQuestionById'
)->setName('getSurveyQuestion');

$app->post(
    '/account/{acc_id}/campaigns/{camp_id}/survey/{sury_id}/{ques_id}/update',
    'SurveyIntegrationController:postCampaignSurveyQuestionById'
);

// Tracking
$app->get('/account/{acc_id}/campaigns/{camp_id}/tracking', 'CampaignIntegrationController:getCampaigntrackingById')
->setName('campInt.tracking');
$app->get('/account/{acc_id}/campaigns/{camp_id}/settings', 'CampaignIntegrationController:getCampaignsettingsById')
->setName('campInt.settings');

//Settings
    //General
$app->get('/account/{acc_id}/campaigns/{camp_id}/general', 'SettingsController:getGeneralSettingsById')
->setName('general.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/general', 'SettingsController:postGeneralSettingsById');
//     //Dataflow
 $app->get('/account/{acc_id}/campaigns/{camp_id}/dataflow', 'SettingsController:getDataflowSettingsById')
 ->setName('dataflow.Integration');

 $app->post('/account/{acc_id}/campaigns/{camp_id}/dataflow', 'SettingsController:postDataflowSettingsById');
//     //Fraud
 $app->get('/account/{acc_id}/campaigns/{camp_id}/fraud', 'SettingsController:getFraudSettingsById')
 ->setName('fraud.Integration');

 $app->post('/account/{acc_id}/campaigns/{camp_id}/fraud', 'SettingsController:postFraudSettingsById');
//     //Messages
 $app->get('/account/{acc_id}/campaigns/{camp_id}/messages', 'SettingsController:getmessageSettingsById')
 ->setName('messages.Integration');

 $app->post('/account/{acc_id}/campaigns/{camp_id}/messages', 'SettingsController:postmessageSettingsById');

 // Flow control
$app->get('/account/{acc_id}/campaigns/{camp_id}/flowcontrol', 'FlowControlController:getBasicFlowControlById')
->setName('flowcontrol.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/flowcontrol', 'FlowControlController:postBasicFlowControlById');

$app->get('/account/{acc_id}/campaigns/{camp_id}/splittest', 'FlowControlController:getSplitTest');
// #########################################################################################################################################################

 //Intergrations
    //mapping
 $app->get('/account/{acc_id}/campaigns/{camp_id}/mappings', 'IntegrationController:getMappingById')
 ->setName('mapping.Integration');

 $app->post('/account/{acc_id}/campaigns/{camp_id}/mappings', 'IntegrationController:postMappingById');
    //Email
$app->get('/account/{acc_id}/campaigns/{camp_id}/email', 'IntegrationController:getEmailDetailsById')
->setName('email.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/email', 'IntegrationController:postEmailDetailsById');

    //webinaris
$app->get('/account/{acc_id}/campaigns/{camp_id}/webinaris', 'IntegrationController:getWebinarisDetailsById')
->setName('webinaris.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/webinaris', 'IntegrationController:postWebinarisDetailsById');

    //digistore
$app->get('/account/{acc_id}/campaigns/{camp_id}/digistore', 'IntegrationController:getDigistoreDetailsById')
->setName('digistore.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/digistore', 'IntegrationController:postDigistoreDetailsById');

    // MailInOne
$app->get('/account/{acc_id}/campaigns/{camp_id}/mailinone', 'IntegrationController:getMailInOneDetailsById')
->setName('mio.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/mailinone', 'IntegrationController:postMailInOneDetailsById');

    //Hubspot
$app->get('/account/{acc_id}/campaigns/{camp_id}/hubspot', 'IntegrationController:getHubspotDetailsById')
->setName('hb.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/hubspot', 'IntegrationController:postHubspotDetailsById');

    //HTTPInbound
$app->get('/account/{acc_id}/campaigns/{camp_id}/HTTPInbound', 'IntegrationController:getHTTPInboundDetailsById');
$app->post('/account/{acc_id}/campaigns/{camp_id}/HTTPInbound', 'IntegrationController:postHTTPInboundDetailsById');

    //HTTPOutbound
$app->get('/account/{acc_id}/campaigns/{camp_id}/HTTPOutbound', 'IntegrationController:getHTTPOutboundDetailsById');
$app->post('/account/{acc_id}/campaigns/{camp_id}/HTTPOutbound', 'IntegrationController:postHTTPOutboundDetailsById');



// #########################################################################################################################################################

//Tracking
    //facebook
$app->get('/account/{acc_id}/campaigns/{camp_id}/facebook', 'TrackingController:getFacebookById')
->setName('facebook.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/facebook', 'TrackingController:postFacebookById');
    //google
$app->get('/account/{acc_id}/campaigns/{camp_id}/google', 'TrackingController:getGoogleById')
->setName('Google.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/google', 'TrackingController:postGoogleById');
     //adeblo
$app->get('/account/{acc_id}/campaigns/{camp_id}/adeblo', 'TrackingController:getAdebloById')
->setName('Adeblo.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/adeblo', 'TrackingController:postAdebloById');
    //webgains
$app->get('/account/{acc_id}/campaigns/{camp_id}/webgains', 'TrackingController:getWebgainsById')
->setName('Webgains.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/webgains', 'TrackingController:postWebgainsById');
    //outbrain
$app->get('/account/{acc_id}/campaigns/{camp_id}/outbrain', 'TrackingController:getOutbrainById')
->setName('Outbrain.Integration');

$app->post('/account/{acc_id}/campaigns/{camp_id}/outbrain', 'TrackingController:postOutbrainById');

// #########################################################################################################################################################


// #########################################################################################################################################################

 //Account level Intergrations
    //digistore
    $app->get('/accountsettingsDigistore', 'AccountIntegrationController:getUpdateSettingsDigistore')
    ->setName('acc.settingsDigistore');

    $app->get('/account/{acc_id}/updateDigistore', 'AccountIntegrationController:getDigistoreSettingsById')
    ->setName('acc.digistore');

    $app->post('/account/{acc_id}/updateDigistore', 'AccountIntegrationController:postDigistoreSettingsById');
    //Webinaris
    $app->get('/accountsettingsWebinaris', 'AccountIntegrationController:getUpdateSettingsWebinaris')
    ->setName('acc.settingsWebinaris');

    $app->get('/account/{acc_id}/updateWebinaris', 'AccountIntegrationController:getWebinarisSettingsById')
    ->setName('acc.Webinaris');

    $app->post('/account/{acc_id}/updateWebinaris', 'AccountIntegrationController:postWebinarisSettingsById');
    //Email
    $app->get('/accountsettingsEmail', 'AccountIntegrationController:getUpdateSettingsEmail')
    ->setName('acc.settingsEmail');

    $app->get('/account/{acc_id}/updateEmail', 'AccountIntegrationController:getEmailSettingsById')
    ->setName('acc.Email');

    $app->post('/account/{acc_id}/updateEmail', 'AccountIntegrationController:postEmailSettingsById');
    //Fraud
    $app->get('/accountsettingsFraud', 'AccountIntegrationController:getUpdateSettingsFraud')->setName('acc.settingsFraud');
    $app->get('/account/{acc_id}/updateFraud', 'AccountIntegrationController:getFraudSettingsById')->setName('acc.Fraud');
    $app->post('/account/{acc_id}/updateFraud', 'AccountIntegrationController:postFraudSettingsById');
    //Mail-In-one
    $app->get('/accountsettingsMIO', 'AccountIntegrationController:getUpdateSettingsMIO')->setName('acc.settingsMIO');
    $app->get('/account/{acc_id}/updateMIO', 'AccountIntegrationController:getMIOSettingsById')->setName('acc.MIO');
    $app->post('/account/{acc_id}/updateMIO', 'AccountIntegrationController:postMIOSettingsById');
    //Hubspot
    $app->get('/accountsettingsHbuspot', 'AccountIntegrationController:getUpdateSettingsHubspot')->setName('acc.settingsHubspot');
    $app->get('/account/{acc_id}/updateHubspot', 'AccountIntegrationController:getHubspotSettingsById')->setName('acc.Hubspot');
    $app->post('/account/{acc_id}/updateHubspot', 'AccountIntegrationController:postHubspotSettingsById');
    //General
    $app->get('/accountsettingsGeneral', 'AccountIntegrationController:getUpdateSettingsGeneral')->setName('acc.settingsGeneral');
    $app->get('/account/{acc_id}/updateGeneral', 'AccountIntegrationController:getGeneralSettingsById')->setName('acc.General');
    $app->post('/account/{acc_id}/updateGeneral', 'AccountIntegrationController:postGeneralSettingsById');
    //URLAPI
    $app->get('/accountsettingsURLApi', 'AccountIntegrationController:getUpdateSettingsURLApi')->setName('acc.settingsURLApi');
    $app->get('/account/{acc_id}/updateURLApi', 'AccountIntegrationController:getURLApiSettingsById')->setName('acc.UrlApi');
    $app->post('/account/{acc_id}/updateURLApi', 'AccountIntegrationController:postURLApiSettingsById');
    //HttpOutbond
    $app->get('/accountsettingsHttpInbound', 'AccountIntegrationController:getUpdateSettingsHttpInbound')->setName('acc.settingsHttpInbound');
    $app->get('/account/{acc_id}/updateHttpInbound', 'AccountIntegrationController:getHttpInboundSettingsById')->setName('acc.HttpInbound');
    $app->post('/account/{acc_id}/updateHttpInbound', 'AccountIntegrationController:postHttpInboundSettingsById');




// #########################################################################################################################################################
