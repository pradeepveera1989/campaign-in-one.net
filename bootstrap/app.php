<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();

require __DIR__ . '/../vendor/autoload.php';

use Chadicus\Slim\OAuth2\Middleware;
use Chadicus\Slim\OAuth2\Routes;
use OAuth2;
use OAuth2\Storage;
use OAuth2\GrantType;
use Slim;

//set up storage for oauth2 server
$storage = new Storage\Memory(
    [
        'client_credentials' => [
            'administrator' => [
                'client_id' => 'administrator',
                'client_secret' => 'password',
                'scope' => 'superUser',
            ],
            'account-admin' => [
                'client_id' => 'company',
                'client_secret' => 'p4ssw0rd',
                'scope' => 'basicUser canViewFoos',
            ],
            'user' => [
                'client_id' => 'user',
                'client_secret' => '!password1',
                'scope' => 'basicUser',
            ],
        ],
    ]
);
// Database connections
$db_host = "localhost";
$db_name = "db13026946-cio";
$db_user = "db13026946-cio";
$db_pass = "6gRjTSD5";

// create the oauth2 server
$server = new OAuth2\Server(
    $storage,
    [
        'access_lifetime' => 3600,
    ],
    [
        new GrantType\ClientCredentials($storage),
        new GrantType\RefreshToken($storage),
    ]
);

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'determineRouteBeforeAppMiddleware' => true,
        'pdo' => [
            'engine' => 'mysql',
            'host' => 'localhost',
            
            'database' => $db_name,
            'username' => $db_user,
            'password' => $db_pass,
            
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'options' => [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => true,
            ],
        ],
    ],
]);

$container = $app->getContainer();

$container['server'] = $server;

$container['campaign_path'] = "/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de";
$container['campaign_url'] = "campaign-in-one.de";
$container['upload_path'] = "/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.net/uploads";
$container['upload_url'] = "https://campaign-in-one.net/uploads/";
    

$container['dbh'] = function ($container) {
    $config = $container->get('settings')['pdo'];
    $dsn = "{$config['engine']}:host={$config['host']};dbname={$config['database']};charset={$config['charset']}";
    $username = $config['username'];
    $password = $config['password'];

    return new PDO($dsn, $username, $password, $config['options']);
};

$container['validator']  = function ($container) {
    return new \App\Validation\Validator;
};

$container['mailinone']  = function ($container) {
    return new \App\Mailinone\Mailinone($container);
};

$container['mail']  = function ($container) {
    return new \App\Mailer($container);
};

$container['LeadController']  = function ($container) {
    return new \App\Controllers\LeadController($container);
};

$container['SurveyIntegrationController']  = function ($container) {
    return new \App\Controllers\SurveyIntegrationController($container);
};

$container['CampaignSettingsController']  = function ($container) {
    return new \App\Controllers\CampaignSettings\CampaignSettingsController($container);
};

$container['FlowControlController'] = function ($container) {
    return new \App\Controllers\CampaignSettings\FlowControlController($container);
};
$container['TrackingController'] = function ($container) {
    return new \App\Controllers\CampaignSettings\TrackingController($container);
};

$container['CampaignController']  = function ($container) {
    return new \App\Controllers\CampaignController($container);
};

$container['AccountController']  = function ($container) {
    return new \App\Controllers\AccountController($container);
};

$container['AuthController']  = function ($container) {
    return new \App\Controllers\AuthController($container);
};

$container['UserController']  = function ($container) {
    return new \App\Controllers\UserController($container);
};

$container['auth'] = function ($container) {
    return new \App\Models\Authenticate($container['dbh']);
};

$container['company'] = function ($container) {
    return new \App\Models\Company($container['dbh']);
};

$container['oauth2'] = function ($container) {
    return new \App\Oauth2\oauth2($container);
};

$container['directory'] = function ($container) {
    return new \App\Directory\Directory($container);
};

$container['SettingsController'] = function ($container) {

    return new \App\Controllers\CampaignSettings\SettingsController($container);
};

$container['IntegrationController'] = function ($container) {

    return new \App\Controllers\CampaignSettings\IntegrationController($container);
};

$container['AccountIntegrationController'] = function ($container) {

    return new \App\Controllers\AccountIntegrationController($container);
};





// Loading Twig views to container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
        'cache' => false,
        'debug' => true,
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    $view->addExtension(new \Twig\Extension\DebugExtension());
        
    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container->auth->check(),
        'user'  => $container->auth->user(),
        'acc_name' => $container->auth->accName(),
        'super_admin'=> $container->auth->checkSuperAdmin(),
        // For Super admin to get all Company Names from DB;
        'acc_details' => $container->company->getCompanyAccountDetials('')
    ]);
    
    return $view;
};

#$authMiddleware = new Middleware\Authorization($server, $app->getContainer());

$app->add(new \App\Middleware\Middleware($container));
$app->add(new \App\Middleware\ValidationErrorsMIddleware($container));
$app->add(new \App\Middleware\AuthMiddleware($container));
$app->add(new \App\Middleware\AccountMiddleware($container));
$app->add(new \App\Middleware\UserMiddleware($container));
$app->add(new \App\Middleware\CampaignMiddleware($container));
//$app->add(new \App\Middleware\OAuth2Middleware($container));

$app->map(['GET', 'POST'], Routes\Authorize::ROUTE, new Routes\Authorize($container['server'], $container['view']))
->setName('authorize');

$app->post(Routes\Token::ROUTE, new Routes\Token($container['server']))->setName('token');

$app->map(['GET', 'POST'], Routes\ReceiveCode::ROUTE, new Routes\ReceiveCode($container['view']))
->setName('receive-code');

$app->post(Routes\Revoke::ROUTE, new Routes\Revoke($container['server']))->setName('revoke');

require __DIR__.'/routes.php';
