<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CampaignTest
 *
 * @author Aravind
 */

use PHPUnit\Framework\TestCase;

class CampaignParamsTest extends TestCase{


    public function setUp() {
        
        $this->general = new App\Models\CampaignParams($db,"General");
        $this->dataFlow = new App\Models\CampaignParams($db,"Data_Flow");
        $this->fraudSettings = new App\Models\CampaignParams($db,"Fraud_Settings");
        $this->messages = new App\Models\CampaignParams($db,"Messages");

    }
    
    /**
     * testcreateCampaign function
     *
     * @return void
     */
    public function testgetSettingDetails(){
       
        $this->assertFalse($this->general->getSettingDetails($camp_id = array()));
        $this->assertFalse($this->general->getSettingDetails($camp_id = "abc"));;

        $this->assertFalse($this->dataFlow->getSettingDetails($camp_id = array()));
        $this->assertFalse($this->dataFlow->getSettingDetails($camp_id = "abc"));;

        $this->assertFalse($this->fraudSettings->getSettingDetails($camp_id = array()));
        $this->assertFalse($this->fraudSettings->getSettingDetails($camp_id = "abc"));;

        $this->assertFalse($this->messages->getSettingDetails($camp_id = array()));
        $this->assertFalse($this->messages->getSettingDetails($camp_id = "abc"));;
    }

    /**
     * testinsertOrUpdateSettings
     *
     * @return void
     */
    public function testinsertOrUpdateSettings(){
       
        $this->assertFalse($this->general->insertOrUpdateSettings($params=array()));
        $this->assertFalse($this->general->insertOrUpdateSettings($params=array('id' => "asdf")));

        $this->assertFalse($this->dataFlow->insertOrUpdateSettings($params=array()));
        $this->assertFalse($this->dataFlow->insertOrUpdateSettings($params=array('id' => "asdf")));

        $this->assertFalse($this->fraudSettings->insertOrUpdateSettings($params=array()));
        $this->assertFalse($this->fraudSettings->insertOrUpdateSettings($params=array('id' => "asdf")));

        $this->assertFalse($this->messages->insertOrUpdateSettings($params=array()));
        $this->assertFalse($this->messages->insertOrUpdateSettings($params=array('id' => "asdf")));
    }

    /**
     * testcreateVersionOfSettings
     *
     * @return void
     */
    public function testcreateVersionOfSettings(){
       
        $this->assertFalse($this->general->createVersionOfSettings($old_camp_id = "abc", $new_camp_id = "bbc"));
        $this->assertFalse($this->general->createVersionOfSettings($old_camp_id = 0, $new_camp_id = 0));

        $this->assertFalse($this->dataFlow->createVersionOfSettings($old_camp_id = "abc", $new_camp_id = "bbc"));
        $this->assertFalse($this->dataFlow->createVersionOfSettings($old_camp_id = 0, $new_camp_id = 0));

        $this->assertFalse($this->fraudSettings->createVersionOfSettings($old_camp_id = "abc", $new_camp_id = "bbc"));
        $this->assertFalse($this->fraudSettings->createVersionOfSettings($old_camp_id = 0, $new_camp_id = 0));

        $this->assertFalse($this->messages->createVersionOfSettings($old_camp_id = "abc", $new_camp_id = "bbc"));
        $this->assertFalse($this->messages->createVersionOfSettings($old_camp_id = 0, $new_camp_id = 0));
    }

    /**
     * testgetInsertOrUpdateSqlQuery
     *
     * @return void
     */
    public function testgetInsertOrUpdateSqlQuery(){
       
        $this->assertFalse($this->general->getInsertOrUpdateSqlQuery($params=array()));
        $this->assertFalse($this->general->getInsertOrUpdateSqlQuery($params=array('company_id' => "asdf")));

        $this->assertFalse($this->dataFlow->getInsertOrUpdateSqlQuery($params=array()));
        $this->assertFalse($this->dataFlow->getInsertOrUpdateSqlQuery($params=array('company_id' => "asdf")));

        $this->assertFalse($this->fraudSettings->getInsertOrUpdateSqlQuery($params=array()));
        $this->assertFalse($this->fraudSettings->getInsertOrUpdateSqlQuery($params=array('company_id' => "asdf")));

        $this->assertFalse($this->messages->getInsertOrUpdateSqlQuery($params=array()));
        $this->assertFalse($this->messages->getInsertOrUpdateSqlQuery($params=array('company_id' => "asdf")));
    }

}