<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CampaignTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;

class CampaignTest extends TestCase {
    //put your code here
    
    public function setUp() {
        
        $this->campaign = new App\Models\Campaign($db);
    }        

    /**
     * testcreateCampaign function
     *
     * @return void
     */
    public function testcreateCampaign(){
       
        $this->assertFalse($this->campaign->createCampaign($params = array()));
        $this->assertFalse($this->campaign->createCampaign($params=array('camp_url' => "asdf", 'camp_name' => "")));
    }

    /**
     * testdeleteCampaignById function
     *
     * @return void
     */
    public function testdeleteCampaignById(){

        $this->assertFalse($this->campaign->deleteCampaignById($params=array()));
        $this->assertFalse($this->campaign->deleteCampaignById($params=['camp_id' => 0]));
        $this->assertFalse($this->campaign->deleteCampaignById($params=['camp_id' => 'asdf']));
    }
    
    /**
     * testupdateCampaignDetails function
     *
     * @return void
     */
    public function testupdateCampaignDetails(){

        $this->assertFalse($this->campaign->updateCampaignDetails($params=array()));
        $this->assertFalse($this->campaign->updateCampaignDetails($params=['id' => 0]));
        $this->assertFalse($this->campaign->updateCampaignDetails($params=['id' => 'asdf']));
    }

    /**
     * testgetCampaignDetialsByURL function
     *
     * @return void
     */
    public function testgetCampaignDetialsByURL(){

        $this->assertEquals($this->campaign->getCampaignDetialsByURL("", ""), []);
        $this->assertEquals($this->campaign->getCampaignDetialsByURL("asdfasdf", "") , []);
        $this->assertEquals($this->campaign->getCampaignDetialsByURL(83629, ""), []);
        $this->assertEquals($this->campaign->getCampaignDetialsByURL(-2, ""), []);
    }

    /**
     * testgetActiveArchivedCampaignsByAccountId function
     *
     * @return void
     */
    public function testgetActiveArchivedCampaignsByAccountId(){

        $this->assertEquals($this->campaign->getActiveArchivedCampaignsByAccountId(0, 9), []);
        $this->assertEquals($this->campaign->getActiveArchivedCampaignsByAccountId(-2, 9), []);
        $this->assertEquals($this->campaign->getActiveArchivedCampaignsByAccountId(2, -9), []);
    }

    /**
     * testgetCampaignsByStatusForAccount function
     *
     * @return void
     */
    public function testgetCampaignsByStatusForAccount(){

        $this->assertEquals($this->campaign->getCampaignsByStatusForAccount(0, "active"), []);
        $this->assertEquals($this->campaign->getCampaignsByStatusForAccount(-3, " "), []);
    }

    /**
     * testactivateCampaignById function
     *
     * @return void
     */
    public function testactivateCampaignById(){

        $this->assertFalse($this->campaign->activateCampaignById([], "filepath"));
        $this->assertFalse($this->campaign->activateCampaignById(['camp_id' => 0], "filepath"));
        $this->assertFalse($this->campaign->activateCampaignById(['camp_id' => 23], ""));
    }

    /**
     * testcreateVersionOfCampaign function
     *
     * @return void
     */
    public function testcreateVersionOfCampaign(){

        $this->assertEquals($this->campaign->createVersionOfCampaign(0, "filepath"), 0);
        $this->assertEquals($this->campaign->createVersionOfCampaign(-1, "filepath"), 0);
        $this->assertEquals($this->campaign->createVersionOfCampaign(1, ""), 0);
    }

    /**
     * testcreateCampaignURL function
     *
     * @return void
     */
    public function testcreateCampaignURL(){

        $this->assertEquals($this->campaign->createCampaignURL([]), "");
        $this->assertEquals($this->campaign->createCampaignURL(['account' => "test", 'name' => "", 'url'=>"testURL"]), "");
        $this->assertEquals($this->campaign->createCampaignURL(['account' => " ", 'name' => "", 'url'=>"testURL"]), "");
        $this->assertEquals($this->campaign->createCampaignURL(['account' => "test", 'name' => "", 'url'=>" "]), "");
    }

    /**
     * testinsertCampaignToDB function
     *
     * @return void
     */
    public function testinsertCampaignToDB(){

        $this->assertEquals($this->campaign->insertCampaignToDB([]), 0);
        $this->assertEquals($this->campaign->insertCampaignToDB(['campany_id' =>"asdf", 'url' => "test ULR", 'account'=> "asdf"]), 0);
        $this->assertEquals($this->campaign->insertCampaignToDB(['campany_id' =>"2", 'url' => " ", 'account'=> "asdf"]), 0);
        $this->assertEquals($this->campaign->insertCampaignToDB(['campany_id' =>"2", 'url' => "test ULR", 'account'=> " "]), 0);
    }

    /**
     * testarchiveCampaignById function
     *
     * @return void
     */
    public function testarchiveCampaignById(){

        $this->assertFalse($this->campaign->archiveCampaignById([], "filepath"));
        $this->assertFalse($this->campaign->archiveCampaignById(['camp_id' => "asdf"], "filepath"));
        $this->assertFalse($this->campaign->archiveCampaignById(['camp_id' => "0"], "filepath"));
        $this->assertFalse($this->campaign->archiveCampaignById(['camp_id' => "-2"], "filepath"));
        $this->assertFalse($this->campaign->archiveCampaignById(['camp_id' => "12"], ""));
    }

    /**
     * testappendLinksForCampaigns function
     *
     * @return void
     */
    public function testappendLinksForCampaigns(){

        $this->assertEquals($this->campaign->appendLinksForCampaigns([]), []);
        $this->assertEquals($this->campaign->appendLinksForCampaigns(['id' => 0]), []);
    }

    // public function testgenerateCampaignLinks(){

    //     $this->assertEquals($this->campaign->generateCampaignLinks(0, "delete"), 0);
    //     $this->assertEquals($this->campaign->generateCampaignLinks("as", "delete"), 0);
    // }

    /**
     * testgetCampaignDetailsById function
     *
     * @return void
     */
    public function testgetCampaignDetailsById(){
        
        $this->assertEquals($this->campaign->getCampaignDetailsById("asddf"), []);
        $this->assertEquals($this->campaign->getCampaignDetailsById("0"), []);
        $this->assertEquals($this->campaign->getCampaignDetailsById("-2"), []);
    }

    /**
     * testgetCampaignDetailsByURL function
     *
     * @return void
     */
    public function testgetCampaignDetailsByURL(){
        
        $this->assertEquals($this->campaign->getCampaignDetailsByURL("Test URL"), []);
        $this->assertEquals($this->campaign->getCampaignDetailsByURL("0"), []);
        $this->assertEquals($this->campaign->getCampaignDetailsByURL("-2"), []);
    }

    /**
     * testcreateCampaignFolderStructure function
     *
     * @return void
     */
    public function testcreateCampaignFolderStructure(){

        $this->assertFalse($this->campaign->createCampaignFolderStructure(['camp_name' => "12", "account"=> "testname", "camp_url"=>""], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['camp_name' => "", "account"=> "testname", "camp_url"=>"test "], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['camp_name' => "12", "account"=> "", "camp_url"=>"test"], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['camp_name' => "12", "account"=> "testname", "camp_url"=>"test"], ""));
    }

    /**
     * testcreateCampaignVersionStucture function
     *
     * @return void
     */
    public function testcreateCampaignVersionStucture(){
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['name' => "", "account"=> "testname", "old_version"=>"1", "version"=>"2"], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['name' => "12", "account"=> "", "old_version"=>"1", "version"=>"2"], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['name' => "12", "account"=> "testname", "old_version"=>"", "version"=>"2"], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['name' => "12", "account"=> "testname", "old_version"=>"1", "version"=>""], "flepath"));
        $this->assertFalse($this->campaign->createCampaignFolderStructure(['name' => "12", "account"=> "testname", "old_version"=>"1", "version"=>"2"], ""));
    }

}
