<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CampaignIntegrationsTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;

class SurveyTest  extends TestCase {
    //put your code here
    
    public function setUp() {
        
        $this->survey = new App\Models\Survey($db);
        
    }         
    
    public function testPutQuestionsByCampaignId(){
        
        $this->assertSame($this->survey->putQuestionsByCampaignId(array()), []);
        $this->assertSame($this->survey->putQuestionsByCampaignId(['question'=> " "]), []);
        $this->assertSame($this->survey->putQuestionsByCampaignId(['question'=> "Test question" ]), []);
        $this->assertSame($this->survey->putQuestionsByCampaignId(['question'=> "Test question", 'campaign_id' => "0"]), []);
    }
    
    public function testPutAnswerOptionsByQuestionId(){
        
        $this->assertSame($this->survey->putAnswerOptionsByQuestionId([]), []);
        $this->assertSame($this->survey->putAnswerOptionsByQuestionId(['question_id'=> " ", "value" => "Test"]), []);
        $this->assertSame($this->survey->putAnswerOptionsByQuestionId(['question_id'=> "-2", "value" => "Test"]), []);
        
    }
    
    public function testGetQuestionByCampaignId(){
        
        $this->assertSame($this->survey->getQuestionByCampaignId([]), []);
        $this->assertSame($this->survey->getQuestionByCampaignId(['camp_id' => "0" ]), []);
        $this->assertSame($this->survey->getQuestionByCampaignId(['camp_id' => "-2" ]), []);
        $this->assertSame($this->survey->getQuestionByCampaignId(['camp_id' => 'asdf' ]), []);
    }
    
    public function testGetSurvey(){
        
        $this->assertSame($this->survey->getSurvery([]), []);
        $this->assertSame($this->survey->getSurvery(['camp_id' => "0" ]), []);
        $this->assertSame($this->survey->getSurvery(['camp_id' => "-3"]), []);
    }
    
    public function testGetSurveryFormatQuestionTypeInputfield(){
      
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "inputfield", 'id' => "0", 'question' => "Test"]));    // Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "NotInputField", 'id' => "1", 'question' => "Test"])); // question_type is not Inputfield 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "inputfield", 'id' => "-1", 'question' => ""]));       // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "inputfield", 'id' => "1", 'question' => " "]));       // Question is null 
    }
    
    public function testGetSurveryFormatQuestionTypeSlider(){
        
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSlider(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "slider", 'id' => "0", 'question' => "Test", 'answer_options_id' => "1"] ));    // Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "NotSlider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "1"])); // question_type is not Inputfield 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "slider", 'id' => "-1", 'question' => "", 'answer_options_id' => "1"]));       // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "slider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "0"]));    // Question is null 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "-3"]));//AnserId is -3     
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeInputfield(['question_type' => "slider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "asdf"])); 
    }
    
    public function testGetSurveryFormatQuestionTypeMultiChoice(){

        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "0", 'question' => "Test", 'answer_options_id' => "1"] ));// Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "NotSlider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "1"])); // question_type  
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "-1", 'question' => "", 'answer_options_id' => "1"]));    // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "0"])); // AnserId is 0 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "-3"]));//AnserId is -3     
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeMultiChoice(['question_type' => "multiple_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "asdf"]));//AnserId        
    }
    
    public function testGetSurveryFormatQuestionTypeSingleChoice(){

        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(array()));
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' => "0", 'question' => "Test", 'answer_options_id' => "1"] ));// Id is zero
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "NotSlider", 'id' => "1", 'question' => "Test", 'answer_options_id' => "1"])); // question_type  
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' =>"-1", 'question' => "", 'answer_options_id' => "1"]));    // Id is -1 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' =>"1", 'question' => "Test", 'answer_options_id' => "0"])); // AnserId is 0 
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "-3"]));//AnserId is -3     
        $this->assertFalse($this->survey->getSurveryFormatQuestionTypeSingleChoice(['question_type' => "single_choice", 'id' => "1", 'question' => "Test", 'answer_options_id' => "asdf"]));//AnserId        
    }
   
    
    public function testSearchQuestionIdInSurvey(){

        $this->assertFalse($this->survey->searchQuestionIdInSurvey($id="0"));
        $this->assertFalse($this->survey->searchQuestionIdInSurvey($id="Test"));
        $this->assertFalse($this->survey->searchQuestionIdInSurvey($id="-2"));
    }
    
    public function testCheckQuestionsByCampaignId(){

        $this->assertSame($this->survey->checkQuestionsByCampaignId(array()), []);
        $this->assertSame($this->survey->checkQuestionsByCampaignId(['question'=> " ",'question_type'=>'slider',       'campaign_id' => "1"]), []);
        $this->assertSame($this->survey->checkQuestionsByCampaignId(['question'=>"Test",'question_type'=>' ',       'campaign_id' => "1"]), []);
        $this->assertSame($this->survey->checkQuestionsByCampaignId(['question'=>"Test ", 'question_type'=>'notslider', 'campaign_id' => "1"]), []);
        $this->assertSame($this->survey->checkQuestionsByCampaignId(['question'=>"Test", 'question_type'=>'slider', 'campaign_id' => "0"]), []);

    }    
    
    public function testUpdateAnswerOptionsByAnswerId(){
        
        $this->assertSame($this->survey->updateAnswerOptionsByAnswerId(array()), []);
        $this->assertSame($this->survey->updateAnswerOptionsByAnswerId(['id' => "0"]), []);
        $this->assertSame($this->survey->updateAnswerOptionsByAnswerId(['id' => "-2"]), []);
        $this->assertSame($this->survey->updateAnswerOptionsByAnswerId(['id' => 'test']), []);        
    }
    
    public function testUpdateQuestionsByQuestionId(){
        
        $this->assertSame($this->survey->updateQuestionsByQuestionId(array()), []);
        $this->assertSame($this->survey->updateQuestionsByQuestionId(['id' => "0", 'text' =>'Test']), []);
        $this->assertSame($this->survey->updateQuestionsByQuestionId(['id' => "-2", 'text' =>'Test']), []);
        $this->assertSame($this->survey->updateQuestionsByQuestionId(['id' => 'test', 'text'=>'Test']), []);  
        $this->assertSame($this->survey->updateQuestionsByQuestionId(['id' => "3", 'text'=> " "]), []);  
    }
    
    public function testDeleteSurveyQuestionById(){
        
        $this->assertFalse($this->survey->deleteSurveyQuestionById(array()));
        $this->assertFalse($this->survey->deleteSurveyQuestionById(['camp_id' => "0", 'ques_id' => "2"]));
        $this->assertFalse($this->survey->deleteSurveyQuestionById(['camp_id' => "test", 'ques_id' => "2"]));
        $this->assertFalse($this->survey->deleteSurveyQuestionById(['camp_id' => "2", 'ques_id' => "0"]));
        $this->assertFalse($this->survey->deleteSurveyQuestionById(['camp_id' => "0", 'ques_id' => "Test"]));
    }
    
    public function testGetSurveyByQuestionId(){
        
        $this->assertFalse($this->survey->getSurveyByQuestionId(array()));
        $this->assertFalse($this->survey->getSurveyByQuestionId(['ques_id' => "0"]));
        $this->assertFalse($this->survey->getSurveyByQuestionId(['ques_id' => "test"]));
    }
    
    public function testUpdateSurveyByQuestionId(){
        $upload_files = "test";
        $question_type = ['survey'][1]['questiosn']['question_type'];
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, array()));
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, ['camp_id' => "1", "ques_id" => "1", "image_dir" => "Test"]));
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, ['camp_id' => "test", "ques_id" => "1", "image_dir" => "Test"]));
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, ['camp_id' => "1", "ques_id" => "0", "image_dir" => "Test"]));
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, ['camp_id' => "1", "ques_id" => "test", "image_dir" => "Test"]));
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, ['camp_id' => "0", "ques_id" => "1", "image_dir" => "0"]));
        $this->assertFalse($this->survey->updateSurveyByQuestionId($upload_files, ['camp_id' => "0", "ques_id" => "1", "image_dir" => " "]));
    }

    public function testUpdateOrInsertOptionValue(){
        
        $this->assertFalse($this->survey->updateOrInsertOptionValue(array(), array()));
        $this->assertFalse($this->survey->updateOrInsertOptionValue(['id' => "0"],['value' => "Test"]));
        $this->assertFalse($this->survey->updateOrInsertOptionValue(['id' => " "],['value' => "Test"]));
        $this->assertFalse($this->survey->updateOrInsertOptionValue(['id' => "-2"],['value' => "Test"]));
        $this->assertFalse($this->survey->updateOrInsertOptionValue(['id' => "1"],['value' => " "]));
    }
    
    public function testUpdateOrInsertImage(){
        
        $this->assertFalse($this->survey->updateOrInsertImage(array(), array()));
        $this->assertFalse($this->survey->updateOrInsertImage(['image_dir' => " "], ['camp_id' => "1", 'acc_id' => "1"]));
        $this->assertFalse($this->survey->updateOrInsertImage(['image_dir' => "Test"], ['camp_id' => "0", 'acc_id' => "1"]));
        $this->assertFalse($this->survey->updateOrInsertImage(['image_dir' => " "], ['camp_id' => "-1", 'acc_id' => "1"]));
        $this->assertFalse($this->survey->updateOrInsertImage(['image_dir' => " "], ['camp_id' => "1", 'acc_id' => "0"]));
        $this->assertFalse($this->survey->updateOrInsertImage(['image_dir' => " "], ['camp_id' => "1", 'acc_id' => "-1"]));
    }
}