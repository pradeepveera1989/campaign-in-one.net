<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;

class CompanyTest extends TestCase
{
    public function setUp()
    {
        $this->company = new App\Models\Company($db);
    }
        
    public function testCreateCompany()
    {
        $this->assertEquals($this->company->createCompany(""), 0);
        $this->assertEquals($this->company->createCompany(123), 0);
    }

    public function testGetCompanyDetialsByWebsite()
    {
        $this->assertEquals($this->company->getCompanyDetialsByWebsite(" "), []);
        $this->assertEquals($this->company->getCompanyDetialsByWebsite(234), []);
    }

    public function testUpdateCompanyById()
    {
        //$this->assertFalse($this->company->updateCompanyById([]));
        $this->assertFalse($this->company->updateCompanyById(['acc_id' => 0]));
        $this->assertFalse($this->company->updateCompanyById(['name' => "treaction AG"]));
    }

    public function testGetCompanyDetailsById()
    {
        $this->assertEquals($this->company->getCompanyDetailsById(0) []);
        $this->assertEquals($this->company->getCompanyDetailsById(0.23) []);
        $this->assertEquals($this->company->getCompanyDetailsById(-4) []);
    }

    public function testgetAllUsersOfCompany()
    {
        $this->assertEquals($this->company->getAllUsersOfCompany(0), []);
        $this->assertEquals($this->company->getAllUsersOfCompany(-3), []);
    }

    public function testvalidateFreeDomains()
    {
        $this->assertFalse($this->company->validateFreeDomains(""));
        $this->assertFalse($this->company->validateFreeDomains(23));
    }
}
