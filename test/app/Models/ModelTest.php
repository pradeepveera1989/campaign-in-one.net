<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * php7.0 Unit Test cases for namespace APP/Model/ModelTest.php
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;


class ModelTest extends TestCase {
    
    public function setUp() {
        
        $this->model = new \App\Models\Model($db);
    }    
    
    public function testgetUpdateDetials(){
        $exp_date = $this->model->getUpdateDetials();
        $act_date = date('Y-m-d');
        $this->assertSame($exp_date['date'], $act_date);   
    }
    
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty argument passed
     */    
    public function testgetUserByEmail(){
        $this->model->getUserByEmail("");
    }

    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid Email address
     */    
    public function testgetUserByEmailByInvalidEmail(){
        $this->model->getUserByEmail("asdfasdf");
    }    
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid User Id passed
     */        
    public function testgetUserDetials(){
        $this->model->getUserDetials();
    }
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid query statement passed
     */         
    public function testexcuteQuerySET(){
        $this->model->excuteQuerySET();
    }
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid query statement passed
     */         
    public function testexcuteQueryGET(){
        $this->model->excuteQueryGET();
    }    
}

