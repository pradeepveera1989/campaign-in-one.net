<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTest
 *
 * @author Pradeep
 */

use PHPUnit\Framework\TestCase;


class UserTest extends TestCase {

    public function setUp() {
        
        $this->user = new App\Models\User($db);
    }        
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to Create User
     */       
    public function testcreateUser(){
        
        $this->user->createUser($p = array());
    }

    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to Block User
     */       
    public function testblockUser(){
        
        $this->user->blockUser($p = array());
    }
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to Block User
     */       
    public function testblockUserWithNoId(){
        
        $p =[
            "email" => "test@email.com"
        ];
                
        $this->user->blockUser($p);
    }   
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to UnBlock User
     */       
    public function testUnBlockUser(){
        
        $this->user->unBlockUser($p = array());
    }
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to UnBlock User
     */       
    public function testUnBlockUserWithNoId(){
        
        $p =[
            "email" => "test@email.com"
        ];
                
        $this->user->unBlockUser($p);
    }     
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to updatePassword
     */       
    public function testUpdatePassword(){
        
        $this->user->updatePassword($p=array());
    }   
    
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Empty arguments passed to updatePassword
     */       
    public function testUpdatePasswordWithNoPassword(){
        $p = [
            'email' => 'test@email.com',
            'first_name' => 'Test',
            'password' => ''
        ];
        
        $this->user->updatePassword($p);
    }       
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid User Id
     */        
    public function testGetUserDetailsById(){
        $p ="";
        $this->user->getUserDetailsById($p);
    }
    
    
    /**
     * @expectedException  InvalidArgumentException
     * @expectedExceptionMessage Invalid User Id
     */        
    public function testGetUserDetailsByIdWithZero(){
        
        $this->user->getUserDetailsById(0);
    }
    
}
