"use strict";
var KTDatatablesBasicBasic = function () {

    var initTable1 = function () {
        var lead_status = "";
        // Helper function to generate URL Using Ids
        function getURLForOperation(ids, operation) {

            var url = ""
            var urlObj = window.location;

            if ((ids.length === 0) || (operation.trim() == "") || $.isEmptyObject(urlObj)) {
                return url;
            }

            var host = urlObj.host;
            var protocol = urlObj.protocol;
            var create_url = protocol + '//' + host + '/account/' + ids[0] + '/lead/' + ids[2] + '/' + operation;
            if (isValidURL(create_url)) {
                url = create_url;
            }
            return url;
        }



        // Validates if the String is URL or not
        function isValidURL(string) {
            var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
            if (res == null)
                return false;
            else
                return true;
        }



        var table = $('#kt_table_1');

        // begin first table
        table.DataTable({
            responsive: true,
            // processing: true,
            // serverside:true,
            ajax:"https://dev.campaign-in-one.net/resources/json_data/JsonLead.php",

            // // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            select: {
                style: 'multi'
            },

            lengthMenu: [5, 10, 25, 50],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            //order: [[1, 'desc']],

            headerCallback: function (thead, data, start, end, display) {
                thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input type="checkbox" value="" class="m-group-checkable">
                        <span></span>
                    </label>`;
            },

            columnDefs: [
                {
                    targets: 0,
                    width: '30px',
                    className: 'dt-right',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="` + data + `" class="m-checkable">
                            <span></span>
                        </label>`;
                    },
                },
                {
                    targets :1,
                    className:'noClick align-middle'
                },
                {
                    targets :2,
                    className:'noClick align-middle'
                },                
                {
                    targets :3,
                    className:'noClick align-middle'
                },              
                {
                    targets :4,
                    className:'noClick align-middle'
                },                
                {
                    targets :5,
                    className:'noClick align-middle'
                },
                {
                    targets :6,
                    className:'noClick align-middle'
                },            
                {
                    targets :7,
                    className:'noClick align-middle'
                },            
                {
                    targets :8,
                    className:'noClick align-middle'
                },
                {
                    targets :9,
                    className:'noClick align-middle',
                    visible:false
                },                       
                {
                    targets :10,
                    className:'noClick align-middle',
                    visible:false
                }, 
                {
                    targets :11,
                    className:'noClick align-middle',
                    visible:false
                },
                {
                    targets :12,
                    className:'noClick align-middle',
                    visible:false
                },
                {
                    targets :13,
                    className:'noClick align-middle',
                    visible:false
                },
                {
                    targets :14,
                    className:'noClick align-middle',
                    visible:true
                },
                {
                    targets :15,
                    className:'noClick align-middle',
                    visible:true
                },
                {
                    targets :16,
                    className:'noClick align-middle',
                    visible:true
                },                
                                                 
                {
                    targets: 18,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        var lead_detail_url = getURLForOperation(data, 'detail');
                        var deactivate_url = getURLForOperation(data, 'deactivate');
                        var reactivate_url = getURLForOperation(data, 'reactivate');
                        var activation_menu = "";

                        if (lead_status == "active") {
                            activation_menu = `<a class="dropdown-item" href="` + deactivate_url + `"><i class="la la-minus-circle"></i> Deactivate lead</a>`;
                        } else if (lead_status == "inactive") {
                            activation_menu = `<a class="dropdown-item" href="` + reactivate_url + `"><i class="la la-minus-circle"></i> Reactivate lead</a>`;
                        } else {
                            activation_menu = "";
                        }

                        return `
       
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-edit"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" id="export_lead_detail" href="` + lead_detail_url + `"><i class="la la-search-plus"></i> Show lead details</a>
                                ` + activation_menu + `
                            </div>
                        </span>
                       `;
                    },
                },
                {
                    targets: 17,
                    render: function (data, type, full, meta) {
                        var status = {
                            active: { 'title': 'active', 'class': 'kt-badge--success' },
                            inactive: { 'title': 'inactive', 'class': ' kt-badge--danger' },

                        };

                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        lead_status = status[data].title;
                        return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
                    },
                },
            ],

        });


        table.on('change', '.kt-group-checkable', function () {
            var set = $(this).closest('table').find('td:first-child .kt-checkable');
            var checked = $(this).is(':checked');

            $(set).each(function () {
                if (checked) {
                    $(this).prop('checked', true);
                    $(this).closest('tr').addClass('active');

                } else {
                    $(this).prop('checked', false);
                    $(this).closest('tr').removeClass('active');
                }
            });
        });

        table.on('change', 'tbody tr .kt-checkbox', function () {
            $(this).parents('tr').toggleClass('active');
            $(this).trigger("click");

        });

        table.on('click', 'tbody tr td', function () {
            var tr = $(this);
            if (tr.hasClass("noClick")) {
                return false;
            }
        });

    };

    return {

        //main function to initiate the module
        init: function () {
            initTable1();
        },

    };


}();

jQuery(document).ready(function () {
    KTDatatablesBasicBasic.init();

    var table = $('#kt_table_1').DataTable();

    //Search Lead Table
    $('#lead-search').keyup(function (e) {
        var value = $(this).val();
        // Search value only on 'Enter' is pressed
        if (e.keyCode == 13 && value.length >= 3) {
            table
                .search(value)
                .draw();
        } else {
            table
                .search('')
                .draw();
        }

    });

    // Filter By Campaign
    $('#filter_by_campaign').change(function (e) {
        var value = $(this).children("option:selected").val();
        var regex = '\\b' + value + '\\b';
        table
            .columns(1)
            .search(regex, true, false)
            .draw();

    });

    // Filter By Status
    $('#filter_by_status').change(function (e) {
        var value = $(this).children("option:selected").val();
        var regex = '\\b' + value + '\\b';
        table
            .columns(9)
            .search(regex, true, false)
            .draw();


    });

    // Date Range Picker
    $('input[name="daterange"]').daterangepicker({
        timePicker: false,
        autoApply: false,
        autoUpdateInput: true,
        alwaysShowCalendars: false,

        locale: {
            format: 'Y-MM-D',
            cancelLabel: 'Clear'
        },

        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }

    });

    // Search Datatable with selected Date range
    $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
        var d_range = picker.startDate.format('Y-MM-D') + ' - ' + picker.endDate.format('Y-MM-D')
        $(this).val(d_range);
        var d_arr = getDiffDates(d_range);
        var searchString = '(' + d_arr.join('|') + ')';
        table.search(searchString, true).draw(true);
    });

    // Reset the Date range filter
    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        table
            .search('')
            .draw();
    });


    // Get all the dates between Min and Max Date.
    function getDiffDates(daterange) {
        var dates = [];
        var split_range = daterange.split(" - ");
        var from_date = new Date(split_range[0]);
        var to_date = new Date(split_range[1]);
        var current_date = from_date;

        while (current_date <= to_date) {
            var date = new Date(current_date);
            var m = (date.getMonth() + 1).toString().replace(/(^.$)/, "0$1");
            var y = date.getFullYear();
            var d = (date.getDate() < 10 ? '0' : '') + date.getDate()
            var f_date = y + '-' + m + '-' + d;
            dates.push((f_date));
            current_date.setDate(current_date.getDate() + 1);
        }
        return dates;
    }

    // Appends Export Button to datatable. 
    var buttons = new $.fn.dataTable.Buttons(table, {
        buttons: [
            'excelHtml5',
        ]
    }).container().appendTo($('#export-table'));

});

