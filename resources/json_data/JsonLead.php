<?php

require '../../bootstrap/app.php';


class JsonLead
{
    public $acc_id;

    /**
     * __construct function
     *
     * @param integer $acc_id
     */
    public function __construct(int $acc_id)
    {
        $this->acc_id = $acc_id;
        $this->conn = $this->connectDb();
    }


    /**
     * connectDb function
     *
     * @return void
     */
    private function connectDb()
    {
        $host = $GLOBALS['db_host'];
        $dbName = $GLOBALS['db_name'];
        $user = $GLOBALS['db_user'];
        $password = $GLOBALS['db_pass'];
        $conn = mysqli_connect($host, $user, $password, $dbName);

        return $conn;
    }


    /**
     * getJsonLeadData function
     *
     * @return array
     */
    public function getJsonLeadData():array
    {
        $json_data = [];

        if ($this->acc_id <= 0 || empty($this->conn)) {
            return json_data;
        }

        $arr_lead = $this->getLeadDataFromDB();
        $arr_fmt_lead = $this->formatLeadData($arr_lead);

        return $arr_fmt_lead;
    }


    /**
     * getLeadDataFromDB function
     *
     * @return array
     */
    private function getLeadDataFromDB() :array
    {
        $sql = " SELECT `cio.leads`.`campaign_id`,`cio.leads`.`lead_reference`, 
        `cio.leads`.`created`,`cio.leads`.`salutation`, `cio.leads`.`first_name`, 
        `cio.leads`.`last_name`, `cio.leads`.`email`, `cio.leads`.`postal_code`, 
        `cio.leads`.`status`,`cio.leads`.`id`, `cio.leads`.`trafficsource`,
        `cio.leads`.`utm_parameters`,`cio.leads`.`targetgroup`,`cio.leads`.`affiliateID`,
        `cio.leads`.`affiliateSubID`,
        `cio.leads`.`id` AS lead_id,
        `cio.company`.`account_no`,`cio.campaign`.`name` AS camp_name
/*        `cio.leads_customdata`.`name` AS lead_cus_name,
        `cio.leads_customdata`.`type` AS lead_cus_type,
        `cio.leads_customdata`.`value` AS lead_cus_value*/
        FROM `cio.leads` 
        LEFT OUTER JOIN `cio.campaign` 
        ON `cio.campaign`.`id` = `cio.leads`.`campaign_id` 
        LEFT OUTER JOIN `cio.company` 
        ON `cio.company`.`id` = `cio.leads`.`company_id`
/*        LEFT OUTER JOIN `cio.leads_customfield` 
        ON `cio.leads_customfield`.campaign_id = `cio.leads`.`campaign_id`
        LEFT OUTER JOIN `cio.leads_customdata` 
        ON `cio.leads_customdata`.`lead_customfield_id` = `cio.leads_customfield`.id*/
        WHERE `cio.company`.`account_no` = ".$this->acc_id." 
        ORDER BY `cio.leads`.`id` DESC ";

        $return = $this->excuteQuery($sql);

        return $return;
    }


    /**
     * formatLeadData function
     *
     * @param array $arr_lead
     * @return array
     */
    private function formatLeadData(array $arr_lead): array
    {
        $count = count($arr_lead);
        $data =[
            "draw" => 1,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => []
        ];

        foreach ($arr_lead as $lead) {
            $action = [
                $lead['account_no'],
                $lead['campaign_id'],
                $lead['lead_id'],
            ];
            $lead_data =[
                $lead['lead_id'],
                $lead['camp_name'],
                $lead['lead_reference'],
                $lead['created'],
                $lead['salutation'],
                $lead['first_name'],
                $lead['last_name'],
                $lead['email'],
                $lead['postal_code'],

                $lead['trafficsource'],
                $lead['utm_parameters'],
                $lead['targetgroup'],
                $lead['affiliateID'],
                $lead['affiliateSubID'],
/*                $lead['lead_cus_name'],
                $lead['lead_cus_type'],
                $lead['lead_cus_value'],*/
                $lead['status'],
                $action,
            ];

            array_push($data['data'], $lead_data);
        }
        return $data;
    }


    /**
     * excuteQuery function
     *
     * @param string $sql_stmt
     * @return array
     */
    private function excuteQuery(string $sql_stmt): array
    {
        $return = [];
        if (empty($sql_stmt) || empty($this->conn)) {
            return $return;
        }

        @mysqli_set_charset($this->conn, "utf8");
        $result = $this->conn->query($sql_stmt);
        $return = mysqli_fetch_all($result, MYSQLI_ASSOC);

        return $return;
    }
}


$acc_id = (int) $_SESSION['acc_id'];
$jsnObj = new JsonLead($acc_id);
$data = $jsnObj->getJsonLeadData();

echo json_encode($data);
