"use strict";
var KTDatatablesBasicBasic_1 = function () {

    var initTable2 = function () {

        var data = [
            [
                "email",
                "email",
                "EMAIL",
                "EMAIL",
                "EMAIL",
                "standard",
                "test"
            ],
            [
                "salutation",
                "salutation",
                "SALUTATION",
                "SALUTATION",
                "SALUTATION",
                "standard",
                "test"
            ]
        ]

        var table = $('#dt-int-maping');
        // begin first table
        table.DataTable({
            paging: false,
            searching: false,
            bSort: false,
            data: data,
            columnDefs: [
                {
                    targets :0,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle" type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
                {
                    targets :1,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle" type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
                {
                    targets :2,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle" type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
                {
                    targets :3,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle"  type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
                {
                    targets :4,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle" type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
                {
                    targets :5,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle" type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
                {
                    targets :6,
                    orderable: false,
                    render : function(data, type, row) {
                        return '<input class="form-control form-control-sm" align="middle" type="text" maxlength="2" size="6" name="" value="' + data + '">';
                    },
                    className:'noClick align-middle'
                },
            ],

        });

    };

    return {
        //main function to initiate the module
        init: function () {
            initTable2();
        },
    };
}();

jQuery(document).ready(function () {

    KTDatatablesBasicBasic_1.init();
    var table = $('#dt-int-maping').DataTable();

    $('button#addRow').on( 'click', function () {
        const row = ['-','-','-','-','-','-','-']
        table.row.add(row).draw( false );
    } );

    $( "#camp-int-map" ).submit(function( event ) {
        event.preventDefault();
        var dtJson = getDTJson();
        console.log(dtJson);
    });

    function getDTJson() {
        let heads = [];
        $("thead").find("th").each(function () {
            heads.push($(this).text().trim());
        });

        let rows = [];
        $("tbody tr").each(function () {
            let cur = {};
            $(this).find("td").each(function(i, v) {
                cur[heads[i]] = $(this).find("input.form-control").val();
            });
            console.log(cur);
            cur = JSON.stringify(cur)
            rows.push(cur);
        });

        return rows;
    }
});
