"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function () {
    // Private functions

    // demo initializer
    var demo = function () {

        var lead_status = "";
        // Helper function to generate URL Using Ids
        function getURLForOperation(ids, operation) {

            var url = ""
            var urlObj = window.location;

            if ((ids.trim() == "") || (operation.trim() == "") || $.isEmptyObject(urlObj)) {
                return url;
            }

            var host = urlObj.host;
            var protocol = urlObj.protocol;
            var arr_ids = ids.split(',');
            var create_url = protocol + '//' + host + '/account/' + arr_ids[0] + '/lead/' + arr_ids[2] + '/' + operation;
            if (isValidURL(create_url)) {
                url = create_url;
            }
            return url;
        }



        // Validates if the String is URL or not
        function isValidURL(string) {
            var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
            if (res == null)
                return false;
            else
                return true;
        }


        var datatable = $('.kt-dashbd-leads').DataTable({

            responsive: true,

            // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            pageLength: 10,

            lengthMenu: [10],

            sortable: true,

            pagination: true,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            //order: [[1, 'desc']],

            headerCallback: function (thead, data, start, end, display) {
                thead.getElementsByTagName('th')[0].innerHTML = `
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                        <input type="checkbox" value="" class="m-group-checkable">
                        <span></span>
                    </label>`;
            },

            columnDefs: [
                {
                    targets: 0,
                    width: '30px',
                    className: 'dt-right',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return `
                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                            <input type="checkbox" value="" class="m-checkable">
                            <span></span>
                        </label>`;
                    },
                },
                {
                    targets: 8,
                    render: function (data, type, full, meta) {
                        var status = {
                            active: { 'title': 'active', 'class': 'kt-badge--success' },
                            inactive: { 'title': 'inactive', 'class': ' kt-badge--danger' },

                        };

                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        lead_status = status[data].title;
                        return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: 9,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        var lead_detail_url = getURLForOperation(data, 'detail');
                        var deactivate_url = getURLForOperation(data, 'deactivate');
                        var reactivate_url = getURLForOperation(data, 'reactivate');
                        var activation_menu = "";

                        if (lead_status == "active") {
                            activation_menu = `<a class="dropdown-item" href="` + deactivate_url + `"><i class="la la-minus-circle"></i> Deactivate lead</a>`;
                        } else if (lead_status == "inactive") {
                            activation_menu = `<a class="dropdown-item" href="` + reactivate_url + `"><i class="la la-minus-circle"></i> Reactivate lead</a>`;
                        } else {
                            activation_menu = "";
                        }

                        return `
       
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-edit"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" id="export_lead_detail" href="` + lead_detail_url + `"><i class="la la-search-plus"></i> Show lead details</a>
                                <!--a class="dropdown-item lead-csv" href="#"><i class="la la-download"></i> Download lead information</a-->
                                ` + activation_menu + `
                            </div>
                        </span>
                       `;
                    },
                },

            ],


        });

    };

    return {
        // Public functions
        init: function () {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    KTDatatableHtmlTableDemo.init();


    var table = $('.kt-dashbd-leads').DataTable();

    console.log(table);


    // Search Datatable with selected Date range
    $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
        var d_range = picker.startDate.format('Y-MM-D') + ' - ' + picker.endDate.format('Y-MM-D')
        $(this).val(d_range);
        var d_arr = getDiffDates(d_range);
        console.log("date range", d_range);
        var searchString = '(' + d_arr.join('|') + ')';
        table.search(searchString, true).draw(true);
    });

    // Reset the Date range filter
    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        table
            .search('')
            .draw();
    });


    // Get all the dates between Min and Max Date.
    function getDiffDates(daterange) {
        var dates = [];
        var split_range = daterange.split(" - ");
        var from_date = new Date(split_range[0]);
        var to_date = new Date(split_range[1]);
        var current_date = from_date;

        while (current_date <= to_date) {
            var date = new Date(current_date);
            var m = (date.getMonth() + 1).toString().replace(/(^.$)/, "0$1");
            var y = date.getFullYear();
            var d = (date.getDate() < 10 ? '0' : '') + date.getDate()
            var f_date = y + '-' + m + '-' + d;
            dates.push((f_date));
            current_date.setDate(current_date.getDate() + 1);
        }
        return dates;
    }    
});
