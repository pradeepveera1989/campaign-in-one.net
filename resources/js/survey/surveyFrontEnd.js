/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {


    // Initiate Dropzone JS
    if ($('#dropzone-upload')[0]) {
        $('#dropzone-upload').dropzone({
            url: "/file/post",
            addRemoveLinks: true
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Hide all theAnswer field
    $('.answerfield').hide();

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Recursive function for check the div existence
    var d = 0;
    function checkdiv(div_id, i) {

        if (i < 0) {
            return d;
        }

        i = i + 1;

        // Check the existence of div
        if ($(div_id + i).length != 0) {
            // Call the function if the div exists
            checkdiv(div_id, i);
        } else {
            // Return the div name if not exists
            d = i;
        }
        return d;
    }


    // Popup the Answer Field
    $("select#select_question_type").change(function () {
        var option = $(this).find("option:selected").val();
        i = 0;
        popupAnswerFild(option, i);
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Add option to Dropdown 
    $('button.add_option').click(function () {

        var button_id = $(this).attr('id');
        var question_id = button_id.split('_')[1];
        var count = $('input.select_option').length;
        var select_id = checkdiv("select_option_", count);

        var html_div_option = addOptionSelectField(parseInt(question_id), select_id, true);

        // Append new question 
        $("div#answer_select").append(html_div_option);

    });


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Add option to Multiple choice 
    $('button.add_checkbox').click(function () {

        var button_id = $(this).attr('id');
        var question_id = button_id.split('_')[1];
        var count = $('input.checkbox_option').length;
        var chbox_id = checkdiv("checkbox_option_", count);


        var html_div_chbox = addOptionCheckBoxField(parseInt(question_id), chbox_id, true);

        // Append new question 
        $("div#answer_checkbox").append(html_div_chbox);
    });

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *  Add Question
     */
    $("button#add_question").click(function () {

        var count = $("div.survey_card").length;
        var i = checkdiv("div.survey_card_", count);
        var j = i - 1;
        var survey_card = '<div class="kt-portlet__body survey_card survey_card_' + i + '" id="survey_card_' + i + '">' + '<div class="">' +
            '<input type="number" value="' + j + '" name="survey[question][' + i + '][predecessor_id]" class="col-md-1 ques_pred_id" hidden>' +
            '<div class="form-group">' +
            '<div class="form-row">' +
            '<div class="col-md-9 ">' +
            '<div class="form-row">' +
            '<div class="col-md-1">' +
            '<input type="number" value="' + i + '" name="survey[question][' + i + '][id]" class="form-control ques-id " placeholder="Question order Id">' +
            '</div>' +
            '<div class="col-md-11">' +
            '<input type="text" class="form-control input-sm" name="survey[question][' + i + '][text]" placeholder="Question" required>' +
            '<i class="form-grou p __bar"></i> ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-3 ">' +
            '<select class="form-control survey[question][' + i + '][type]" name="survey[question][' + i + '][type]" id="select_question_type_' + i + '"style="width:100%;">' +
            '<option value="#" selected disabled>Select Question Type</option>' +
            '<option value="checkbox">Multiple Choice</option>' +
            '<option value="select">Dropdown</option>' +
            '<option value="range">Slider</option>' +
            '<option value="textarea"> Single Text box</option>' +
            '</select>' + '</div>' +
            '</div>' +
            '</div>' +
            '<div class="form-group">' +
            '<input type="text" class="form-control" name="survey[question][' + i + '][hint]" placeholder="Hint">' +
            '<i class="form-group__bar"></i>' +
            '</div> ' +
            '<!--Answer for Input field-->' +
            '<div class="form-group answerfield" id="answer_text_' + i + '">' +
            '<input type="text" class= "form-control" name="survey[question][' + i + '][textarea][options][1][value]" placeholder="Answer">' +
            '<i class="form-group__bar"></i>' +
            '</div>' +

            '<!--Answer for Single choice-->' +
            '<div class="form-group answerfield" id="answer_select_' + i + '">' +
            '	<div class="form-group">' +
            '		<div class="form-row">' +
            '			<div class="col-sm-4">' +
            '				<input type="text" class="form-control select_' + i + '_option" id="select_option_' + i + '" name="survey[question][' + i + '][select][options][1][value]" placeholder="Options for Dropdown ">' +
            '				<i class="form-group__bar"></i>' +
            '               <br>' +
            '               <input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>' +
            '               <input type="file" name="survey[question][' + i + '][select][options][1][link_ref]" accept="image/*" />' +
            '			</div>' +
            '           <div class="col-sm-2"><input type="number" class="form-control survey_' + i + '_options_goto" name="survey[question][' + i + '][select][options][1][goto]" placeholder="Branching Question Number"></div>' +
            '           <div class="col-sm-2"><input type="number" class="form-control survey_' + i + '_options_score" name="survey[question][' + i + '][select][options][1][score]" placeholder="Score"></div>' +
            '			<div class="col-sm-2 d-flex justify-content-around">' +
            '                <label class="kt-radio kt-radio--solid kt-radio--success">' +
            '                   <input type="radio" name="survey[question][' + i + '][select][options][1][status]" value="WAHR" class="custom-control-input">Right' +
            '                    <span class="cio-treaction-color"></span>' +
            '                </label>' +
            '                <!--Radio button for Wrong button-->' +
            '                <label class="kt-radio kt-radio--solid kt-radio--success">' +
            '                    <input type="radio" name="survey[question][' + i + '][select][options][1][status]" value="FALSCH" class="custom-control-input">Wrong' +
            '                    <span class="cio-treaction-color"></span>'  +
            '                </label>' +
            '            </div>' +
            '            <div class="col-sm-2">' +
            '				<button class="btn btn-secondary float-right add_option_' + i + '" id="addoption_' + i + '"  name="survey[question][' + i + '][select][add_option]" type="button" id="add_option"> + Add Option</button>' +
            '			</div>' +
            '		</div>' +
            '	</div>' +
            '</div>' +

            ' <!--Answer for Multiple choice-->' +
            '<div class="form-group answerfield" id="answer_checkbox_' + i + '">' +
            '<div class="form-group">' +
            '<div class="form-row">' +
            '<div class="col-md-4">' +
            '<input type="text" class="form-control checkbox_' + i + '_option"   id="checkbox_option" name="survey[question][' + i + '][checkbox][options][1][value]" placeholder="Options for Multiple choice  ">' +
            '<i class="form-group__bar"></i>' +
            '               <br>' +
            '               <input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>' +
            '               <input type="file" name="survey[question][' + i + '][checkbox][options][1][link_ref]" accept="image/*" />' +
            '</div>' +
            '<div class="col-sm-2"><input type="number" class="form-control survey_1_options_goto"  id="survey_1_options_goto" name="survey[question][' + i + '][checkbox][options][1][goto]" placeholder="Branching Question Number"></div>'+
            '<div class="col-sm-2"><input type="number" class="form-control survey_1_options_score"  id="survey_1_options_score" name="survey[question][' + i + '][checkbox][options][1][score]" placeholder="Score"></div>'+
            '			<div class="col-sm-2 d-flex justify-content-around">' +
            '                <label class="kt-radio kt-radio--solid kt-radio--success">' +
            '                    <input type="radio" name="survey[question][' + i + '][checkbox][options][1][status]" value="WAHR" >Right' +
            '                    <span class="cio-treaction-color"></span>' +
            '                </label>' +
            '                <!--Radio button for Wrong button-->' +
            '                <label class="kt-radio kt-radio--solid kt-radio--success">' +
            '                    <input type="radio" name="survey[question][' + i + '][checkbox][options][1][status]" value="FALSCH" >Wrong' +
            '               	 <span class="cio-treaction-color"></span>' +
            '                </label>' +
            '              </div>' +
            '             <div class="col-sm-2">' +
            '<button class="btn btn-secondary float-right add_checkbox_' + i + '"  id="addmultiplechoice_' + i + '" name="checkbox_option' + i + ' " type="button"> + Add Option</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '<!--Answer for Slider-->' +
            ' <div class="form-group answerfield" id="answer_range_' + i + '">' +
            '<div class="form-row">' +
            '<div class="col-md-3 ">' +
            '<input type="text" class="form-control" name="survey[question][' + i + '][range][options][1][min]" placeholder="Slider Minimum Value">' +
            ' <i class="form-group__bar"></i>' +
            '  </div>    ' +
            '  <div class="col-md-2">' +
            ' <input type="text" class="form-control" name="survey[question][' + i + '][range][options][1][max]" placeholder="Slider Maximum Value">' +
            ' <i class="form-group__bar"></i>' +
            ' </div>    ' +
            '  <div class="col-md-2">' +
            ' <input type="text" class="form-control" name="survey[question][' + i + '][range][options][1][right]" placeholder="Slider Right Value">' +
            ' <i class="form-group__bar"></i>' +
            ' </div>    ' +            
            '  <div class="col-md-2">' +
            '<input type="text" class="form-control" name="survey[question][' + i + '][range][options][1][step]" placeholder="Slider Step Value">' +
            '<i class="form-group__bar"></i>' +
            ' </div>           ' +
            '  <div class="col-md-3">' +
            ' <input type="text" class="form-control" name="survey[question][' + i + '][range][options][1][inital]" placeholder="Slider Intial Value">' +
            ' <i class="form-group__bar"></i>' +
            ' </div>    ' +
            ' </div>    ' +
            ' </div>    ' +
            '   </div>' +
            ' <div class="form-group float-right ">' +
            '<div class="col-md-3  float-right">' +
            '<button class="btn btn-secondary float-right" name="remove_question" type="button" id="remove_question_' + i + '"> - Remove Question</button> ' +
            '</div>' +
            ' </div>' +
            '</div>     ' +
            '  </div>  ';

        // Append new question 
        $("div#survey").append(survey_card);

        // Hide all the answers options    
        $('#answer_text_' + i).hide();
        $('#answer_picture_' + i).hide();
        $('#answer_checkbox_' + i).hide();
        $('#answer_range_' + i).hide();
        $('#answer_select_' + i).hide();



        //Add option to Multiple choice 
        $('button.add_checkbox_' + i).click(function () {

            var button_id = $(this).attr('id');
            var question_id = button_id.split('_')[1];
            var count = $('input.checkbox_' + i + '_option').length;
            var chbox_id = checkdiv('checkbox_' + i + '_option', count);

            var html_div_option = addOptionCheckBoxField(parseInt(question_id), chbox_id, false);

            // Append new question 
            $('div#answer_checkbox_' + question_id).append(html_div_option);
        });




        //Add option to Dropdown 
        $('button.add_option_' + i).click(function () {

            var button_id = $(this).attr('id');
            var question_id = button_id.split('_')[1];
            var count = $('input.select_' + i + '_option').length;
            var select_id = checkdiv('select_' + i + '_option_', count);

            var html_div_option = addOptionSelectField(parseInt(question_id), select_id, false);

            // Append new question 
            $('div#answer_select_' + question_id).append(html_div_option);
        });


        //Event handler for the newly added question
        $('select#select_question_type_' + i).change(function () {

            var option = $(this).find("option:selected").val();
            popupAnswerFild(option, i);
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Remove question
        $('button#remove_question_' + i).click(function () {

            $('.survey_card_' + i).fadeOut();
            $('.survey_card_' + i).remove();
        });

    });


    // Function Add Option to Checkbox Field
    function addOptionCheckBoxField(question_id, chbox_id, firstquestion = true) {

        var html_div_option = "";
        if (question_id <= 0 && chbox_id <= 0) {

            return html_div_option;
        }

        var class_name_chbox = (firstquestion === true) ? 'checkbox_option' : 'checkbox_' + question_id + '_option';
        var html_div_option = '<div class="form-group"><div class="row">' +
            '<div class="col-md-4">' +
            '<input type="text" class="form-control ' + class_name_chbox + '" id="checkbox_option_' + question_id + '" name="survey[question][' + question_id + '][checkbox][options][' + chbox_id + '][value]" placeholder="Option for Multiple ">' +
            '<i class="form-group__bar"></i>' +
            '<br>' +
            '<input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>' +
            '<input type="file" name="survey[question][' + question_id + '][checkbox][options][' + chbox_id + '][link_ref]" accept="image/*" />' +
            '</div>' +
            '<div class="col-sm-2"><input type="number" class="form-control survey_' + question_id + '_options_goto" id="survey_' + question_id + '_options_goto" name="survey[question][' + question_id + '][checkbox][options][' + chbox_id + '][goto]" placeholder="Branching Question Number"></div>'+
            '<div class="col-sm-2"><input type="number" class="form-control survey_' + question_id + '_options_score" id="survey_' + question_id + '_options_score" name="survey[question][' + question_id + '][checkbox][options][' + chbox_id + '][score]" placeholder="Score"></div>'+
            '<div class="col-md-2 d-flex justify-content-around">' +
            '<label class="kt-radio kt-radio--solid kt-radio--success">' +
            '<input type="radio" name="survey[question][' + question_id + '][checkbox][options][' + chbox_id + '][status]" value="WAHR" >Right' +
            '<span class="cio-treaction-color"></span>' +
            '</label>' +
            '<label class="kt-radio kt-radio--solid kt-radio--success">' +
            '<input type="radio" name="survey[question][' + question_id + '][checkbox][options][' + chbox_id + '][status]" value="FALSCH">Wrong' +
            '<span class="cio-treaction-color"></span>' +
            '</label>' +
            '</div></div>' +
            '</div>';
        return html_div_option
    }


    // Function Add Option to Select field
    function addOptionSelectField(question_id, select_id, firstquestion = true) {

        var html_div_option = "";
        if (question_id <= 0 && select_id <= 0) {

            return html_div_option;
        }

        var class_name_sel = (firstquestion === true) ? 'select_option' : 'select_' + question_id + '_option';

        html_div_option = '<div class="form-group"><div class="row">' +
            '<div class="col-md-4">' +
            '<input type="text" class="form-control ' + class_name_sel + '" id="select_option_' + question_id + '" name="survey[question][' + question_id + '][select][options][' + select_id + '][value]" placeholder="Option for Dropdown">' +
            '<i class="form-group__bar"></i>' +
            '<br>' +
            '<input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>' +
            '<input type="file" name="survey[question][' + question_id + '][select][options][' + select_id + '][link_ref]" accept="image/*" />' +
            '</div>' +

            '<div class="col-sm-2"><input type="number" class="form-control survey_' + question_id + '_options_goto" id="survey_' + question_id + '_options_goto" name="survey[question][' + question_id + '][select][options][' + select_id + '][goto]" placeholder="Branching Question Number"></div>' +
            '<div class="col-sm-2"><input type="number" class="form-control survey_' + question_id + '_options_score" id="survey_' + question_id + '_options_score" name="survey[question][' + question_id + '][select][options][' + select_id + '][score]" placeholder="Score"></div>' +
            '<div class="col-md-2 d-flex justify-content-around">' +
            '<label class="kt-radio kt-radio--solid kt-radio--success">' +
            '<input type="radio" name="survey[question][' + question_id + '][select][options][' + select_id + '][status]" value="WAHR" >Right' +
            '<span class="cio-treaction-color"></span>' +
            '</label>' +
            '<label class="kt-radio kt-radio--solid kt-radio--success">' +
            '<input type="radio" name="survey[question][' + question_id + '][select][options][' + select_id + '][status]" value="FALSCH" >Wrong' +
            '<span class="cio-treaction-color"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>';


        return html_div_option;
    }


    // Popup the Answer Field
    function popupAnswerFild(option, i) {

        if ($.trim(option) == "") {
            return false;
        }

        // Get the Div values
        var div_text = (i > 0) ? $('#answer_text_' + i) : $('#answer_text');
        var div_picture = (i > 0) ? $('#answer_picture_' + i) : $('#answer_picture');
        var div_checkbox = (i > 0) ? $('#answer_checkbox_' + i) : $('#answer_checkbox');
        var div_range = (i > 0) ? $('#answer_range_' + i) : $('#answer_range');
        var div_select = (i > 0) ? $('#answer_select_' + i) : $('#answer_select');

        // Hide all the fields
        div_text.hide();
        div_text.find("input").attr("disabled", "disabled");

        div_picture.hide();
        div_picture.find("input").attr("disabled", "disabled");

        div_checkbox.hide();
        div_checkbox.find("input").attr("disabled", "disabled");

        div_range.hide();
        div_range.find("input").attr("disabled", "disabled");

        div_select.hide();
        div_select.find("input").attr("disabled", "disabled");

        if (option == "select") {
            div_select.fadeIn();
            div_select.find("input").removeAttr("disabled");

        } else if (option == "range") {
            div_range.fadeIn();
            div_range.find("input").removeAttr("disabled");

        } else if (option == "textarea") {
            div_text.fadeIn();
            div_text.find("input").removeAttr("disabled");

        } else if (option == "checkbox") {
            div_checkbox.fadeIn();
            div_checkbox.find("input").removeAttr("disabled");
        } else {
            return false;
        }
    }

});

